/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */
//#include "interface.h"
/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/
#define FSMC_TIME 5

#define KIN0_Pin GPIO_PIN_2
#define KIN0_GPIO_Port GPIOE
#define KIN1_Pin GPIO_PIN_3
#define KIN1_GPIO_Port GPIOE
#define KIN2_Pin GPIO_PIN_4
#define KIN2_GPIO_Port GPIOE
#define KIN3_Pin GPIO_PIN_5
#define KIN3_GPIO_Port GPIOE
#define KIN4_Pin GPIO_PIN_6
#define KIN4_GPIO_Port GPIOE

#define LCD_BACKLIGHT_Pin GPIO_PIN_6
#define LCD_BACKLIGHT_GPIO_Port GPIOF
#define LCD_RESET_Pin GPIO_PIN_7
#define LCD_RESET_GPIO_Port GPIOF
#define BUZZER_Pin GPIO_PIN_8
#define BUZZER_GPIO_Port GPIOF
#define EMG_Pin GPIO_PIN_0
#define EMG_GPIO_Port GPIOC
#define MOLD_EMG_Pin GPIO_PIN_1
#define MOLD_EMG_GPIO_Port GPIOC
#define SW2_Pin GPIO_PIN_13
#define SW2_GPIO_Port GPIOB
#define SW0_Pin GPIO_PIN_14
#define SW0_GPIO_Port GPIOB
#define SW1_Pin GPIO_PIN_15
#define SW1_GPIO_Port GPIOB
#define SD_WRITE_PROTECTION_Pin GPIO_PIN_6
#define SD_WRITE_PROTECTION_GPIO_Port GPIOC
#define SD_DETECT_Pin GPIO_PIN_7
#define SD_DETECT_GPIO_Port GPIOC
#define USB_OTG_FS_VBUS_Pin GPIO_PIN_9
#define USB_OTG_FS_VBUS_GPIO_Port GPIOA

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
#define LCD_E_Pin					GPIO_PIN_4
#define LCD_E_GPIO_Port			GPIOD

	  
#define KOUT0_Pin GPIO_PIN_0
#define KOUT0_GPIO_Port GPIOG
#define KOUT1_Pin GPIO_PIN_1
#define KOUT1_GPIO_Port GPIOG
#define KOUT2_Pin GPIO_PIN_2
#define KOUT2_GPIO_Port GPIOG

#define KOUT3_Pin GPIO_PIN_3
#define KOUT3_GPIO_Port GPIOG4 rr
#define KOUT4_Pin GPIO_PIN_4
#define KOUT4_GPIO_Port GPIOG


//	extern MOLD_MSG MoldMsg;
//extern IO_MSG IOMsg;
//extern ERROR_MSG ErrMsg;
//extern SETTING_MSG SettingMsg;

#define LCD_RS_Pin				GPIO_PIN_0
#define LCD_RS_GPIO_Port		PORTF
//#define LCD_RW_Pin				GPIO_PIN_5
//#define LCD_RW_GPIO_Port		PORTD
//#define LCD_E_Pin					GPIO_PIN_4
//#define LCD_E_GPIO_Port			PORTD
//
//#define LCD_D0_Pin				GPIO_PIN_14
//#define LCD_D0_GPIO_Port		PORTD
//#define LCD_D1_Pin				GPIO_PIN_15
//#define LCD_D1_GPIO_Port		PORTD
//#define LCD_D2_Pin				GPIO_PIN_2
//#define LCD_D2_GPIO_Port		PORTD
//#define LCD_D3_Pin				GPIO_PIN_3
//#define LCD_D3_GPIO_Port		PORTD
//
//#define LCD_D4_Pin				GPIO_PIN_7
//#define LCD_D4_GPIO_Port		PORTE
//#define LCD_D5_Pin				GPIO_PIN_8
//#define LCD_D5_GPIO_Port		PORTE
//#define LCD_D6_Pin				GPIO_PIN_9
//#define LCD_D6_GPIO_Port		PORTE
//#define LCD_D7_Pin				GPIO_PIN_10
//#define LCD_D7_GPIO_Port		PORTE
//
//#define LCD_CS1_Pin				GPIO_PIN_7
//#define LCD_CS1_GPIO_Port		PORTD
//#define LCD_CS2_Pin				GPIO_PIN_9
//#define LCD_CS2_GPIO_Port		PORTG

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
