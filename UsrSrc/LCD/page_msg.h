#ifndef __PAGE_MSG_H
#define	__PAGE_MSG_H

#ifdef	__cplusplus
extern "C" {
#endif
//#include "def.h"
/****************************************************/
/* 				charactor 			 						 */
/****************************************************/
#define MAX_CHAR_INPUT_DATA			39
char  Char_Input_Cursor_Num;
char  Char_Input_Data[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
						'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
						'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3',
						'4', '5', '6', '7', '8', '9', '_', '-', ' '};

/****************************************************/
/* 				define of PAGE NAME						 */
/****************************************************/
char ** Page_Name_List;
char * Page_Name_List_Kor[] = {"", "TOPIV Ver 2.06+", "", "", "수동운전", "금형검색", "금형관리", "금형삭제", "새금형", "출력(입력\x11)",
							"입력(출력\x10)", "타이머", "카운터", "스텝운전", "싸이클운전", "자동운전", "버전정보", "에러이력"};
char * Page_Name_List_Eng[] = {"", "TOPIV Ver 2.06+", "", "", "Manual", "MoldNo", "MoldMgr", "MoldDel", "NewMold", "Output(In\x11)",
							"Input(Out\x10)", "Timer", "Counter", "StepRun", "Cycle", "AutoMod", "Version", "ErrHist"};
uint16_t Page_Name_List_China[18][8] = {{0xFFFF}, {0xFFFF}, {0xFFFF}, {0xFFFF}, {4154, 2115, 5143, 5410, 0xFFFF},
							{3503, 2963, 4249, 4287, 0xFFFF}, {3503, 2963, 2460, 3177, 0xFFFF}, {4030, 1893, 3503, 2963, 0xFFFF}, {4734, 3503, 2963, 0xFFFF}, {4168, 1886, 208, 4168, 3975, 300, 209, 0xFFFF},
							{4168, 3975, 208, 4168, 1886, 300, 209, 0xFFFF}, {4117, 2768, 0xFFFF}, {2738, 4193, 0xFFFF}, {1729, 5372, 2115, 5487, 0xFFFF}, {4813, 2623, 5143, 5410, 0xFFFF},
							{5452, 2115, 5143, 5410, 0xFFFF}, {1570, 1630, 3873, 1608, 0xFFFF}, {1608, 2915, 3336, 3190, 0xFFFF}};

char * Normal_Option[] = {"<", ">", "%", "/", "mm"};
char * Machine_Type_Name[] = {"TOPIV-A", "TOPIV-X", "TOPIV-XC", "TOPIV-Twin","TP Ver : 02.06+", "SC Ver : 02.06"};

uint8_t System_Info_Cursor_Num;

/****************************************************/
/* 				define of FACTORY SETUP PAGE			 */
/****************************************************/
uint8_t File_Del_Option;
uint8_t Error_List_Del_Option;
uint8_t Factory_Cursor_Num;
char ** Factory_Setup_Msg;
char * Factory_Setup_Msg_Kor[] = {"에러판단", "전자동입력", "안전도어", "자동사출", "미성형사용",
								"공정시간", "날짜", "시간", "전체금형삭제", "에러이력삭제", "안전도어열림","사출기타입","회전유무"};		//IMMType
char * Factory_Setup_Msg_Eng[] = {"FindError", "FullAuto", "SafetyDoor", "Injection", "RejectIMM",
								"ProceTime", "Date", "Time", "DelAllMold", "DelErrHist", "DrSigCh","IMMType","Rotation"};
uint16_t Factory_Setup_Msg_China[11][7] = {{3648, 2147, 1977, 4583, 0xFFFF}, {4168, 3975, 3911, 5452, 2115, 0xFFFF}, {1518, 3911, 3437, 0xFFFF}, {5452, 2115, 5402, 4260, 0xFFFF}, {4125, 5035, 4520, 1841, 4745, 0xFFFF},
								{2404, 1844, 4117, 2768, 0xFFFF}, {3953, 3758, 0xFFFF}, {4117, 2768, 0xFFFF}, {4030, 1893, 3911, 1731, 3503, 2963, 0xFFFF}, {4030, 1893, 1608, 2915, 3336, 3190, 0xFFFF},
								{3010, 3437, 0xFFFF}};

char ** Factory_Setup_Option;
char * Factory_Setup_Option_Kor[] = {"초", "사용안함", "사용", "아니오", "예", "자동정지", "수동정지","표준","입형"};
char * Factory_Setup_Option_Eng[] = {"s", "NoRun", "Run", "No", "Yes", "AStop", "MStop","Stand","Verti"};
uint16_t Factory_Setup_Option_China[7][4] = {{3475, 0xFFFF}, {1727, 4125, 5035, 0xFFFF}, {4125, 5035, 0xFFFF}, {1727, 4139, 0xFFFF}, {4139, 0xFFFF},
										{5452, 2115, 0xFFFF}, {4154, 2215, 0xFFFF}};

char ** Factory_Setup_Option_Setting;
char * Factory_Setup_Option_SettingKor[] = {"사용", "사용안함"};
char * Factory_Setup_Option_SettingEng[] = {"Run", "NoRun"};
/****************************************************/
/* 				define of FILE SEARCH PAGE				 */
/****************************************************/
uint8_t File_Cursor_Num;
char ** File_Search_Msg;
char * File_Search_Msg_Kor[] = {"검색할 금형번호를", "입력해 주십시오"};
char * File_Search_Msg_Eng[] = {"Input", "Mold Number."};
uint16_t File_Search_Msg_China[2][11] = {{4249, 4287, 4772, 4910, 4125, 5035, 2036, 3503, 2963, 2212, 0xFFFF}, {2537, 214, 0xFFFF}};

/****************************************************/
/* 				define of DELETE PAGE 					 */
/****************************************************/
char ** Delete_Page_Msg;
char * Delete_Page_Msg_Kor[] = {"를 삭제하시겠습니까?", "[예(", ")/아니오(정지)]","금형파일을", "삭제 중 입니다."};
char * Delete_Page_Msg_Eng[] = {"Delete?", "[Y(", ")/N(Stop)]", "[", "(", ")/", "(Stop)]","Deleting mold file.",""};
uint16_t Delete_Page_Msg_China[4][5] = {{4910, 4030, 1893, 0xFFFF}, {4536, 2794, 3380, 231, 0xFFFF}, {4139, 0xFFFF}, {1727, 0xFFFF}};

/****************************************************/
/* 				define of MODE PAGE						 */
/****************************************************/
uint8_t Mode_Cursor_Num;
char ** Mode_Page_Msg;
char * Mode_Page_Msg_Kor[] = {"동작암", "척크", "흡착", "척회전", 
							"외부대기", "제품동작", "런너동작", "제품하강", "런너하강", "척개방", "흡착개방",
							"런너개방", "척불량품", "흡착불량품"};
char * Mode_Page_Msg_Eng[] = {"ArmSet", "Chuck", "Vacuum","ChuRot", 
							"OutWai", "M-Arm",  "S-Arm", "MArmDn", "SArmDn", "ChuOff", "VacOff",
							"SChOff", "ChuRej", "VacRej" };
uint16_t Mode_Page_Msg_China[15][6] = {{2115, 5487, 1659, 0xFFFF}, {2748, 0xFFFF}, {4592, 5437, 0xFFFF}, {2748, 4793, 5410, 0xFFFF}, {3815, 5451, 0xFFFF},
							{4466, 1731, 1993, 3758, 0xFFFF}, {1790, 3723, 2115, 5487, 0xFFFF}, {1790, 3723, 4634, 2821, 0xFFFF}, {2748, 3010, 2237, 0xFFFF}, {4592, 5437, 3010, 2237, 0xFFFF},
							{4214, 3058, 3010, 2237, 0xFFFF}, {2748, 1727, 3228, 3723, 0xFFFF}, {4592, 5437, 1727, 3228, 0xFFFF}, {4214, 3058, 2115, 5487, 0xFFFF}, {4214, 3058, 4634, 2821, 0xFFFF}};

char * Mode_Page_Common[] = {"M00", "M01", "M02", "M03", "M04", "M05", "M06", "M07", "M08", "M09", "M10", "M11", "M12","M13", "M14"};
//IMMType
char ** Mode_Page_Option;
char * Mode_Page_Option_Kor[] = {"제품단독", "런너단독", "제품런너", "사용안함", "사용",
								"L동작", "U동작", "I동작", "이동측", "고정측",
								"형내", "회전후", "2차하강", "2차상승","척회전후"};
char * Mode_Page_Option_Eng[] = {"M-Arm", "S-Arm", "M&S", "NoUse", "Use",
								"LType", "UType", "IType", "Clamp", "Nozzl",
								"InMol", "OutSi","2Down", "2Up","ChuRo"};
uint16_t Mode_Page_Option_China[14][4] = {{1790, 3723, 4214, 0xFFFF}, {1790, 3723, 0xFFFF}, {4214, 3058, 0xFFFF}, {1727, 4125, 5035, 0xFFFF}, {4125, 5035, 0xFFFF},
							{244, 2115, 5487, 0xFFFF}, {253, 2115, 5487, 0xFFFF}, {241, 2115, 5487, 0xFFFF}, {2444, 2108, 1764, 0xFFFF}, {4938, 2115, 1764, 0xFFFF},
							{2194, 1946, 4634, 0xFFFF}, {4745, 3558, 0xFFFF}, {4793, 5410, 2583, 0xFFFF}, {2194, 1946, 4047, 0xFFFF}};

/****************************************************/
/* 				define of IO PAGE	 						 */
/****************************************************/
char ** IO_Page_Msg;
char * IO_Page_Msg_Kor[] = {"상승완료센서", "", "제품집게확인", "회전완료센서", "회전복귀센서","",
						  "흡착확인센서", "", "런너상승완료", "", "런너집게확인", "",
						  "전자동", "자동사출", "형개완료", "안전도어", "미성형", "사출비상",
						  "상하강", "전후진",	"집게", "회전", "회전복귀", "척회전", "흡착",
						  "니퍼절단", "런너상하강", "런너전후진", "런너집게",
							"경보", "사이클스타트", "형개폐", "에젝터", "컨베어", "부저", ""};
char * IO_Page_Msg_Eng[] = {"MArmUpOK", "", "ChuckOK", "SwingOK", "SwingRtOK", "",
						  "VacuumOK", "", "SArmUpOK", "", "SArmGripOK", "",
						  "FullAuto", "Injection", "MoldOpen", "SafetyDoor", "Reject", "IMM E-Stop",
						  "Down", "Kick", "Chuck", "Swing", "SwingRt", "ChuckRo", "Vacuum",
						  "NipperCut", "SArmDown", "SArmKick", "SArmGrip",
						  "Alarm", "CycleStart", "MoldOp/Cl", "EjectorSig", "Conveyor", "Buzzer", ""};
uint16_t IO_Page_Msg_China[31][7] = {{1790, 3723, 4047, 4093, 4474, 1647, 0xFFFF}, {1790, 3723, 3587, 5451, 3923, 3947, 0xFFFF}, {4793, 5410, 4474, 1647, 0xFFFF}, {4793, 5410, 2320, 4527, 4474, 1647, 0xFFFF}, {3923, 3947, 4592, 5437, 0xFFFF},
							{4214, 3058, 4047, 4093, 4474, 1647, 0xFFFF}, {4214, 3058, 3587, 5451, 3923, 3947, 0xFFFF}, {3911, 5452, 2115, 0xFFFF}, {5452, 2115, 4068, 1886, 0xFFFF}, {4745, 3010, 4474, 3243, 0xFFFF},
							{1518, 3911, 3437, 0xFFFF}, {4520, 1841, 4745, 0xFFFF}, {5402, 4260, 2239, 1803, 0xFFFF}, {1790, 3723, 4634, 2821, 0xFFFF}, {1790, 3723, 3816, 2888, 0xFFFF},
							{2748, 0xFFFF}, {4793, 5410, 0xFFFF}, {4793, 5410, 2320, 4527, 0xFFFF}, {2748, 4793, 5410, 0xFFFF}, {4592, 5437, 0xFFFF},
							{3815, 5451, 3848, 2147, 0xFFFF}, {4214, 3058, 4634, 2821, 0xFFFF}, {4214, 3058, 3816, 2888, 0xFFFF}, {4214, 3058, 3587, 5451, 0xFFFF}, {2915, 1608, 0xFFFF},
							{5387, 2071, 5120, 0xFFFF}, {3901, 1886, 4474, 3243, 0xFFFF}, {4745, 3010, 1653, 0xFFFF}, {2105, 5275, 4737, 2537, 0xFFFF}, {4168, 4245, 2690, 0xFFFF},
							{2268, 3489, 3787, 0xFFFF}};

char * IO_Page_Num_Msg[] = {"X11", "", "X16", "X14", "X15", "", "X17", "", "X1G", "", "X1F", "",
						  "X1H", "X19", "X18", "X1A", "X1B", "X1I",
						  "Y20", "Y21", "Y22", "Y23", "Y2F", "Y24", "Y25", "Y26", "Y2D", "Y2E", "Y27", "Y28",
						  "Y29", "Y2A", "Y2B", "Y2C", "Y28", ""};

/****************************************************/
/* 				define of TIMER PAGE					 	 */
/****************************************************/
uint8_t Timer_Page_Cursor_Num;
char ** Timer_Page_Msg;
char * Timer_Page_Msg_Kor[] = {"하강", "전진", "에젝터", "척크", "후진", "상승", "회전", "2차하강", "개방", "2차상승",
							 "척복귀", "회전복귀", "니퍼ON", "컨베어"};
char * Timer_Page_Msg_Eng[] = {"Down", "Kick", "Eject", "Chuck", "KicRt", "Up", "Swing", "2Down", "Open", "2Up",
							 "CRoRt", "SwRt", "NipON", "Conve"};
uint16_t Timer_Page_Msg_China[14][4] = {{4634, 2821, 0xFFFF}, {3816, 2888, 0xFFFF}, {2105, 5275, 0xFFFF}, {2748, 0xFFFF}, {2583,4443, 0xFFFF},
							{4047, 4093, 0xFFFF}, {4793, 5410, 0xFFFF}, {2748, 4793, 5410, 0xFFFF}, {3010, 2237, 0xFFFF}, {218, 4047, 4093, 0xFFFF},
							{2748, 2320, 4527, 0xFFFF}, {4793, 5410, 2320, 0xFFFF}, {3815, 5451, 0xFFFF}, {4168, 4245, 1988, 0xFFFF}};

char * Timer_Page_Num_Msg[] = {"T00", "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09",
							 "T10", "T11", "T12", "T13"};

/****************************************************/
/* 				define of COUNT SETUP PAGE				 */
/****************************************************/
uint8_t Count_Setup_Cursor_Num;
char ** Count_Setup_Page_Msg;
char * Count_Setup_Page_Msg_Kor[] = {"총수량", "불량품", "취출불량"};
char * Count_Setup_Page_Msg_Eng[] = {"TotQty", "RejQty", "DetFai"};
uint16_t Count_Setup_Page_Msg_China[3][5] = {{5460, 4193, 3231, 0xFFFF}, {1727, 3228, 3723, 0xFFFF}, {3901, 1886, 1727, 3228, 0xFFFF}};

char * Count_Setup_Page_Msg_Option[] = {"C0", "C1", "C2"};

/****************************************************/
/* 				define of STEP RUN PAGE				    */
/****************************************************/
uint8_t Run_Cursor;
uint8_t New_Cursor_Num;
char ** Step_Name_Table;
//char * Step_Name_Table_Kor[] = {"", "하강", "전진", "에젝터돌출", "척ON", "후진", "상승", "회전", "척회전", "2차하강",
//							  "니퍼커팅", "척OFF", "척OFF", "척OFF", "척OFF", "2차상승", "회전복귀", "후진", "전진", "런너하강",
//							  "제품상승", "척회전복귀"};
//char * Step_Name_Table_Eng[] = {"", "Down","Kick", "Eject", "ChuOn", "KickRt", "Up", "Swing" ,"ChuRot", "2Down",
//							  "NipCut", "ChuOff", "ChuOff", "ChuOff", "ChuOff", "2Up", "SwRt", "KickRt", "Kick", "SArmDn",
//							  "MArmUp", "ChRoRt"};
//uint16_t Step_Name_Table_China[22][5] = {{0xFFFF}, {4634, 2821, 0xFFFF}, {3816, 2888, 0xFFFF}, {2105, 5275, 2105, 1886, 0xFFFF}, {2748, 247, 246, 0xFFFF},
//								{2583, 4443, 0xFFFF}, {4047, 4093, 0xFFFF}, {4793, 5410, 0xFFFF}, {2748, 4793, 5410, 0xFFFF}, {218, 1946, 4634, 2821, 0xFFFF},
//								{3587, 5451, 2784, 2147, 0xFFFF}, {2748, 247, 238, 238, 0xFFFF}, {2748, 247, 238, 238, 0xFFFF}, {2748, 247, 238, 238, 0xFFFF}, {2748, 247, 238, 238, 0xFFFF},
//								{218, 1946, 4047, 4093, 0xFFFF}, {4793, 5410, 2320, 4527, 0xFFFF}, {2583, 4443, 0xFFFF}, {3816, 2888, 0xFFFF}, {4214, 3058, 4634, 2821, 0xFFFF},
//								{1790, 3723, 4047, 4093, 0xFFFF}, {2748, 2320, 4527, 0xFFFF}};

char * Step_Name_Table_Kor[] = {"하강", "전진", "에젝터돌출", "척ON", "후진", "상승", "회전", "척회전", "2차하강",
							  "니퍼커팅", "척OFF", "2차상승", "척회전복귀", "회전복귀", "런너하강", "제품상승"};
char * Step_Name_Table_Eng[] = {"Down","Kick", "Eject", "ChuOn", "KickRt", "Up", "Swing" ,"ChuRot", "2Down",
							  "NipCut", "ChuOff", "2Up", "ChRoRt", "SwRt", "SArmDn", "MArmUp"};
uint16_t Step_Name_Table_China[22][5] = {{0xFFFF}, {4634, 2821, 0xFFFF}, {3816, 2888, 0xFFFF}, {2105, 5275, 2105, 1886, 0xFFFF}, {2748, 247, 246, 0xFFFF},
								{2583, 4443, 0xFFFF}, {4047, 4093, 0xFFFF}, {4793, 5410, 0xFFFF}, {2748, 4793, 5410, 0xFFFF}, {218, 1946, 4634, 2821, 0xFFFF},
								{3587, 5451, 2784, 2147, 0xFFFF}, {2748, 247, 238, 238, 0xFFFF}, {2748, 247, 238, 238, 0xFFFF}, {2748, 247, 238, 238, 0xFFFF}, {2748, 247, 238, 238, 0xFFFF},
								{218, 1946, 4047, 4093, 0xFFFF}, {4793, 5410, 2320, 4527, 0xFFFF}, {2583, 4443, 0xFFFF}, {3816, 2888, 0xFFFF}, {4214, 3058, 4634, 2821, 0xFFFF},
								{1790, 3723, 4047, 4093, 0xFFFF}, {2748, 2320, 4527, 0xFFFF}};

/****************************************************/
/* 				define of ERROR POPUP PAGE			    */
/****************************************************/
char ** Error_Pop_Page_Msg;
char * Error_Pop_Page_Msg_Kor[] = {"에러","날짜 :","시간 :"};
char * Error_Pop_Page_Msg_Eng[] = {"Error","Date:","Time:"};
uint16_t Error_Pop_Page_Msg_China[][3] = {{1977, 4583, 0xFFFF}};

/****************************************************/
/* 				define of MASSAGE POPUP				    */
/****************************************************/
char ** Massage_Pop_Msg;
char * Massage_Pop_Msg_Kor[] = {"운전준비중 입니다", 																	//0
											"존재하지 않는 금형파일", "입니다.", 												//1,2
											"가장 근접한 금형번호로", "이동하겠습니다.",										//3,4
											"금형파일의 로보트타입과", "TP의 로보트타입이", "다릅니다.", 					//5,6,7
											"제품 및 런너전후진을", "전진시킨 후, 척회전을", "해주십시오.", 				//8,9,10
											"척회전복귀를 한 후,", "제품 및 런너 전후진을", "전진시켜 주십시오.", 		//11,12,13
											"안전도어를 열어 제품을", "꺼낸 후 자동운전을", "하십시오.", 					//14,15,16
											"안전도어가 열려 있으면", "대기위치로 이동하지", "않습니다.","",				//17,18,19,20
											"","","","",																			//21,22,23,24
											"안전도어를 닫고","에러 해제 후","스텝운전을 하십시오.", "싸이클운전을 하십시오.", "자동운전을 하십시오.",	//25,26,27,28,29
											"안전문 신호","대기중 입니다.",														//30,31
											"삭제할 수 없는","금형파일 입니다.",													//32,33
											"전원을","재인가 해주십시오"};														//34,35
char * Massage_Pop_Msg_Eng[] = {"Ready to Work", 																		//0
											"Mold number is", "not right,",													//1,2
											"move next mold", "number", 														//3,4
											"Different", "Version of TP", "installed!",									//5,6,7
											"Kick or KickRt", "for Both arms", "before rotate",						//8,9,10
											"Rotate ChuckRt", "before Kick or", "KickRt for both",					//11,12,13
											"OpenSafetyDoor", "From mold and", "Remove parts",							//14,15,16
											"Robot arm can't", "move to the wait", "Position, close", "SafetyDoorFirst",			//17,18,19,20
											"Wait Position.", "chuck.", "arm before.", "RestartAutoMode",			//21,22,23,24
											"CloseSafetyDoor", "and Restart", "StepRun", "CycleRun", "AutoRun",	//25,26,27,28,29
											"Waiting for", "SafetyDoor",														//30,31
											"This mold file","cannot delete.",												//32,33
											"Reboot","the System"};																//34,35
uint16_t Massage_Pop_Msg_China[][11] = {{5143, 5410, 5428, 1624, 5348, 0xFFFF}, {1727, 1970, 5158, 2036, 3503, 2963, 1664, 2537, 0xFFFF}, {4938, 2115, 2029, 5478, 2851, 2892, 2036, 3503, 2963, 1664, 0xFFFF}, {2537, 0xFFFF}, {0xFFFF},
								{3503, 2963, 4536, 2794, 2036, 5452, 2115, 5416, 5335, 0xFFFF}, {3164, 4745, 2545, 252, 248, 213, 250, 247, 245, 2036, 0xFFFF}, {5452, 2115, 5416, 5335, 3164, 4745, 1727, 4412, 0xFFFF}, {1549, 1790, 3723, 2545, 4214, 3058, 3816, 2583, 2888, 0xFFFF}, {5416, 5335, 4438, 4682, 3816, 2229, 2583, 2888, 4748, 0xFFFF},
								{2748, 4793, 5410, 0xFFFF}, {2748, 4793, 5410, 2320, 5113, 5314, 2583, 1549, 0xFFFF}, {1790, 3723, 2545, 4214, 3058, 2036, 3816, 2583, 2888, 0xFFFF}, {5416, 5335, 4682, 3816, 4938, 2115, 0xFFFF}, {0xFFFF},
								{3010, 1518, 3911, 3437, 1549, 1790, 3723, 3535, 1886, 2583, 0xFFFF}, {5452,2115,5143,5410, 0xFFFF}, {0xFFFF}, {1518, 3911, 3437, 3010, 4117, 0xFFFF}, {1727, 4910, 4938, 2115, 1993, 2690, 4527, 5335, 0xFFFF},
								{2456, 1653, 1518, 3911, 3437, 3071, 1729, 1812, 5461, 0xFFFF}, {2456, 1653, 1518, 3911, 3437, 0xFFFF}, {2888, 4748, 4813, 2623, 5143, 5410, 0xFFFF}, {2456, 1653, 1518, 3911, 3437, 5452, 2115, 5143, 5410, 0xFFFF}};

/****************************************************/
/* 				define of BASIC MODE PAGE			    */
/****************************************************/
char ** Basic_Mode_Page_Msg;
char * Basic_Mode_Page_Msg_Kor[] = {"외부대기", "제품하강", "척개방", "흡착개방", "런너하강", "런너개방"};
char * Basic_Mode_Page_Msg_Eng[] = {"OutWai", "MArmDn", "ChuOff", "VacOff", "SArmDn", "SChOff"};
uint16_t Basic_Mode_Page_Msg_China[6][5] = {{4466, 1731, 1993, 3758, 0xFFFF}, {1790, 3723, 4634, 2821, 0xFFFF}, {2748, 3010, 2237, 0xFFFF}, {4592, 5437, 3010, 2237, 0xFFFF}, {4214, 3058, 4634, 2821, 0xFFFF},
									{4214, 3058, 3010, 2237, 0xFFFF}};

char ** Basic_Mode_Page_Option;
char * Basic_Mode_Page_Option_Kor[] = {"사용안함", "사용", "고정측", "이동측", "2차하강", "형내", "회전후", "2차상승"};
char * Basic_Mode_Page_Option_Eng[] = {"NoRun", "Run", "Nozzl", "Clamp", "2Down", "InMol", "OutSi", "2Up"};
uint16_t Basic_Mode_Page_Option_China[8][4] = {{1727, 4125, 5035, 0xFFFF}, {4125, 5035, 0xFFFF}, {2444, 2108, 1764, 0xFFFF}, {4938, 2115, 1764, 0xFFFF}, {2194, 1946, 4634, 0xFFFF},
										{4745, 3558, 0xFFFF}, {4793, 5410, 2583, 0xFFFF}, {2194, 1946, 4047, 0xFFFF}};

/****************************************************/
/* 				define of AUTO RUN MSG POPUP		    */
/****************************************************/
char ** Auto_Run_Popup_Msg;
char * Auto_Run_Popup_Msg_Kor[] = {"자동 key를 누르면", "자동운전을 시작합니다.",""};
char * Auto_Run_Popup_Msg_Eng[] = {"Press AUTO btn", "to operate auto", "mode."};
uint16_t Auto_Run_Popup_Msg_China[2][9] = {{1520, 5452, 2115, 243, 269, 289, 3010, 4128, 0xFFFF}, {5452, 2115, 5143, 5410, 0xFFFF}};

/****************************************************/
/* 				define of ERROR MASSAGE					 */
/****************************************************/
char ** Error_Msg;
char * Error_Msg_Kor[] = {"SC-CRC수신에러","SC명령수신시간초과","없는명령어(SC)","현재실행불가명령어",
								   "SC의데이터에러","SC가응답없음","비정상 헤더문자","명령어보내지못함",
									"비상정지","사출기비상정지","회전동시","런너상승완료에러","제품상승완료에러","런너상승에러","제품상승에러",
									"회전에러","회전복귀에러","흡착에러","척크에러","런너집게에러","형개완료미검지","형개완료신호에러","형개완료안됨",
									"공정시간초과","잘못된 키 동작","잘못된 파일 번호","잘못된 통신 코드","잘못된 페이지코드","파일생성한계","설정에러","설정에러",
									"SC버전정보에러","지정되지않은오류",
									"SD카드 부팅에러","SD카드 파일없음","SD카드 잠금","SD카드 없음"};
char * Error_Msg_Eng[] = {"SC-CRC Error","SC Error","Not Cmd(SC)","NotExeCmd",
									"ComDataError","No Response","Header Err","NotSendCmd",
									"ROBOT EMO","IMM EMO",
									"SwSensorErr","SubArmUpOk","MainArmUpOk","SArmUpError","MArmUpError","SwingError","SwingRtError",
									"VacuumFail","ChuckFail","SArmGripFail",
									"MoldOpenND","MoldOpenErr",
									"NoMoldOpen","TimeLimitExc",
									"KeySig.Fail","FileLoadFail","ComCodeFail","PageModeFail","NoDiskSpace","SetupValFail","SetupValFail","SCInfoError","NoInfoErr",
									"SD BootingErr","SD Not File","SD Locking","SD Not Card","Reboot","the System"};



uint16_t Error_Msg_China[][10] = {{235,  250,  235, 2851, 4153, 2915, 1608, 0xFFFF}, 		{3492, 3278, 2851, 4153, 1812, 2493, 0xFFFF},
							{4562, 3492, 3278, 5079, 0xFFFF}, 							{4654, 5158, 4109, 4748, 1727, 3041, 3492, 3278, 5079, 0xFFFF},
							{ 251,  235, 4193, 2961, 2851, 4153, 1977, 4583, 0xFFFF}, 	{ 251,  235, 4562, 5006, 1980, 0xFFFF},
							{2239, 5293, 1803, 4640, 4423, 4536, 5454, 0xFFFF}, 		{1727, 3560, 1911, 4245, 3492, 3278, 5079, 0xFFFF},
							{2239, 1803, 2071, 5120, 0xFFFF}, 							{4071, 1886, 2690, 2239, 1803, 4403, 5325, 0xFFFF},
							{2656, 5410, 4412, 4117, 0xFFFF}, 							{4214, 3058, 4047, 4093, 4474, 1647, 1977, 4583, 0xFFFF},
							{1790, 3723, 4047, 4093, 4474, 1647, 1977, 4583, 0xFFFF},	{4214, 3058, 4047, 4093, 2915, 1608, 0xFFFF},
							{1790, 3723, 4047, 4093, 2915, 1608, 0xFFFF},				{2656, 5410, 2915, 1608, 0xFFFF},
							{2656, 5410, 2320, 4527, 2915, 1608, 0xFFFF}, 				{4592, 5437, 1977, 4583, 0xFFFF},
							{2760, 5451, 1977, 4583, 0xFFFF}, 							{4214, 3058, 3587, 5451, 1977, 4583, 0xFFFF},
							{4745, 3010, 2865, 4188, 4520, 2776, 1773, 0xFFFF}, 		{4745, 3010, 4474, 1647, 4737, 2537, 1977, 4583, 0xFFFF},
							{1727, 4745, 3010, 0xFFFF}, 								{1812, 2493, 2404, 1844, 4117, 2768, 0xFFFF},
							{1520, 3605, 2115, 5487, 1977, 4583, 0xFFFF}, 				{1977, 4583, 4536, 2794, 2212, 2537, 0xFFFF},
							{4408, 4737, 1769, 5489, 1977, 4583, 0xFFFF}, 				{1977, 4583, 4919, 3470, 4746, 4129, 0xFFFF},
							{4536, 2794, 4090, 1841, 0xFFFF}, 							{4072, 2108, 1977, 4583, 0xFFFF},
							{ 251,  235, 3873, 1608, 1977, 4583, 0xFFFF}, 				{0xFFFF}};

char ** Error_Action_Msg;
char * Error_Action_Msg_Kor[] = {"전원을","재인가 해주십시오",
											"비상정비버튼을 해제하고","C key를 눌러주십시오","사출기비상정지버튼을","해제해주십시오",
											"회전,회전복귀센서를","확인해주십시오","에어 및 런너상승센서를","확인해주십시오","에어 및 제품상승센서를","확인해주십시오",
											"로봇동작법위밖에서 공압","및 센서를 점검해주십시오","회전센서(X14)를","확인해주십시오","회전복귀센서(X15)를","확인해주십시오",
											"안전도어를 열어 문제점을","해결한 후, 닫아주십시오","문제점을 해결한 후", "C key를 눌러주십시오","문제점을 해결한 후", "C key를 눌러주십시오","문제점을 해결한 후", "C key를 눌러주십시오",
											"인터록신호를", "확인해주십시오","사출기를형개시켜주십시오","사출기나 주변기기","확인해주십시오","당사문의",
											"SD카드를 확인하십시오","SD카드 잠금을","해제하십시오","SD카드를 삽입해주십시오"};
char * Error_Action_Msg_Eng[] = {"Reboot","the System", 
											"Restore Robot ", "EMO Stop Button","Restore IMM","EMO Stop Button",
											"Check Swing/Rt", "Sensor(X14,X15)","Sub Arm Up Ok", "X1G Sensor!","Main Arm Up Ok", "X11 Sensor!",
											"Check Sensor", "And AirPressure","Check Swing", "Sensor(X14)","Check SwingRt", "Sensor(X15)",
											"Open SafetyDoor" ,"Fix Prob,Close","Check EOAT and","Vac,Press Ckey", "Check EOAT and","Chuc,Press Ckey","Check EOAT and","Grip,Press Ckey",
											"Check IntSg and", "relay","Open Mold.","Check IMM", "and Robot.","Contact Factory","Delete old File",
											"Check SD Card","Unlock SD Card","Insert SD Card"};
uint16_t Error_Action_Msg_China[][11] = {{5157, 3947, 3041, 2071, 5120, 0xFFFF}, 								{0xFFFF},
									{2866, 1893, 2239, 1803, 4403, 5325, 1520, 3605, 212, 0xFFFF}, 			{1520, 235, 2792, 0xFFFF},
									{2866, 1893, 4071, 1886, 2690, 2239, 1803, 4403, 5325, 1520, 0xFFFF}, 	{3605, 0xFFFF},
									{3923, 3947, 2656, 5410,  212, 2656, 5410, 2320, 4527, 0xFFFF}, 		{1911, 2348, 3787,  256,  217,  220,  212,  256,  217,  221, 0xFFFF},
									{3875, 3923, 3947, 3053, 3788, 4952, 2716,  256,  217, 239, 0xFFFF}, 	{4214, 3058, 4047, 4093, 1911, 2348, 3787, 0xFFFF},
									{3875, 3923, 3947, 3053, 3788, 4952, 2716,  256,  217, 217, 0xFFFF}, 	{1790, 3723, 4047, 4093, 1911, 2348, 3787, 0xFFFF},
									{3875, 5158, 5143, 5487, 2222, 4507, 4952, 4466, 2036, 0xFFFF}, 		{2056, 2229, 2776, 1773, 3053, 4825, 2545, 1911, 2348, 3787, 0xFFFF},
									{3923, 3947, 2656, 5410, 1911, 2348, 3787, 0xFFFF}, 					{ 256,  217,  220, 0xFFFF},
									{3923, 3947, 2656, 5410, 2320, 4527, 1911, 2348, 3787, 0xFFFF}, 		{ 256,  217,  221, 0xFFFF},
									{1518, 3911, 3437, 1982, 3010, 2811, 4542, 4366, 2067, 0xFFFF}, 		{2866, 1893, 2583, 2456, 1653, 0xFFFF},
									{2866, 2986, 4542, 4366, 5314, 2583, 3875, 1520,  235, 2792, 0xFFFF}, 	{0xFFFF},
									{3875, 3923, 3947, 3053, 3788, 2545,  256,  217,  217, 0xFFFF}, 		{1790, 3723, 4047, 4093, 1911, 2348, 3787, 0xFFFF},
									{3923, 3947, 4663, 3323, 4737, 2537, 0xFFFF}, 							{2811, 4071, 1886, 2690, 4745, 3010, 0xFFFF},
									{3923, 3947, 4071, 1886, 2690, 2682, 5360, 1663, 2690, 4139, 0xFFFF}, 	{2281, 5048, 4542, 4366, 0xFFFF},
									{4682, 2017, 4071, 4815, 4542, 0xFFFF}, 								{4030, 1893, 3427, 1656, 4910, 2036, 3503, 2963, 4536, 2794, 0xFFFF}};


#ifdef	__cplusplus
}
#endif

#endif	/* __PAGE_MSG_H */
