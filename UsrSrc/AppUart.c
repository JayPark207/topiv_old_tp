/* Includes -------------------------------------------------------------------*/
#include "AppUart.h"
#include "string.h"
#include "stdlib.h"
/* typedef --------------------------------------------------------------------*/

/* define ---------------------------------------------------------------------*/
//uint8_t ModbusMap[1024];
BUFF_TYPE BufU1Rx[QUEUE_SIZE];
BUFF_TYPE BufU1Tx[QUEUE_SIZE];

QUEUE	qU1Rx;
QUEUE	qU1Tx;

QUEUE *pqU1Rx;
QUEUE *pqU1Tx;

LOCAL_COMM_MODE_ENUM AppCommMode = INIT_UART;

extern uint8_t u1RxData;

extern ROBOT_CONFIG RobotCfg;
extern CONTROL_MSG ControlMsg;
extern IO_MSG IOMsg;
extern SEQ_MSG SeqMsg;
extern ERROR_MSG ErrMsg;
MODBUS_MSG ModbusMsg;
MODBUS_MSG* pModbusMsg;
uint8_t LoopAuth = LOOP_AUTH_COMM;
uint8_t fUartShare = UART_START;

/* macro ----------------------------------------------------------------------*/

/* function prototypes --------------------------------------------------------*/
void InitQueue(void);
/* ----------------------------------------------------------------------------*/


extern UART_HandleTypeDef huart1;

void AddCRC(QUEUE* pQueue, uint8_t Count)
{
  uint8_t crc_hi = 0xFF; /* high CRC byte initialized */
  uint8_t crc_lo = 0xFF; /* low CRC byte initialized */
  unsigned int i, k, val; /* will index into CRC lookup */
  
  /* pass through message buffer */
  for(i=0; i < Count; i++)
  {
	 val = (*pQueue->pBuff)[i];
	 k = crc_hi ^ val;
	 crc_hi = crc_lo ^ table_crc_hi[k];
	 crc_lo = table_crc_lo[k];
  }
  
  WriteSingleByte(pQueue, crc_hi);
  IncHead(pQueue, 1);
  WriteSingleByte(pQueue, crc_lo);
  IncHead(pQueue, 1);
}

uint8_t CheckRCV(QUEUE* pQueue)
{
  uint8_t crc_hi = 0xFF; /* high CRC byte initialized */
  uint8_t crc_lo = 0xFF; /* low CRC byte initialized */
  uint16_t i, k, val; /* will index into CRC lookup */
  
  uint8_t cntQ;
  
  /**************Check Slave Address*****************************/
  if(GetQueueData(pQueue, MODBUS_POS_SA) != MODBUS_SLAVE_ID) return SA_NOT_MATCH;
  /**************************************************************/
  
  cntQ = GetExistCnt(pQueue);
  
  for(i=0; i < cntQ-3; i++)
  {
	 val = (*pQueue->pBuff)[i];
	 k = crc_hi ^ val;
	 crc_hi = crc_lo ^ table_crc_hi[k];
	 crc_lo = table_crc_lo[k];
  }
  if(crc_hi != (*pQueue->pBuff)[cntQ - 2]) return CRC_ERROR_HI;
  if(crc_lo != (*pQueue->pBuff)[cntQ - 1]) return CRC_ERROR_LOW;
  
  
  return CRC_ERROR_NONE;
}

void SendPacket(QUEUE* pQueue)
{
  uint8_t	u1TxData;
  uint8_t CntOfQueue;
  
  CntOfQueue = GetExistCnt(pQueue);
  for(int i=0;i<CntOfQueue;i++)
  {
	 u1TxData = GetQueueData(pQueue, i);
	 HAL_UART_Transmit(&huart1, &u1TxData, 1, 5000);
  }
}
void RcvProcess(QUEUE* pQRx)
{
  uint8_t Mode;
  uint8_t TempErrState = 0;
  Mode = GetQueueData(pQRx, 2);
  
  if(Mode == MODE_INIT)
  {
	 memcpy(&IOMsg.Y20_Down, ((uint8_t*)(pQRx->pBuff) + 3), SIZE_IO);
	 ControlMsg.CommStateForDL = 1;
  }
  else if(Mode == MODE_SEQ)
  {
	 ControlMsg.fHomingDone = GetQueueData(pQRx, 3);
//	 printf("ControlMsg.fHomingDone : %d\n",ControlMsg.fHomingDone);
	 memcpy(&IOMsg, ((uint8_t*)(pQRx->pBuff) + 4), SIZE_IO);
	 ControlMsg.CommStateForDL = 2;
  }
  else if(Mode == MODE_MANUAL)         ///MANUAL/MANUAL/MANUAL/MANUAL/MANUAL/MANUAL/MANUAL/MANUAL/MANUAL
  {
	 memcpy(&(IOMsg.Y28_Alarm), ((uint8_t*)(pQRx->pBuff) + 14), SIZE_IO-11);   //SIZE_IO
	 //      memcpy(&(IOMsg.Y20_Down), ((uint8_t*)(pQRx->pBuff) + 3), SIZE_IO);   //SIZE_IO
	 //		memcpy(&(IOMsg.X11_MainArmUpComplete), ((uint8_t*)(pQRx->pBuff) + 3), SIZE_INPUT);
	 ControlMsg.OutConveyor = GetQueueData(pQRx, 34);
	 IOMsg.Y2C_Conveyor = ControlMsg.OutConveyor;
  }
  else if(Mode == MODE_STEP)      //STEP//STEP//STEP//STEP//STEP//STEP//STEP//STEP//STEP//STEP//STEP//STEP//STEP//STEP
  {
	 memcpy(&IOMsg, ((uint8_t*)(pQRx->pBuff) + 3), SIZE_IO);
	 SeqMsg.SeqPos = GetQueueData(pQRx, 34);
	 ControlMsg.fHomingDone = GetQueueData(pQRx, 35);
  }
  else if(Mode == MODE_CYCLE_PAUSE)
  {
	 memcpy(&IOMsg.Y20_Down, ((uint8_t*)(pQRx->pBuff) + 3), SIZE_IO);
	 SeqMsg.SeqPos = GetQueueData(pQRx, 34);   	//SeqPos
	 SeqMsg.Timer = GetQueueData(pQRx, 35);      //Timer
	 ControlMsg.fHomingDone = GetQueueData(pQRx, 36);
  }
  else if(Mode == MODE_CYCLE)
  {
	 memcpy(&IOMsg.Y20_Down, ((uint8_t*)(pQRx->pBuff) + 3), SIZE_IO);
	 SeqMsg.SeqPos = GetQueueData(pQRx, 34);   //SeqPos
	 SeqMsg.Timer = GetQueueData(pQRx, 35);      //Timer
	 ControlMsg.fHomingDone = GetQueueData(pQRx, 36);
  }
  else if(Mode == MODE_AUTO)
  {
	 memcpy(&IOMsg.Y20_Down, ((uint8_t*)(pQRx->pBuff) + 3), SIZE_IO);
	 memcpy(&(RobotCfg.Count), ((uint8_t*)(pQRx->pBuff) + 34), SIZE_COUNT);   //Count
	 SeqMsg.SeqPos = GetQueueData(pQRx, 46);   //SeqPos
	 SeqMsg.Timer = GetQueueData(pQRx, 47);      //Timer
	 ControlMsg.fHomingDone = GetQueueData(pQRx, 48);

	 //      test = SeqMsg.SeqPos;
	 //      if(test != SeqMsg.SeqPos)
	 //      printf("SeqMsg.SeqArr[SeqMsg.SeqPos] : %x\t\n",SeqMsg.SeqArr[SeqMsg.SeqPos]);
  }
#ifdef CODE_ERROR	
  else if(Mode == MODE_ERROR)
  {
	 if(ControlMsg.CommMode != MODE_ERROR)
	 {
		ControlMsg.CommMode = MODE_ERROR;
	 }
	 else if(ControlMsg.CommMode == MODE_ERROR)
	 {
		TempErrState = GetQueueData(pQRx, 3);
		//			RobotCfg.ErrorState = 
		if(GetQueueData(pQRx, 4) != 0)
		  RobotCfg.ErrCode = GetQueueData(pQRx, 4);
		
		memcpy(&(RobotCfg.Count), ((uint8_t*)(pQRx->pBuff) + 5), SIZE_COUNT);   //Error Count
	 }
	 if(TempErrState == ERROR_STATE_NONE)
	 {
		if(RobotCfg.ErrorState == ERROR_STATE_NONE)
		{
		}
		else if(RobotCfg.ErrorState == ERROR_STATE_OCCUR)
		{
		  RobotCfg.ErrorState = ERROR_STATE_OCCUR;
		}
		else if(RobotCfg.ErrorState == ERROR_STATE_CLEAR)
		{
		  RobotCfg.ErrorState = ERROR_STATE_CLEAR;
		}
		else if(RobotCfg.ErrorState == ERROR_STATE_ACCEPT)
		{
		  RobotCfg.ErrorState = ERROR_STATE_NONE;
		}
	 }
	 else if(TempErrState == ERROR_STATE_OCCUR)
	 {
		if(RobotCfg.ErrorState == ERROR_STATE_NONE)
		{
		  RobotCfg.ErrorState = ERROR_STATE_OCCUR;
		}
		else if(RobotCfg.ErrorState == ERROR_STATE_OCCUR)
		{
		  RobotCfg.ErrorState = ERROR_STATE_OCCUR;
		}
		else if(RobotCfg.ErrorState == ERROR_STATE_CLEAR)
		{
		  RobotCfg.ErrorState = ERROR_STATE_CLEAR;
		}
		else if(RobotCfg.ErrorState == ERROR_STATE_ACCEPT)
		{
		  RobotCfg.ErrorState = ERROR_STATE_ACCEPT;
		}
	 }
	 else if(TempErrState == ERROR_STATE_CLEAR)
	 {
		if(RobotCfg.ErrorState == ERROR_STATE_NONE)
		{
		  RobotCfg.ErrorState = ERROR_STATE_CLEAR;
		}
		else if(RobotCfg.ErrorState == ERROR_STATE_OCCUR)
		{
		  RobotCfg.ErrorState = ERROR_STATE_CLEAR;
		}
		else if(RobotCfg.ErrorState == ERROR_STATE_CLEAR)
		{
		  RobotCfg.ErrorState = ERROR_STATE_CLEAR;
		}
		else if(RobotCfg.ErrorState == ERROR_STATE_ACCEPT)
		{
		  RobotCfg.ErrorState = ERROR_STATE_ACCEPT;
		}
	 }
	 else if(TempErrState == ERROR_STATE_ACCEPT)
	 {
		if(RobotCfg.ErrorState == ERROR_STATE_NONE)
		{
		  RobotCfg.ErrorState = ERROR_STATE_NONE;
		}
		else if(RobotCfg.ErrorState == ERROR_STATE_OCCUR)
		{
		  RobotCfg.ErrorState = ERROR_STATE_OCCUR;
		}
		else if(RobotCfg.ErrorState == ERROR_STATE_CLEAR)
		{
		  RobotCfg.ErrorState = ERROR_STATE_ACCEPT;
		}
		else if(RobotCfg.ErrorState == ERROR_STATE_ACCEPT)
		{
		  RobotCfg.ErrorState = ERROR_STATE_NONE;
		}
	 }
	 //		if(RobotCfg.ErrorState != TempErrState)
	 //		{
	 //			if(TempErrState == ERROR_STATE_ACCEPT) RobotCfg.ErrorState = ERROR_STATE_NONE;
	 //			else RobotCfg.ErrorState = TempErrState;
	 //			
	 //			if(TempErrState < RobotCfg.ErrorState) TempErrState = RobotCfg.ErrorState;
	 //		}
	 
	 //		if(RobotCfg.ErrorState == ERROR_STATE_NONE) ControlMsg.CommMode = MODE_AUTO;	//Only Auto, Safty Door
	 //		printf("ErrorState : %x \t ErrCode : %x\n\r", RobotCfg.ErrorState, RobotCfg.ErrCode);
	 
  }
  
  
  
  //	else				//1st Error Occur from SC
  //	{
  //	  //SC Error State
  //	  RobotCfg.ErrorState = ERROR_STATE_OCCUR;
  ////	  RobotCfg.ErrCode = GetQueueData(pQRx, 4);
  //	  ControlMsg.CommMode = MODE_ERROR;
  //	  
  ////	  printf("Mode : %x\n",Mode);
  ////	  printf("TP8<->SC ErrorState : %x \t ErrCode : %x\t\n", RobotCfg.ErrorState, RobotCfg.ErrCode);
  //	}
#endif
}
uint8_t ChkModeChange(COMM_MODE* pAppCommMode)
{
  static COMM_MODE OldMode = MODE_STANDBY;
  
  if(OldMode == ControlMsg.CommMode) return MODE_STAY;
  else {
	 OldMode = ControlMsg.CommMode;
	 *pAppCommMode = ControlMsg.CommMode;
	 return MODE_CHANGED;
  }
}
void MakePacket(COMM_MODE* pCommMode, QUEUE* pqTx)
{
  if((*pCommMode) < MODE_MANUAL) ModbusMsg.FuncCode = FC_04_READ_INPUT_REGISTERS;
  else ModbusMsg.FuncCode = FC_16_PRESET_MULTIPLE_REGISTERS;
  
  memcpy(pqTx->pBuff, pModbusMsg, 2);
  IncHead(pqTx, 2);
  
  WriteSingleByte(pqTx, *pCommMode);
  IncHead(pqTx, 1);
  
  if((*pCommMode) == MODE_INIT)
  {
	 WriteSingleByte(pqTx, RobotCfg.MotionMode.OutsideWait);
	 IncHead(pqTx, 1);
	 WriteSingleByte(pqTx, RobotCfg.MotionMode.MainArmDownPos);
	 IncHead(pqTx, 1);
	 WriteSingleByte(pqTx, RobotCfg.MotionMode.SubArmDownPos);
	 IncHead(pqTx, 1);
	 
	 WriteSingleByte(pqTx, RobotCfg.Setting.ErrorTime);
	 IncHead(pqTx, 1);
	 WriteSingleByte(pqTx, RobotCfg.Setting.ProcessTime);
	 IncHead(pqTx, 1);
	 WriteSingleByte(pqTx, RobotCfg.Setting.ItlSaftyDoor);
	 IncHead(pqTx, 1);
	 WriteSingleByte(pqTx, RobotCfg.MotionMode.MotionArm);
	 IncHead(pqTx, 1);
	 WriteSingleByte(pqTx, ControlMsg.fDoHoming);
	 IncHead(pqTx, 1);
	 
	 WriteSingleByte(pqTx, RobotCfg.Setting.ItlAutoInjection);			//Euromap Interlock
	 IncHead(pqTx, 1);
	 WriteSingleByte(pqTx, RobotCfg.Function.Buzzer);			//Euromap Interlock
	 IncHead(pqTx, 1);
	 
  }
  else if((*pCommMode) == MODE_SEQ)
  {
	 WriteSingleByte(pqTx, SeqMsg.SeqPos);
	 IncHead(pqTx, 1);
	 memcpy(&((*pqTx->pBuff)[GetHead(pqTx)]), (uint8_t*)(&SeqMsg.SeqArr), SIZE_SEQARR);
	 IncHead(pqTx, SIZE_SEQARR);
	 
	 memcpy(&((*pqTx->pBuff)[GetHead(pqTx)]), (uint8_t*)(&RobotCfg.Count), SIZE_COUNT);
	 IncHead(pqTx, SIZE_COUNT);
	 
	 memcpy(&((*pqTx->pBuff)[GetHead(pqTx)]), (uint8_t*)(&RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
	 IncHead(pqTx, sizeof(MOTION_DELAY));
  }
  else if((*pCommMode) == MODE_MANUAL)
  {
	 memcpy(&(*pqTx->pBuff)[GetHead(pqTx)], (uint8_t*)(&IOMsg), SIZE_OUTPUT);
	 IncHead(pqTx, SIZE_OUTPUT);
  }
  else if((*pCommMode) == MODE_STEP)
  {
	 WriteSingleByte(pqTx, ControlMsg.StepSeq);
	 IncHead(pqTx, 1);
  }
  else if((*pCommMode) == MODE_CYCLE)
  {
	 
  }
  else if((*pCommMode) == MODE_CYCLE_PAUSE)
  {
	 
  }
  else if((*pCommMode) == MODE_AUTO)
  {
	 memcpy(&(*pqTx->pBuff)[GetHead(pqTx)], (uint8_t*)(&RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
	 IncHead(pqTx, sizeof(MOTION_DELAY));
  }
  else if((*pCommMode) == MODE_ERROR)
  {
	 WriteSingleByte(pqTx, RobotCfg.ErrorState);
	 IncHead(pqTx, 1);
	 WriteSingleByte(pqTx, RobotCfg.ErrCode);
	 IncHead(pqTx, 1);
  }
  AddCRC(pqTx, pqTx->Head);
}

void LoopAppComm(void)
{
  static COMM_MODE AppCommMode = MODE_STANDBY;
  static uint8_t Retry = 0;
  static uint64_t TempCommTick = 0;
  //	static uint64_t CommWatchDogTick = 0;
  static uint64_t UartTick = 0;
  static enum
  {
	 INIT_COMM_LOOP,
	 
	 RESET_COMM,
	 
	 CHECK_MODE,
	 
	 MAKE_PACKET,
	 RCV_PACKET,
	 
	 COMM_TIME_OUT,
	 COMM_SUCCESS,
	 
  }COMMLoopState = INIT_COMM_LOOP;
  switch(COMMLoopState)
  {
  case INIT_COMM_LOOP:
	 pModbusMsg = &ModbusMsg;
	 ModbusMsg.SlaveNo = MODBUS_SLAVE_ID;
	 ControlMsg.CommMode = MODE_STANDBY;
	 InitQueue();
	 COMMLoopState = RESET_COMM;
	 break;
	 
	 
  case RESET_COMM:
	 HAL_UART_Receive_IT(&huart1, &u1RxData, 1);
	 Retry = 0;
	 if(ControlMsg.CommMode != MODE_STANDBY) COMMLoopState = CHECK_MODE;
	 break;
	 
  case CHECK_MODE:
	 ResetQueue(pqU1Rx);
	 ResetQueue(pqU1Tx);
	 if(ChkModeChange(&AppCommMode)) COMMLoopState = RESET_COMM;
	 else
	 {
		SetSysTick(&TempCommTick, INTERVAL_COMM);
		COMMLoopState = MAKE_PACKET;
	 }
	 break;
	 
  case MAKE_PACKET:
	 if(!ChkExpireSysTick(&TempCommTick)) break;
	 ResetQueue(pqU1Tx);
	 MakePacket(&AppCommMode, pqU1Tx);
	 SendPacket(pqU1Tx);
	 SetSysTick(&UartTick, UART_RESPONSE);
	 //		SetSysTick(&CommWatchDogTick, WATCHDOG_TIMEOUT);
	 COMMLoopState = RCV_PACKET;
	 break;
	 
  case RCV_PACKET:
	 if(!ChkExpireSysTick(&UartTick)) break;
	 
	 if(!CheckRCV(pqU1Rx)) 
	 {
		Retry++;
		ResetQueue(pqU1Rx);
		COMMLoopState = MAKE_PACKET;
		if(Retry == MAX_RETRY)		//ERROR//ERROR//ERROR//ERROR//ERROR//ERROR//ERROR//ERROR//ERROR//ERROR
		{
		  COMMLoopState = COMM_TIME_OUT;
		}
	 }
	 else
	 {
		RcvProcess(pqU1Rx);
		COMMLoopState = COMM_SUCCESS;
		
	 }
	 //		if(ChkExpireSysTick(&WATCHDOG_TIMEOUT)) COMMLoopState = RESET_COMM;	//ERROR//ERROR//ERROR//ERROR//ERROR
	 break;
	 
  case COMM_TIME_OUT:
	 //		ResetQueue(pqU1Rx);
	 //		ResetQueue(pqU1Tx);
	 COMMLoopState = CHECK_MODE;
	 break;
	 
  case COMM_SUCCESS:
	 //		ResetQueue(pqU1Rx);
	 //		ResetQueue(pqU1Tx);
	 COMMLoopState = CHECK_MODE;
	 break;
  }
}


void InitQueue(void)
{
  pqU1Tx = &qU1Tx;
  NewQueue(pqU1Tx, &BufU1Tx, QUEUE_SIZE);	
  pqU1Rx = &qU1Rx;
  NewQueue(pqU1Rx, &BufU1Rx, QUEUE_SIZE);	
}
