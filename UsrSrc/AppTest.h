/* 
 * File:   AppTest.h
 * Author: Jay Park
 *
 * Created on 2016년 11월 14일 (Mon), PM 3:33
 */

#ifndef __APPTEST_H
#define	__APPTEST_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "main.h"   
   
#define TIME_TEST 300
#define TIME_KEY_READ	50
#define TIME_KEYCMD_FOR_LCD	1500
	
#define TIME_RTC_TEST	5000

/* FatFs includes component */
#include "ff_gen_drv.h"
#include "sd_diskio.h"
	

#define TEST_LOG			1
#define TEST_SETTING		2
#define TEST_MOLD			3
#define TEST_RTC			4
#define TEST_SEQUENCE	5
#define TEST_MODE		TEST_LOG
	

void LoopAppTest(void);


#ifdef	__cplusplus
}
#endif

#endif	/* __APPTEST_H */

