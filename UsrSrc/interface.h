/**
******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __INTERFACE_H
#define __INTERFACE_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "timer.h"
//#include "fatfs.h"
//#include "sdio.h"
//#include "stm32f4xx_hal_def.h"
/* Defines  ------------------------------------------------------------------*/

#define CODE_ERROR

#define MAX_MOLDNAME_LENGTH             10

#define MAX_LINE_TITLE                  2
#define MAX_LINE_OUTLINE                3
#define MAX_LINE_MENU                   4

#define TIMER_MAX_PAGE		        4
#define IO_MAX_PAGE			5
#define MODE_MAX_PAGE		        3
#define SETTING_MAX_PAGE	        2
#define SETTING_MAX_PAGE_IMMTYPE        3//IMMType
#define IO_OUTPUT			1
#define IO_INPUT			0

#define IO_DISPLAY_ON		        1
#define IO_DISPLAY_OFF		        0
#define IO_NOT_DISPLAY		        0xFF

#define DISABLE_EDIT			0
#define ENABLE_EDIT			1

#define CONTROLMODE_INIT_READ_TYPE	0
#define CONTROLMODE_INIT_DISP_LOGO	1 
#define CONTROLMODE_INIT_LOAD_SD	2

#define CONTROLMODE_MANUAL	        1
#define CONTROLMODE_AUTO	        2
#define CONTROLMODE_EMER	        3
#define CONTROLMODE_STOP	        4
#define CONTROLMODE_STEP	        5
#define CONTROLMODE_CYCLE	        6

#define CONTROLMODE_FILE_SAVED          7

#define DATE_DISPLAY_YYMMDD	        0
#define DATE_DISPLAY_DDMMYY	        1
#define TIME_DISPLAY_24H	        0
#define TIME_DISPLAY_12H	        1

#define VERSION_MAIN	                1
#define VERSION_SUB	                2
#define VERSION_PATCH	                3

#define LANGUAGE_KOR	                0
#define LANGUAGE_ENG	                1
#define LANGUAGE_CHN	                2
#define LANGUAGE_ESP	                3

#define ROBOT_TYPE_A 		        0x00
#define ROBOT_TYPE_X 		        0x01
#define ROBOT_TYPE_XC 		        0x02
#define ROBOT_TYPE_TWIN		        0x03

/**********IO********************/	 

/**************START OF STATE DEFINES**************/
#define MAINARM_DOWN	                3
#define MAINARM_UP	                2
#define MAINARM_UP_DONE	                0

#define KICK_FW	                        1
#define KICK_RETURN	                0

#define SWING_FW	                3
#define SWING_DONE	                1
#define SWING_RETURN	                2
#define SWING_RETURN_DONE	        0

#define SUBARM_DOWN	                3
#define SUBARM_UP	                2
#define SUBARM_UP_DONE	                0

#define CHUCK_ON	                1
#define CHUCK_OFF	                0

#define VACUUM_ON	                0
#define VACUUM_OFF	                1

#define CHUCKROTATE_FW	                0
#define CHUCKROTATE_RETURN	        1

#define SUBARMGRIPPER_ON	        0
#define SUBARMGRIPPER_OFF	        1

#define FULLAUTO_ON	                0
#define FULLAUTO_OFF	                1

#define AUTOINJECTION_ON	        0
#define AUTOINJECTION_OFF	        1

#define MOLDOPENCOMPLETE_ON	        0
#define MOLDOPENCOMPLETE_OFF	        1


#define SAFETYDOOR_ON	                0
#define SAFETYDOOR_OFF	                1

#define ENABLEMOLD_ON	                0
#define ENABLEMOLD_OFF	                1

#define ENABLEEJECTOR_ON	        0
#define ENABLEEJECTOR_OFF	        1
/**************END OF STATE DEFINES**************/	


#define FUNC_DISABLE	                0
#define FUNC_ENABLE	                1

#define MAX_MOLD_NAME	                8

#define KEY_READY			0x00
#define KEY_PRESSED		        0x01

#define NO_CHANGE			0
#define NEED_CHANGE		        1

#define BACKLIGHT_OFF		        1
#define BACKLIGHT_ON			0
/**************MANUFACTURER SETTING**************/	
#define MAX_TIME_FIND_ERR	        9
#define MAX_TIME_PROCESS	        99

#define NO_RUN		                0x00
#define RUN			        0x01

//#define AUTO_STOP		        0x00
#define MANUAL_STOP	                0x01

/**************MODE****************************/	  
/**************MODE****************************/	 
#define ROBOT_MAIN_MAIN			0x00
#define ROBOT_MAIN_SUB			0x01
#define ROBOT_MAIN_MS			0x02

#define NO_USE				0x00
#define USE				0x01

#define ROTATION			0x00
#define NO_ROTATION			0x01

#define STANDARD			0x00
#define VERTI				0x01

#define ARM_NO_USE			0xFF
#define ARM_L_TYPE			0
#define ARM_U_TYPE			1
#define ARM_I_TYPE			2

#define ARM_DOWNPOS_NO_USE		0xFF
#define ARM_DOWNPOS_NOZZLE		0
#define ARM_DOWNPOS_CLAMP		1

#define RELEASE_POS_NO_USE		0xFF  
#define RELEASE_POS_INMOLD		0
#define RELEASE_POS_OUTSIDE		1
#define RELEASE_POS_2ndDOWN		2
#define RELEASE_POS_2ndUP		3
#define RELEASE_POS_ChuRo		4//IMMType

#define POINTER_MODE_MOTIONARM		0
#define POINTER_MODE_USAGE		3
#define POINTER_MODE_MOTION		5
#define POINTER_MODE_DOWN		8
#define POINTER_MODE_OPEN		10
#define POINTER_MODE_REJECT		10

#define POS_FAC_ROTATION		0
#define POS_FAC_SEC			0
#define POS_FAC_NORUN			1
#define POS_FAC_NO			3
#define POS_FAC_MSTOP			5
#define POS_FAC_IMMType			7
///******************************************/	
#define SIZE_SEQARR			99
#define MAX_MOLD_COUNT			100
//#define MODE_ROBOT_NO			0x00
//#define MODE_ROBOT_MAIN_MAIN		0x01
//#define MODE_ROBOT_MAIN_SUB		0x02
//#define MODE_ROBOT_MAIN_MS		0x03
//					
//#define NO_USE			0x00
//#define USE				0x01
//
//#define MODE_ARM_NO_USE		0
//#define MODE_ARM_L_TYPE		1
//#define MODE_ARM_U_TYPE		2
//#define MODE_ARM_I_TYPE		3
//
//#define MODE_ARM_DOWNPOS_NO_USE	0//oxF0
//#define MODE_ARM_DOWNPOS_NOZZLE	1
//#define MODE_ARM_DOWNPOS_CLAMP	2
//
//#define MODE_RELEASE_POS_NO_USE	0//0xF0	  
//#define MODE_RELEASE_POS_INMOLD	1
//#define MODE_RELEASE_POS_OUTSIDE	2
//#define MODE_RELEASE_POS_2ndDOWN	3
//#define MODE_RELEASE_POS_2ndUP	4
//
//#define POINTER_MODE_ MOTIONARM	0
//#define POINTER_MODE_USAGE		3
//#define POINTER_MODE_MOTION		5
//#define POINTER_MODE_DOWN		8
//#define POINTER_MODE_OPEN		10
//#define POINTER_MODE_REJECT		10
//
//#define POS_FAC_SEC			0
//#define POS_FAC_NORUN			1
//#define POS_FAC_NO			3
//#define POS_FAC_MSTOP			5
/******************************************/	

/******** SEQ ************/
//#define SEQ_ROBOT_MAIN_MAIN		0x00
//#define SEQ_ROBOT_MAIN_SUB		0x01
//#define SEQ_ROBOT_MAIN_MS		0x02
//
//#define SEQ_ARM_NO_USE		-1
//#define SEQ_ARM_L_TYPE		0
//#define SEQ_ARM_U_TYPE		1
//#define SEQ_ARM_I_TYPE		2
//
//#define SEQ_ARM_DOWNPOS_NO_USE	-1
//#define SEQ_ARM_DOWNPOS_NOZZLE	0
//#define SEQ_ARM_DOWNPOS_CLAMP		1
//
//#define SEQ_RELEASE_POS_NO_USE	-1//0xF0	  
//#define SEQ_RELEASE_POS_INMOLD	0
//#define SEQ_RELEASE_POS_OUTSIDE	1
//#define SEQ_RELEASE_POS_2ndDOWN	2
//#define SEQ_RELEASE_POS_2ndUP		3
/******** SEQ ************/




#define DO_NOT_DISPALY_ICON	        0xFF

#define IO_M_UP		                0
#define IO_M_DOWN		        1

#define SIGNAL_ON			1
#define SIGNAL_OFF		        0

#define FUNC_OFF		        0
#define FUNC_ON		                1

/******************************************/	
#define CMD_STATE_NORMAL		0x80

#define CMD_STATE_STEP_RUN              0x40
#define CMD_STATE_MAKE_SEQ              0x20

#define CMD_STATE_MANUAL		0x01
#define CMD_STATE_STEP			0x02
#define CMD_STATE_CYCLE			0x04
#define CMD_STATE_AUTO			0x08
#define CMD_STATE_EMERGENCY	        0x10

#define MASK_CMD_STATE_SEQ              0x6E

//#define ROBOT_DISABLE			0xFF
/* Exported types ------------------------------------------------------------*/
typedef uint8_t	IO;			//Digital Input / Ouptut
typedef uint8_t	ITL;		        //Interlock


#define SEQ_STATE_RUN			0x01
#define SEQ_STATE_PAUSE			0x02
#define SEQ_STATE_MAKE_SEQ		0x80//TP Cmd : Make Sequence --> SC : State to INIT
#define SEQ_STATE_INIT			0x10

#define STEP_ING                        0x00
#define STEP_END                        0x01

#define FHOMING_DONE                    0x01
#define FHOMING_ING                     0x00

#define MANUAL_ERR_CHYCK_RO		0x01
#define MANUAL_ERR_CHYCK_ROR		0x02
#define MANUAL_ERR_MOLD_DELETE		0x03
#define MANUAL_ERR_ROBOT_TYPE		0x04
#define MANUAL_ERR_NO_MOLDNO		0x05
#define MANUAL_ERR_MOLD			0x06
#define MANUAL_ERR_SAFTYDOOR_STEP	0x07
#define MANUAL_ERR_SAFTYDOOR_CYCLE	0x08
#define MANUAL_ERR_SAFTYDOOR_AUTO	0x09
#define MANUAL_ERR_SAFTYDOOR_CHANGE	0x10
#define MANUAL_ERR_REBOOTING		0x11
#define MANUAL_ERR_SAFTYDOOR_CLOSE	0x12

#define ERRCODE_SD_DISABLE		0x30
#define ERRCODE_SD_NOT_FILE		0x31
#define ERRCODE_SD_LOCKING		0x32
#define ERRCODE_SD_FORMAT		0x33//???
#define ERRCODE_SD_EMPTY		0x34

#define ERRCODE_EMG                	0x60//TP 비상스위치
#define ERRCODE_IMM_EMG            	0x62//사출 비상
#define ERRCODE_SWING_DUPLICATE    	0x84//스윙 신호 둘다 켜짐
#define ERRCODE_SUB_UP             	0x86//하강 후 센서 안꺼짐
#define ERRCODE_MAIN_UP            	0x87//하강 후 센서 안꺼짐
#define ERRCODE_SUB_UP_AIR         	0x94//상승 후 센서 안켜짐
#define ERRCODE_MAIN_UP_AIR        	0x96//상승 후 센서 안켜짐
#define ERRCODE_SWING              	0x98
#define ERRCODE_SWING_RETURN       	0x99

#define ERRCODE_VACCUM             	0xA0//흡착 타임아웃
#define ERRCODE_CHUCK              	0xA1//척 타임아웃
#define ERRCODE_RUNNER_PICK        	0xA3//런너 타임아웃

#define ERRCODE_MO_SENSOR          	0xC5//???????
#define ERRCODE_MO_SENS_MISS       	0xCA//하강 후 형개완료 신호 없어짐.
#define ERRCODE_MO_DOWN            	0xD6//하강 시, 형개완료 없음.(매뉴얼 모드)
#define ERRCODE_PROCESS_TIME       	0xD7      

#define ERROR_STATE_NONE      		0x00
#define ERROR_STATE_OCCUR      		0x01 //SC, TP //ErrCode
#define ERROR_STATE_CLEAR      		0x02 //TP Clear
#define ERROR_STATE_ACCEPT      	0x03 //SC OK --> TP 0x00
#define ERROR_STATE_EMO			0x04

typedef enum _SEQ_ENUM
{
  OUTPUT_UPDOWN_Y20_ON = 0x01,          //0x01
  OUTPUT_KICK_Y21_ON,             
  OUTPUT_CHUCK_Y22_ON,                                 
  OUTPUT_SWING_Y23_ON,        
  OUTPUT_SWING_RETURN_Y2F_ON,
  OUTPUT_ROTATE_CHUCK_Y24_ON,      
  OUTPUT_VACCUM_Y25_ON,                           
  OUTPUT_NIPPER_Y26_ON,      
  OUTPUT_SUB_UPDOWN_Y2D_ON,
  OUTPUT_SUB_KICK_Y2E_ON,
  OUTPUT_SUB_CHUCK_Y27_ON,                        
  OUTPUT_ALARM_Y28_ON,   
  OUTPUT_MAIN_POWER_Y2G_ON,
  
  ITLO_CYCLE_START_Y29_ON,              //0x0E
  ITLO_MOLD_OPENCLOSE_Y2A_ON,
  ITLO_EJECTOR_Y2B_ON,
  ITLO_CONVEYOR_Y2C_ON,
  ITLO_BUZZER_Y28_ON,
  
  INPUT_UP_COMPLETE_X11_ON,             //0x13
  INPUT_CHUCK_OK_X16_ON,                //0x14
  INPUT_SWING_DONE_X14_ON,
  INPUT_SWING_RETURN_DONE_X15_ON,
  INPUT_VACCUM_CHECK_X17_ON,            //0x17
  INPUT_SUB_UP_COMPLETE_X1G_ON,
  INPUT_SUB_CHUCK_OK_X1F_ON,            //0x19
  
  ITLI_FULLAUTO_X1H_ON,                 //0x1A
  ITLI_AUTO_INJECTION_X19_ON,
  ITLI_MOLD_OPENED_X18_ON,
  ITLI_SAFTY_DOOR_X1A_ON,
  ITLI_REJECT_X1B_ON,
  ITLI_EMO_IMM_X1I_ON,
  
  
  
  OUTPUT_UPDOWN_Y20_OFF = 0x41,         //0x41
  OUTPUT_KICK_Y21_OFF,
  OUTPUT_CHUCK_Y22_OFF,
  OUTPUT_SWING_Y23_OFF,
  OUTPUT_SWING_RETURN_Y2F_OFF,
  OUTPUT_ROTATE_CHUCK_Y24_OFF,
  OUTPUT_VACCUM_Y25_OFF,
  OUTPUT_NIPPER_Y26_OFF, 
  OUTPUT_SUB_UPDOWN_Y2D_OFF,
  OUTPUT_SUB_KICK_Y2E_OFF,
  OUTPUT_SUB_CHUCK_Y27_OFF,
  OUTPUT_ALARM_Y28_OFF,
  OUTPUT_MAIN_POWER_Y2G_OFF,
  
  ITLO_CYCLE_START_Y29_OFF,             //0x4E
  ITLO_MOLD_OPENCLOSE_Y2A_OFF,
  ITLO_EJECTOR_Y2B_OFF,
  ITLO_CONVEYOR_Y2C_OFF,
  ITLO_BUZZER_Y28_OFF,
  
  INPUT_UP_COMPLETE_X11_OFF,            //0x53
  INPUT_CHUCK_OK_X16_OFF,
  INPUT_SWING_DONE_X14_OFF,
  INPUT_SWING_RETURN_DONE_X15_OFF,
  INPUT_VACCUM_CHECK_X17_OFF,
  INPUT_SUB_UP_COMPLETE_X1G_OFF,
  INPUT_SUB_CHUCK_OK_X1F_OFF,
  
  ITLI_FULLAUTO_X1H_OFF,                //0x5A
  ITLI_AUTO_INJECTION_X19_OFF,
  ITLI_MOLD_OPENED_X18_OFF,
  ITLI_SAFTY_DOOR_X1A_OFF,
  ITLI_REJECT_X1B_OFF,
  ITLI_EMO_IMM_X1I_OFF,
  
  DELAY_DOWN = 0x81,
  DELAY_KICK,
  DELAY_EJECTOR,
  DELAY_CHUCK,
  DELAY_KICK_RETURN,
  DELAY_UP,
  DELAY_SWING,
  DELAY_DOWN2ND,
  DELAY_OPEN,
  DELAY_UP2ND,		                //0x8a
  DELAY_CHUCK_ROTATE_RETURN,
  DELAY_SWING_RETURN,
  DELAY_NIPPER,
  DELAY_CONVEYOR,
  DELAY_USER1,
  DELAY_USER2,
  DELAY_USER3,
  DELAY_USER4,
  DELAY_USER5,
  DELAY_USER6,
  DELAY_USER7,
  DELAY_USER8,
  
  ITLO_CYCLE_START_Y29_ON_FIRST = 0xC0,
  ITLO_MOLD_OPENCLOSE_Y2A_ON_FIRST,
  
  ITLI_REJECT_STATE,	                //Reject On/OFF 
  SEQ_END = 0xFF,
}SEQ_ENUM;

typedef struct _MOTION_DELAY{
  uint8_t DownDelay;
  uint8_t KickDelay;
  uint8_t EjectorDelay;
  uint8_t ChuckDelay;
  uint8_t KickReturnDelay;
  uint8_t UpDelay;
  uint8_t SwingDelay;
  uint8_t Down2ndDelay;
  uint8_t OpenDelay;
  uint8_t Up2ndDelay;
  uint8_t ChuckRotateReturnDelay;
  uint8_t SwingReturnDelay;	
  uint8_t NipperOnDelay;
  uint8_t ConveyorDelay;
  uint8_t UserDelay1;
  uint8_t UserDelay2;
  uint8_t UserDelay3;
  uint8_t UserDelay4;
  uint8_t UserDelay5;
  uint8_t UserDelay6;
  uint8_t UserDelay7;
  uint8_t UserDelay8;
}MOTION_DELAY;

typedef enum _SEQ_DISP_ENUM
{
  SEQ_DOWN,
  SEQ_KICK,
  SEQ_EJECT,
  SEQ_CHUCK_ON,
  SEQ_KICK_RETURN,
  SEQ_UP,
  SEQ_SWING,
  SEQ_CHUCK_ROTATE,
  SEQ_DOWN_2ND,
  SEQ_NIPPER_CUT,
  SEQ_CHUCK_OFF,
  SEQ_UP_2ND,
  SEQ_SWING_RETURN,
  //SEQ_KICK_RETURN,
  //SEQ_KICK,
  SEQ_SUB_ARM_DOWN,
  SEQ_MAIN_ARM_UP,
  SEQ_DISP_END,
}SEQ_DISP_ENUM;

typedef struct _SEQ_MSG{
  uint8_t SeqState;			
  
  uint8_t SeqPos;
  SEQ_ENUM SeqArr[SIZE_SEQARR];		//IO  Sequence
  uint8_t Timer;	                //Delay (Real Time)
  
  uint8_t SeqDispPos;		        //0~20
  //  SEQ_DISP_ENUM SeqDispArr[15];
  uint8_t SeqDispArr[20];
  
  uint8_t SeqTime[10];
}SEQ_MSG;



typedef struct _IO_MSG{
  IO Y20_Down;
  IO Y21_Kick;
  IO Y22_Chuck;
  IO Y23_Swing;
  IO Y2F_SwingReturn;
  IO Y24_ChuckRotation;
  IO Y25_Vaccum;
  IO Y26_Nipper;
  IO Y2D_SubUp;
  IO Y2E_SubKick;
  IO Y27_SubGrip;
  IO Y28_Alarm;
  IO Y2G_MainPower;
  
  ITL Y29_CycleStart;
  ITL Y2A_MoldOpenClose;
  ITL Y2B_Ejector;
  ITL Y2C_Conveyor;
  ITL Y28_Buzzer;					
  
  IO X11_MainArmUpComplete;
  IO X16_MainChuckOk;
  IO X14_SwingOk;
  IO X15_SwingReturnOk;
  IO X17_MainVaccumOk;
  IO X1G_SubUpComplete;
  IO X1F_SubGripComplete;       
  
  ITL X1H_FullAuto;      
  ITL X19_AutoInjection;
  ITL X18_MoldOpenComplete;
  ITL X1A_SaftyDoor;
  ITL X1B_Reject;
  ITL X1I_EMOFromIMM;
}IO_MSG;

typedef struct _MOTION_MODE{
  uint8_t MotionArm;
  uint8_t MainChuck;
  uint8_t MainVaccum;
  uint8_t MainRotateChuck;	
  uint8_t OutsideWait;			
  uint8_t MainArmType;			
  uint8_t SubArmType;			
  uint8_t MainArmDownPos;		
  uint8_t SubArmDownPos;		
  uint8_t ChuckOff;
  uint8_t VaccumOff;
  uint8_t SubArmOff;
  uint8_t ChuckRejectPos;
  uint8_t VaccumRejectPos;
  uint8_t MainNipper;
}MOTION_MODE;



typedef struct _COUNT_MSG{
  uint32_t TotalCnt;
  uint32_t RejectCnt;
  uint32_t DetectFail;
}COUNT_MSG;

typedef struct _FUNC_MSG{
  uint8_t Buzzer;
  uint8_t Detection;
  uint8_t Ejector;
  uint8_t Reject;
  uint8_t FuncSaveCnt;			//Function Count Save
}FUNC_MSG;

typedef struct _STRUCT_TIME{
  RTC_DateTypeDef Date;
  RTC_TimeTypeDef Time;
}STRUCT_TIME;

typedef struct _MANUFACTURER_SETTING{
  uint8_t ErrorTime;		        //센서 에러 검지
  uint8_t ItlFullAuto;
  uint8_t ItlSaftyDoor;
  uint8_t ItlAutoInjection;
  uint8_t ItlReject;
  uint8_t ProcessTime;
  uint8_t DelMoldData;
  uint8_t DelErrHistory;
  uint8_t DoorSignalChange;             //DrSigCh
  uint8_t IMMType;	                //IMMType
  uint8_t RotationState;	        //IMMType
  
  STRUCT_TIME Time;
}MANUFACTURER_SETTING;

typedef struct _ROBOT_CONFIG{
  uint8_t RobotType;
  
  MOTION_MODE MotionMode;
  MOTION_DELAY MotionDelay;
  
  COUNT_MSG Count;
  
  MANUFACTURER_SETTING Setting;
  FUNC_MSG Function;
  
  char MoldName[MAX_MOLDNAME_LENGTH];
  uint8_t ErrorState;
  uint8_t ErrCode;
  
  uint16_t MoldNo;			//Present Mold Number
  uint8_t ErrorCnt;			//total Error Count
  uint8_t Language;
}ROBOT_CONFIG;


/**************************************************************************/
#define IO_UPDATE_READY			0x00
#define IO_UPDATE_PROCESS		0x01

#define IO_EMERGENCY_INACTIVE	        0x00
#define IO_EMERGENCY_ACTIVE	        0x01

#define EMC_CHK_MASK			0x0000//0xA000

typedef enum _TP_MODE
{
  INIT_READ_TYPE = 0x00,
  INIT_DISP_LOGO = 0x01,
  INIT_LOAD_SD = 0x02,
  INIT_MOLD_LIST_UP = 0x03,
  INIT_SC	=	0x04,
  
  MANUAL_OPERATING = 0x10,
  MANUAL_MODE = 0x11,
  MANUAL_COUNTER = 0x12,
  MANUAL_IO = 0x13,					
  MANUAL_TIMER = 0x14,
  MANUAL_STEP_OPERATING = 0x15,
  MANUAL_CYCLE_OPERATING = 0x16,
  MANUAL_MOLD_SEARCH = 0x17,
  MANUAL_MOLD_MANAGE = 0x18,
  MANUAL_MOLD_MODE = 0x19,
  MANUAL_MOLD_NEW = 0x1A,
  MANUAL_MOLD_DELETE = 0x1B,
  
  MANUAL_ERROR_LIST = 0x1C,
  MANUAL_VERSION = 0x1D,
  
  AUTO_MESSAGE_INIT = 0x1E,
  AUTO_MESSAGE = 0x1F,
  AUTO_OPERATING = 0x20,
  AUTO_MODE = 0x21,	
  AUTO_COUNTER = 0x22,
  AUTO_IO = 0x23,
  AUTO_TIMER = 0x24,
  AUTO_ERROR_LIST = 0x25,
  
  OCCUR_ERROR = 0x30,			//Exist Error Code
  SETTING_MANUFACTURER = 0x31,
  
  MANUAL_ERROR = 0x32,			//Non Error Code
  MANUAL_MOLD_DELETE_ING = 0x33,
  
  MANUAL_HOMING = 0x34,
  
  SD_BACKUP = 0x40,
  SD_WRITE = 0x41
}TP_MODE;

typedef enum _COMM_MODE
{
  MODE_STANDBY = 0x00,			//
  MODE_INIT = 0x01,
  //	MODE_STANDBY = 0x02,		//normal
  MODE_SEQ = 0x04,
  
  MODE_MANUAL = 0x10,	
  MODE_STEP = 0x11,
  
  MODE_CYCLE_PAUSE = 0x20,
  MODE_CYCLE = 0x21,
  MODE_AUTO = 0x22,
  
  MODE_ERROR = 0x80,
}COMM_MODE;

//typedef struct _COMM_MSG{
//	COMM_MODE SCMode;		
//	uint8_t OutWait;		//0 : default 1: Outwait	RobotCfg.MotionMode
//	uint8_t MainProductPos; //0:이동측, 1: 고정측		RobotCfg.MotionMode
//	uint8_t SubProductPos; 	//0:이동측, 1: 고정측		RobotCfg.MotionMode
//	IO_MSG* pIOMsg;
//	uint8_t* pSeqPos;
//	uint8_t* pSeqArr;
//	COUNT_MSG* pCount;
//}COMM_MSG;

typedef struct _ERROR_MSG{
  uint16_t	ErrorNo;
  STRUCT_TIME	Time;
  uint16_t ErrorCode;
  uint16_t ErrorCount;
}ERROR_MSG;

typedef struct _CONTROL_MSG{
  TP_MODE TpMode;
  
  //  uint8_t* pStepDispTemp;
  //  uint8_t StepDispTemp[20];
  
  uint8_t TimerDisp[20];
  uint8_t TimerDispTemp[20];
  uint8_t TimerDelay[20];
  uint8_t TimerDelayTemp[20];
  
  uint8_t ModeDisp[20];
  uint8_t ModeDispTemp[20];
  uint8_t ModeSelect[20];
  
  uint8_t StepDispTemp[20];
  
  //  uint8_t StepDisp[20];
  
  
  SEQ_ENUM SeqDisp[100];
  
  uint8_t KeyReady;		        //KEY_READY, KEY_PRESSED
  uint32_t KeyValue;
  
  uint8_t IsChangedLCD;	                //NO_CHANGE, NEED_CHANGE
  uint8_t IsChangedBL;
  uint8_t BackLight;
  
  uint8_t fHomingDone;
  
  uint8_t StepSeq;  
  //uint8_t DoComm;		        //Delete
  COMM_MODE CommMode;
  
  uint8_t PageNo;
  uint8_t LineNo;
  uint8_t AutoPageNo;
  uint8_t AutoLineNo;
  uint8_t CursorPos;
  uint8_t EnableEdit;
  uint8_t fDoHoming;
  uint8_t OutConveyor;
  
  uint8_t CommStateForDL;	        //MODE_INIT, MODE_SEQ
}CONTROL_MSG;

typedef struct _MOLD_MSG{
  uint16_t MoldNo;
  uint16_t ExistMold[MAX_MOLD_COUNT];	//add
  
  uint16_t MoldCount;			//add
  
  char MoldName[MAX_MOLDNAME_LENGTH];
  char ExistMoldName[MAX_MOLD_COUNT][MAX_MOLDNAME_LENGTH];
  
  MOTION_MODE MotionMode;
  MOTION_DELAY MotionDelay;
  
  COUNT_MSG Count; 
  
  FUNC_MSG Function;
}MOLD_MSG;

//
//typedef struct _CONTROL_MSG{
//	uint8_t IO_Update;	         //when comm, emergency, Auto Sequence
//	uint8_t IO_Emergency;          
//}CONTROL_MSG;
//typedef struct _ERROR_MSG{
//	uint16_t	ErrorNo;
////	STRUCT_TIME	Time;
//	uint16_t ErrorCode;
//}ERROR_MSG;


/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */


#endif /* __INTERFACE_H */

/******END OF FILE****/

