#include "ff.h"
#include "SDCard.h"
#include <stdlib.h>
#include <string.h>
extern RTC_HandleTypeDef hrtc;

extern DMA_HandleTypeDef hdma_sdio_rx;
extern DMA_HandleTypeDef hdma_sdio_tx;
extern SD_HandleTypeDef hsd;

FIL SettingFile;
FIL MoldFile;
FIL LogFile;
FIL LanguageFile;
FIL FileTest;

SDRES sdRes;
FIL CheckFile;

FATFS SDFatFs;  /* File system object for SD card logical drive */
char SDPath[4]; /* SD card logical drive path */
uint8_t buffer[_MAX_SS]; /* a work buffer for the f_mkfs() */

FIL MyFile;
FRESULT res;
uint32_t byteswritten, bytesread; /* File write/read counts */

uint8_t wtext[1000] = ""; /* File write buffer */
uint8_t rtext[1000]; /* File read buffer */
char retValueString[10] = {0,};

SDRES MatchingResult(FRESULT fRes)
{
  if(fRes == FR_OK) return SD_OK;							  /* (0) Succeeded */
  else if(fRes == FR_DISK_ERR) return SD_FAIL_INIT;           /* (1) A hard error occurred in the low level disk I/O layer */
  else if(fRes == FR_INT_ERR) return SD_FAIL_INIT;            /* (2) Assertion failed */
  else if(fRes == FR_NOT_READY) return SD_FAIL_INIT;          /* (3) The physical drive cannot work */
  else if(fRes == FR_NO_FILE) return SD_FILE_OPEN_FAIL;            /* (4) Could not find the file */
  else if(fRes == FR_NO_PATH) return SD_FILE_OPEN_FAIL;            /* (5) Could not find the path */
  else if(fRes == FR_INVALID_NAME) return SD_FILE_OPEN_FAIL;       /* (6) The path name format is invalid */
  else if(fRes == FR_DENIED) return SD_FILE_OPEN_FAIL;             /* (7) Access denied due to prohibited access or directory full */
  else if(fRes == FR_EXIST) return SD_FILE_OPEN_FAIL;              /* (8) Access denied due to prohibited access */
  else if(fRes == FR_INVALID_OBJECT) return SD_FILE_OPEN_FAIL;     /* (9) The file/directory object is invalid */
  else if(fRes == FR_WRITE_PROTECTED) return SD_SLOT_WR_PROTECT;    /* (10) The physical drive is write protected */
  else if(fRes == FR_INVALID_DRIVE) return SD_FAIL_INIT;      /* (11) The logical drive number is invalid */
  else if(fRes == FR_NOT_ENABLED) return SD_FAIL_INIT;        /* (12) The volume has no work area */
  else if(fRes == FR_NO_FILESYSTEM) return SD_FAIL_INIT;      /* (13) There is no valid FAT volume */
  else if(fRes == FR_MKFS_ABORTED) return SD_FAIL_INIT;       /* (14) The f_mkfs() aborted due to any problem */
  else if(fRes == FR_TIMEOUT) return SD_FAIL_INIT;            /* (15) Could not get a grant to access the volume within defined period */
  else if(fRes == FR_LOCKED) return SD_FILE_OPEN_FAIL;             /* (16) The operation is rejected according to the file sharing policy */
  else if(fRes == FR_NOT_ENOUGH_CORE) return SD_FILE_OPEN_FAIL;    /* (17) LFN working buffer could not be allocated */
  else if(fRes == FR_TOO_MANY_OPEN_FILES) return SD_FILE_OPEN_FAIL;/* (18) Number of open files > _FS_LOCK */
  else if(fRes == FR_INVALID_PARAMETER) return SD_FILE_OPEN_FAIL;  /* (19) Given parameter is invalid */
  return SD_FAIL;
}
SDRES MountSD(void)
{
  SDRES	SDres = SD_OK;
  if(FATFS_LinkDriver(&SD_Driver, PATH_ROOT) != 0)
  {
	 //	 printf("Fail to LinkDriver!!\n");
	 return SD_SLOT_EMPTY;
  }
  
  if(f_mount(&SDFatFs, (TCHAR const*)PATH_ROOT, 0) != FR_OK)
  {
	 //	 printf("Fail to Mount!!\n");
	 return SD_SLOT_EMPTY;
  }
  
  //  printf("MountSD OK\n");
  return	SDres;
}

SDRES OpenFile(
					FIL* fp,			/* Pointer to the blank file object */
					const TCHAR* path,	/* Pointer to the file name */
					BYTE mode			/* Access mode and file open mode flags */
					  )
{
  FRESULT fRes;
  //  FA_OPEN_EXISTING | FA_WRITE | FA_READ
  fRes = f_open(fp, path, mode);
  if(fRes != FR_OK)
  {
	 //	  printf("Error open file : %3d\n", fRes);
	 return MatchingResult(fRes);
  }
  return SD_OK;
} 

void CloseFile(FIL* fp)
{
  f_close(fp);
}




int ReadValueInFileInt(FIL* fp, uint16_t pos, uint8_t btr)
{
  int retValue;
  f_lseek(fp, pos);
  f_read(fp, rtext, btr, (void *)&bytesread);
  retValue = atoi((char const*)rtext);
  
  memset(rtext, NULL, 100);
  return retValue;
}

void ReadValueInFileString(FIL* fp, uint16_t pos, uint8_t btr)
{
  f_lseek(fp, pos);
  f_read(fp, retValueString, btr, (void *)&bytesread);
}


//uint16_t CountError(void)
//{
//   uint16_t ret;
//   OpenFile(&MyFile, FILE_NAME_ERROR, FA_OPEN_APPEND | FA_WRITE | FA_READ);
//   
//   ret = (uint16_t)(f_size(&MyFile) / FILE_ERROR_LINE_LEN);
//	printf("f_size : %d\n\r", f_size(&MyFile));
//   CloseFile(&MyFile);
//   return ret;
//}
SDRES WriteError(ERROR_MSG* ErrMsg)
{
  SDRES	SDres;
  uint16_t ErrCnt = 0;
  
  SDres = OpenFile(&MyFile, FILE_NAME_ERROR, FA_OPEN_APPEND | FA_WRITE | FA_READ);
  if(SDres != SD_OK) return SDres;
  //	f_lseek(&MyFile, 0);
  f_printf(&MyFile, "[ErrorNo] : %3d\t",
			  ErrMsg->ErrorNo);
  f_printf(&MyFile, "[ErrorCode] : %3d\t",
			  ErrMsg->ErrorCode);
  
  f_printf(&MyFile, "[Time] : %2d/%2d/%2d\t",
			  ErrMsg->Time.Date.Year, ErrMsg->Time.Date.Month, ErrMsg->Time.Date.Date);
  f_printf(&MyFile, "%2d:%2d:%2d\n",
			  ErrMsg->Time.Time.Hours, ErrMsg->Time.Time.Minutes, ErrMsg->Time.Time.Seconds);
  
  
  ErrCnt = (uint16_t)(f_size(&MyFile) / FILE_ERROR_LINE_LEN);
  
  CloseFile(&MyFile);
  
  if(ErrCnt > MAX_COUNT_ERROR)
  {
	 f_unlink(FILE_NAME_ERROR2);
	 f_rename(FILE_NAME_ERROR, FILE_NAME_ERROR2);
	 OpenFile(&MyFile, FILE_NAME_ERROR, FA_CREATE_ALWAYS | FA_WRITE | FA_READ);
	 CloseFile(&MyFile);
	 //	  printf("err2 Crea\n\r");
  }
  
  return SD_OK;
}

SDRES ClearError(void)
{
  SDRES	SDres;
  
  SDres = OpenFile(&MyFile, FILE_NAME_ERROR, FA_CREATE_ALWAYS);
  //  printf("SDRES 1 : %d\n",SDres);
  if(SDres != SD_OK) return SDres;
  
  //  printf("SDRES 2 : %d\n",SDres);
  
  CloseFile(&MyFile);
  return SD_OK;
}

uint16_t CountErr(void)
{
  uint16_t retCnt, ErrCnt;
  
  OpenFile(&MyFile, FILE_NAME_ERROR, FA_OPEN_APPEND | FA_WRITE | FA_READ);
  
  ErrCnt = (uint16_t)(f_size(&MyFile) / FILE_ERROR_LINE_LEN);
  
  //for Load Error
  if(ErrCnt >= 100) retCnt = 100;
  else retCnt = ErrCnt;
  
  CloseFile(&MyFile);
  
  return retCnt;
}
void ReadError(ERROR_MSG* arrErrMsg)
{
  //  SDRES	SDres;
  uint16_t ErrCnt = 0;
  uint8_t MaxLoad = 0;
  //  ErrCnt = CountError();
  
  //  SDres = OpenFile(&MyFile, FILE_NAME_ERROR, FA_OPEN_APPEND | FA_WRITE | FA_READ);
  OpenFile(&MyFile, FILE_NAME_ERROR, FA_OPEN_APPEND | FA_WRITE | FA_READ);
  ErrCnt = (uint16_t)(f_size(&MyFile) / FILE_ERROR_LINE_LEN);
  
  //for Load Error
  if(ErrCnt >= 100) MaxLoad = 100;
  else MaxLoad = ErrCnt;
  
  
  for(int i = 1; i <= MaxLoad;i++)
  {
	 arrErrMsg[i-1].ErrorNo = ReadValueInFileInt(&MyFile, (FILE_SEEK_ERROR_CHAR_1LINE*(ErrCnt-i)) + FILE_SEEK_ERROR_NUM, 3);
	 arrErrMsg[i-1].ErrorCode = ReadValueInFileInt(&MyFile, (FILE_SEEK_ERROR_CHAR_1LINE*(ErrCnt-i)) + FILE_SEEK_ERROR_CODE, 3);
	 arrErrMsg[i-1].Time.Date.Year = ReadValueInFileInt(&MyFile, (FILE_SEEK_ERROR_CHAR_1LINE*(ErrCnt-i)) + FILE_SEEK_ERROR_YEAR, 2);
	 arrErrMsg[i-1].Time.Date.Month = ReadValueInFileInt(&MyFile, (FILE_SEEK_ERROR_CHAR_1LINE*(ErrCnt-i)) + FILE_SEEK_ERROR_MONTH, 2);
	 arrErrMsg[i-1].Time.Date.Date = ReadValueInFileInt(&MyFile, (FILE_SEEK_ERROR_CHAR_1LINE*(ErrCnt-i)) + FILE_SEEK_ERROR_DAY, 2);
	 
	 arrErrMsg[i-1].Time.Time.Hours = ReadValueInFileInt(&MyFile, (FILE_SEEK_ERROR_CHAR_1LINE*(ErrCnt-i)) + FILE_SEEK_ERROR_HOUR, 2);
	 arrErrMsg[i-1].Time.Time.Minutes = ReadValueInFileInt(&MyFile, (FILE_SEEK_ERROR_CHAR_1LINE*(ErrCnt-i)) + FILE_SEEK_ERROR_MINUTE, 2);
	 arrErrMsg[i-1].Time.Time.Seconds = ReadValueInFileInt(&MyFile, (FILE_SEEK_ERROR_CHAR_1LINE*(ErrCnt-i)) + FILE_SEEK_ERROR_SECOND, 2);
	 
	 //	  printf("[ErrorNo] : %3d\t",
	 //				  arrErrMsg[i-1].ErrorNo);
	 //	  printf("[ErrorCode] : %3d\t",
	 //				  arrErrMsg[i-1].ErrorCode);
	 //	  
	 //	  printf("[Time] : %2d/%2d/%2d\t",
	 //				  arrErrMsg[i-1].Time.Date.Year, arrErrMsg[i-1].Time.Date.Month, arrErrMsg[i-1].Time.Date.Date);
	 //	  printf("%2d:%2d:%2d\n\r",
	 //				  arrErrMsg[i-1].Time.Time.Hours, arrErrMsg[i-1].Time.Time.Minutes, arrErrMsg[i-1].Time.Time.Seconds);
  }
  CloseFile(&MyFile);
}

SDRES DeleteMoldFile(uint16_t MoldNo)
{
  SDRES	SDres;
  FRESULT fRes;
  char path[8];
  sprintf(path, "%0.3d.TXT", MoldNo);
  fRes = f_unlink(path);
  SDres = MatchingResult(fRes);
  return SDres;
}
//SDRES CheckExistMoldFile(uint16_t MoldNo)
SDRES CheckExistMoldFile(uint16_t MoldNo, char* pAMoldName)
{
  SDRES	SDres;
  char filename[8];
  
  sprintf(filename, "%0.3d.TXT", MoldNo);
  SDres = OpenFile(&MyFile, filename, FA_OPEN_EXISTING | FA_READ);
  
  if(SDres != SD_OK) 
  {
	 CloseFile(&MyFile);
	 return SD_DO_NOT_EXIST_FILE;
  }
  else 
  {
	 //Add MoldName
	 ReadValueInFileString(&MyFile, FILE_MOLD_MOLD_NAME	, 8);
	 strcpy(pAMoldName, retValueString);
	 CloseFile(&MyFile);
	 return SD_FILE_EXIST;
  }
}
SDRES WriteMold(MOLD_MSG* pMoldMsg)
{
  SDRES	SDres;
  char path[8];
  sprintf(path, "%0.3d.TXT", pMoldMsg->MoldNo);
  
  SDres = OpenFile(&MyFile, path, FA_CREATE_ALWAYS | FA_WRITE);
  if(SDres != SD_OK) return SDres;
  
  
  f_printf(&MyFile, "[MoldNo] : %3d\n",
			  pMoldMsg->MoldNo);
  f_printf(&MyFile, "[MoldName] : %8s\n",
			  pMoldMsg->MoldName);
  
  f_printf(&MyFile, "\n[MotionMode]\n");
  
  f_printf(&MyFile, "MotionArm : %3d\n",
			  pMoldMsg->MotionMode.MotionArm);
  f_printf(&MyFile, "MainChuck : %3d\n",
			  pMoldMsg->MotionMode.MainChuck);
  f_printf(&MyFile, "MainVaccum : %3d\n",
			  pMoldMsg->MotionMode.MainVaccum);
  f_printf(&MyFile, "MainRotateChuck : %3d\n",
			  pMoldMsg->MotionMode.MainRotateChuck);
  f_printf(&MyFile, "OutsideWait : %3d\n",
			  pMoldMsg->MotionMode.OutsideWait);
  f_printf(&MyFile, "MainArmType : %3d\n",
			  pMoldMsg->MotionMode.MainArmType);
  f_printf(&MyFile, "SubArmType : %3d\n",
			  pMoldMsg->MotionMode.SubArmType);
  f_printf(&MyFile, "MainArmDownPos : %3d\n",
			  pMoldMsg->MotionMode.MainArmDownPos);
  f_printf(&MyFile, "SubArmDownPos : %3d\n",
			  pMoldMsg->MotionMode.SubArmDownPos);
  f_printf(&MyFile, "ChuckOff : %3d\n",
			  pMoldMsg->MotionMode.ChuckOff);
  f_printf(&MyFile, "VaccumOff : %3d\n",
			  pMoldMsg->MotionMode.VaccumOff);
  f_printf(&MyFile, "SubArmOff : %3d\n",
			  pMoldMsg->MotionMode.SubArmOff);
  f_printf(&MyFile, "ChuckRejectPos : %3d\n",
			  pMoldMsg->MotionMode.ChuckRejectPos);
  f_printf(&MyFile, "VaccumRejectPos : %3d\n",
			  pMoldMsg->MotionMode.VaccumRejectPos);
  f_printf(&MyFile, "MainNipper : %3d\n",
			  pMoldMsg->MotionMode.MainNipper);
  
  f_printf(&MyFile, "\n[MotionDelay]\n");
  f_printf(&MyFile, "DownDelay : %3d\n",
			  pMoldMsg->MotionDelay.DownDelay);
  f_printf(&MyFile, "KickDelay : %3d\n",
			  pMoldMsg->MotionDelay.KickDelay);
  f_printf(&MyFile, "EjectorDelay : %3d\n",
			  pMoldMsg->MotionDelay.EjectorDelay);
  f_printf(&MyFile, "ChuckDelay : %3d\n",
			  pMoldMsg->MotionDelay.ChuckDelay);
  f_printf(&MyFile, "KickReturnDelay : %3d\n",
			  pMoldMsg->MotionDelay.KickReturnDelay);
  f_printf(&MyFile, "UpDelay : %3d\n",
			  pMoldMsg->MotionDelay.UpDelay);
  f_printf(&MyFile, "SwingDelay : %3d\n",
			  pMoldMsg->MotionDelay.SwingDelay);
  f_printf(&MyFile, "Down2ndDelay : %3d\n",
			  pMoldMsg->MotionDelay.Down2ndDelay);
  f_printf(&MyFile, "OpenDelay : %3d\n",
			  pMoldMsg->MotionDelay.OpenDelay);
  f_printf(&MyFile, "Up2ndDelay : %3d\n",
			  pMoldMsg->MotionDelay.Up2ndDelay);
  f_printf(&MyFile, "ChuckRotateReturnDelay : %3d\n",
			  pMoldMsg->MotionDelay.ChuckRotateReturnDelay);
  f_printf(&MyFile, "SwingReturnDelay : %3d\n",
			  pMoldMsg->MotionDelay.SwingReturnDelay);
  f_printf(&MyFile, "NipperOnDelay : %3d\n",
			  pMoldMsg->MotionDelay.NipperOnDelay);
  f_printf(&MyFile, "ConveyorDelay : %3d\n",
			  pMoldMsg->MotionDelay.ConveyorDelay);
  
  f_printf(&MyFile, "\n[COUNT_MSG]\n");
  f_printf(&MyFile, "TotalCnt : %5d\n",
			  pMoldMsg->Count.TotalCnt);
  f_printf(&MyFile, "RejectCnt : %5d\n",
			  pMoldMsg->Count.RejectCnt);
  f_printf(&MyFile, "DetectFail : %5d\n",
			  pMoldMsg->Count.DetectFail);
  
  f_printf(&MyFile, "\n[FUNC_MSG]\n");
  f_printf(&MyFile, "Buzzer : %5d\n",
			  pMoldMsg->Function.Buzzer);
  f_printf(&MyFile, "Detection : %5d\n",
			  pMoldMsg->Function.Detection);
  f_printf(&MyFile, "Ejector : %5d\n",
			  pMoldMsg->Function.Ejector);
  f_printf(&MyFile, "Reject : %5d\n",
			  pMoldMsg->Function.Reject);
  f_printf(&MyFile, "FuncSaveCnt : %5d\n",
			  pMoldMsg->Function.FuncSaveCnt);
  
  CloseFile(&MyFile);
  
  //  printf("NEW Mold \n");
  return SD_OK;
}
SDRES ReadMold(MOLD_MSG* pMoldMsg, uint16_t MoldNo)
{
  SDRES	SDres;
  char filename[8];
  sprintf(filename, "%0.3d.TXT", MoldNo);
  
  SDres = OpenFile(&MyFile, filename, FA_OPEN_EXISTING | FA_READ);
  
  pMoldMsg->MoldNo = ReadValueInFileInt(&MyFile, FILE_MOLD_MOLD_NO, 3);
  //	pMoldMsg->MoldName = ReadValueInFileInt(&MyFile, FILE_MOLD_MOLD_NAME, 3);
  ReadValueInFileString(&MyFile, FILE_MOLD_MOLD_NAME	, 8);
  strcpy(pMoldMsg->MoldName, retValueString);
  
  pMoldMsg->MotionMode.MotionArm  = ReadValueInFileInt(&MyFile, FILE_MOLD_MOTION_ARM, 3);
  pMoldMsg->MotionMode.MainChuck  = ReadValueInFileInt(&MyFile, FILE_MOLD_MAIN_CHUCK			, 3);
  pMoldMsg->MotionMode.MainVaccum  = ReadValueInFileInt(&MyFile, FILE_MOLD_MAIN_VACCUM			, 3);
  pMoldMsg->MotionMode.MainRotateChuck  = ReadValueInFileInt(&MyFile, FILE_MOLD_MAIN_ROTATE_CHUCK, 3);
  pMoldMsg->MotionMode.OutsideWait  = ReadValueInFileInt(&MyFile, FILE_MOLD_OUTWAIT				, 3);
  pMoldMsg->MotionMode.MainArmType  = ReadValueInFileInt(&MyFile, FILE_MOLD_MAIN_TYPE			, 3);
  pMoldMsg->MotionMode.SubArmType  = ReadValueInFileInt(&MyFile, FILE_MOLD_SUB_TYPE         , 3);
  pMoldMsg->MotionMode.MainArmDownPos  = ReadValueInFileInt(&MyFile, FILE_MOLD_MAIN_DOWN_POS    , 3);
  pMoldMsg->MotionMode.SubArmDownPos  = ReadValueInFileInt(&MyFile, FILE_MOLD_SUB_DOWN_POS     , 3);
  pMoldMsg->MotionMode.ChuckOff  = ReadValueInFileInt(&MyFile, FILE_MOLD_CHUCK_OFF        , 3);
  pMoldMsg->MotionMode.VaccumOff  = ReadValueInFileInt(&MyFile, FILE_MOLD_VACCUM_OFF       , 3);
  pMoldMsg->MotionMode.SubArmOff  = ReadValueInFileInt(&MyFile, FILE_MOLD_SUB_OFF          , 3);
  pMoldMsg->MotionMode.ChuckRejectPos  = ReadValueInFileInt(&MyFile, FILE_MOLD_CHUCK_REJECT_POS , 3);
  pMoldMsg->MotionMode.VaccumRejectPos = ReadValueInFileInt(&MyFile, FILE_MOLD_VACCUM_REJECT_POS, 3);
  pMoldMsg->MotionMode.MainNipper = ReadValueInFileInt(&MyFile, FILE_MOLD_MAIN_NIPPER      , 3);
  
  pMoldMsg->MotionDelay.DownDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_DOWN					, 3);
  pMoldMsg->MotionDelay.KickDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_KICK					, 3);
  pMoldMsg->MotionDelay.EjectorDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_EJECT              , 3);
  pMoldMsg->MotionDelay.ChuckDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_CHUCK              , 3);
  pMoldMsg->MotionDelay.KickReturnDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_KICK_RETURN        , 3);
  pMoldMsg->MotionDelay.UpDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_UP                 , 3);
  pMoldMsg->MotionDelay.SwingDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_SWING              , 3);
  pMoldMsg->MotionDelay.Down2ndDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_DOWN_2ND           , 3);
  pMoldMsg->MotionDelay.OpenDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_OPEN               , 3);
  pMoldMsg->MotionDelay.Up2ndDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_UP_2ND             , 3);
  pMoldMsg->MotionDelay.ChuckRotateReturnDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_CHUCK_ROTATE_RETURN, 3);	
  pMoldMsg->MotionDelay.SwingReturnDelay	 = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_SWING_RETURN       , 3);
  pMoldMsg->MotionDelay.NipperOnDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_NIPPER             , 3);
  pMoldMsg->MotionDelay.ConveyorDelay = ReadValueInFileInt(&MyFile, FILE_MOLD_DELAY_CONVEYOR           , 3);
  
  pMoldMsg->Count.TotalCnt = ReadValueInFileInt(&MyFile, FILE_MOLD_CNT_TOTAL      , 5);
  pMoldMsg->Count.RejectCnt = ReadValueInFileInt(&MyFile, FILE_MOLD_CNT_REJECT     , 5);
  pMoldMsg->Count.DetectFail = ReadValueInFileInt(&MyFile, FILE_MOLD_CNT_DETECT_FAIL, 5);
  
  pMoldMsg->Function.Buzzer = ReadValueInFileInt(&MyFile, FILE_MOLD_BUZZER		, 3);
  pMoldMsg->Function.Detection = ReadValueInFileInt(&MyFile, FILE_MOLD_DETECTION  , 3);
  pMoldMsg->Function.Ejector = ReadValueInFileInt(&MyFile, FILE_MOLD_EJECTOR    , 3);
  pMoldMsg->Function.Reject = ReadValueInFileInt(&MyFile, FILE_MOLD_REJECT     , 3);
  pMoldMsg->Function.FuncSaveCnt = ReadValueInFileInt(&MyFile, FILE_MOLD_FUNCSAVECNT, 3);
  
  
  CloseFile(&MyFile);
  if(SDres != SD_OK) return SDres;
  
  return SD_OK;
}

SDRES WriteRobotCfg(ROBOT_CONFIG* pRobotCfg)
{
  SDRES	SDres;
  SDres = OpenFile(&MyFile, FILE_NAME_CONFIG, FA_CREATE_ALWAYS | FA_WRITE | FA_READ);
  if(SDres != SD_OK) return SDres;
  
  f_printf(&MyFile, "[ROBOT TYPE] : %3d\n\n",
			  pRobotCfg->RobotType);
  
  f_printf(&MyFile, "\n[MotionMode]\n");
  
  f_printf(&MyFile, "MotionArm : %3d\n",
			  pRobotCfg->MotionMode.MotionArm);
  f_printf(&MyFile, "MainChuck : %3d\n",
			  pRobotCfg->MotionMode.MainChuck);
  f_printf(&MyFile, "MainVaccum : %3d\n",
			  pRobotCfg->MotionMode.MainVaccum);
  f_printf(&MyFile, "MainRotateChuck : %3d\n",
			  pRobotCfg->MotionMode.MainRotateChuck);
  f_printf(&MyFile, "OutsideWait : %3d\n",
			  pRobotCfg->MotionMode.OutsideWait);
  f_printf(&MyFile, "MainArmType : %3d\n",
			  pRobotCfg->MotionMode.MainArmType);
  f_printf(&MyFile, "SubArmType : %3d\n",
			  pRobotCfg->MotionMode.SubArmType);
  f_printf(&MyFile, "MainArmDownPos : %3d\n",
			  pRobotCfg->MotionMode.MainArmDownPos);
  f_printf(&MyFile, "SubArmDownPos : %3d\n",
			  pRobotCfg->MotionMode.SubArmDownPos);
  f_printf(&MyFile, "ChuckOff : %3d\n",
			  pRobotCfg->MotionMode.ChuckOff);
  f_printf(&MyFile, "VaccumOff : %3d\n",
			  pRobotCfg->MotionMode.VaccumOff);
  f_printf(&MyFile, "SubArmOff : %3d\n",
			  pRobotCfg->MotionMode.SubArmOff);
  f_printf(&MyFile, "ChuckRejectPos : %3d\n",
			  pRobotCfg->MotionMode.ChuckRejectPos);
  f_printf(&MyFile, "VaccumRejectPos : %3d\n",
			  pRobotCfg->MotionMode.VaccumRejectPos);
  f_printf(&MyFile, "MainNipper : %3d\n",
			  pRobotCfg->MotionMode.MainNipper);
  
  f_printf(&MyFile, "\n[MotionDelay]\n");
  f_printf(&MyFile, "DownDelay : %3d\n",
			  pRobotCfg->MotionDelay.DownDelay);
  f_printf(&MyFile, "KickDelay : %3d\n",
			  pRobotCfg->MotionDelay.KickDelay);
  f_printf(&MyFile, "EjectorDelay : %3d\n",
			  pRobotCfg->MotionDelay.EjectorDelay);
  f_printf(&MyFile, "ChuckDelay : %3d\n",
			  pRobotCfg->MotionDelay.ChuckDelay);
  f_printf(&MyFile, "KickReturnDelay : %3d\n",
			  pRobotCfg->MotionDelay.KickReturnDelay);
  f_printf(&MyFile, "UpDelay : %3d\n",
			  pRobotCfg->MotionDelay.UpDelay);
  f_printf(&MyFile, "SwingDelay : %3d\n",
			  pRobotCfg->MotionDelay.SwingDelay);
  f_printf(&MyFile, "Down2ndDelay : %3d\n",
			  pRobotCfg->MotionDelay.Down2ndDelay);
  f_printf(&MyFile, "OpenDelay : %3d\n",
			  pRobotCfg->MotionDelay.OpenDelay);
  f_printf(&MyFile, "Up2ndDelay : %3d\n",
			  pRobotCfg->MotionDelay.Up2ndDelay);
  f_printf(&MyFile, "ChuckRotateReturnDelay : %3d\n",
			  pRobotCfg->MotionDelay.ChuckRotateReturnDelay);
  f_printf(&MyFile, "SwingReturnDelay : %3d\n",
			  pRobotCfg->MotionDelay.SwingReturnDelay);
  f_printf(&MyFile, "NipperOnDelay : %3d\n",
			  pRobotCfg->MotionDelay.NipperOnDelay);
  f_printf(&MyFile, "ConveyorDelay : %3d\n",
			  pRobotCfg->MotionDelay.ConveyorDelay);
  
  f_printf(&MyFile, "\n[COUNT_MSG]\n");
  f_printf(&MyFile, "TotalCnt : %5d\n",
			  pRobotCfg->Count.TotalCnt);
  f_printf(&MyFile, "RejectCnt : %5d\n",
			  pRobotCfg->Count.RejectCnt);
  f_printf(&MyFile, "DetectFail : %5d\n",
			  pRobotCfg->Count.DetectFail);
  
  f_printf(&MyFile, "\n[MANUFACTURER_SETTING]\n");
  f_printf(&MyFile, "ErrorTime : %3d\n",
			  pRobotCfg->Setting.ErrorTime);
  f_printf(&MyFile, "ItlFullAuto : %3d\n",
			  pRobotCfg->Setting.ItlFullAuto);
  f_printf(&MyFile, "ItlSaftyDoor : %3d\n",
			  pRobotCfg->Setting.ItlSaftyDoor);
  f_printf(&MyFile, "ItlAutoInjection : %3d\n",
			  pRobotCfg->Setting.ItlAutoInjection);
  f_printf(&MyFile, "ItlReject : %3d\n",
			  pRobotCfg->Setting.ItlReject);
  f_printf(&MyFile, "ProcessTime : %3d\n",
			  pRobotCfg->Setting.ProcessTime);
  f_printf(&MyFile, "DelMoldData : %3d\n",
			  pRobotCfg->Setting.DelMoldData);
  f_printf(&MyFile, "DelErrHistory : %3d\n",
			  pRobotCfg->Setting.DelErrHistory);
  f_printf(&MyFile, "DoorSignalChange : %3d\n",
			  pRobotCfg->Setting.DoorSignalChange);
  

  f_printf(&MyFile, "\n[FUNC_MSG]\n");
  f_printf(&MyFile, "Buzzer : %3d\n",
			  pRobotCfg->Function.Buzzer);
  f_printf(&MyFile, "Detection : %3d\n",
			  pRobotCfg->Function.Detection);
  f_printf(&MyFile, "Ejector : %3d\n",
			  pRobotCfg->Function.Ejector);
  f_printf(&MyFile, "Reject : %3d\n",
			  pRobotCfg->Function.Reject);
  f_printf(&MyFile, "FuncSaveCnt : %3d\n",
			  pRobotCfg->Function.FuncSaveCnt);
  
  f_printf(&MyFile, "\n[MoldNo] : %3d\n",
			  pRobotCfg->MoldNo);
  
  f_printf(&MyFile, "[MoldName] : %8s\n",
			  pRobotCfg->MoldName);
  
  f_printf(&MyFile, "[ErrorCnt] : %3d\n",
			  pRobotCfg->ErrorCnt);
  f_printf(&MyFile, "[Language] : %3d\n",
			  pRobotCfg->Language);
  
    //IMMType
  f_printf(&MyFile, "[IMMType] : %3d\n",
			  pRobotCfg->Setting.IMMType);
  f_printf(&MyFile, "[RotationState] : %3d\n",
			  pRobotCfg->Setting.RotationState);
  
  CloseFile(&MyFile);
  return SD_OK;
}

SDRES WriteRobotCfgBuakUp(ROBOT_CONFIG* pRobotCfg)
{
  SDRES	SDres;
  SDres = OpenFile(&MyFile, FILE_NAME_CONFIG_BACKUP, FA_CREATE_ALWAYS | FA_WRITE | FA_READ);
  if(SDres != SD_OK) return SDres;
  
  f_printf(&MyFile, "[ROBOT TYPE] : %3d\n\n",
			  pRobotCfg->RobotType);
  
  f_printf(&MyFile, "\n[MotionMode]\n");
  
  f_printf(&MyFile, "MotionArm : %3d\n",
			  pRobotCfg->MotionMode.MotionArm);
  f_printf(&MyFile, "MainChuck : %3d\n",
			  pRobotCfg->MotionMode.MainChuck);
  f_printf(&MyFile, "MainVaccum : %3d\n",
			  pRobotCfg->MotionMode.MainVaccum);
  f_printf(&MyFile, "MainRotateChuck : %3d\n",
			  pRobotCfg->MotionMode.MainRotateChuck);
  f_printf(&MyFile, "OutsideWait : %3d\n",
			  pRobotCfg->MotionMode.OutsideWait);
  f_printf(&MyFile, "MainArmType : %3d\n",
			  pRobotCfg->MotionMode.MainArmType);
  f_printf(&MyFile, "SubArmType : %3d\n",
			  pRobotCfg->MotionMode.SubArmType);
  f_printf(&MyFile, "MainArmDownPos : %3d\n",
			  pRobotCfg->MotionMode.MainArmDownPos);
  f_printf(&MyFile, "SubArmDownPos : %3d\n",
			  pRobotCfg->MotionMode.SubArmDownPos);
  f_printf(&MyFile, "ChuckOff : %3d\n",
			  pRobotCfg->MotionMode.ChuckOff);
  f_printf(&MyFile, "VaccumOff : %3d\n",
			  pRobotCfg->MotionMode.VaccumOff);
  f_printf(&MyFile, "SubArmOff : %3d\n",
			  pRobotCfg->MotionMode.SubArmOff);
  f_printf(&MyFile, "ChuckRejectPos : %3d\n",
			  pRobotCfg->MotionMode.ChuckRejectPos);
  f_printf(&MyFile, "VaccumRejectPos : %3d\n",
			  pRobotCfg->MotionMode.VaccumRejectPos);
  f_printf(&MyFile, "MainNipper : %3d\n",
			  pRobotCfg->MotionMode.MainNipper);
  
  f_printf(&MyFile, "\n[MotionDelay]\n");
  f_printf(&MyFile, "DownDelay : %3d\n",
			  pRobotCfg->MotionDelay.DownDelay);
  f_printf(&MyFile, "KickDelay : %3d\n",
			  pRobotCfg->MotionDelay.KickDelay);
  f_printf(&MyFile, "EjectorDelay : %3d\n",
			  pRobotCfg->MotionDelay.EjectorDelay);
  f_printf(&MyFile, "ChuckDelay : %3d\n",
			  pRobotCfg->MotionDelay.ChuckDelay);
  f_printf(&MyFile, "KickReturnDelay : %3d\n",
			  pRobotCfg->MotionDelay.KickReturnDelay);
  f_printf(&MyFile, "UpDelay : %3d\n",
			  pRobotCfg->MotionDelay.UpDelay);
  f_printf(&MyFile, "SwingDelay : %3d\n",
			  pRobotCfg->MotionDelay.SwingDelay);
  f_printf(&MyFile, "Down2ndDelay : %3d\n",
			  pRobotCfg->MotionDelay.Down2ndDelay);
  f_printf(&MyFile, "OpenDelay : %3d\n",
			  pRobotCfg->MotionDelay.OpenDelay);
  f_printf(&MyFile, "Up2ndDelay : %3d\n",
			  pRobotCfg->MotionDelay.Up2ndDelay);
  f_printf(&MyFile, "ChuckRotateReturnDelay : %3d\n",
			  pRobotCfg->MotionDelay.ChuckRotateReturnDelay);
  f_printf(&MyFile, "SwingReturnDelay : %3d\n",
			  pRobotCfg->MotionDelay.SwingReturnDelay);
  f_printf(&MyFile, "NipperOnDelay : %3d\n",
			  pRobotCfg->MotionDelay.NipperOnDelay);
  f_printf(&MyFile, "ConveyorDelay : %3d\n",
			  pRobotCfg->MotionDelay.ConveyorDelay);
  
  f_printf(&MyFile, "\n[COUNT_MSG]\n");
  f_printf(&MyFile, "TotalCnt : %5d\n",
			  pRobotCfg->Count.TotalCnt);
  f_printf(&MyFile, "RejectCnt : %5d\n",
			  pRobotCfg->Count.RejectCnt);
  f_printf(&MyFile, "DetectFail : %5d\n",
			  pRobotCfg->Count.DetectFail);
  
  f_printf(&MyFile, "\n[MANUFACTURER_SETTING]\n");
  f_printf(&MyFile, "ErrorTime : %3d\n",
			  pRobotCfg->Setting.ErrorTime);
  f_printf(&MyFile, "ItlFullAuto : %3d\n",
			  pRobotCfg->Setting.ItlFullAuto);
  f_printf(&MyFile, "ItlSaftyDoor : %3d\n",
			  pRobotCfg->Setting.ItlSaftyDoor);
  f_printf(&MyFile, "ItlAutoInjection : %3d\n",
			  pRobotCfg->Setting.ItlAutoInjection);
  f_printf(&MyFile, "ItlReject : %3d\n",
			  pRobotCfg->Setting.ItlReject);
  f_printf(&MyFile, "ProcessTime : %3d\n",
			  pRobotCfg->Setting.ProcessTime);
  f_printf(&MyFile, "DelMoldData : %3d\n",
			  pRobotCfg->Setting.DelMoldData);
  f_printf(&MyFile, "DelErrHistory : %3d\n",
			  pRobotCfg->Setting.DelErrHistory);
  f_printf(&MyFile, "DoorSignalChange : %3d\n",
			  pRobotCfg->Setting.DoorSignalChange);
  

  f_printf(&MyFile, "\n[FUNC_MSG]\n");
  f_printf(&MyFile, "Buzzer : %3d\n",
			  pRobotCfg->Function.Buzzer);
  f_printf(&MyFile, "Detection : %3d\n",
			  pRobotCfg->Function.Detection);
  f_printf(&MyFile, "Ejector : %3d\n",
			  pRobotCfg->Function.Ejector);
  f_printf(&MyFile, "Reject : %3d\n",
			  pRobotCfg->Function.Reject);
  f_printf(&MyFile, "FuncSaveCnt : %3d\n",
			  pRobotCfg->Function.FuncSaveCnt);
  
  f_printf(&MyFile, "\n[MoldNo] : %3d\n",
			  pRobotCfg->MoldNo);
  
  f_printf(&MyFile, "[MoldName] : %8s\n",
			  pRobotCfg->MoldName);
  
  f_printf(&MyFile, "[ErrorCnt] : %3d\n",
			  pRobotCfg->ErrorCnt);
  f_printf(&MyFile, "[Language] : %3d\n",
			  pRobotCfg->Language);
  
    //IMMType
  f_printf(&MyFile, "[IMMType] : %3d\n",
			  pRobotCfg->Setting.IMMType);
  f_printf(&MyFile, "[RotationState] : %3d\n",
			  pRobotCfg->Setting.RotationState);
  
  CloseFile(&MyFile);
  return SD_OK;
}


SDRES ReadRobotCfgBackUp(ROBOT_CONFIG* pRobotCfg)
{
  SDRES	SDres;
  
  SDres = OpenFile(&MyFile, FILE_NAME_CONFIG_BACKUP, FA_OPEN_EXISTING | FA_WRITE | FA_READ);
  //  printf("Read Cfg Open SDres : %x\n\r",SDres);
  if(SDres != SD_OK) return SDres;
  
  
  pRobotCfg->RobotType = ReadValueInFileInt(&MyFile, FILE_SEEK_ROBOT_TYPE, 3);
  
  pRobotCfg->MotionMode.MotionArm  = ReadValueInFileInt(&MyFile, FILE_SEEK_MOTION_ARM, 3);
  pRobotCfg->MotionMode.MainChuck  = ReadValueInFileInt(&MyFile, FILE_SEEK_MAIN_CHUCK			, 3);
  pRobotCfg->MotionMode.MainVaccum  = ReadValueInFileInt(&MyFile, FILE_SEEK_MAIN_VACCUM			, 3);
  pRobotCfg->MotionMode.MainRotateChuck  = ReadValueInFileInt(&MyFile, FILE_SEEK_MAIN_ROTATE_CHUCK, 3);
  pRobotCfg->MotionMode.OutsideWait  = ReadValueInFileInt(&MyFile, FILE_SEEK_OUTWAIT				, 3);
  pRobotCfg->MotionMode.MainArmType  = ReadValueInFileInt(&MyFile, FILE_SEEK_MAIN_TYPE			, 3);
  pRobotCfg->MotionMode.SubArmType  = ReadValueInFileInt(&MyFile, FILE_SEEK_SUB_TYPE         , 3);
  pRobotCfg->MotionMode.MainArmDownPos  = ReadValueInFileInt(&MyFile, FILE_SEEK_MAIN_DOWN_POS    , 3);
  pRobotCfg->MotionMode.SubArmDownPos  = ReadValueInFileInt(&MyFile, FILE_SEEK_SUB_DOWN_POS     , 3);
  pRobotCfg->MotionMode.ChuckOff  = ReadValueInFileInt(&MyFile, FILE_SEEK_CHUCK_OFF        , 3);
  pRobotCfg->MotionMode.VaccumOff  = ReadValueInFileInt(&MyFile, FILE_SEEK_VACCUM_OFF       , 3);
  pRobotCfg->MotionMode.SubArmOff  = ReadValueInFileInt(&MyFile, FILE_SEEK_SUB_OFF          , 3);
  pRobotCfg->MotionMode.ChuckRejectPos  = ReadValueInFileInt(&MyFile, FILE_SEEK_CHUCK_REJECT_POS , 3);
  pRobotCfg->MotionMode.VaccumRejectPos = ReadValueInFileInt(&MyFile, FILE_SEEK_VACCUM_REJECT_POS, 3);
  pRobotCfg->MotionMode.MainNipper = ReadValueInFileInt(&MyFile, FILE_SEEK_MAIN_NIPPER      , 3);
  
  pRobotCfg->MotionDelay.DownDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_DOWN					, 3);
  pRobotCfg->MotionDelay.KickDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_KICK					, 3);
  pRobotCfg->MotionDelay.EjectorDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_EJECT              , 3);
  pRobotCfg->MotionDelay.ChuckDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_CHUCK              , 3);
  pRobotCfg->MotionDelay.KickReturnDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_KICK_RETURN        , 3);
  pRobotCfg->MotionDelay.UpDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_UP                 , 3);
  pRobotCfg->MotionDelay.SwingDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_SWING              , 3);
  pRobotCfg->MotionDelay.Down2ndDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_DOWN_2ND           , 3);
  pRobotCfg->MotionDelay.OpenDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_OPEN               , 3);
  pRobotCfg->MotionDelay.Up2ndDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_UP_2ND             , 3);
  pRobotCfg->MotionDelay.ChuckRotateReturnDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_CHUCK_ROTATE_RETURN, 3);	
  pRobotCfg->MotionDelay.SwingReturnDelay	 = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_SWING_RETURN       , 3);
  pRobotCfg->MotionDelay.NipperOnDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_NIPPER             , 3);
  pRobotCfg->MotionDelay.ConveyorDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_CONVEYOR           , 3);
  
  pRobotCfg->Count.TotalCnt = ReadValueInFileInt(&MyFile, FILE_SEEK_CNT_TOTAL      , 5);
  pRobotCfg->Count.RejectCnt = ReadValueInFileInt(&MyFile, FILE_SEEK_CNT_REJECT     , 5);
  pRobotCfg->Count.DetectFail = ReadValueInFileInt(&MyFile, FILE_SEEK_CNT_DETECT_FAIL, 5);
  
  
  pRobotCfg->Setting.ErrorTime = ReadValueInFileInt(&MyFile, FILE_SEEK_SET_ERROR_TIME		, 3);
  pRobotCfg->Setting.ItlFullAuto = ReadValueInFileInt(&MyFile, FILE_SEEK_ITL_FULLAUTO		, 3);
  pRobotCfg->Setting.ItlSaftyDoor = ReadValueInFileInt(&MyFile, FILE_SEEK_ITL_SAFTYDOOR    , 3);
  pRobotCfg->Setting.ItlAutoInjection = ReadValueInFileInt(&MyFile, FILE_SEEK_ITL_AUTOINJECTION, 3);
  pRobotCfg->Setting.ItlReject = ReadValueInFileInt(&MyFile, FILE_SEEK_ITL_REJECT       , 3);
  pRobotCfg->Setting.ProcessTime = ReadValueInFileInt(&MyFile, FILE_SEEK_PROCESS_TIME     , 3);
  pRobotCfg->Setting.DelMoldData = ReadValueInFileInt(&MyFile, FILE_SEEK_DEL_MOLD_DATA    , 3);
  pRobotCfg->Setting.DelErrHistory = ReadValueInFileInt(&MyFile, FILE_SEEK_DEL_ERR          , 3);
  pRobotCfg->Setting.DoorSignalChange = ReadValueInFileInt(&MyFile, FILE_SEEK_DOOR_SIG_CHANGE  , 3);
 
  pRobotCfg->Function.Buzzer = ReadValueInFileInt(&MyFile, FILE_SEEK_BUZZER		, 3);
  pRobotCfg->Function.Detection = ReadValueInFileInt(&MyFile, FILE_SEEK_DETECTION  , 3);
  pRobotCfg->Function.Ejector = ReadValueInFileInt(&MyFile, FILE_SEEK_EJECTOR    , 3);
  pRobotCfg->Function.Reject = ReadValueInFileInt(&MyFile, FILE_SEEK_REJECT     , 3);
  pRobotCfg->Function.FuncSaveCnt = ReadValueInFileInt(&MyFile, FILE_SEEK_FUNCSAVECNT, 3);
  
  pRobotCfg->MoldNo = ReadValueInFileInt(&MyFile, FILE_SEEK_MOLD_NO	, 3);
  
  ReadValueInFileString(&MyFile, FILE_SEEK_MOLD_NAME	, 8);
  strcpy(pRobotCfg->MoldName, retValueString);
  
  
  pRobotCfg->ErrorCnt = ReadValueInFileInt(&MyFile, FILE_SEEK_ERROR_CNT	, 3);
  pRobotCfg->Language = ReadValueInFileInt(&MyFile, FILE_SEEK_LANGUAGE		, 3);
  
    //IMMType
  pRobotCfg->Setting.IMMType = ReadValueInFileInt(&MyFile, FILE_SEEK_IMM_TYPE  , 3);
  pRobotCfg->Setting.RotationState = ReadValueInFileInt(&MyFile, FILE_SEEK_ROTATION_STATE  , 3);
  
  CloseFile(&MyFile);
  
  return SD_OK;	
}




SDRES ReadRobotCfg(ROBOT_CONFIG* pRobotCfg)
{
  SDRES	SDres;
  
  SDres = OpenFile(&MyFile, FILE_NAME_CONFIG, FA_OPEN_EXISTING | FA_WRITE | FA_READ);
  //  printf("Read Cfg Open SDres : %x\n\r",SDres);
  if(SDres != SD_OK) return SDres;
  
  
  pRobotCfg->RobotType = ReadValueInFileInt(&MyFile, FILE_SEEK_ROBOT_TYPE, 3);
  
  pRobotCfg->MotionMode.MotionArm  = ReadValueInFileInt(&MyFile, FILE_SEEK_MOTION_ARM, 3);
  pRobotCfg->MotionMode.MainChuck  = ReadValueInFileInt(&MyFile, FILE_SEEK_MAIN_CHUCK			, 3);
  pRobotCfg->MotionMode.MainVaccum  = ReadValueInFileInt(&MyFile, FILE_SEEK_MAIN_VACCUM			, 3);
  pRobotCfg->MotionMode.MainRotateChuck  = ReadValueInFileInt(&MyFile, FILE_SEEK_MAIN_ROTATE_CHUCK, 3);
  pRobotCfg->MotionMode.OutsideWait  = ReadValueInFileInt(&MyFile, FILE_SEEK_OUTWAIT				, 3);
  pRobotCfg->MotionMode.MainArmType  = ReadValueInFileInt(&MyFile, FILE_SEEK_MAIN_TYPE			, 3);
  pRobotCfg->MotionMode.SubArmType  = ReadValueInFileInt(&MyFile, FILE_SEEK_SUB_TYPE         , 3);
  pRobotCfg->MotionMode.MainArmDownPos  = ReadValueInFileInt(&MyFile, FILE_SEEK_MAIN_DOWN_POS    , 3);
  pRobotCfg->MotionMode.SubArmDownPos  = ReadValueInFileInt(&MyFile, FILE_SEEK_SUB_DOWN_POS     , 3);
  pRobotCfg->MotionMode.ChuckOff  = ReadValueInFileInt(&MyFile, FILE_SEEK_CHUCK_OFF        , 3);
  pRobotCfg->MotionMode.VaccumOff  = ReadValueInFileInt(&MyFile, FILE_SEEK_VACCUM_OFF       , 3);
  pRobotCfg->MotionMode.SubArmOff  = ReadValueInFileInt(&MyFile, FILE_SEEK_SUB_OFF          , 3);
  pRobotCfg->MotionMode.ChuckRejectPos  = ReadValueInFileInt(&MyFile, FILE_SEEK_CHUCK_REJECT_POS , 3);
  pRobotCfg->MotionMode.VaccumRejectPos = ReadValueInFileInt(&MyFile, FILE_SEEK_VACCUM_REJECT_POS, 3);
  pRobotCfg->MotionMode.MainNipper = ReadValueInFileInt(&MyFile, FILE_SEEK_MAIN_NIPPER      , 3);
  
  pRobotCfg->MotionDelay.DownDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_DOWN					, 3);
  pRobotCfg->MotionDelay.KickDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_KICK					, 3);
  pRobotCfg->MotionDelay.EjectorDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_EJECT              , 3);
  pRobotCfg->MotionDelay.ChuckDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_CHUCK              , 3);
  pRobotCfg->MotionDelay.KickReturnDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_KICK_RETURN        , 3);
  pRobotCfg->MotionDelay.UpDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_UP                 , 3);
  pRobotCfg->MotionDelay.SwingDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_SWING              , 3);
  pRobotCfg->MotionDelay.Down2ndDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_DOWN_2ND           , 3);
  pRobotCfg->MotionDelay.OpenDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_OPEN               , 3);
  pRobotCfg->MotionDelay.Up2ndDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_UP_2ND             , 3);
  pRobotCfg->MotionDelay.ChuckRotateReturnDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_CHUCK_ROTATE_RETURN, 3);	
  pRobotCfg->MotionDelay.SwingReturnDelay	 = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_SWING_RETURN       , 3);
  pRobotCfg->MotionDelay.NipperOnDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_NIPPER             , 3);
  pRobotCfg->MotionDelay.ConveyorDelay = ReadValueInFileInt(&MyFile, FILE_SEEK_DELAY_CONVEYOR           , 3);
  
  pRobotCfg->Count.TotalCnt = ReadValueInFileInt(&MyFile, FILE_SEEK_CNT_TOTAL      , 5);
  pRobotCfg->Count.RejectCnt = ReadValueInFileInt(&MyFile, FILE_SEEK_CNT_REJECT     , 5);
  pRobotCfg->Count.DetectFail = ReadValueInFileInt(&MyFile, FILE_SEEK_CNT_DETECT_FAIL, 5);
  
  
  pRobotCfg->Setting.ErrorTime = ReadValueInFileInt(&MyFile, FILE_SEEK_SET_ERROR_TIME		, 3);
  pRobotCfg->Setting.ItlFullAuto = ReadValueInFileInt(&MyFile, FILE_SEEK_ITL_FULLAUTO		, 3);
  pRobotCfg->Setting.ItlSaftyDoor = ReadValueInFileInt(&MyFile, FILE_SEEK_ITL_SAFTYDOOR    , 3);
  pRobotCfg->Setting.ItlAutoInjection = ReadValueInFileInt(&MyFile, FILE_SEEK_ITL_AUTOINJECTION, 3);
  pRobotCfg->Setting.ItlReject = ReadValueInFileInt(&MyFile, FILE_SEEK_ITL_REJECT       , 3);
  pRobotCfg->Setting.ProcessTime = ReadValueInFileInt(&MyFile, FILE_SEEK_PROCESS_TIME     , 3);
  pRobotCfg->Setting.DelMoldData = ReadValueInFileInt(&MyFile, FILE_SEEK_DEL_MOLD_DATA    , 3);
  pRobotCfg->Setting.DelErrHistory = ReadValueInFileInt(&MyFile, FILE_SEEK_DEL_ERR          , 3);
  pRobotCfg->Setting.DoorSignalChange = ReadValueInFileInt(&MyFile, FILE_SEEK_DOOR_SIG_CHANGE  , 3);
 
  pRobotCfg->Function.Buzzer = ReadValueInFileInt(&MyFile, FILE_SEEK_BUZZER		, 3);
  pRobotCfg->Function.Detection = ReadValueInFileInt(&MyFile, FILE_SEEK_DETECTION  , 3);
  pRobotCfg->Function.Ejector = ReadValueInFileInt(&MyFile, FILE_SEEK_EJECTOR    , 3);
  pRobotCfg->Function.Reject = ReadValueInFileInt(&MyFile, FILE_SEEK_REJECT     , 3);
  pRobotCfg->Function.FuncSaveCnt = ReadValueInFileInt(&MyFile, FILE_SEEK_FUNCSAVECNT, 3);
  
  pRobotCfg->MoldNo = ReadValueInFileInt(&MyFile, FILE_SEEK_MOLD_NO	, 3);
  
  ReadValueInFileString(&MyFile, FILE_SEEK_MOLD_NAME	, 8);
  strcpy(pRobotCfg->MoldName, retValueString);
  
  
  pRobotCfg->ErrorCnt = ReadValueInFileInt(&MyFile, FILE_SEEK_ERROR_CNT	, 3);
  pRobotCfg->Language = ReadValueInFileInt(&MyFile, FILE_SEEK_LANGUAGE		, 3);
  
    //IMMType
  pRobotCfg->Setting.IMMType = ReadValueInFileInt(&MyFile, FILE_SEEK_IMM_TYPE  , 3);
  pRobotCfg->Setting.RotationState = ReadValueInFileInt(&MyFile, FILE_SEEK_ROTATION_STATE  , 3);
  
  CloseFile(&MyFile);
  
  return SD_OK;	
}




STRUCT_TIME GetTime(void)
{
  STRUCT_TIME sTime;
  HAL_RTC_GetTime(&hrtc, &(sTime.Time), FORMAT_BIN);
  HAL_RTC_GetDate(&hrtc, &(sTime.Date), FORMAT_BIN);
  
  return sTime;
}
void SetTime(STRUCT_TIME* pTime)
{
  HAL_RTC_SetTime(&hrtc, &(pTime->Time), FORMAT_BIN);
  HAL_RTC_SetDate(&hrtc, &(pTime->Date), FORMAT_BIN);
}
uint8_t ByteToAscii(uint8_t data)
{
  if(data < 10) return (data + 0x30);
  else return(data - 10 + 0x41);
}
SDRES ChkSD(void)
{
  if(!HAL_GPIO_ReadPin(SD_DETECT_GPIO_Port, SD_DETECT_Pin)) return SD_SLOT_EMPTY;
  if(!HAL_GPIO_ReadPin(SD_WRITE_PROTECTION_GPIO_Port, SD_WRITE_PROTECTION_Pin)) return SD_SLOT_WR_PROTECT;
  
  return SD_OK;
}
