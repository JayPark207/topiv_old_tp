/* 
 * File:   AppMain.h
 * Author: Jay Park
 *
 * Created on 2018년 6월 14일 (), PM 3:33
 */

#ifndef __APPMAIN_H
#define	__APPMAIN_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "main.h"   
#include "interface.h"   

#define SWING_IO_OFF
	
#define TYPE_A		0x00
#define TYPE_X		0x01
#define TYPE_XC	0x02
#define TYPE_TWIN	0x03
  
#define MANUAL_PAGE_SD 0x00
#define MANUAL_MODE_SD 0x01
#define MANUAL_COUNTER_SD 0x02
#define MANUAL_TIMER_SD 0x03
#define MANUAL_HOMING_SD 0x04
#define MANUAL_NEWMOLD_SD 0x05
#define MANUAL_ERROR_SD 0x06
  
#define MANUAL_OPENMOLD_SD 0x06
  
#define AUTO_PAGE_SD 0x11
#define AUTO_COUNTER_PAGE_SD 0x12
#define AUTO_TIMER_PAGE_SD 0x13
  
#define WAITING_SD 0xFF
	
#define TIME_BOOTING								   1000
#define TIME_LOAD_DELAY								300
#define TIME_MOLD_OPEN_ERROR						3000
#define TIME_SC_BOOTING								200
#define TIME_SC_DELAY								30
#define TIME_RTC       								1000
#define TIME_CYCLE      							100
  
#define HOMING_START									0
#define HOMING_DONE									1
  
#define BOOTING_ERROR_COUNT					   3
	
#define KEY_IS_NUM									1
#define KEY_IS_NOT_NUM								0
								
#define DECIMAL_PLACE								100

#define MAX_MOLDNAME_CHAR_POS						38
#define MIN_MOLDNAME_CHAR_POS						0
#define MAX_MOLDNO_LENGTH							999
#define MAX_MOLDNAME_POS							8
#define MIN_MOLDNAME_POS							0
  		
#define MOLDNO_BLINK									0
#define NO_BLINK										1
#define MOLENAME_UNDERBAR_BLINK					2

#define DOUBLECLICK									2
  
#define PAGE_NONE										0
#define PAGE_MANUAL									1
#define PAGE_STEP										2
#define PAGE_CYCLE									3
  
#define TOTAL_COUNT_INDEX							19
#define MAX_INDEX										20
  
#define MODE_AUTO_OP									0
#define MODE_CYCLE_OP								1
  
#define ERROR_CHECK									0
#define ERROR_WRITE									1
#define ERROR_CLEAR									2
 
/////////////////////// Timer page ///////////////////////
#define DOWN_TIME										0
#define KICK_TIME										1
#define EJECTOR_TIME									2
#define CHUCK_TIME									3
#define KICK_RETURN_TIME							4
#define UP_TIME										5
#define SWING_TIME									6
#define SECOND_DOWN_TIME							7
#define RELEASE_TIME									8
#define SECOND_UP_TIME								9
#define CHUCK_ROTATION_TIME						10
#define SWING_RETURN_TIME							11
#define NIPPER_TIME									12
#define CONVEYOR_TIME								13
/////////////////////////////////////////////////////////
  
/////////////////////// Mode page ///////////////////////
#define ARMSET_MODE									0
#define CHUCK_MODE									1
#define VACUUM_MODE									2
#define CHUCK_ROTATION_MODE						3
#define OUTSIDE_MODE									4
#define MAIN_TAKEOUT_MODE							5
#define SUB_TAKEOUT_MODE							6
#define MAIN_DOWN_MODE								7
#define SUB_DOWN_MODE								8
#define MAIN_CHUCK_RELEASE_MODE					9
#define VACUUM_RELEASE_MODE						10
#define SUB_CHUCK_RELEASE_MODE					11
#define MAIN_CHUCK_REJECT_MODE					12
#define MAIN_VAUUM_REJECT_MODE					13
/////////////////////////////////////////////////////////
  
/////////////////////// Step page ///////////////////////
#define DOWN_STEP										0
#define KICK_STEP										1
#define EJECTOR_STEP									2
#define CHUCK_STEP									3
#define KICK_RETURN_STEP							4
#define UP_STEP										5
#define SWING_STEP									6
#define CHUCK_ROTATION_STEP						7
#define SECOND_DOWN_STEP							8
#define NIPPERCUT_STEP								9
#define CHOUCK_RELEASE_STEP						10
#define SECOND_UP_STEP								11
#define CHUCK_ROTATION_RETURN_STEP				12
#define SWING_RETURN_STEP							13
//////////////////////////////////////////////////////////
  
////////////////////// RTC_Underbar /////////////////////
#define YEAR_POS										1
#define MONTH_POS 									2
#define DATE_POS  									3
#define HOUR_POS  									4
#define MINUTE_POS  									5
#define SECOND_POS  									6
/////////////////////////////////////////////////////////
  
/////////////////////// Mold page ///////////////////////
#define NEW_MOLD										0
#define POSSIBLE_MOLD_NO							100
#define MIN_FIX_MOLD_NO								1
#define MAX_FIX_MOLD_NO								99
////////////////////////////////////////////////////////
  
void LoopAppMain(void);
void ClearPage(void);
void LoopAppError(void);

#ifdef	__cplusplus
}
#endif

#endif	/* __APPMAIN_H */

