#include "AppKey.h"

extern CONTROL_MSG ControlMsg;
extern ROBOT_CONFIG RobotCfg;

//void LoopAppEMO(void)
//{
//  static uint64_t TempTick = 0;
//
//  static enum
//  {
//	 STATE_CHECK = 0,
//	 STATE_ON,
//	 STATE_OFF,
//	 
//  }EMOLoopState = STATE_CHECK;
//  
//  switch(EMOLoopState)
//  {
//  case STATE_CHECK : 
//	 if(HAL_GPIO_ReadPin(EMG_GPIO_Port,EMG_Pin) == GPIO_PIN_RESET)
//	 {
//		SetSysTick(&TempTick,TIME_EMO_BUZZER);	 
//		EMOLoopState = STATE_ON;
//	 }
//	 break;
//	 
//  case STATE_ON : 
//	 HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
//	 ControlMsg.TpMode = OCCUR_ERROR;
//	 
//	 if(ChkExpireSysTick(&TempTick))
//	 {
//		SetSysTick(&TempTick,TIME_EMO_BUZZER);	 
//		EMOLoopState = STATE_OFF;
//	 } 
//	 if(HAL_GPIO_ReadPin(EMG_GPIO_Port,EMG_Pin) == GPIO_PIN_SET)
//	 {
//		HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);
//		EMOLoopState = STATE_CHECK;
//	 }
//	 break;
//	 
//  case STATE_OFF :
//	 if(HAL_GPIO_ReadPin(EMG_GPIO_Port,EMG_Pin) == GPIO_PIN_RESET)
//	 {
//		HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);
//		if(ChkExpireSysTick(&TempTick))
//		{
//		  SetSysTick(&TempTick,TIME_EMO_BUZZER);	 
//		  EMOLoopState = STATE_ON;
//		}
//	 }
//	 if(HAL_GPIO_ReadPin(EMG_GPIO_Port,EMG_Pin) == GPIO_PIN_SET)
//	 {
//		HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);
//		EMOLoopState = STATE_CHECK;
//	 }
//	 break;
//  }
//}

void changeGPIO(void)
{
  //	GPIO_InitTypeDef GPIO_InitStruct;
  
}
void LoopAppKey(void)
{
  static uint8_t  NowRow = 0;
  static uint64_t TempTick = 0;
  static uint32_t tempKey;
  static uint64_t BackLightTick = 0;
  static uint16_t LongCnt = 0;
//  static uint8_t BeepFlag = 0;
  static uint64_t BeepTick = 0;
  static KeyTypeDef Key;
  static uint8_t  ValueKey[MAX_ROW] = {0,};
  
  GPIO_InitTypeDef GPIO_InitStruct;
  
  static enum
  {
	 INIT = 0,
	 
	 STATE_READY_KEY,
	 STATE_SET_ROW,
	 STATE_READ_KEY,
	 STATE_DO_KEY,
//	 STATE_KEY_BEEP,
	 
  }KeyLoopState = INIT; 
  //  static enum
  //  {
  //	 INIT = 0,
  //	 
  //	 STATE_READY_KEY,
  //	 STATE_SET_ROW,
  //	 STATE_READ_KEY,
  //	 STATE_DO_KEY,
  //	 
  //  }BeepLoopState = INIT; 
  //  static enum
  //  {
  //	 INIT = 0,
  //	 
  //	 STATE_READY_KEY,
  //	 STATE_SET_ROW,
  //	 STATE_READ_KEY,
  //	 STATE_DO_KEY,
  //	 
  //  }BackligntLoopState = INIT; 
  
  //EMO
  if(HAL_GPIO_ReadPin(EMG_GPIO_Port,EMG_Pin) == GPIO_PIN_RESET) 
	 RobotCfg.ErrorState = ERROR_STATE_EMO;
  
  if(HAL_GPIO_ReadPin(EMG_GPIO_Port,EMG_Pin) != GPIO_PIN_SET) return;
  switch(KeyLoopState)
  {
  case INIT:
	 SetSysTick(&TempTick, TIME_KEY);
	 SetSysTick(&BeepTick, TIME_BEEP);
	 SetSysTick(&BackLightTick,TIME_BACKLIGHT);
	 NowRow = 0;
	 KeyLoopState = STATE_READY_KEY;
	 break;
	 
  case STATE_READY_KEY:
	 if(ChkExpireSysTick(&TempTick))
	 {
		
		ResetKey();
		KeyLoopState = STATE_SET_ROW;
	 }
//	 if(ChkExpireSysTick(&BeepTick))
		
	 break;
	 
  case STATE_SET_ROW:
	 if(NowRow < MAX_ROW)		//NowRow = 0~4
		RowKey(NowRow);
	 else //if(NowRow == MAX_ROW)
	 {
		GPIO_InitStruct.Pin = KIN0_Pin|KIN1_Pin|KIN2_Pin|KIN3_Pin|KIN4_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		GPIO_InitStruct.Pull = GPIO_PULLDOWN;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
		
		GPIO_InitStruct.Pin = GPIO_PIN_2;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_PULLDOWN;//GPIO_PULLUP//GPIO_PULLDOWN//GPIO_NOPULL
		HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);
		
		HAL_GPIO_WritePin(GPIOG, GPIO_PIN_2, GPIO_PIN_SET);
	 }
	 
	 SetSysTick(&TempTick, TIME_SCAN);
	 KeyLoopState = STATE_READ_KEY;
	 
  case STATE_READ_KEY:
	 if(!ChkExpireSysTick(&TempTick))
		break;
	 
	 if(NowRow < MAX_ROW) ValueKey[NowRow] = ReadKey();
	 
	 else //if(NowRow == MAX_ROW)
	 {
		//		  printf("%x\t", (GPIOE->IDR >> 2) & 0x1F);
		if(((GPIOE->IDR >> 2) & 0x1F) == 0x11)
		{
		  ValueKey[0] |= 0x04;
		  ValueKey[4] |= 0x04;
		}
		
		GPIO_InitStruct.Pin = KIN0_Pin|KIN1_Pin|KIN2_Pin|KIN3_Pin|KIN4_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_PULLDOWN;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
		
		GPIO_InitStruct.Pin = GPIO_PIN_2;
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		GPIO_InitStruct.Pull = GPIO_PULLDOWN;//GPIO_PULLUP//GPIO_PULLDOWN//GPIO_NOPULL
		HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);
	 }
	 
	 NowRow++;
	 
	 KeyLoopState = STATE_READY_KEY;
	 
	 if(NowRow > MAX_ROW)
	 {
		NowRow = 0;
		KeyLoopState = STATE_DO_KEY;
		HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);  //Buzzer Off
	 }
	 
	 break;
	 
  case STATE_DO_KEY:
	 Key = KEY_NONE;
	 for(int i=0;i<MAX_ROW;i++)
	 {
		if(ValueKey[i] != 0) 
		{
		  Key |= (ValueKey[i] << (i * MAX_ROW));  //Make All key data to 32bit(Key)
		}
	 }
	 
	 
	 
	 /************* BackLight Time **************/
	 if(HAL_GPIO_ReadPin(LCD_BACKLIGHT_GPIO_Port, LCD_BACKLIGHT_Pin) == GPIO_PIN_RESET)	//if backlight off,
	 {
		if(Key != KEY_NONE)
		{
		  SetSysTick(&BackLightTick,TIME_BACKLIGHT);
		  ControlBacklight(LCD_BACKLIGHT_ON);
		  //			 Key = KEY_NONE;
		  tempKey = Key;
		}
	 }
	 else
	 {
		if(Key == KEY_NONE)
		{
		  if(ChkExpireSysTick(&BackLightTick))	ControlBacklight(LCD_BACKLIGHT_OFF);
		}
		else 
		{
		  SetSysTick(&BackLightTick,TIME_BACKLIGHT);
		  ControlBacklight(LCD_BACKLIGHT_ON);
		}
	 }
	 //	 if(Key == KEY_NONE)
	 //	 {
	 //		 if(ChkExpireSysTick(&BackLightTick))	ControlBacklight(LCD_BACKLIGHT_OFF);
	 //	 }
	 //	 else 
	 //	 {
	 //		 SetSysTick(&BackLightTick,TIME_BACKLIGHT);
	 //		 ControlBacklight(LCD_BACKLIGHT_ON);
	 //	 }
	 
	 /***********************************************/
	 
	 //Duplicated Key
	 if((Key != KEY_NONE) && (Key != KEY_BIT2))
	 {
		if(tempKey != Key) 
		{
		  ControlMsg.KeyReady = KEY_PRESSED;
		  ControlMsg.KeyValue = Key;
		  //		  printf("KeyValue : %8x\n\r",ControlMsg.KeyValue);
		  //		  BeepFlag = 0;
		  tempKey = Key;
//		  if(RobotCfg.Function.Buzzer == FUNC_ON)
			 HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
		}
		LongCnt++;
		if(LongCnt > LONG_COUNT)
		{
		  Key = Key | LONG_KEY;
		  ControlMsg.KeyReady = KEY_PRESSED;
		  ControlMsg.KeyValue = Key;
		  LongCnt = 0;
		}
	 }
	 else tempKey = KEY_NONE;
	 
	 //	 if(Key != KEY_NONE && Key != KEY_BIT2)         //Prevent to continous beep 
	 
	 
	 //	 		if(Key != KEY_NONE)
	 //	 		{
	 //	 		  tempKey = Key;
	 //		  
	 //Beep On
//	 if(BeepFlag == 0)
//	 {
//		SetSysTick(&BeepTick, TIME_BEEP);
//		BeepFlag++;
//	 }
	 //		  /**********Sequence for Long key****************/
	 //	 LongCnt++;
	 //	 if(LongCnt > LONG_COUNT)
	 //	 {
	 //		Key = Key | LONG_KEY;
	 //		ControlMsg.KeyReady = KEY_PRESSED;
	 //		ControlMsg.KeyValue = Key;
	 //		LongCnt = 0;
	 //	 }
	 //		  
	 //		  /****************************************/
	 //		}
	 /***********Printf***************/
	 //		if(Key != KEY_NONE)
	 //		{
	 //			printf("key : %10x", Key);
	 //			printf("\n");
	 //		}
	 /**********************************/
	 
	 SetSysTick(&TempTick, TIME_KEY);
	 KeyLoopState = STATE_READY_KEY;
	 break;
  }
  
}	