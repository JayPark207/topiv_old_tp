/* 
 * File:   __APPTLCD_H
 * Author: Jay Park
 *
 * Created on 2016�� 11�� 14�� (Mon), PM 3:33
 */

#ifndef __APPLCD_H
#define	__APPLCD_H

#ifdef	__cplusplus
extern "C" {
#endif
#include "main.h"    
#include "interface.h"
#include "AppMain.h"

/* Private typedef -----------------------------------------------------------*/
	
#define TIME_LCD_TEST			500
#define LCD_BACKLIGHT_ON		GPIO_PIN_SET
#define LCD_BACKLIGHT_OFF		GPIO_PIN_RESET
typedef struct
{
  __IO uint8_t LCD_INST;
  __IO uint8_t LCD_DATA;
} LCD_TypeDef;


//#define LCD_BASE    ((uint32_t)(0x60000000 | 0x00020000 - 2 ) )


//#define LCD_BASE    		((uint32_t)(0x60000000)
//#define LCD_BASE_NE2		((uint32_t)(0x64000000)
//#define LCD         		((LCD_TypeDef *) LCD_BASE)
//#define LCD_CS1         ((LCD_TypeDef *) LCD_BASE)
//#define LCD_CS2			((LCD_TypeDef *) LCD_BASE_NE2)

//#define LCD_BASE_NE1    ((uint32_t)(0x60000000))
//#define LCD_BASE_NE2		((uint32_t)(0x64000000))
//
//
////#define LCD         		((LCD_TypeDef *) LCD_BASE)
//#define LCD_CS2         (LCD_BASE_NE1)
//#define LCD_CS1			(LCD_BASE_NE2)

#define LCD_CS1         (0x60000008)
#define LCD_CS2			(0x60000010)

#define LCD_READ			0x00000002
#define LCD_WRITE			0x00000000

#define LCD_INST			0x00000000
#define LCD_DATA			0x00000001

#define LCD_DISABLE		0x00000000
#define LCD_ENABLE		0x00000004


#define LCD_INST_DISPLAY_ON	0x3F
#define LCD_INST_DISPLAY_OFF	0x3E

#define LCD_INST_Y_ADDR			0x40		//Set address
#define LCD_INST_X_ADDR			0xB8		//Set page
#define LCD_INST_Z_ADDR			0xC0		//Display start line

#define LCD_INST_STATUS_BUSY	0x80		//H: Busy
#define LCD_INST_STATUS_ONOFF	0x20		//H: OFF
#define LCD_INST_STATUS_RESET	0x10		//H: Reset

#define LCD_PAGE_1			1
#define LCD_PAGE_2			2

#define REDRAW_OFF		1
#define REDRAW_ON			0

//LCD display mode
#define RIGHT_ALIGN					1
#define LEFT_ALIGN					2
#define CENTER_ALIGN					3


#define BACKGROUND_LOGO				0
#define BACKGROUND_OUTLINE			1
#define BACKGROUND_ERROR			2
#define BACKGROUND_TOP_HALF		3
#define BACKGROUND_HALF				4
#define BACKGROUND_IO				5

#define TITLE_LINE_MAX				3
#define OUTLINE_LINE_MAX			4
#define IO_GAP							18

#define MAX_UNDERBAR_POS 			8
#define TIME_BLINK 					500

#define BLINK_ON						1
#define BLINK_OFF						0

#define MOLD_NO						0
#define MOLD_NAME						2

#define DISPLAY_OFF					0
#define DISPLAY_ON					1

////#define LCD_BASE_ADDR			(*((volatile unsigned short *) 0x60000000))
//#define LCD_BASE_ADDR			(0x60000000))
//	
////#define LCD_REG_ADDR					((uint32_t) LCD_BASE_ADDR)
////#define LCD_RAM_ADDR					((uint32_t) LCD_BASE_ADDR | 0x00000001)
//	//#define LCD_REG_ADDR					((uint32_t) 0x60000000)
//#define LCD_RAM_ADDR					((uint32_t) 0x60000001)
	
//#define LCD_REG              (*((volatile unsigned short *) 0x6F000000)) /* RS = 0 (01101111000000000000000000000000)*/
//#define LCD_RAM              (*((volatile unsigned short *) 0x6F010000)) /* RS = 1 (01101111000000010000000000000000)*/

///****************************************************/
///*		define of LCD access value					*/
///****************************************************/
//#define LCD_CS_BIT_MASK				0xFFFFFFFB
//#define LCD_BASE_ADDR				0x100000
//#define LCD_RESET_PORT				rPDAT7
//#define LCD_RESET_MASK				0xBF
//#define LCD_RESET_EN				0x40
//#define LCD_BACKLIGHT_PORT			rPDAT5
//#define LCD_BACKLIGHT_MASK			0xEF
//#define LCD_BACKLIGHT_EN			0x10
//#define LCD_CS1						0x00
//#define LCD_CS2						0x04
//#define LCD_DISPLAY_ON_OFF_ADDR		(LCD_BASE_ADDR + 0x00)
//#define LCD_DISPLAY_ON_OFF_DATA		0x3E
//#define LCD_DISPLAY_START_ADDR		(LCD_BASE_ADDR + 0x00)
//#define LCD_DISPLAY_START_DATA		0xC0
//#define LCD_PAGE_ADDR_SET_ADDR		(LCD_BASE_ADDR + 0x00)
//#define LCD_PAGE_ADDR_SET_DATA		0xB8
//#define LCD_SET_ADDRESS_ADDR		(LCD_BASE_ADDR + 0x00)
//#define LCD_SET_ADDRESS_DATA		0x40
//#define LCD_STATUS_READ_ADDR		(LCD_BASE_ADDR + 0x01)
//#define LCD_WRITE_DISPLAY_DATA_ADDR	(LCD_BASE_ADDR + 0x02)
//#define LCD_READ_DISPLAY_DATA_ADDR	(LCD_BASE_ADDR + 0x03)
//	
//#define GLCD_RS_INSTRUCTION		0x00
//#define GLCD_RS_DATA				0x01
//	
//#define GLCD_CS1		0x01	//LEFT
//#define GLCD_CS2		0x02	//RIGHT
//#define GLCD_CS_BOTH	0x03	//BOTH
//	
//#define GLCD_DISPLAY_ON		0x3F
//#define GLCD_DISPLAY_OFF	0x3E
//	
//#define GLCD_DATA_GPIO_Port GPIOA
//	
//#define GLCD_CMD_SET_Y_ADDRESS			0x40
//#define GLCD_CMD_SET_PAGE					0xB8
//#define GLCD_CMD_DISPLAY_START_LINE		0xC0
//
//#define TIME_LCD_TEST			1000
//#define TIME_us_LCD				20
//	typedef enum {
//	LCD_DONE = 0,				/* (0) Succeeded */
//	LCD_WRITING,
//	LCD_READING,
//	LCD_PROCESSING,
//	LCD_FAIL,
//} LCD_RES;
//#define STATUS_ON			1
//#define STATUS_OFF		2
//#define STATUS_SET_X		3
//
//
//#define TIME_US_INTERVAL		2		//450us
//
//#define LCD_GPIO_INPUT		2
//#define LCD_GPIO_OUTPUT		1
//
//#define LCD_RW_WRITE			1
//#define LCD_RW_READ			2
//
//#define LCD_RS_INST			1
//#define LCD_RS_DATA			2
//	

//



//#define ResetKey()		GPIOE->BSRR = (0x1F << 2)
//#define RowKey(x)			GPIOE->BSRR = (0x01 << (x + 18))
//#define ReadKey()			(((GPIOF->IDR >> 1) & 0x1F) ^ 0x1F)

//#define LCD_RS_HIGH              HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_SET)
//#define LCD_RS_LOW               HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET)
//
//#define LCD_Read                 HAL_GPIO_WritePin(LCD_RW_GPIO_Port, LCD_RW_Pin, GPIO_PIN_SET)
//#define LCD_Write                HAL_GPIO_WritePin(LCD_RW_GPIO_Port, LCD_RW_Pin, GPIO_PIN_RESET)
//#define LCD_RW_Toggle				HAL_GPIO_TogglePin(LCD_RW_GPIO_Port, LCD_RW_Pin)
//
//#define LCD_EN_HIGH          		HAL_GPIO_WritePin(LCD_E_GPIO_Port, LCD_E_Pin, GPIO_PIN_SET)
//#define LCD_EN_LOW           		HAL_GPIO_WritePin(LCD_E_GPIO_Port, LCD_E_Pin, GPIO_PIN_RESET)
//	
//#define LCD_CS1_HIGH             HAL_GPIO_WritePin(LCD_CS1_GPIO_Port, LCD_CS1_Pin, GPIO_PIN_SET)
//#define LCD_CS1_LOW              HAL_GPIO_WritePin(LCD_CS1_GPIO_Port, LCD_CS1_Pin, GPIO_PIN_RESET)
//
//#define LCD_CS2_HIGH             HAL_GPIO_WritePin(LCD_CS2_GPIO_Port, LCD_CS2_Pin, GPIO_PIN_SET)
//#define LCD_CS2_LOW              HAL_GPIO_WritePin(LCD_CS2_GPIO_Port, LCD_CS2_Pin, GPIO_PIN_RESET)

//#define STATUS_BUSY_CHECK        	HAL_GPIO_ReadPin(LCD_D7_GPIO_Port, LCD_D7_Pin)
//	/* Defines -------------------------------------------------------------------*/
//
//#define  GLCD_Display_ON           	GLCD_DISPLAY_ON
//#define  GLCD_Display_OFF           GLCD_DISPLAY_OFF
//#define  START_LINE                 GLCD_CMD_DISPLAY_START_LINE 
//#define  PAGE                       GLCD_CMD_SET_PAGE
//#define  COLUMN                     GLCD_CMD_SET_Y_ADDRESS
	
void LoopAppLCD(void);
void ControlBacklight(uint8_t cmd);
//	extern MOLD_MSG MoldMsg;
//extern IO_MSG IOMsg;
//extern ERROR_MSG ErrMsg;
//extern SETTING_MSG SettingMsg;
#ifdef	__cplusplus
}
#endif

#endif	/* __APPLCD_H */