#include <stdio.h>// 12_03
#include <string.h>
#include <stdlib.h>
#include "AppMain.h"
#include "SDCard.h"
#include "AppKey.h"

ERROR_MSG ErrMsg;
ERROR_MSG ErrMsgTemp[100];
CONTROL_MSG ControlMsg;
CONTROL_MSG *ControlMsgTEST;
ROBOT_CONFIG RobotCfg;
IO_MSG IOMsg;
MOTION_DELAY MotionDelayTemp;											
MOTION_DELAY CycleDelay;															//For Timer in Cycle page
MOTION_MODE MotionModeTemp;
MANUFACTURER_SETTING SettingTemp;
SEQ_MSG SeqMsg;					
MOLD_MSG MoldMsg;
COUNT_MSG CountMsg;

//extern FATFS SDFatFs;						
//FIL TestFile;						
//DIR TestDir;						
SDRES SdRes;
SDRES SdResTemp;

uint8_t StepCursor = 0;																//For Display arrow StepPage
char NewMoldName[MAX_MOLDNAME_LENGTH];											//For New Mold Name
char NewMoldNameTemp[MAX_MOLDNAME_LENGTH];									//For New Mold Name Temp. change one character
uint8_t MoldNameCharPos = 5;														//For Char_Input_Data[] 
uint8_t MoldNamePos = 0;															//For NewMoldName[]
uint16_t MoldNoTemp = 0;															//For New Mold No
uint8_t UnderbarPos = 0;
//uint8_t BuzzerState = 0;

uint8_t SCStepPos=0; // for����

uint8_t ClickCount = 0;

uint8_t AutoStepDelay[20];
uint8_t AutoStep[20];
uint8_t AutoStepPos = 0;
uint64_t AutoTempTick = 0;

uint64_t SDTempTick = 0;
uint8_t fPreviousPage = 0;
uint8_t AutoInjectionTemp = 0;
//uint32_t byteswritten, bytesread;
//FIL* fp;
//	FILINFO fno;
//	FRESULT fr;
//	FRESULT res;

uint8_t ReadType(void)
{
  return (uint8_t)(((GPIOB->IDR >> 14) & 0x03) ^ 0x03);
}
uint8_t KeyIsNum(uint32_t* key)
{
  uint8_t ret;
  switch(*key)
  {
  case KEY_NO_0:
  case KEY_NO_1:
  case KEY_NO_2:
  case KEY_NO_3:
  case KEY_NO_4:
  case KEY_NO_5:
  case KEY_NO_6:
  case KEY_NO_7:
  case KEY_NO_8:
  case KEY_NO_9:
    ret = KEY_IS_NUM;
    break;
  default:
    ret = KEY_IS_NOT_NUM;
    break;
  }
  return ret;
}
uint8_t KeyNum(uint32_t* key)
{
  uint8_t ret;
  switch(*key)
  {
  case KEY_NO_0:
    ret = 0;
    break;
  case KEY_NO_1:
    ret = 1;
    break;
  case KEY_NO_2:
    ret = 2;
    break;
  case KEY_NO_3:
    ret = 3;
    break;
  case KEY_NO_4:
    ret = 4;
    break;
  case KEY_NO_5:
    ret = 5;
    break;
  case KEY_NO_6:
    ret = 6;
    break;
  case KEY_NO_7:
    ret = 7;
    break;
  case KEY_NO_8:
    ret = 8;
    break;
  case KEY_NO_9:
    ret = 9;
    break;
  default:
    ret = 0xFF;
    break;
  }
  return ret;
}

void ChangeSCMode(void)
{
  ControlMsg.CommStateForDL = 0;
  ControlMsg.CommMode = MODE_INIT;
}

void ClearPage(void)
{
  ControlMsg.PageNo = 0;
  ControlMsg.LineNo = 0;
  ControlMsg.CursorPos = 0;
  ControlMsg.EnableEdit = DISABLE_EDIT;
  ControlMsg.KeyValue = 0;
}

void ClearAuto(void)
{
  ClearPage();
  ControlMsg.fDoHoming = 0;
  AutoStepPos = 0;
  AutoTempTick = 0;
  ControlMsg.AutoPageNo = 0;
  ControlMsg.AutoLineNo = 0;
  memset(AutoStep,0,sizeof(AutoStep));
  ChangeSCMode();
  //  ControlMsg.TpMode = MANUAL_OPERATING;
}

void ChangeLanguage(void)
{
  if(RobotCfg.Language == LANGUAGE_ENG) RobotCfg.Language = LANGUAGE_KOR;
  else if(RobotCfg.Language == LANGUAGE_KOR) RobotCfg.Language = LANGUAGE_ENG;
  WriteRobotCfg(&RobotCfg);
}

void TimerDispCheck(void)
{
  //According to ModeSetting TimerPageSetting
  uint8_t TimerCounter = 0;
  
  ControlMsg.TimerDisp[DOWN_TIME] = USE;  
  ControlMsg.TimerDisp[KICK_TIME] = USE;   
  ControlMsg.TimerDisp[EJECTOR_TIME] = USE; 
  ControlMsg.TimerDisp[CHUCK_TIME] = USE;
  ControlMsg.TimerDisp[KICK_RETURN_TIME] = USE;  
  ControlMsg.TimerDisp[UP_TIME] = USE;  
  ControlMsg.TimerDisp[SWING_TIME] = USE;  
  ControlMsg.TimerDisp[SECOND_DOWN_TIME] = USE;  
  ControlMsg.TimerDisp[RELEASE_TIME] = USE;  
  ControlMsg.TimerDisp[SECOND_UP_TIME] = USE;  
  
  if (RobotCfg.MotionMode.MainRotateChuck == USE)   
    ControlMsg.TimerDisp[CHUCK_ROTATION_TIME] = USE;
  else
    ControlMsg.TimerDisp[CHUCK_ROTATION_TIME] = NO_USE;
  
  ControlMsg.TimerDisp[SWING_RETURN_TIME] = USE;
  ControlMsg.TimerDisp[NIPPER_TIME] = NO_USE;
  ControlMsg.TimerDisp[CONVEYOR_TIME] = USE; 
  
  //Total Timer Menu Count
  for (uint8_t TimeDispSetting = 0 ; TimeDispSetting < MAX_INDEX ; TimeDispSetting++)
  {
    if (ControlMsg.TimerDisp[TimeDispSetting] == USE)
    {
      ControlMsg.TimerDispTemp[TimerCounter] = TimeDispSetting;
      TimerCounter = TimerCounter+1;
    }
  }
  ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX] = TimerCounter;
}

//IMMType
void IMMTypeINIT(uint16_t MoldNo)
{
  if((RobotCfg.Setting.IMMType == VERTI) && (RobotCfg.Setting.RotationState == NO_ROTATION))
  {
    if((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC))
    {
      if((MoldNo == 23) || (MoldNo == 33) || (MoldNo == 37))
      {
        MotionModeTemp.ChuckOff = RELEASE_POS_ChuRo;
        MotionModeTemp.ChuckRejectPos = RELEASE_POS_ChuRo;
        memcpy(&(RobotCfg.MotionMode.ChuckOff), &MotionModeTemp.ChuckOff, sizeof(MotionModeTemp.ChuckOff));
        memcpy(&(RobotCfg.MotionMode.ChuckRejectPos), &MotionModeTemp.ChuckRejectPos, sizeof(MotionModeTemp.ChuckRejectPos));
        memcpy(&(MoldMsg.MotionMode.ChuckOff), &MotionModeTemp.ChuckOff, sizeof(MotionModeTemp.ChuckOff));
        memcpy(&(MoldMsg.MotionMode.ChuckRejectPos), &MotionModeTemp.ChuckRejectPos, sizeof(MotionModeTemp.ChuckRejectPos));
        if(MoldNo != NEW_MOLD)
        {
          WriteRobotCfg(&RobotCfg);
          WriteMold(&MoldMsg);
        }
      }
    }
    if(RobotCfg.RobotType == ROBOT_TYPE_XC)
    {
      if((MoldNo == 36) || (MoldNo == 37))
      {
        MotionModeTemp.VaccumOff = RELEASE_POS_ChuRo;
        MotionModeTemp.VaccumRejectPos = RELEASE_POS_ChuRo;
        memcpy(&(RobotCfg.MotionMode.VaccumOff), &MotionModeTemp.VaccumOff, sizeof(MotionModeTemp.VaccumOff));
        memcpy(&(RobotCfg.MotionMode.VaccumRejectPos), &MotionModeTemp.VaccumRejectPos, sizeof(MotionModeTemp.VaccumRejectPos));
        memcpy(&(MoldMsg.MotionMode.VaccumOff), &MotionModeTemp.VaccumOff, sizeof(MotionModeTemp.VaccumOff));
        memcpy(&(MoldMsg.MotionMode.VaccumRejectPos), &MotionModeTemp.VaccumRejectPos, sizeof(MotionModeTemp.VaccumRejectPos));
        if(MoldNo != NEW_MOLD)
        {
          WriteRobotCfg(&RobotCfg);
          WriteMold(&MoldMsg);
        }
      }
    }
  }
  else if((RobotCfg.Setting.IMMType == STANDARD) || (RobotCfg.Setting.RotationState == ROTATION))
  {
    if((RobotCfg.RobotType == ROBOT_TYPE_XC) || (RobotCfg.RobotType == ROBOT_TYPE_X))
    {
      if((MoldNo == 23) || (MoldNo == 33) || (MoldNo == 37))
      {
        MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
        MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
        memcpy(&(RobotCfg.MotionMode.ChuckOff), &MotionModeTemp.ChuckOff, sizeof(MotionModeTemp.ChuckOff));
        memcpy(&(RobotCfg.MotionMode.ChuckRejectPos), &MotionModeTemp.ChuckRejectPos, sizeof(MotionModeTemp.ChuckRejectPos));
        memcpy(&(RobotCfg.MotionMode.ChuckRejectPos), &MotionModeTemp.ChuckRejectPos, sizeof(MotionModeTemp.ChuckRejectPos));
        memcpy(&(MoldMsg.MotionMode.ChuckRejectPos), &MotionModeTemp.ChuckRejectPos, sizeof(MotionModeTemp.ChuckRejectPos));
        if(MoldNo != NEW_MOLD)
        {
          WriteRobotCfg(&RobotCfg);
          WriteMold(&MoldMsg);
        }
      }
    }
    if(RobotCfg.RobotType == ROBOT_TYPE_XC)
    {
      if((MoldNo == 36) || (MoldNo == 37))
      {
        MotionModeTemp.VaccumOff = RELEASE_POS_2ndDOWN;
        MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
        memcpy(&(RobotCfg.MotionMode.VaccumOff), &MotionModeTemp.VaccumOff, sizeof(MotionModeTemp.VaccumOff));
        memcpy(&(RobotCfg.MotionMode.VaccumRejectPos), &MotionModeTemp.VaccumRejectPos, sizeof(MotionModeTemp.VaccumRejectPos));
        memcpy(&(RobotCfg.MotionMode.VaccumRejectPos), &MotionModeTemp.VaccumRejectPos, sizeof(MotionModeTemp.VaccumRejectPos));
        memcpy(&(MoldMsg.MotionMode.VaccumRejectPos), &MotionModeTemp.VaccumRejectPos, sizeof(MotionModeTemp.VaccumRejectPos));
        if(MoldNo != NEW_MOLD)
        {
          WriteRobotCfg(&RobotCfg);
          WriteMold(&MoldMsg);
        }
      }
    }
  }
}

void ModeDispCheck(uint16_t MoldNoMode)
{
  //According to MoldNo ModeSettingValue
  uint8_t ModeCounter = 0;
  
  if(MoldNoMode == 22)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 32)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 23)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   	
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 33)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   					
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }                                                          
  else if (MoldNoMode == 36)
  {                                                          
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   					
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = USE;       	
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if (MoldNoMode == 37)
  {                                                          
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   					
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = USE;       	
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 40)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 41)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 43)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 45)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   					
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        			
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;       
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 82)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   					
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = NO_USE;   			
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        			
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;       
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  }
  else if(MoldNoMode == 84)
  {
    ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_MODE] = NO_USE;   			   
    ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;        		
    ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;   
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = NO_USE;   		
    ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;        	
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = NO_USE;   				
    ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        		
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = NO_USE;   	
    ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;       
    ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;    
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = NO_USE;  	
    ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE;   
  } 
  else
  {
    //According to RobotType ModeSettingValue
    if (RobotCfg.RobotType == ROBOT_TYPE_TWIN)   
    {
      ControlMsg.ModeDisp[ARMSET_MODE] = USE;        			
      ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = USE;        	
      ControlMsg.ModeDisp[SUB_DOWN_MODE] = USE;        		
      ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = USE;    
    }                                                       
    else                                                    
    {
      ControlMsg.ModeDisp[ARMSET_MODE] = NO_USE;            
      ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] = NO_USE;       
      ControlMsg.ModeDisp[SUB_DOWN_MODE] = NO_USE;          
      ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] = NO_USE; 
    }                                                       
    
    ControlMsg.ModeDisp[CHUCK_MODE] = USE;   					
    
    if ((RobotCfg.RobotType == ROBOT_TYPE_XC) || (RobotCfg.RobotType == ROBOT_TYPE_TWIN))  
    {
      ControlMsg.ModeDisp[VACUUM_MODE] = USE;         		
      ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = USE;       
      ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = USE;    
    }                                                       
    else                                                    
    {                                                       
      ControlMsg.ModeDisp[VACUUM_MODE] = NO_USE;            
      ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] = NO_USE;    
      ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] = NO_USE; 
    }
    
    if (RobotCfg.RobotType != ROBOT_TYPE_A)      
      ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = USE;       
    else                                                    
      ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] = NO_USE;    
    
    ControlMsg.ModeDisp[OUTSIDE_MODE] = USE;   					
    ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] = USE;   			
    ControlMsg.ModeDisp[MAIN_DOWN_MODE] = USE;   				
    ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] = USE;  	
    ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] = USE;
  }
  
  //IMMType
  IMMTypeINIT(MoldNoMode);
  
  //Total Mode Menu Count
  for (uint8_t ModeDispSetting = 0 ; ModeDispSetting < MAX_INDEX ; ModeDispSetting++)
  {
    if (ControlMsg.ModeDisp[ModeDispSetting] == USE)
    {
      ControlMsg.ModeDispTemp[ModeCounter] = ModeDispSetting;
      ModeCounter = ModeCounter+1;
    }
  }
  ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX] = ModeCounter;
}

void MoldInit(void)
{
  //According to MoldNo Default Mode Setting Value
  if ((MoldMsg.MoldNo == 22) || (MoldMsg.MoldNo == 32))
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;
    MotionModeTemp.MainChuck = USE;
    MotionModeTemp.MainVaccum = NO_USE;
    MotionModeTemp.MainRotateChuck = NO_USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    if (MoldMsg.MoldNo == 22)
      MotionModeTemp.MainArmType = ARM_U_TYPE;
    else if (MoldMsg.MoldNo == 32)
      MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_NO_USE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NO_USE;
    MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
    MotionModeTemp.SubArmOff = RELEASE_POS_NO_USE;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
  }
  else if ((MoldMsg.MoldNo == 23) || (MoldMsg.MoldNo == 33))
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;
    MotionModeTemp.MainChuck = USE;
    MotionModeTemp.MainVaccum = NO_USE;
    MotionModeTemp.MainRotateChuck = USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    if (MoldMsg.MoldNo == 23) 
      MotionModeTemp.MainArmType = ARM_U_TYPE;
    else if (MoldMsg.MoldNo == 33)
      MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_NO_USE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NO_USE;
    MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
    MotionModeTemp.SubArmOff = RELEASE_POS_NO_USE;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
  }
  else if ((MoldMsg.MoldNo == 36) || (MoldMsg.MoldNo == 37))
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;
    if (MoldMsg.MoldNo == 36)
      MotionModeTemp.MainChuck = NO_USE;
    else if (MoldMsg.MoldNo == 37)
      MotionModeTemp.MainChuck = USE;
    MotionModeTemp.MainVaccum = USE;
    MotionModeTemp.MainRotateChuck = USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_NO_USE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NO_USE;
    if (MoldMsg.MoldNo == 36)
      MotionModeTemp.ChuckOff = RELEASE_POS_NO_USE;
    else if (MoldMsg.MoldNo == 37)
      MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.SubArmOff = RELEASE_POS_NO_USE;
    if (MoldMsg.MoldNo == 36)
      MotionModeTemp.ChuckRejectPos = RELEASE_POS_NO_USE;
    else if (MoldMsg.MoldNo == 37)
      MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
  }
  else if (MoldMsg.MoldNo == 40)
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MS;
    MotionModeTemp.MainChuck = USE;
    MotionModeTemp.MainVaccum = NO_USE;
    MotionModeTemp.MainRotateChuck = NO_USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_L_TYPE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
    MotionModeTemp.SubArmOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
  }
  else if (MoldMsg.MoldNo == 41)
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MS;
    MotionModeTemp.MainChuck = USE;
    MotionModeTemp.MainVaccum = NO_USE;
    MotionModeTemp.MainRotateChuck = USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_L_TYPE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.SubArmOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
  }
  else if (MoldMsg.MoldNo == 43)
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MS;
    MotionModeTemp.MainChuck = NO_USE;
    MotionModeTemp.MainVaccum = USE;
    MotionModeTemp.MainRotateChuck = USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_L_TYPE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
    MotionModeTemp.SubArmOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
  }
  else if (MoldMsg.MoldNo == 45)
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_MS;
    MotionModeTemp.MainChuck = USE;
    MotionModeTemp.MainVaccum = USE;
    MotionModeTemp.MainRotateChuck = USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    MotionModeTemp.MainArmType = ARM_L_TYPE;
    MotionModeTemp.SubArmType = ARM_L_TYPE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.VaccumOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.SubArmOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
  }
  else if ((MoldMsg.MoldNo == 82) || (MoldMsg.MoldNo == 84))
  {
    MotionModeTemp.MotionArm = ROBOT_MAIN_SUB;
    MotionModeTemp.MainChuck = NO_USE;
    MotionModeTemp.MainVaccum = NO_USE;
    MotionModeTemp.MainRotateChuck = NO_USE;
    MotionModeTemp.MainNipper = NO_USE;
    MotionModeTemp.OutsideWait = NO_USE;
    MotionModeTemp.MainArmType = ARM_NO_USE;
    if (MoldMsg.MoldNo == 82)
      MotionModeTemp.SubArmType = ARM_U_TYPE;
    else if (MoldMsg.MoldNo == 84)
      MotionModeTemp.SubArmType = ARM_L_TYPE;
    MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_NO_USE;
    MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
    MotionModeTemp.ChuckOff = RELEASE_POS_NO_USE;
    MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
    MotionModeTemp.SubArmOff = RELEASE_POS_2ndDOWN;
    MotionModeTemp.ChuckRejectPos = RELEASE_POS_NO_USE;
    MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
  }
  else
  {
    if(RobotCfg.RobotType == ROBOT_TYPE_TWIN)
    {
      MotionModeTemp.MotionArm = ROBOT_MAIN_MS;
      MotionModeTemp.MainChuck = USE;
      MotionModeTemp.MainVaccum = USE;
      MotionModeTemp.MainRotateChuck = USE;
      MotionModeTemp.MainNipper = NO_USE;
      MotionModeTemp.OutsideWait = NO_USE;
      MotionModeTemp.MainArmType = ARM_L_TYPE;
      MotionModeTemp.SubArmType = ARM_L_TYPE;
      MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
      MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
      MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.VaccumOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.SubArmOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
      MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
    }
    else if(RobotCfg.RobotType == ROBOT_TYPE_A)
    {
      MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;
      MotionModeTemp.MainChuck = USE;
      MotionModeTemp.MainVaccum = 0xFF;
      MotionModeTemp.MainRotateChuck = 0xFF;
      MotionModeTemp.MainNipper = 0xFF;
      MotionModeTemp.OutsideWait = NO_USE;
      MotionModeTemp.MainArmType = ARM_L_TYPE;
      MotionModeTemp.SubArmType = ARM_NO_USE;
      MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
      MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NO_USE;
      MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
      MotionModeTemp.SubArmOff = RELEASE_POS_NO_USE;
      MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
      MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
    }
    else if(RobotCfg.RobotType == ROBOT_TYPE_X)
    {
      MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;
      MotionModeTemp.MainChuck = USE;
      MotionModeTemp.MainVaccum = 0xFF;
      MotionModeTemp.MainRotateChuck = USE;
      MotionModeTemp.MainNipper = 0xFF;
      MotionModeTemp.OutsideWait = NO_USE;
      MotionModeTemp.MainArmType = ARM_L_TYPE;
      MotionModeTemp.SubArmType = ARM_NO_USE;
      MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
      MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NO_USE;
      MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.VaccumOff = RELEASE_POS_NO_USE;
      MotionModeTemp.SubArmOff = RELEASE_POS_NO_USE;
      MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
      MotionModeTemp.VaccumRejectPos = RELEASE_POS_NO_USE;
    }
    else if(RobotCfg.RobotType == ROBOT_TYPE_XC)
    {
      MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;
      MotionModeTemp.MainChuck = USE;
      MotionModeTemp.MainVaccum = USE;
      MotionModeTemp.MainRotateChuck = USE;
      MotionModeTemp.MainNipper = 0xFF;
      MotionModeTemp.OutsideWait = NO_USE;
      MotionModeTemp.MainArmType = ARM_L_TYPE;
      MotionModeTemp.SubArmType = ARM_NO_USE;
      MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
      MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NO_USE;
      MotionModeTemp.ChuckOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.VaccumOff = RELEASE_POS_2ndDOWN;
      MotionModeTemp.SubArmOff = RELEASE_POS_NO_USE;
      MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
      MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
    }
  }
  //Dealy Default 0.5s
  for(uint8_t DelayPos = 0 ; DelayPos < sizeof(MoldMsg.MotionDelay) ; DelayPos++)
  {
    (*(&(MoldMsg.MotionDelay.DownDelay) + DelayPos)) = 5;
  }
  MoldMsg.Function.Buzzer = USE;
  MoldMsg.Function.Detection = USE;
  MoldMsg.Function.Ejector = NO_USE;
  MoldMsg.Function.Reject = NO_USE;
}

void NewMoldInit(uint16_t NewMoldNo)		
{
  //NEW Mold No & Name Init
  //Default Mold Name
  NewMoldNameTemp[0] = 5;						//'F'
  NewMoldNameTemp[1] = 8;						//'I'
  NewMoldNameTemp[2] = 11;						//'L'
  NewMoldNameTemp[3] = 4;						//'E'
  
  //Default Mold No
  if(NewMoldNo == 0)
  {
    NewMoldNameTemp[4] = 27;					//'1'
    NewMoldNameTemp[5] = 26;					//'0'
    NewMoldNameTemp[6] = 26;					//'0'
  }
  else
  {
    if(NewMoldNo/100 == 1)
      NewMoldNameTemp[4] = 27;				//'1'
    else if(NewMoldNo/100 == 2)
      NewMoldNameTemp[4] = 28;				//'2'
    else if(NewMoldNo/100 == 3)
      NewMoldNameTemp[4] = 29;				//'3'
    else if(NewMoldNo/100 == 4)
      NewMoldNameTemp[4] = 30;				//'4'
    else if(NewMoldNo/100 == 5)
      NewMoldNameTemp[4] = 31;				//'5'
    else if(NewMoldNo/100 == 6)
      NewMoldNameTemp[4] = 32;				//'6'
    else if(NewMoldNo/100 == 7)
      NewMoldNameTemp[4] = 33;				//'7'
    else if(NewMoldNo/100 == 8)
      NewMoldNameTemp[4] = 34;				//'8'
    else if(NewMoldNo/100 == 9)
      NewMoldNameTemp[4] = 35;				//'9'
    
    if(((NewMoldNo%100)/10) == 0)
      NewMoldNameTemp[5] = 26;				//'0'
    else if(((NewMoldNo%100)/10) == 1)
      NewMoldNameTemp[5] = 27;				//'1'
    else if(((NewMoldNo%100)/10) == 2)
      NewMoldNameTemp[5] = 28;				//'2'
    else if(((NewMoldNo%100)/10) == 3)
      NewMoldNameTemp[5] = 29;				//'3'
    else if(((NewMoldNo%100)/10) == 4)
      NewMoldNameTemp[5] = 30;				//'4'
    else if(((NewMoldNo%100)/10) == 5)
      NewMoldNameTemp[5] = 31;				//'5'
    else if(((NewMoldNo%100)/10) == 6)
      NewMoldNameTemp[5] = 32;				//'6'
    else if(((NewMoldNo%100)/10) == 7)
      NewMoldNameTemp[5] = 33;				//'7'
    else if(((NewMoldNo%100)/10) == 8)
      NewMoldNameTemp[5] = 34;				//'8'
    else if(((NewMoldNo%100)/10) == 9)
      NewMoldNameTemp[5] = 35;				//'9'
    
    if(((NewMoldNo%100)%10) == 0)
      NewMoldNameTemp[6] = 26;				//'0'
    else if(((NewMoldNo%100)%10) == 1)
      NewMoldNameTemp[6] = 27;				//'1'
    else if(((NewMoldNo%100)%10) == 2)
      NewMoldNameTemp[6] = 28;				//'2'
    else if(((NewMoldNo%100)%10) == 3)
      NewMoldNameTemp[6] = 29;				//'3'
    else if(((NewMoldNo%100)%10) == 4)
      NewMoldNameTemp[6] = 30;				//'4'
    else if(((NewMoldNo%100)%10) == 5)
      NewMoldNameTemp[6] = 31;				//'5'
    else if(((NewMoldNo%100)%10) == 6)
      NewMoldNameTemp[6] = 32;				//'6'
    else if(((NewMoldNo%100)%10) == 7)
      NewMoldNameTemp[6] = 33;				//'7'
    else if(((NewMoldNo%100)%10) == 8)
      NewMoldNameTemp[6] = 34;				//'8'
    else if(((NewMoldNo%100)%10) == 9)
      NewMoldNameTemp[6] = 35;				//'9'
  }
  NewMoldNameTemp[7] = 38;						//' '
  NewMoldNameTemp[8] = '\0';					//null
}

void InMenuUpButton(uint8_t Max_Line)
{
  if(ControlMsg.LineNo > 0) ControlMsg.LineNo--;
  else
  {
    if(ControlMsg.PageNo > 0)
    {
      ControlMsg.PageNo--;
      ControlMsg.LineNo = Max_Line;
    }
  }
}

void InMenuDownButton(uint8_t Max_Line , uint8_t Max_Page, uint8_t *DispTempArr)
{
  //Last line check
  if(ControlMsg.LineNo < Max_Line)	
  {
    //last page and last line check
    if ((ControlMsg.PageNo*Max_Page  + ControlMsg.LineNo) != DispTempArr[TOTAL_COUNT_INDEX] -1)
      ControlMsg.LineNo++;  //next line
  }
  else
  { 
    //last page check
    if (ControlMsg.PageNo != DispTempArr[TOTAL_COUNT_INDEX]/Max_Page)
    {
      //last page and last line check
      if ((ControlMsg.PageNo == DispTempArr[TOTAL_COUNT_INDEX]/Max_Page -1) &&
          (ControlMsg.LineNo == Max_Line) && (DispTempArr[TOTAL_COUNT_INDEX]%Max_Page == 0));
      else
      {
        //next page
        ControlMsg.PageNo++;
        ControlMsg.LineNo = 0;
      }
    }
  }
}

void ImportDelayTime(void)
{
  if((ControlMsg.TimerDisp[DOWN_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == DOWN_TIME))
    MotionDelayTemp.DownDelay = ((MotionDelayTemp.DownDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[KICK_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == KICK_TIME))
    MotionDelayTemp.KickDelay = ((MotionDelayTemp.KickDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[EJECTOR_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == EJECTOR_TIME))
    MotionDelayTemp.EjectorDelay = ((MotionDelayTemp.EjectorDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[CHUCK_TIME] == USE)  && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == CHUCK_TIME))
    MotionDelayTemp.ChuckDelay = ((MotionDelayTemp.ChuckDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[KICK_RETURN_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == KICK_RETURN_TIME))
    MotionDelayTemp.KickReturnDelay = ((MotionDelayTemp.KickReturnDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[UP_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == UP_TIME))
    MotionDelayTemp.UpDelay = ((MotionDelayTemp.UpDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[SWING_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == SWING_TIME))
    MotionDelayTemp.SwingDelay = ((MotionDelayTemp.SwingDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[SECOND_DOWN_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == SECOND_DOWN_TIME))
    MotionDelayTemp.Down2ndDelay = ((MotionDelayTemp.Down2ndDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[RELEASE_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == RELEASE_TIME))
    MotionDelayTemp.OpenDelay = ((MotionDelayTemp.OpenDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[SECOND_UP_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == SECOND_UP_TIME))
    MotionDelayTemp.Up2ndDelay = ((MotionDelayTemp.Up2ndDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[CHUCK_ROTATION_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == CHUCK_ROTATION_TIME))
    MotionDelayTemp.ChuckRotateReturnDelay = ((MotionDelayTemp.ChuckRotateReturnDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[SWING_RETURN_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == SWING_RETURN_TIME))
    MotionDelayTemp.SwingReturnDelay = ((MotionDelayTemp.SwingReturnDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[NIPPER_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == NIPPER_TIME))
    MotionDelayTemp.NipperOnDelay = ((MotionDelayTemp.NipperOnDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
  else if((ControlMsg.TimerDisp[CONVEYOR_TIME] == USE) && (ControlMsg.TimerDispTemp[ControlMsg.PageNo*MAX_LINE_OUTLINE + ControlMsg.LineNo] == CONVEYOR_TIME))
    MotionDelayTemp.ConveyorDelay = ((MotionDelayTemp.ConveyorDelay)*10 + KeyNum(&ControlMsg.KeyValue)) % DECIMAL_PLACE;
}

void ImportMode(void)
{
  if(ControlMsg.KeyValue == KEY_ARROW_LEFT)
  {
    if((ControlMsg.ModeDisp[ARMSET_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == ARMSET_MODE))
    {
      if((MotionModeTemp.MotionArm == ROBOT_MAIN_MAIN) && (ControlMsg.KeyValue == KEY_ARROW_LEFT))
        MotionModeTemp.MotionArm = ROBOT_MAIN_MS;
      else MotionModeTemp.MotionArm--;
    }
    else if((ControlMsg.ModeDisp[CHUCK_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == CHUCK_MODE))
    {
      MotionModeTemp.MainChuck ^= USE;
    }
    else if((ControlMsg.ModeDisp[VACUUM_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == VACUUM_MODE))
    {
      MotionModeTemp.MainVaccum ^= USE;
    }
    else if((ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == CHUCK_ROTATION_MODE))
    {
      MotionModeTemp.MainRotateChuck ^= USE;
    }
    else if((ControlMsg.ModeDisp[OUTSIDE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == OUTSIDE_MODE))
    {
      //IMMType
      if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.RotationState == NO_ROTATION))
        MotionModeTemp.OutsideWait = NO_USE;
      else
        MotionModeTemp.OutsideWait ^= USE;
    }
    else if((ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_TAKEOUT_MODE))
    {
      if((MotionModeTemp.MainArmType == ARM_L_TYPE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT))
        MotionModeTemp.MainArmType = ARM_I_TYPE;
      else MotionModeTemp.MainArmType--;
    }
    else if((ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == SUB_TAKEOUT_MODE))
    {
      if((MotionModeTemp.SubArmType == ARM_L_TYPE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT))
        MotionModeTemp.SubArmType = ARM_I_TYPE;
      else MotionModeTemp.SubArmType--;
    }
    else if((ControlMsg.ModeDisp[MAIN_DOWN_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_DOWN_MODE))
    {
      if((MotionModeTemp.MainArmDownPos == ARM_DOWNPOS_NOZZLE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
        MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_CLAMP;
      else MotionModeTemp.MainArmDownPos--;
    }
    else if((ControlMsg.ModeDisp[SUB_DOWN_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == SUB_DOWN_MODE))
    {
      if((MotionModeTemp.SubArmDownPos == ARM_DOWNPOS_NOZZLE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
        MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_CLAMP;
      else MotionModeTemp.SubArmDownPos--;
    }
    else if((ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_CHUCK_RELEASE_MODE))
    {
      //IMMType
      if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.ChuckOff = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          if((MotionModeTemp.ChuckOff == RELEASE_POS_OUTSIDE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
            MotionModeTemp.ChuckOff = RELEASE_POS_ChuRo;
          else MotionModeTemp.ChuckOff--; 
        }
      }
      else
      {
        if((MotionModeTemp.ChuckOff == RELEASE_POS_INMOLD) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
          MotionModeTemp.ChuckOff = RELEASE_POS_2ndUP;
        else MotionModeTemp.ChuckOff--;
      }
    }
    else if((ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == VACUUM_RELEASE_MODE))
    {
      //IMMType
      if((RobotCfg.RobotType == ROBOT_TYPE_XC) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.VaccumOff = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          if((MotionModeTemp.VaccumOff == RELEASE_POS_OUTSIDE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
            MotionModeTemp.VaccumOff = RELEASE_POS_ChuRo;
          else MotionModeTemp.VaccumOff--;
        }
      }
      else
      {
        if((MotionModeTemp.VaccumOff == RELEASE_POS_INMOLD) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
          MotionModeTemp.VaccumOff = RELEASE_POS_2ndUP;
        else MotionModeTemp.VaccumOff--;
      }
    }
    else if((ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == SUB_CHUCK_RELEASE_MODE))
    {
      if((MotionModeTemp.SubArmOff == RELEASE_POS_INMOLD) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
        MotionModeTemp.SubArmOff = RELEASE_POS_2ndUP;
      else MotionModeTemp.SubArmOff--;
    }
    else if((ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_CHUCK_REJECT_MODE))
    {
      //IMMType
      if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.ChuckRejectPos = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          if((MotionModeTemp.ChuckRejectPos == RELEASE_POS_OUTSIDE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
            MotionModeTemp.ChuckRejectPos = RELEASE_POS_ChuRo;
          else MotionModeTemp.ChuckRejectPos--;
        }
      }
      else
      {
        if((MotionModeTemp.ChuckRejectPos == RELEASE_POS_INMOLD) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
          MotionModeTemp.ChuckRejectPos = RELEASE_POS_2ndUP;
        else MotionModeTemp.ChuckRejectPos--;
      }
    }
    else if((ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_VAUUM_REJECT_MODE))
    {
      //IMMType
      if((RobotCfg.RobotType == ROBOT_TYPE_XC) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.VaccumRejectPos = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          if((MotionModeTemp.VaccumRejectPos == RELEASE_POS_OUTSIDE) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
            MotionModeTemp.VaccumRejectPos = RELEASE_POS_ChuRo;
          else MotionModeTemp.VaccumRejectPos--;
        }
      }
      else
      {
        if((MotionModeTemp.VaccumRejectPos == RELEASE_POS_INMOLD) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
          MotionModeTemp.VaccumRejectPos = RELEASE_POS_2ndUP;
        else MotionModeTemp.VaccumRejectPos--;
      }
    }
  }
  else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT)
  {
    if((ControlMsg.ModeDisp[ARMSET_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == ARMSET_MODE))
    {
      MotionModeTemp.MotionArm++;
      if(MotionModeTemp.MotionArm > ROBOT_MAIN_MS) MotionModeTemp.MotionArm = ROBOT_MAIN_MAIN;				  
    }
    else if((ControlMsg.ModeDisp[CHUCK_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == CHUCK_MODE))
    {
      MotionModeTemp.MainChuck ^= USE;
    }
    else if((ControlMsg.ModeDisp[VACUUM_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == VACUUM_MODE))
    {
      MotionModeTemp.MainVaccum ^= USE;
    }
    else if((ControlMsg.ModeDisp[CHUCK_ROTATION_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == CHUCK_ROTATION_MODE))
    {
      MotionModeTemp.MainRotateChuck ^= USE;
    }
    else if((ControlMsg.ModeDisp[OUTSIDE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == OUTSIDE_MODE))
    {
      //IMMType
      if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.RotationState == NO_ROTATION))
        MotionModeTemp.OutsideWait = NO_USE;
      else
        MotionModeTemp.OutsideWait ^= USE;
    }
    else if((ControlMsg.ModeDisp[MAIN_TAKEOUT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_TAKEOUT_MODE))
    {
      MotionModeTemp.MainArmType++;
      if(MotionModeTemp.MainArmType > ARM_I_TYPE) MotionModeTemp.MainArmType = ARM_L_TYPE;
    }
    else if((ControlMsg.ModeDisp[SUB_TAKEOUT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == SUB_TAKEOUT_MODE))
    {
      MotionModeTemp.SubArmType++;
      if(MotionModeTemp.SubArmType > ARM_I_TYPE) MotionModeTemp.SubArmType = ARM_L_TYPE;
    }
    else if((ControlMsg.ModeDisp[MAIN_DOWN_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_DOWN_MODE))
    {
      MotionModeTemp.MainArmDownPos++;
      if(MotionModeTemp.MainArmDownPos > ARM_DOWNPOS_CLAMP) MotionModeTemp.MainArmDownPos = ARM_DOWNPOS_NOZZLE;
    }
    else if((ControlMsg.ModeDisp[SUB_DOWN_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == SUB_DOWN_MODE))
    {
      MotionModeTemp.SubArmDownPos++;
      if(MotionModeTemp.SubArmDownPos > ARM_DOWNPOS_CLAMP) MotionModeTemp.SubArmDownPos = ARM_DOWNPOS_NOZZLE;
    }
    else if((ControlMsg.ModeDisp[MAIN_CHUCK_RELEASE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_CHUCK_RELEASE_MODE))
    {
      //IMMType
      if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.ChuckOff = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          MotionModeTemp.ChuckOff++;
          if(MotionModeTemp.ChuckOff > RELEASE_POS_ChuRo) MotionModeTemp.ChuckOff = RELEASE_POS_OUTSIDE;
        }
      }
      else
      {
        MotionModeTemp.ChuckOff++;
        if(MotionModeTemp.ChuckOff > RELEASE_POS_2ndUP) MotionModeTemp.ChuckOff = RELEASE_POS_INMOLD;
      }
    }
    else if((ControlMsg.ModeDisp[VACUUM_RELEASE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == VACUUM_RELEASE_MODE))
    {
      //IMMType
      if((RobotCfg.RobotType == ROBOT_TYPE_XC) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.VaccumOff = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          MotionModeTemp.VaccumOff++;
          if(MotionModeTemp.VaccumOff > RELEASE_POS_ChuRo) MotionModeTemp.VaccumOff = RELEASE_POS_OUTSIDE;
        }
      }
      else
      {
        MotionModeTemp.VaccumOff++;
        if(MotionModeTemp.VaccumOff > RELEASE_POS_2ndUP) MotionModeTemp.VaccumOff = RELEASE_POS_INMOLD;
      }
    }
    else if((ControlMsg.ModeDisp[SUB_CHUCK_RELEASE_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == SUB_CHUCK_RELEASE_MODE))
    {
      MotionModeTemp.SubArmOff++;
      if(MotionModeTemp.SubArmOff > RELEASE_POS_2ndUP) MotionModeTemp.SubArmOff = RELEASE_POS_INMOLD;
    }
    else if((ControlMsg.ModeDisp[MAIN_CHUCK_REJECT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_CHUCK_REJECT_MODE))
    {
      //IMMType
      if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.ChuckRejectPos = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          MotionModeTemp.ChuckRejectPos++;
          if(MotionModeTemp.ChuckRejectPos > RELEASE_POS_ChuRo) MotionModeTemp.ChuckRejectPos = RELEASE_POS_OUTSIDE;
        }
      }
      else
      {
        MotionModeTemp.ChuckRejectPos++;
        if(MotionModeTemp.ChuckRejectPos > RELEASE_POS_2ndUP) MotionModeTemp.ChuckRejectPos = RELEASE_POS_INMOLD;
      }
    }
    else if((ControlMsg.ModeDisp[MAIN_VAUUM_REJECT_MODE] == USE) && (ControlMsg.ModeDispTemp[ControlMsg.PageNo*MAX_LINE_MENU + ControlMsg.LineNo] == MAIN_VAUUM_REJECT_MODE))
    {
      //IMMType
      if((RobotCfg.RobotType == ROBOT_TYPE_XC) && (RobotCfg.Setting.IMMType == VERTI))
      {
        if(RobotCfg.Setting.RotationState == NO_ROTATION)
          MotionModeTemp.VaccumRejectPos = RELEASE_POS_ChuRo;
        else if(RobotCfg.Setting.RotationState == ROTATION)
        {
          MotionModeTemp.VaccumRejectPos++;
          if(MotionModeTemp.VaccumRejectPos > RELEASE_POS_ChuRo) MotionModeTemp.VaccumRejectPos = RELEASE_POS_OUTSIDE;
        }
      }
      else
      {
        MotionModeTemp.VaccumRejectPos++;
        if(MotionModeTemp.VaccumRejectPos > RELEASE_POS_2ndUP) MotionModeTemp.VaccumRejectPos = RELEASE_POS_INMOLD;
      }
    }		  
  }
}

void WriteErrorList(void)
{
  ErrMsg.ErrorCode = RobotCfg.ErrCode;
  ErrMsg.ErrorNo++;
  ErrMsg.Time = GetTime();
  WriteError(&ErrMsg);
}

//void Buzzer(void)
//{
//  static uint64_t BuzzerDelayTick;
//  static enum
//  {
//	 BUZZER_INIT = 0,
//	 BUZZER_ON,
//	 BUZZER_OFF,
//  }BuzzerLoopState = BUZZER_INIT;
//  
//  if(BuzzerState == BUZZER_START)
//  {
//	 switch(BuzzerLoopState)
//	 {
//	 case BUZZER_INIT :
//		SetSysTick(&BuzzerDelayTick,TIME_TOGGLE_BUZZER);
//		BuzzerLoopState = BUZZER_ON;
//		break;
//		
//	 case BUZZER_ON : 
//		HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
//		if(ChkExpireSysTick(&BuzzerDelayTick))
//		{
//		  SetSysTick(&BuzzerDelayTick,TIME_TOGGLE_BUZZER);	 
//		  BuzzerLoopState = BUZZER_OFF;
//		}
//		break;
//		
//	 case BUZZER_OFF :
//		HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);
//		if(ChkExpireSysTick(&BuzzerDelayTick))
//		{	
//		  SetSysTick(&BuzzerDelayTick,TIME_TOGGLE_BUZZER);	 
//		  BuzzerLoopState = BUZZER_ON;
//		}
//		break;
//	 }
//  }
//  else if(BuzzerState == BUZZER_STOP)
//  {
//	 HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);
//	 BuzzerLoopState = BUZZER_INIT;
//  }
//}

void MakeSequence(void)
{
  uint8_t MakeSeqPos = 0;
  uint8_t StepSEQPos = 0;
  uint8_t AutoSetpSEQPos = 0;
  
  //Interlock
  if (RobotCfg.Function.Ejector == FUNC_ON)
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_EJECTOR_Y2B_OFF;
  else if (RobotCfg.Function.Ejector == FUNC_OFF)
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_EJECTOR_Y2B_ON;
  
  SeqMsg.SeqArr[MakeSeqPos++] = ITLO_CYCLE_START_Y29_ON_FIRST;
  SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_ON_FIRST;
  SeqMsg.SeqArr[MakeSeqPos++] = ITLI_MOLD_OPENED_X18_OFF;
  SeqMsg.SeqArr[MakeSeqPos++] = ITLO_CYCLE_START_Y29_OFF;
  
  if (RobotCfg.Setting.ItlAutoInjection == USE)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = ITLI_AUTO_INJECTION_X19_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = ITLI_AUTO_INJECTION_X19_OFF;
  }
  
  SeqMsg.SeqArr[MakeSeqPos++] = ITLI_MOLD_OPENED_X18_ON;
  
  if (RobotCfg.Setting.ItlFullAuto == USE)
    SeqMsg.SeqArr[MakeSeqPos++] = ITLI_FULLAUTO_X1H_ON;
  
  SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_OFF;
  
  if (RobotCfg.MotionMode.OutsideWait == USE)
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SWING_RETURN_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING_RETURN;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_RETURN_DONE_X15_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_DONE_X14_OFF;
#ifdef SWING_IO_OFF
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_OFF;
#endif
  }
  //DOWN
  SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
  ControlMsg.StepDispTemp[AutoSetpSEQPos++] = DOWN_STEP;
  SeqMsg.SeqArr[MakeSeqPos++] = DELAY_DOWN;
  if (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_OFF;
  }
  else if (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_OFF;
  }
  else if (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_OFF;
  }
  //Vauum ON
  if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainVaccum == USE))
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_ON;										
  //L type Kick 
  if ((RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE))
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = KICK_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_KICK;					
    
    if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
    {
      if(RobotCfg.MotionMode.MainArmType == ARM_L_TYPE)
      {
        if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
        else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
      }
    }
    if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
    {
      if (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE)
      {
        if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
        else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
      }
    }
  }
  //Ejector
  if (RobotCfg.Function.Ejector == FUNC_ON)
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = EJECTOR_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_EJECTOR;
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_EJECTOR_Y2B_ON;
  }
  //TakeOut
  SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
  ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_STEP;
  SeqMsg.SeqArr[MakeSeqPos++] = DELAY_CHUCK;		
  
  if((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainChuck == USE))
  {
    if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
      SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_ON; 
  }
  if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
  {
    if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
      SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_ON;
  }
  
  //Kick Return L type OR Kick U type
  if((RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE))
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = KICK_RETURN_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_KICK_RETURN;
    if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainArmType == ARM_L_TYPE))
    {
      if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
      else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
    }
    if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE))
    {
      if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
      else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
    }
  }
  if ((RobotCfg.MotionMode.MainArmType == ARM_U_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE))
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = KICK_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_KICK;
    if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainArmType == ARM_U_TYPE))
    {
      if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
      else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
    }
    if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE))
    {
      if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
      else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
    }
  }
  //InMold Release
  if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
       && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_INMOLD)
                                                       || ((RobotCfg.Setting.ItlReject == USE)
                                                           && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
           || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_INMOLD)
                                                           || ((RobotCfg.Setting.ItlReject == USE)
                                                               && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))))
      || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_INMOLD)))
  {
    //Detection
    if (RobotCfg.Function.Detection == FUNC_ON)
    {
      if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
      {
        if ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_INMOLD)
                                                       || ((RobotCfg.Setting.ItlReject == USE)
                                                           && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_CHUCK_OK_X16_ON; 
        if ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_INMOLD)
                                                        || ((RobotCfg.Setting.ItlReject == USE)
                                                            && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_VACCUM_CHECK_X17_ON;
      }
      if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_INMOLD))
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_CHUCK_OK_X1F_ON;
    }
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHOUCK_RELEASE_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
  }
  if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
      && ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_INMOLD)
                                                     || ((RobotCfg.Setting.ItlReject == USE)
                                                         && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON)))))
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
  if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
      && ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_INMOLD)
                                                      || ((RobotCfg.Setting.ItlReject == USE)
                                                          && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON)))))
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
  if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_INMOLD))
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_OFF;
  //UP
  SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
  ControlMsg.StepDispTemp[AutoSetpSEQPos++] = UP_STEP;
  SeqMsg.SeqArr[MakeSeqPos++] = DELAY_UP;
  if (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_ON;
  }
  else if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_ON;
  }
  else if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
  {
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_ON;
  }
  //OutsideWait NO Use
  if (RobotCfg.MotionMode.OutsideWait == NO_USE)
  {
    //Detection
    if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (((RobotCfg.MotionMode.ChuckOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.ChuckOff != RELEASE_POS_NO_USE)) || ((RobotCfg.MotionMode.VaccumOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.VaccumOff != RELEASE_POS_NO_USE))
                                                              || ((RobotCfg.MotionMode.ChuckRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.ChuckRejectPos != RELEASE_POS_NO_USE))
                                                                || ((RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_NO_USE)))
        || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && ((RobotCfg.MotionMode.SubArmOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.SubArmOff != RELEASE_POS_NO_USE))))
    {
      if (RobotCfg.Function.Detection == FUNC_ON)
      {
        if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
        {
          if ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.ChuckOff != RELEASE_POS_NO_USE)))
            SeqMsg.SeqArr[MakeSeqPos++] = INPUT_CHUCK_OK_X16_ON; 
          if ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.VaccumOff != RELEASE_POS_NO_USE)))
            SeqMsg.SeqArr[MakeSeqPos++] = INPUT_VACCUM_CHECK_X17_ON;
        }
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && ((RobotCfg.MotionMode.SubArmOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.SubArmOff != RELEASE_POS_NO_USE)))
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_CHUCK_OK_X1F_ON;
      }
    }
    //Interlock ON
    if (RobotCfg.Function.Ejector == FUNC_ON)
    {
      SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_OFF;
      SeqMsg.SeqArr[MakeSeqPos++] = ITLO_EJECTOR_Y2B_OFF;
    }
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_CYCLE_START_Y29_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_ON;
  }
  else if (RobotCfg.MotionMode.OutsideWait == USE)
  {
    //Interlock ON
    if (RobotCfg.Function.Ejector == FUNC_ON)
    {
      SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_OFF;
      SeqMsg.SeqArr[MakeSeqPos++] = ITLO_EJECTOR_Y2B_OFF;
    }
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_CYCLE_START_Y29_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_ON;
  }
  //IMMType
  if ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.MainRotateChuck == USE) && (RobotCfg.Setting.IMMType == VERTI) && (RobotCfg.Setting.RotationState == NO_ROTATION)
      && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_ChuRo)
                                                      || ((RobotCfg.Setting.ItlReject == USE)
                                                          && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_ChuRo) && (RobotCfg.Function.Reject == SIGNAL_ON) && (IOMsg.X1B_Reject == SIGNAL_ON))))
          || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_ChuRo)
                                                          || ((RobotCfg.Setting.ItlReject == USE)
                                                              && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_ChuRo) && (RobotCfg.Function.Reject == SIGNAL_ON) && (IOMsg.X1B_Reject == SIGNAL_ON))))))
  {
    //Chuck Rotation
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos-1;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_ROTATION_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_ROTATE_CHUCK_Y24_ON;
    
    //IMMType Verti type Chuck Rotation OFF
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHOUCK_RELEASE_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
    
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
    
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_ROTATION_RETURN_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_CHUCK_ROTATE_RETURN;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_ROTATE_CHUCK_Y24_OFF;
  }
  else
  {
    //Not In Mold Release
    if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (((RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.ChuckOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.ChuckOff != RELEASE_POS_NO_USE)) 
                                                              || ((RobotCfg.MotionMode.MainVaccum == USE) && (RobotCfg.MotionMode.VaccumOff != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.VaccumOff != RELEASE_POS_NO_USE))
                                                                || ((RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.ChuckRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.ChuckRejectPos != RELEASE_POS_NO_USE))
                                                                  || ((RobotCfg.MotionMode.MainVaccum == USE) && (RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_NO_USE)))
        || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && ((RobotCfg.MotionMode.SubArmOff != RELEASE_POS_INMOLD)) && (RobotCfg.MotionMode.SubArmOff != RELEASE_POS_NO_USE)))
    {
      //Kick MS L type
      if((RobotCfg.RobotType == ROBOT_TYPE_TWIN) && (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS))
      {
        if ((RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE))
        {
          SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
          ControlMsg.StepDispTemp[AutoSetpSEQPos++] = KICK_STEP;
          SeqMsg.SeqArr[MakeSeqPos++] = DELAY_KICK;
          
          if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)
          {
            if(RobotCfg.MotionMode.MainArmType == ARM_L_TYPE)
            {
              if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
                SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
              else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
                SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
            }
            if (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE)
            {
              if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
                SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
              else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
                SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
            }
          }
        }
      }
      //Rotation
      SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
      ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SWING_STEP;
      SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING;
      SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_OFF;
      SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_ON;
      SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_DONE_X14_ON;
      SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_RETURN_DONE_X15_OFF;
#ifdef SWING_IO_OFF
      SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_OFF;
#endif
      //OutsideWait USE
      if (RobotCfg.MotionMode.OutsideWait == USE)
      {
        //Detection
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && ((RobotCfg.MotionMode.ChuckOff != RELEASE_POS_INMOLD) || (RobotCfg.MotionMode.VaccumOff != RELEASE_POS_INMOLD)
                                                                  || (RobotCfg.MotionMode.ChuckRejectPos != RELEASE_POS_INMOLD) 
                                                                    || (RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_INMOLD))
            || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff != RELEASE_POS_INMOLD)))
        {
          if (RobotCfg.Function.Detection == FUNC_ON)
          {
            if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
            {
              if (RobotCfg.MotionMode.MainChuck == USE)
                SeqMsg.SeqArr[MakeSeqPos++] = INPUT_CHUCK_OK_X16_ON; 
              if (RobotCfg.MotionMode.MainVaccum == USE)
                SeqMsg.SeqArr[MakeSeqPos++] = INPUT_VACCUM_CHECK_X17_ON;
            }
            if (RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
              SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_CHUCK_OK_X1F_ON;
          }
        }
        //Interlock
        if (RobotCfg.Function.Ejector == FUNC_ON)
        {
          SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_OFF;
          SeqMsg.SeqArr[MakeSeqPos++] = ITLO_EJECTOR_Y2B_OFF;
        }
        SeqMsg.SeqArr[MakeSeqPos++] = ITLO_CYCLE_START_Y29_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = ITLO_MOLD_OPENCLOSE_Y2A_ON;
      }
      //Chuck Rotation
      if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainRotateChuck == USE)) 
          && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff != RELEASE_POS_INMOLD)
                                                          || ((RobotCfg.Setting.ItlReject == USE)
                                                              && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
              || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff != RELEASE_POS_INMOLD)
                                                              || ((RobotCfg.Setting.ItlReject == USE)
                                                                  && (RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))))
      {
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos-1;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_ROTATION_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_ROTATE_CHUCK_Y24_ON;
      }
      //IMMType Verti type Chuck Rotation OFF
      if (((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.MainRotateChuck == USE) && (RobotCfg.Setting.IMMType == VERTI) && (RobotCfg.Setting.RotationState == ROTATION))
          && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_ChuRo)
                                                          || ((RobotCfg.Setting.ItlReject == USE)
                                                              && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_ChuRo) && (RobotCfg.Function.Reject == FUNC_ON) && (IOMsg.X1B_Reject == SIGNAL_ON))))
              || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_ChuRo)
                                                              || ((RobotCfg.Setting.ItlReject == USE)
                                                                  && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_ChuRo) && (RobotCfg.Function.Reject == FUNC_ON) && (IOMsg.X1B_Reject == SIGNAL_ON))))))
      {
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHOUCK_RELEASE_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        if ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_ChuRo)
                                                       || ((RobotCfg.Setting.ItlReject == USE)
                                                           && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_ChuRo) && (RobotCfg.Function.Reject == FUNC_ON))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
        if ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_ChuRo)
                                                        || ((RobotCfg.Setting.ItlReject == USE)
                                                            && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_ChuRo) && (RobotCfg.Function.Reject == FUNC_ON))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
      }
      //Special case
      if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && 
         ((RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_OUTSIDE)) 
           && ((RobotCfg.MotionMode.MainVaccum == USE) && (RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN))
             && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN))
      {
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_DOWN_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_DOWN2ND;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHOUCK_RELEASE_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_DOWN_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_DOWN2ND;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHOUCK_RELEASE_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_UP_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_UP2ND;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_ON;
      }
      else if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && 
              ((RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP))
                && ((RobotCfg.MotionMode.MainVaccum == USE) && (RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN))
                  && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN))
      {
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_DOWN_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_DOWN2ND;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHOUCK_RELEASE_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_UP_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_UP2ND;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_ON;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHOUCK_RELEASE_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_OFF;
        
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_UP_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_UP2ND;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_ON;
      }
      else
      {
        //Rotation Release
        if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
             && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_OUTSIDE)
                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                 && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_OUTSIDE) && (RobotCfg.Function.Reject == FUNC_ON))))
                 || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_OUTSIDE)
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_OUTSIDE) && (RobotCfg.Function.Reject == FUNC_ON))))))
            || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_OUTSIDE)))
        {
          SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
          ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHOUCK_RELEASE_STEP;
          SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        }
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
            && ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_OUTSIDE)
                                                           || ((RobotCfg.Setting.ItlReject == USE)
                                                               && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_OUTSIDE) && (RobotCfg.Function.Reject == FUNC_ON)))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
            && ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_OUTSIDE)
                                                            || ((RobotCfg.Setting.ItlReject == USE)
                                                                && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_OUTSIDE) && (RobotCfg.Function.Reject == FUNC_ON)))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_OUTSIDE))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_OFF;
        //2nd Down
        if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
             && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN)
                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                 && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN)
                                                                   && (RobotCfg.Function.Reject == FUNC_ON))))
                 || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN)
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN)
                                                                       && (RobotCfg.Function.Reject == FUNC_ON))))))
            || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN)))
        {
          SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
          ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_DOWN_STEP;
          SeqMsg.SeqArr[MakeSeqPos++] = DELAY_DOWN2ND;
        }
        if (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) 
        {
          if (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN)
                                                          || ((RobotCfg.Setting.ItlReject == USE) 
                                                              && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN) 
                                                                && (RobotCfg.Function.Reject == FUNC_ON))))
              || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN)
                                                              || ((RobotCfg.Setting.ItlReject == USE)
                                                                  && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN)
                                                                    && (RobotCfg.Function.Reject == FUNC_ON)))))
          {
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_ON;
            SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_OFF;
          }
          if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN))
          {
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_ON;
            SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_OFF;
          }
        }
        else if ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) 
                 && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN)
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN)
                                                                       && (RobotCfg.Function.Reject == FUNC_ON))))
                     || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN)
                                                                     || ((RobotCfg.Setting.ItlReject == USE)
                                                                         && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN)
                                                                           && (RobotCfg.Function.Reject == FUNC_ON))))))
        {
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_ON;
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_OFF;
        }
        else if ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN))
        {
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_ON;
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_OFF;
        }
        //Nipper ON
        //  if ((RobotCfg.MotionMode.MainNipper == USE) && (RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.MainVaccum == NO_USE))
        //  {
        //	 SeqMsg.SeqArr[MakeSeqPos++] = DELAY_NIPPER;
        //	 SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_NIPPER_Y26_ON;
        //  }
        //2nd Down Release
        if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
             && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN)
                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                 && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN) && (RobotCfg.Function.Reject == FUNC_ON))))
                 || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN)
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN) && (RobotCfg.Function.Reject == FUNC_ON))))))
            || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN)))
        {
          SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
          ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHOUCK_RELEASE_STEP;
          SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        }
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
            && ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN)
                                                           || ((RobotCfg.Setting.ItlReject == USE)
                                                               && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN) && (RobotCfg.Function.Reject == FUNC_ON)))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
            && ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN)
                                                            || ((RobotCfg.Setting.ItlReject == USE)
                                                                && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN) && (RobotCfg.Function.Reject == FUNC_ON)))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_OFF;
        //2nd UP
        if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) 
             && (((RobotCfg.MotionMode.MainChuck == USE) && (((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP)) 
                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                 && ((RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndUP)) 
                                                                   && (RobotCfg.Function.Reject == FUNC_ON))))
                 || ((RobotCfg.MotionMode.MainVaccum == USE) && (((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN) ||(RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndUP))
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && ((RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndUP))
                                                                       && (RobotCfg.Function.Reject == FUNC_ON))))))
            || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && ((RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndUP))))
        {
          SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
          ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SECOND_UP_STEP;
          SeqMsg.SeqArr[MakeSeqPos++] = DELAY_UP2ND;
        }
        if (((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) 
             && (((RobotCfg.MotionMode.MainChuck == USE) && (((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP)) 
                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                 && ((RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndUP)) 
                                                                   && (RobotCfg.Function.Reject == FUNC_ON))))
                 || ((RobotCfg.MotionMode.MainVaccum == USE) && (((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN) ||(RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndUP))
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && ((RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndUP))
                                                                       && (RobotCfg.Function.Reject == FUNC_ON))))))
            || ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && ((RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndUP))))
        {
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_OFF;
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_OFF;
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_ON;
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_ON;
        }
        else if ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) 
                 && (((RobotCfg.MotionMode.MainChuck == USE) && (((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP)) 
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && ((RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndUP)) 
                                                                       && (RobotCfg.Function.Reject == FUNC_ON))))
                     || ((RobotCfg.MotionMode.MainVaccum == USE) && (((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndDOWN) ||(RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndUP))
                                                                     || ((RobotCfg.Setting.ItlReject == USE)
                                                                         && ((RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndUP))
                                                                           && (RobotCfg.Function.Reject == FUNC_ON))))))
        {
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_UPDOWN_Y20_OFF;
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_UP_COMPLETE_X11_ON;
        }
        else if ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) && ((RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndDOWN) || (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndUP)))
        {
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_UPDOWN_Y2D_OFF;
          SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SUB_UP_COMPLETE_X1G_ON;
        }
        //Nipper OFF
        //  if ((RobotCfg.MotionMode.MainNipper == USE) && (RobotCfg.MotionMode.MainChuck == USE) && (RobotCfg.MotionMode.MainVaccum == NO_USE))
        //	 SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_NIPPER_Y26_OFF;
        
        //2nd UP Release
        if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP)
                                                                                                               || ((RobotCfg.Setting.ItlReject == USE) 
                                                                                                                   && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndUP) && (RobotCfg.Function.Reject == FUNC_ON))))
                                                                   || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndUP)
                                                                                                                   || ((RobotCfg.Setting.ItlReject == USE)
                                                                                                                       && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndUP) && (RobotCfg.Function.Reject == FUNC_ON))))))
            || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndUP)))
        {
          SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
          ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHOUCK_RELEASE_STEP;
          SeqMsg.SeqArr[MakeSeqPos++] = DELAY_OPEN;
        }
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && ((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_2ndUP)
                                                                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                                                                 && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_2ndUP) && (RobotCfg.Function.Reject == FUNC_ON)))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_CHUCK_Y22_OFF;
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_2ndUP)
                                                                                                              || ((RobotCfg.Setting.ItlReject == USE)
                                                                                                                  && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_2ndUP) && (RobotCfg.Function.Reject == FUNC_ON)))))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_VACCUM_Y25_OFF;
        if ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_2ndUP))
          SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_CHUCK_Y27_OFF;
      }
      
      //Converyor
      SeqMsg.SeqArr[MakeSeqPos++] = ITLO_CONVEYOR_Y2C_ON;
      
      //Chuck Rotation Return
      if (((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.MainRotateChuck == USE)) 
          && (((RobotCfg.MotionMode.MainChuck == USE) && ((RobotCfg.MotionMode.ChuckOff != RELEASE_POS_INMOLD)
                                                          || ((RobotCfg.Setting.ItlReject == USE)
                                                              && (RobotCfg.MotionMode.ChuckRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
              || ((RobotCfg.MotionMode.MainVaccum == USE) && ((RobotCfg.MotionMode.VaccumOff != RELEASE_POS_INMOLD)
                                                              || ((RobotCfg.Setting.ItlReject == USE)
                                                                  && (RobotCfg.MotionMode.VaccumRejectPos != RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))))
      {
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = CHUCK_ROTATION_RETURN_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_CHUCK_ROTATE_RETURN;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_ROTATE_CHUCK_Y24_OFF;
      }
      //SwingReturn
      if (RobotCfg.MotionMode.OutsideWait == NO_USE)
      {
        SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
        ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SWING_RETURN_STEP;
        SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING_RETURN;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_OFF;
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_RETURN_DONE_X15_ON;
        SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_DONE_X14_OFF;
#ifdef SWING_IO_OFF
        SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_OFF;
#endif
      }
    }
  }
  //Twin L type OR U type Kick Return
  if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)
  {
    if ((RobotCfg.MotionMode.MainArmType == ARM_U_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE) ||
        (RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) || (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE))
    {
      SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
      ControlMsg.StepDispTemp[AutoSetpSEQPos++] = KICK_RETURN_STEP;
      SeqMsg.SeqArr[MakeSeqPos++] = DELAY_KICK_RETURN;
      if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
      {
        if (RobotCfg.MotionMode.MainArmType == ARM_U_TYPE)
        {
          if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
          else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
        }
        else if (RobotCfg.MotionMode.MainArmType == ARM_L_TYPE)
        {
          if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
          else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
        }
      }
      if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
      {
        if (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE)
        {
          if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
          else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
        }
        else if (RobotCfg.MotionMode.SubArmType == ARM_L_TYPE)
        {
          if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
          else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
        }
      }
    }
  }
  else 
  {
    //U type Kick Return
    if (((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) && (RobotCfg.MotionMode.MainArmType == ARM_U_TYPE)) || 
        ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE)))
    {
      SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
      ControlMsg.StepDispTemp[AutoSetpSEQPos++] = KICK_RETURN_STEP;
      SeqMsg.SeqArr[MakeSeqPos++] = DELAY_KICK_RETURN;
      
      if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB)
      {
        if(RobotCfg.MotionMode.MainArmType == ARM_U_TYPE)
        {
          if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_OFF;
          else if (RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_KICK_Y21_ON;
        }
      }
      if(RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN)
      {
        if (RobotCfg.MotionMode.SubArmType == ARM_U_TYPE)
        {
          if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_ON;
          else if (RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE)
            SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SUB_KICK_Y2E_OFF;
        }
      }
    }
  }
  //Outside Wait Swing
  if ((((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) 
        && (((RobotCfg.MotionMode.MainChuck == USE) && (((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_INMOLD) || (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_NO_USE))
                                                        || ((RobotCfg.Setting.ItlReject == USE)
                                                            && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
            && ((RobotCfg.MotionMode.MainVaccum == USE) && (((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_INMOLD) || (RobotCfg.MotionMode.VaccumOff == RELEASE_POS_NO_USE))
                                                            || ((RobotCfg.Setting.ItlReject == USE)
                                                                && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))))
       && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_INMOLD))
      && (RobotCfg.MotionMode.OutsideWait == USE))
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SWING_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_DONE_X14_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_RETURN_DONE_X15_OFF;
#ifdef SWING_IO_OFF
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_OFF;
#endif
  }
  else if ((((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) 
             && (((RobotCfg.MotionMode.MainChuck == USE) && (((RobotCfg.MotionMode.ChuckOff == RELEASE_POS_INMOLD) || (RobotCfg.MotionMode.ChuckOff == RELEASE_POS_NO_USE))
                                                             || ((RobotCfg.Setting.ItlReject == USE)
                                                                 && (RobotCfg.MotionMode.ChuckRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON))))
                 || ((RobotCfg.MotionMode.MainVaccum == USE) && (((RobotCfg.MotionMode.VaccumOff == RELEASE_POS_INMOLD) || (RobotCfg.MotionMode.VaccumOff == RELEASE_POS_NO_USE))
                                                                 || ((RobotCfg.Setting.ItlReject == USE)
                                                                     && (RobotCfg.MotionMode.VaccumRejectPos == RELEASE_POS_INMOLD) && (RobotCfg.Function.Reject == FUNC_ON)))))))
           && (RobotCfg.MotionMode.OutsideWait == USE))
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SWING_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_DONE_X14_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_RETURN_DONE_X15_OFF;
#ifdef SWING_IO_OFF
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_OFF;
#endif
  }
  else if (((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) && (RobotCfg.MotionMode.SubArmOff == RELEASE_POS_INMOLD))
           && (RobotCfg.MotionMode.OutsideWait == USE))
  {
    SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
    ControlMsg.StepDispTemp[AutoSetpSEQPos++] = SWING_STEP;
    SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_RETURN_Y2F_OFF;
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_DONE_X14_ON;
    SeqMsg.SeqArr[MakeSeqPos++] = INPUT_SWING_RETURN_DONE_X15_OFF;
#ifdef SWING_IO_OFF
    SeqMsg.SeqArr[MakeSeqPos++] = OUTPUT_SWING_Y23_OFF;
#endif
  }
  
  SeqMsg.SeqDispArr[StepSEQPos++] = SEQ_END;
  SeqMsg.SeqArr[MakeSeqPos] = SEQ_END;
  //Total Step Count
  ControlMsg.StepDispTemp[19] = AutoSetpSEQPos;
}

void StepCheck(void)
{
  for (uint8_t AutoStepSetting = 0 ; AutoStepSetting < MAX_INDEX ; AutoStepSetting++)
  {
    if (ControlMsg.StepDispTemp[AutoStepSetting] == DOWN_STEP)
    {
      AutoStep[AutoStepSetting] = DOWN_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.DownDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == KICK_STEP)
    {
      AutoStep[AutoStepSetting] = KICK_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.KickDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == EJECTOR_STEP)
    {
      AutoStep[AutoStepSetting] = EJECTOR_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.EjectorDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == CHUCK_STEP)
    {
      AutoStep[AutoStepSetting] = CHUCK_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.ChuckDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == KICK_RETURN_STEP)
    {
      AutoStep[AutoStepSetting] = KICK_RETURN_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.KickReturnDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == UP_STEP)
    {
      AutoStep[AutoStepSetting] = UP_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.UpDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == SWING_STEP)
    {
      AutoStep[AutoStepSetting] = SWING_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.SwingDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == CHUCK_ROTATION_STEP)
    {
      AutoStep[AutoStepSetting] = 0xff;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == SECOND_DOWN_STEP)
    {
      AutoStep[AutoStepSetting] = SECOND_DOWN_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.Down2ndDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == CHOUCK_RELEASE_STEP)
    {
      AutoStep[AutoStepSetting] = RELEASE_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.OpenDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == SECOND_UP_STEP)
    {
      AutoStep[AutoStepSetting] = SECOND_UP_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.Up2ndDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == CHUCK_ROTATION_RETURN_STEP)
    {
      AutoStep[AutoStepSetting] = CHUCK_ROTATION_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.ChuckRotateReturnDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == SWING_RETURN_STEP)
    {
      AutoStep[AutoStepSetting] = SWING_RETURN_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.SwingReturnDelay;
    }
    else if (ControlMsg.StepDispTemp[AutoStepSetting] == NIPPERCUT_STEP)
    {
      AutoStep[AutoStepSetting] = NIPPER_TIME;
      AutoStepDelay[AutoStepSetting] = CycleDelay.NipperOnDelay;
    }
    else if (SWING_RETURN_STEP < ControlMsg.StepDispTemp[AutoStepSetting])
    {
      AutoStep[AutoStepSetting] = 0xff;
      AutoStepDelay[AutoStepSetting] = 0xff;
    }
  }
}



void OpenMold(uint16_t MoldNo)
{
  ReadMold(&MoldMsg,MoldNo);
  memcpy(&(RobotCfg.Count), &(MoldMsg.Count), sizeof(COUNT_MSG));
  memcpy(&(RobotCfg.Function), &(MoldMsg.Function), sizeof(FUNC_MSG));
  memcpy(&(RobotCfg.MoldName), &(MoldMsg.MoldName), sizeof(MoldMsg.MoldName));
  memcpy(&(RobotCfg.MoldNo), &(MoldMsg.MoldNo), sizeof(MoldMsg.MoldNo));
  memcpy(&(RobotCfg.MotionDelay), &(MoldMsg.MotionDelay), sizeof(MOTION_DELAY));
  memcpy(&(RobotCfg.MotionMode), &(MoldMsg.MotionMode), sizeof(MOTION_MODE));
  TimerDispCheck();
  ModeDispCheck(MoldMsg.MoldNo);
  StepCheck();
  MakeSequence();
}

void WriteFunction(void)
{
  memcpy(&(MoldMsg.Function), &RobotCfg.Function, sizeof(FUNC_MSG));
  memset(SeqMsg.SeqDispArr,0,sizeof(SeqMsg.SeqDispArr));
  memset(ControlMsg.StepDispTemp,0,sizeof(ControlMsg.StepDispTemp));
  memset(SeqMsg.SeqArr,0,sizeof(SeqMsg.SeqArr));
  //  WriteMold(&MoldMsg);
  WriteRobotCfg(&RobotCfg);
  MakeSequence();
}

void ResetAutoSetting(void)
{
  StepCheck();
  ControlMsg.AutoPageNo = 0;
  ControlMsg.AutoLineNo = 0;
  AutoStepPos = 0;
}

void AutoState(uint8_t AutoState)
{
  static enum
  {
    AUTO_INIT = 0,
    AUTO_OPERATION,
    AUTO_NEXT_STEP,
    AUTO_TIMER_RESET,
  }AutoLoopState = AUTO_INIT;
  
  
  //  SeqMsg.SeqDispArr[StepSEQPos++] = MakeSeqPos;
  //  SeqMsg.SeqArr[MakeSeqPos++] = DELAY_SWING;
  //  
  
  switch(AutoLoopState)  
  {
  case AUTO_INIT:
    SetSysTick(&AutoTempTick,TIME_CYCLE);
    if((SeqMsg.SeqArr[SeqMsg.SeqPos] == (AutoStep[AutoStepPos]+DELAY_DOWN)) || (AutoStep[AutoStepPos] == SEQ_END))
    {
      AutoLoopState = AUTO_OPERATION;
      //      printf("***1-1  ");
    }
    else if((SeqMsg.SeqDispArr[AutoStepPos] < SeqMsg.SeqPos) && (AutoStepPos != 0)) { 
      AutoLoopState = AUTO_OPERATION; 
      //      printf("***1-3  "); 
    }
    else if((SeqMsg.SeqDispArr[0] > SeqMsg.SeqPos)) { 
      if((AutoStepPos != 0)){
        AutoLoopState = AUTO_OPERATION;
        //        printf("***1-2-1  ");
      }
      else{
        //        printf("***1-2-2  ");
      }
    }
    else if((SeqMsg.SeqDispArr[AutoStepPos] < SeqMsg.SeqPos) && (SeqMsg.SeqDispArr[AutoStepPos+2] > SeqMsg.SeqPos) && (AutoStepPos == 0)){
      AutoLoopState = AUTO_OPERATION;
      //      printf("***1-4  ");
    }
    
    //    printf("%d -- %d -- %d -- %d     ", (SeqMsg.SeqDispArr[AutoStepPos]), SeqMsg.SeqDispArr[SCStepPos+1] ,( SeqMsg.SeqPos),AutoStepPos);
    
    break;
    
  case AUTO_TIMER_RESET:
    SetSysTick(&AutoTempTick,TIME_CYCLE);
    AutoLoopState = AUTO_OPERATION;
    break;
    
  case AUTO_NEXT_STEP:
    if((SeqMsg.SeqArr[SeqMsg.SeqPos] == (AutoStep[AutoStepPos]+DELAY_DOWN)) || (AutoStep[AutoStepPos] == SEQ_END))
    {
      AutoLoopState = AUTO_OPERATION;
      //      printf("***2-1  ");
    }
    else if((SeqMsg.SeqDispArr[AutoStepPos] < SeqMsg.SeqPos) && (AutoStepPos != 0)) { 
      AutoLoopState = AUTO_OPERATION; 
      //      printf("***2-3  "); 
    }
    else if((SeqMsg.SeqDispArr[0] > SeqMsg.SeqPos)) { 
      if((AutoStepPos != 0)){
        AutoLoopState = AUTO_OPERATION;
        //        printf("***2-2-1  ");
      }
      else{
        //        printf("***2-2-2  ");
      }
    }
    else if((SeqMsg.SeqDispArr[AutoStepPos] < SeqMsg.SeqPos) && (SeqMsg.SeqDispArr[AutoStepPos+2] > SeqMsg.SeqPos) && (AutoStepPos == 0)){
      AutoLoopState = AUTO_OPERATION;
      //      printf("***2-4  ");
    }
    
    //    printf("%d -- %d -- %d -- %d     ", (SeqMsg.SeqDispArr[AutoStepPos]), SeqMsg.SeqDispArr[SCStepPos+1] ,( SeqMsg.SeqPos),AutoStepPos);
    break;
    
  case AUTO_OPERATION:
    if(!(ChkExpireSysTick(&AutoTempTick))) break;
    if(AutoStepDelay[AutoStepPos] > 99)	//201123
    {
      AutoStepDelay[AutoStepPos] = 0;
      AutoLoopState = AUTO_TIMER_RESET;
    }
    if(AutoStepDelay[AutoStepPos] != 0)	//Current Step Delay -0.1s
    {
      AutoStepDelay[AutoStepPos] = AutoStepDelay[AutoStepPos]-1;
      AutoLoopState = AUTO_TIMER_RESET;
    }
    else	//Next Step
    {
      AutoLoopState = AUTO_NEXT_STEP;
      AutoStepPos++;
      if(ControlMsg.AutoLineNo < MAX_LINE_TITLE)	//Page Last Line Check
      {
        //Last Page And Last Line Step Check
        if ((ControlMsg.AutoPageNo*MAX_LINE_OUTLINE + ControlMsg.AutoLineNo) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX] -1)
          ControlMsg.AutoLineNo++;	//Next Line
        else
        {
          if (AutoState == MODE_AUTO_OP) ResetAutoSetting();
          else if (AutoState == MODE_CYCLE_OP) 
          {
            AutoLoopState = AUTO_INIT;
            ClearAuto();
            ControlMsg.TpMode = MANUAL_OPERATING;
          }
        }
      }
      else	//Last Page And Last Line Step Check
      {
        if ((ControlMsg.AutoPageNo == ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX]/MAX_LINE_OUTLINE -1) &&
            (ControlMsg.AutoLineNo == MAX_LINE_TITLE) && (ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX]%MAX_LINE_OUTLINE == 0))
        {
          if (AutoState == MODE_AUTO_OP) ResetAutoSetting();
          else if (AutoState == MODE_CYCLE_OP) 
          {
            AutoLoopState = AUTO_INIT;
            ClearAuto();
            ControlMsg.TpMode = MANUAL_OPERATING;
          }
        }
        else
        {
          //Next Page
          ControlMsg.AutoPageNo++;
          ControlMsg.AutoLineNo = 0;
        }
      }
    }
    break;
  }
  //Count
  if (AutoState == MODE_AUTO_OP) 
  {
    if((CountMsg.TotalCnt != RobotCfg.Count.TotalCnt) || (CountMsg.DetectFail != RobotCfg.Count.DetectFail) || (CountMsg.RejectCnt != RobotCfg.Count.RejectCnt))
    {
      CountMsg.TotalCnt = RobotCfg.Count.TotalCnt;
      CountMsg.DetectFail = RobotCfg.Count.DetectFail;
      CountMsg.RejectCnt = RobotCfg.Count.RejectCnt;
      
      //		memcpy(&(MoldMsg.Count), &CountMsg, sizeof(CountMsg));
      //		//		WriteMold(&MoldMsg);
      //		WriteRobotCfg(&RobotCfg);
      //		if(ControlMsg.TpMode == AUTO_OPERATING) 
      //		{
      //		  fPreviousPage = AUTO_PAGE_SD;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);	
      //		  ControlMsg.TpMode = SD_BACKUP;
      //		}
      //		else if(ControlMsg.TpMode == AUTO_COUNTER) 
      //		{
      //		  fPreviousPage = AUTO_COUNTER_PAGE_SD;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);	
      //		  ControlMsg.TpMode = SD_BACKUP;
      //		}
      //		else if(ControlMsg.TpMode == AUTO_TIMER) 
      //		{
      //		  fPreviousPage = AUTO_TIMER_PAGE_SD;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);	
      //		  ControlMsg.TpMode = SD_BACKUP;
      //		}
    }
    if((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_OFF))
    {
      memcpy(&(MoldMsg.Count), &CountMsg, sizeof(CountMsg));
      //		WriteMold(&MoldMsg);
      WriteRobotCfg(&RobotCfg);
      if(ControlMsg.TpMode == AUTO_OPERATING) 
      {
        fPreviousPage = MANUAL_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      //		else if(ControlMsg.TpMode == AUTO_COUNTER) 
      //		{
      //		  fPreviousPage = MANUAL_PAGE_SD;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);	
      //		  ControlMsg.TpMode = SD_BACKUP;
      //		}
      //		else if(ControlMsg.TpMode == AUTO_TIMER) 
      //		{
      //		  fPreviousPage = MANUAL_PAGE_SD;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);	
      //		  ControlMsg.TpMode = SD_BACKUP;
      //		}
      ClearAuto();
      AutoLoopState = AUTO_INIT;
    }
  }
  //STOP
  if(ControlMsg.KeyValue == KEY_MENU_STOP)
  {
    memcpy(&(MoldMsg.Count), &CountMsg, sizeof(CountMsg));
    //		WriteMold(&MoldMsg);
    WriteRobotCfg(&RobotCfg);
    if(ControlMsg.TpMode == AUTO_OPERATING || ControlMsg.TpMode == AUTO_MODE || ControlMsg.TpMode == AUTO_COUNTER || 
       ControlMsg.TpMode == AUTO_IO || ControlMsg.TpMode == AUTO_TIMER || ControlMsg.TpMode == AUTO_ERROR_LIST)//201123
    {
      fPreviousPage = MANUAL_PAGE_SD;
      SetSysTick(&SDTempTick, TIME_SC_DELAY);	
      ControlMsg.TpMode = SD_BACKUP;
    }
    else if(ControlMsg.TpMode == MANUAL_CYCLE_OPERATING)	//201123
    {
      ControlMsg.TpMode = MANUAL_OPERATING;
      fPreviousPage = WAITING_SD;
      SetSysTick(&SDTempTick, TIME_SC_DELAY);	
    }
    //	 else if(ControlMsg.TpMode == AUTO_COUNTER) 
    //	 {
    //		fPreviousPage = MANUAL_PAGE_SD;
    //		SetSysTick(&SDTempTick, TIME_SC_DELAY);	
    //		ControlMsg.TpMode = SD_BACKUP;
    //	 }
    //	 else if(ControlMsg.TpMode == AUTO_TIMER) 
    //	 {
    //		fPreviousPage = MANUAL_PAGE_SD;
    //		SetSysTick(&SDTempTick, TIME_SC_DELAY);	
    //		ControlMsg.TpMode = SD_BACKUP;
    //	 }
    ClearAuto();
    AutoLoopState = AUTO_INIT;
  }
}

void ManualInterlock(void)
{
  IOMsg.Y2B_Ejector = SIGNAL_ON;
  if(RobotCfg.MotionMode.OutsideWait == USE)
  {
    if(IOMsg.X15_SwingReturnOk == SIGNAL_ON) IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
    else if (IOMsg.X14_SwingOk == SIGNAL_ON)	IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
  }
  else if(RobotCfg.MotionMode.OutsideWait == NO_USE)
  {
    if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)
    {
      if((IOMsg.X15_SwingReturnOk == SIGNAL_ON) && ((IOMsg.Y20_Down == SIGNAL_ON) || (IOMsg.Y2D_SubUp == SIGNAL_ON)))
        IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
      else IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
    }
    else if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN)
    {
      if((IOMsg.X15_SwingReturnOk == SIGNAL_ON) && (IOMsg.Y20_Down == SIGNAL_ON))
        IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
      else IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
    }
    else if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB)
    {
      if((IOMsg.X15_SwingReturnOk == SIGNAL_ON) && (IOMsg.Y2D_SubUp == SIGNAL_ON))
        IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
      else IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
    }
  }
#ifdef SWING_IO_OFF
  if(IOMsg.X15_SwingReturnOk == SIGNAL_ON) IOMsg.Y2F_SwingReturn = SIGNAL_OFF;
  else if(IOMsg.X14_SwingOk == SIGNAL_ON) IOMsg.Y23_Swing = SIGNAL_OFF;
#endif
  if((RobotCfg.Setting.ItlAutoInjection == USE) && (RobotCfg.Setting.ItlSaftyDoor == USE))
  {
    if(IOMsg.X19_AutoInjection == SIGNAL_ON) AutoInjectionTemp = SIGNAL_ON;
    if((AutoInjectionTemp == SIGNAL_ON) && (IOMsg.X18_MoldOpenComplete == SIGNAL_ON))
    {
      IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
      if(IOMsg.X1A_SaftyDoor == SIGNAL_OFF) 
      {
        AutoInjectionTemp = SIGNAL_OFF;
        IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
      }
    }
  }
  //  if((RobotCfg.Setting.ItlSaftyDoor == NO_USE) && (IOMsg.X18_MoldOpenComplete == SIGNAL_ON) && (IOMsg.Y2A_MoldOpenClose == SIGNAL_OFF))
  //  {
  //	 if((RobotCfg.MotionMode.OutsideWait == NO_USE) && (IOMsg.X11_MainArmUpComplete == SIGNAL_ON))
  //		IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
  //	 else if((RobotCfg.MotionMode.OutsideWait == USE) && (IOMsg.X11_MainArmUpComplete == SIGNAL_ON) && (IOMsg.X14_SwingOk))
  //		IOMsg.Y2A_MoldOpenClose = SIGNAL_ON;
  //  }
}
//printf("%d - %d /// ",RobotCfg.Setting.IMMType,RobotCfg.Setting.RotationState);
void ManualIO(void)
{
  if(ControlMsg.KeyValue == KEY_MAIN_CHUCK)
  {
    if(((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)) 
       && (RobotCfg.MotionMode.MainChuck == USE))
      IOMsg.Y22_Chuck ^= SIGNAL_ON;
  }
  else if(ControlMsg.KeyValue == KEY_MAIN_VACUUM)
  {
    if((RobotCfg.RobotType == ROBOT_TYPE_TWIN) || (RobotCfg.RobotType == ROBOT_TYPE_XC))
    {
      if(((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)) 
         && (RobotCfg.MotionMode.MainVaccum == USE))
        IOMsg.Y25_Vaccum ^= SIGNAL_ON;
    }
  }
  else if(ControlMsg.KeyValue == KEY_MAIN_CHUCK_ROTATE)
  {
    if(RobotCfg.RobotType != ROBOT_TYPE_A)
    {
      if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && (IOMsg.Y21_Kick == SIGNAL_OFF) && (IOMsg.Y24_ChuckRotation == SIGNAL_OFF))
      {
        RobotCfg.ErrCode = MANUAL_ERR_CHYCK_RO;
        ControlMsg.TpMode = MANUAL_ERROR;
      }
      else
      {
        if(((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)) 
           && (RobotCfg.MotionMode.MainRotateChuck == USE))
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE))
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
          {
            if(!((IOMsg.X15_SwingReturnOk == SIGNAL_ON) && (IOMsg.Y20_Down == SIGNAL_ON)))
              IOMsg.Y24_ChuckRotation ^= SIGNAL_ON;
          }
        }
      }
    }
  }
  else if(ControlMsg.KeyValue == KEY_MAIN_UPDOWN)
  {
    if(!((IOMsg.X18_MoldOpenComplete == SIGNAL_OFF) && (IOMsg.X15_SwingReturnOk == SIGNAL_ON)))
    {
      if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN))
      {
        if(RobotCfg.MotionMode.MainArmType == ARM_L_TYPE) 
        {
          if(((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y21_Kick == SIGNAL_OFF))
             || ((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE) && (IOMsg.Y21_Kick == SIGNAL_ON)))
          {
            if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
               || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            {
              if(!((IOMsg.X15_SwingReturnOk == SIGNAL_ON) && (IOMsg.Y24_ChuckRotation == SIGNAL_ON)))
                IOMsg.Y20_Down ^= SIGNAL_ON;
            }
          }
        }
        else 
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
          {
            if(!((IOMsg.X15_SwingReturnOk == SIGNAL_ON) && (IOMsg.Y24_ChuckRotation == SIGNAL_ON)))
              IOMsg.Y20_Down ^= SIGNAL_ON;
          }
        }
      }
    }
    else if(IOMsg.X18_MoldOpenComplete == SIGNAL_OFF)
    {
      RobotCfg.ErrCode = ERRCODE_MO_DOWN;
      RobotCfg.ErrorState = ERROR_STATE_OCCUR;
    }
  }
  else if(ControlMsg.KeyValue == KEY_MAIN_FW)
  {
    if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) && (IOMsg.Y21_Kick == SIGNAL_ON) && (IOMsg.Y24_ChuckRotation == SIGNAL_ON))
    {
      RobotCfg.ErrCode = MANUAL_ERR_CHYCK_ROR;
      ControlMsg.TpMode = MANUAL_ERROR;
    }
    else
    {
      if((RobotCfg.RobotType == ROBOT_TYPE_TWIN) && (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS))
      {
        if((IOMsg.X11_MainArmUpComplete == SIGNAL_OFF) && (IOMsg.X1G_SubUpComplete == SIGNAL_ON))
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE))
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            IOMsg.Y21_Kick ^= SIGNAL_ON;
        }
        else if((IOMsg.X11_MainArmUpComplete == SIGNAL_OFF) && (IOMsg.X1G_SubUpComplete == SIGNAL_OFF))
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE))
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
          {
            IOMsg.Y21_Kick ^= SIGNAL_ON;
            IOMsg.Y2E_SubKick ^= SIGNAL_ON;
          }
        }
        else if((IOMsg.X11_MainArmUpComplete == SIGNAL_ON) && (IOMsg.X1G_SubUpComplete == SIGNAL_ON))
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
          {
            IOMsg.Y21_Kick ^= SIGNAL_ON;
            IOMsg.Y2E_SubKick ^= SIGNAL_ON;
          }
        }
        else if((IOMsg.X11_MainArmUpComplete == SIGNAL_ON) && (IOMsg.X1G_SubUpComplete == SIGNAL_OFF))
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            IOMsg.Y2E_SubKick ^= SIGNAL_ON;
        }
      }
      else 
      {
        if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) 
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            IOMsg.Y21_Kick ^= SIGNAL_ON;
        }
        else if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) 
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            IOMsg.Y2E_SubKick ^= SIGNAL_ON;
        }
      }
    }
  }
  else if (ControlMsg.KeyValue == KEY_SWING)
  {
    //IMMType
	if (RobotCfg.Setting.RotationState == NO_ROTATION)
    {
      RobotCfg.ErrCode = ERRCODE_SWING;
      RobotCfg.ErrorState = ERROR_STATE_OCCUR;
    }
    else
    {
      if(((RobotCfg.RobotType == ROBOT_TYPE_TWIN) && (IOMsg.X11_MainArmUpComplete == SIGNAL_ON) && (IOMsg.X1G_SubUpComplete == SIGNAL_ON) && (IOMsg.Y20_Down == SIGNAL_OFF) && (IOMsg.Y2D_SubUp == SIGNAL_OFF))
         || ((RobotCfg.RobotType != ROBOT_TYPE_TWIN) && (IOMsg.X11_MainArmUpComplete == SIGNAL_ON) && (IOMsg.Y20_Down == SIGNAL_OFF)))
      {
        if(RobotCfg.MotionMode.OutsideWait == NO_USE)
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
          {
            if(IOMsg.Y23_Swing == SIGNAL_ON) 
            {
              IOMsg.Y23_Swing = SIGNAL_OFF;
              IOMsg.Y2F_SwingReturn = SIGNAL_ON;
            }
            else if(IOMsg.Y2F_SwingReturn == SIGNAL_ON) 
            {
              IOMsg.Y2F_SwingReturn = SIGNAL_OFF;
              IOMsg.Y23_Swing = SIGNAL_ON;
            }
            else if((IOMsg.Y2F_SwingReturn == SIGNAL_OFF) && (IOMsg.Y23_Swing == SIGNAL_OFF))
            {
              if(IOMsg.X14_SwingOk == SIGNAL_ON) IOMsg.Y2F_SwingReturn = SIGNAL_ON;
              else if(IOMsg.X15_SwingReturnOk == SIGNAL_ON) IOMsg.Y23_Swing = SIGNAL_ON;
            }
          }
        }
        else if(RobotCfg.MotionMode.OutsideWait == USE)
        {
          if(IOMsg.X18_MoldOpenComplete == SIGNAL_ON)
          {
            if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
               || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            {
              if(IOMsg.Y23_Swing == SIGNAL_ON) 
              {
                IOMsg.Y23_Swing = SIGNAL_OFF;
                IOMsg.Y2F_SwingReturn = SIGNAL_ON;
              }
              else if(IOMsg.Y2F_SwingReturn == SIGNAL_ON) 
              {
                IOMsg.Y2F_SwingReturn = SIGNAL_OFF;
                IOMsg.Y23_Swing = SIGNAL_ON;
              }
              else if((IOMsg.Y2F_SwingReturn == SIGNAL_OFF) && (IOMsg.Y23_Swing == SIGNAL_OFF))
              {
                if(IOMsg.X14_SwingOk == SIGNAL_ON) IOMsg.Y2F_SwingReturn = SIGNAL_ON;
                else if(IOMsg.X15_SwingReturnOk == SIGNAL_ON) IOMsg.Y23_Swing = SIGNAL_ON;
              }
            }
          }
        }
      }
    }
  }
  else if(ControlMsg.KeyValue == KEY_RUNNER_UPDOWN)
  {
    if(!((IOMsg.X18_MoldOpenComplete == SIGNAL_OFF) && (IOMsg.X15_SwingReturnOk == SIGNAL_ON)))
    {
      if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB))
      {
        if(RobotCfg.MotionMode.SubArmType == ARM_L_TYPE) 
        {
          if(((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y2E_SubKick == SIGNAL_ON))
             || ((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE) && (IOMsg.Y2E_SubKick == SIGNAL_OFF)))
          {
            if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
               || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
              IOMsg.Y2D_SubUp ^= SIGNAL_ON;
          }
        }
        else 
        {
          if(((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_ON) && (RobotCfg.Setting.DoorSignalChange == USE)) 
             || (RobotCfg.Setting.ItlSaftyDoor == NO_USE) || (RobotCfg.Setting.DoorSignalChange == NO_USE))
            IOMsg.Y2D_SubUp ^= SIGNAL_ON;
        }
      }
    }
    else if(IOMsg.X18_MoldOpenComplete == SIGNAL_OFF)
    {
      RobotCfg.ErrCode = ERRCODE_MO_DOWN;
      RobotCfg.ErrorState = ERROR_STATE_OCCUR;
    }
  }
  else if(ControlMsg.KeyValue == KEY_RUNNER_GRIPPER)
  {
    if(RobotCfg.RobotType == ROBOT_TYPE_TWIN)
    {
      if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)) 
        IOMsg.Y27_SubGrip ^= SIGNAL_ON;
    }
  }
  //Nipper
  //			 if(RobotCfg.RobotType == ROBOT_TYPE_XN)
  //			 {
  //				if(ControlMsg.KeyValue == KEY_RUNNER_UPDOWN)
  //				{
  //				  if(RobotCfg.MotionMode.MainNipper == USE) IOMsg.Y26_Nipper ^= SIGNAL_ON;
  //				} 
  //			 }
}

void LoopAppError(void)
{
  static enum
  {
    ERR_CHECK = 0,
    ERR_WRITE,
    ERR_TAKEOUT,
    ERR_EMO,
    ERR_SENSOR,
    ERR_CLEAR,
    ERR_CLEAR_ACCEPT,
  }ErrLoopState = ERR_CHECK;
  
  switch(ErrLoopState)
  {
  case ERR_CHECK:
    if((RobotCfg.ErrorState == ERROR_STATE_EMO) || (RobotCfg.ErrCode == ERRCODE_IMM_EMG))
    {
      ControlMsg.CommMode = MODE_ERROR;
      ControlMsg.TpMode = OCCUR_ERROR;
      ErrLoopState = ERR_WRITE;
      
      IOMsg.Y2A_MoldOpenClose = SIGNAL_OFF;
      IOMsg.Y2B_Ejector = SIGNAL_OFF;
      IOMsg.Y29_CycleStart = SIGNAL_OFF;
      IOMsg.Y2C_Conveyor = SIGNAL_OFF;
      RobotCfg.ErrCode = ERRCODE_EMG;
    }
    else if(RobotCfg.ErrorState == ERROR_STATE_OCCUR)
    {
      ControlMsg.CommMode = MODE_ERROR;
      ControlMsg.TpMode = OCCUR_ERROR;
      ErrLoopState = ERR_WRITE;
    }
    break;
    
  case ERR_WRITE:
    if(RobotCfg.ErrCode != 0)
    {
      WriteErrorList();
      if((RobotCfg.ErrorState == ERROR_STATE_EMO) || (RobotCfg.ErrCode == ERRCODE_IMM_EMG))
        ErrLoopState = ERR_EMO;
      else if ((RobotCfg.ErrCode == ERRCODE_VACCUM) || (RobotCfg.ErrCode == ERRCODE_CHUCK) || (RobotCfg.ErrCode == ERRCODE_RUNNER_PICK))
      {
        //Count
        CountMsg.DetectFail = RobotCfg.Count.DetectFail;
        memcpy(&(MoldMsg.Count), &CountMsg, sizeof(CountMsg));
        WriteRobotCfg(&RobotCfg);
        fPreviousPage = MANUAL_ERROR_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
        
        //		  WriteMold(&MoldMsg);
        ErrLoopState = ERR_TAKEOUT;
      }
      else ErrLoopState = ERR_SENSOR;
    }
    break;
    
  case ERR_EMO:
    if (HAL_GPIO_ReadPin(EMG_GPIO_Port,EMG_Pin) == GPIO_PIN_SET)
    {
      RobotCfg.ErrorState = ERROR_STATE_CLEAR;
      ErrLoopState = ERR_CLEAR;
    }
    break;
    
  case ERR_TAKEOUT:
    if((RobotCfg.Setting.ItlSaftyDoor == USE) && (RobotCfg.ErrorState == ERROR_STATE_CLEAR))
    {
      RobotCfg.ErrorState = ERROR_STATE_ACCEPT;
      ErrLoopState = ERR_CLEAR_ACCEPT;
      ControlMsg.KeyValue = KEY_NONE;
    }
    else if(!((RobotCfg.Setting.ItlSaftyDoor == USE) && (RobotCfg.Setting.ItlAutoInjection == USE)))
    {
      if (ControlMsg.KeyValue == KEY_CLEAR)
      {
        RobotCfg.ErrorState = ERROR_STATE_CLEAR;
        ErrLoopState = ERR_CLEAR;
        ControlMsg.KeyValue = KEY_NONE;
      }
    }
    break;
    
  case ERR_SENSOR:
    if(ControlMsg.KeyValue == KEY_CLEAR)
    {
      RobotCfg.ErrorState = ERROR_STATE_CLEAR;
      ErrLoopState = ERR_CLEAR;
      ControlMsg.KeyValue = KEY_NONE;
    }
    break;
    
  case ERR_CLEAR:
    if(ControlMsg.KeyValue == KEY_CLEAR)
    {
      RobotCfg.ErrorState = ERROR_STATE_ACCEPT;
      ErrLoopState = ERR_CLEAR_ACCEPT;
      ControlMsg.KeyValue = KEY_NONE;
    }
    break;
    
  case ERR_CLEAR_ACCEPT:
    if(RobotCfg.ErrorState == ERROR_STATE_NONE)
    {
      ControlMsg.TpMode = MANUAL_OPERATING;
      RobotCfg.ErrCode = 0;
      ClearPage();
      ClearAuto();
      SeqMsg.SeqDispPos = 0;
      ControlMsg.StepSeq = 0;
      ErrLoopState = ERR_CHECK;
    }
    break;
  }
}

void LoopAppMain(void)
{
  //Retry
  static uint8_t BootingRetry = 0;
  
  //Tick
  static uint64_t RTCTempTick = 0;
  static uint64_t LoadingTempTick = 0;
  static uint64_t MoldOpenTempTick = 0;
  static uint64_t SCTempTick = 0;
  
  //Mold mange
  static uint16_t ExistMoldNoIndex = 1;
  static uint16_t CheckMoldNo = 1;
  static char CheckMoldName[MAX_MOLDNAME_LENGTH];
  
  static uint8_t PageChange = 0;
  
  //Mold List Ascending order
  uint16_t ExistMoldNoTempTemp = 0;
  uint16_t ExistMoldNoTemp = 0;
  uint16_t IndexTemp = 0;
  char ExistMoldNameTemp[10];
  char ExistMoldNameTempTemp[10];
  
  static enum
  {
    INIT = 0,
    LOAD_SETTINGS = 1,
    ACTIVE_RUN = 2,
    MANUAL_OPERATING_COMM_LCD = 3,
  }MainLoopState = INIT; 
  
  switch(MainLoopState)
  {
  case INIT:
    SetSysTick(&LoadingTempTick, TIME_BOOTING);
    SetSysTick(&RTCTempTick,TIME_RTC); 
    SetSysTick(&SCTempTick, TIME_SC_DELAY);
    
    ControlMsg.TpMode = INIT_READ_TYPE;
    ControlMsg.IsChangedLCD = NEED_CHANGE;
    
    MainLoopState = LOAD_SETTINGS; 
    break;
    
  case LOAD_SETTINGS:
    ControlMsg.TpMode = INIT_READ_TYPE;
    ControlMsg.IsChangedLCD = NEED_CHANGE;
    MainLoopState = ACTIVE_RUN; 
    break;
    
  case ACTIVE_RUN:
    switch(ControlMsg.TpMode)
    {
    case INIT_READ_TYPE:
      //After 1s
      if(!(ChkExpireSysTick(&LoadingTempTick))) break;
      SdRes = ChkSD();
      if(SdRes == SD_OK)
      {
        BootingRetry = 0;
        SetSysTick(&LoadingTempTick, TIME_LOAD_DELAY);
        ControlMsg.TpMode = INIT_DISP_LOGO;
      }
      //Retry #3, Delay 300ms
      else
      {
        if(!(ChkExpireSysTick(&LoadingTempTick))) break;
        BootingRetry++;
        SetSysTick(&LoadingTempTick, TIME_LOAD_DELAY);
        if(BOOTING_ERROR_COUNT < BootingRetry)
        {
          if(SdRes == SD_SLOT_EMPTY)
          {
            BootingRetry = 0;
            RobotCfg.ErrorState = ERROR_STATE_OCCUR;
            RobotCfg.ErrCode = ERRCODE_SD_EMPTY;
          }
          else if(SdRes == SD_SLOT_WR_PROTECT)
          {
            BootingRetry = 0;
            RobotCfg.ErrorState = ERROR_STATE_OCCUR;
            RobotCfg.ErrCode = ERRCODE_SD_LOCKING;
          }
        }
      }
      break;
      
    case INIT_DISP_LOGO:
      //Mount SD
      SdRes = MountSD();
      SetSysTick(&SDTempTick, TIME_SC_DELAY);
      ControlMsg.TpMode = INIT_LOAD_SD;
      break;
      
    case INIT_LOAD_SD:
      //Mount SD value confirm
      if(SdRes == SD_OK)
      {
        SdRes = ReadRobotCfg(&RobotCfg);
        if(RobotCfg.MoldNo == 0)
        {
          SdResTemp = ReadRobotCfgBackUp(&RobotCfg);
          if(SdResTemp == SD_OK)
          {
            BootingRetry = 0;
            RobotCfg.RobotType = ReadType();
            WriteRobotCfg(&RobotCfg);
            MoldMsg.MoldNo = RobotCfg.MoldNo;
            ReadMold(&MoldMsg,RobotCfg.MoldNo);
            TimerDispCheck();
            ModeDispCheck(MoldMsg.MoldNo);
            StepCheck();
            MakeSequence();
            ControlMsg.CommMode = MODE_INIT;
            
            RobotCfg.Setting.IMMType = STANDARD;
            RobotCfg.Setting.RotationState = ROTATION;
            
            ControlMsg.TpMode = INIT_SC;
            SetSysTick(&LoadingTempTick, TIME_SC_BOOTING);				
            SetSysTick(&SDTempTick, TIME_SC_DELAY);
          }
          else
          {
            BootingRetry = 0;
            RobotCfg.ErrorState = ERROR_STATE_OCCUR;
            RobotCfg.ErrCode = ERROR_STATE_EMO;
          }
        }
        else
        {
          if(SdRes == SD_OK)
          {
            BootingRetry = 0;
            RobotCfg.RobotType = ReadType();
            //				if (!(ChkExpireSysTick(&SDTempTick))) break;
            //			 OpenMold(RobotCfg.MoldNo);
            MoldMsg.MoldNo = RobotCfg.MoldNo;
            ReadMold(&MoldMsg,RobotCfg.MoldNo);
            TimerDispCheck();
            ModeDispCheck(MoldMsg.MoldNo);
            StepCheck();
            MakeSequence();
            ControlMsg.CommMode = MODE_INIT;
            //IMMType
            if(!((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)))
            {
              //				RobotCfg.Language = LANGUAGE_ENG;
              RobotCfg.Setting.IMMType = STANDARD;
              RobotCfg.Setting.RotationState = ROTATION;
            }
            ControlMsg.TpMode = INIT_SC;
            SetSysTick(&LoadingTempTick, TIME_SC_BOOTING);				
            SetSysTick(&SDTempTick, TIME_SC_DELAY);
          }
          else
          {
            BootingRetry = 0;
            RobotCfg.ErrorState = ERROR_STATE_OCCUR;
            RobotCfg.ErrCode = ERRCODE_SD_NOT_FILE;
          }
        }
      }
      else	
      {
        //Retry #3, Delay 300ms
        if(!(ChkExpireSysTick(&LoadingTempTick))) break;
        BootingRetry++;
        SetSysTick(&LoadingTempTick, TIME_LOAD_DELAY);
        if(BootingRetry < BOOTING_ERROR_COUNT) SdRes = MountSD();
        else
        {
          BootingRetry = 0;
          RobotCfg.ErrorState = ERROR_STATE_OCCUR;
          RobotCfg.ErrCode = ERRCODE_SD_DISABLE;
        }
      }
      break;
      
    case INIT_SC:
      if(!(ChkExpireSysTick(&LoadingTempTick))) break;
      MoldMsg.ExistMold[0] = NEW_MOLD;
      ControlMsg.CommMode = MODE_SEQ;
      ControlMsg.TpMode = INIT_MOLD_LIST_UP;
      break;
      
    case INIT_MOLD_LIST_UP:
      //Mold File ListUp
      //Checking SDcard Mold No
      if(CheckMoldNo < MAX_MOLDNO_LENGTH+1)
      {
        SdRes = CheckExistMoldFile(CheckMoldNo,CheckMoldName);	
        if(SdRes == SD_FILE_EXIST)
        {
          MoldMsg.ExistMold[ExistMoldNoIndex] = CheckMoldNo;
          for(uint8_t MoldNameChar=0 ; MoldNameChar < MAX_MOLDNAME_LENGTH+1 ; MoldNameChar++)
          {
            MoldMsg.ExistMoldName[ExistMoldNoIndex][MoldNameChar] = CheckMoldName[MoldNameChar];
          }
          ExistMoldNoIndex++;
        }
        CheckMoldNo++;
        if(CheckMoldNo == MAX_MOLDNO_LENGTH+1) MoldMsg.MoldCount = ExistMoldNoIndex;
      }
      if(MoldMsg.MoldCount == ExistMoldNoIndex)
      {
        CheckMoldNo = 1;
        ExistMoldNoIndex = 1;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        //To go SETTING_MANUFACTURER Page 
        if(ControlMsg.KeyValue == KEY_MANUFACTURER)
        {
          UnderbarPos = 0;
          memcpy(&SettingTemp, &(RobotCfg.Setting), sizeof(MANUFACTURER_SETTING));
          ControlMsg.TpMode = SETTING_MANUFACTURER;
        }
        else
        {
          if((RobotCfg.Setting.DoorSignalChange == USE) && (RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_OFF))
          {
            RobotCfg.ErrCode = MANUAL_ERR_SAFTYDOOR_CHANGE;
            ControlMsg.TpMode = MANUAL_ERROR;
          }
          else
          {
            if(ControlMsg.fHomingDone == HOMING_DONE) 
            {
              ControlMsg.CommStateForDL = 2;
              ControlMsg.TpMode = MANUAL_OPERATING;
            }
            else 
            {
              ControlMsg.TpMode = MANUAL_HOMING;
              PageChange = PAGE_MANUAL;
            }
          }
        }
        ClearPage();
      }
      break;
      
      //Complete	Booting	//Complete	Booting	//Complete	Booting	//Complete	Booting	//Complete	Booting	//Complete	Booting	//Complete	Booting	//Complete	Booting	
      
    case MANUAL_OPERATING:
      //SC CommMode
      if(ControlMsg.CommStateForDL == 1) ControlMsg.CommMode = MODE_SEQ;
      else if(ControlMsg.CommStateForDL == 2)
      {
        ControlMsg.CommMode = MODE_MANUAL;
        ControlMsg.CommStateForDL = 0xFF;
      }
      //HomingState
      if(ControlMsg.fHomingDone == HOMING_DONE) ControlMsg.fHomingDone = HOMING_START;
      
      //Interlock
      ManualInterlock();
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      
      //IO
      ManualIO();
      
      //Page Change
      if(ControlMsg.KeyValue == KEY_MENU_AUTO)
      {
        if((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_OFF))
        {
          RobotCfg.ErrCode = MANUAL_ERR_SAFTYDOOR_AUTO;
          ControlMsg.TpMode = MANUAL_ERROR;
        }
        else if((RobotCfg.Setting.ItlSaftyDoor == USE) && (RobotCfg.Setting.ItlAutoInjection == USE))
        {
          if((IOMsg.X18_MoldOpenComplete == SIGNAL_ON) && (AutoInjectionTemp == SIGNAL_ON))
          {
            if(IOMsg.Y2A_MoldOpenClose == SIGNAL_OFF)
            {
              RobotCfg.ErrCode = MANUAL_ERR_MOLD;
              ControlMsg.TpMode = MANUAL_ERROR;
            }
          }
          else ControlMsg.TpMode = AUTO_MESSAGE_INIT;
        }
        else
        {
          ControlMsg.fHomingDone = HOMING_START;
          ControlMsg.TpMode = AUTO_MESSAGE_INIT;
        }
      }
      else if(ControlMsg.KeyValue == KEY_MENU_MODE)
      {
        ClearPage();
        memcpy(&MotionModeTemp, &(RobotCfg.MotionMode), sizeof(MOTION_MODE));
        memset(ControlMsg.ModeDisp,0,sizeof(ControlMsg.ModeDisp));
        memset(ControlMsg.ModeDispTemp,0,sizeof(ControlMsg.ModeDispTemp));
        ModeDispCheck(RobotCfg.MoldNo);
        ControlMsg.TpMode = MANUAL_MODE;
      }
      else if(ControlMsg.KeyValue == KEY_MENU_TIMER)
      {
        ClearPage();
        memcpy(&MotionDelayTemp, &(RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
        memset(ControlMsg.TimerDisp,0,sizeof(ControlMsg.TimerDisp));
        memset(ControlMsg.TimerDispTemp,0,sizeof(ControlMsg.TimerDispTemp));
        TimerDispCheck();
        ControlMsg.TpMode = MANUAL_TIMER;
      }
      else if(ControlMsg.KeyValue == KEY_MENU_COUNTER)
      {
        ClearPage();
        memcpy(&CountMsg, &(RobotCfg.Count), sizeof(COUNT_MSG));
        ControlMsg.TpMode = MANUAL_COUNTER;
      }
      else if(ControlMsg.KeyValue == KEY_MENU_IO)
      {
        ClearPage();
        ControlMsg.TpMode = MANUAL_IO;
      }
      else if(ControlMsg.KeyValue == KEY_MENU_STEP)
      {
        if((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_OFF))
        {
          RobotCfg.ErrCode = MANUAL_ERR_SAFTYDOOR_STEP;
          ControlMsg.TpMode = MANUAL_ERROR;
        }
        else
        {
          StepCheck();
          StepCursor = DISPLAY_OFF;
          ControlMsg.CommMode = MODE_STEP;
          ControlMsg.TpMode = MANUAL_HOMING;
          PageChange = PAGE_STEP;
        }
      }
      else if(ControlMsg.KeyValue == KEY_MENU_CYCLE)
      {
        if((RobotCfg.Setting.ItlSaftyDoor == USE) && (IOMsg.X1A_SaftyDoor == SIGNAL_OFF))
        {
          RobotCfg.ErrCode = MANUAL_ERR_SAFTYDOOR_CYCLE;
          ControlMsg.TpMode = MANUAL_ERROR;
        }
        else
        {
          memcpy(&CycleDelay, &(RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
          StepCheck();
          ControlMsg.CommMode = MODE_CYCLE;
          ControlMsg.TpMode = MANUAL_HOMING;
          PageChange = PAGE_CYCLE;
        }
      }
      else if(ControlMsg.KeyValue == KEY_MENU_MOLD)
      {
        ClearPage();
        MoldNoTemp = 0;
        ControlMsg.TpMode = MANUAL_MOLD_SEARCH;
      }
      else if(ControlMsg.KeyValue == KEY_ERROR_LOG)
      {
        ClearPage();
        ReadError(ErrMsgTemp);
        ErrMsg.ErrorCount = CountErr();
        ControlMsg.TpMode = MANUAL_ERROR_LIST;
      }
      else if(ControlMsg.KeyValue == KEY_DISP_VERSION)
      {
        ClearPage();
        ControlMsg.TpMode = MANUAL_VERSION;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      
      //Function
      else if(ControlMsg.KeyValue == KEY_ALARM)
      {
        RobotCfg.Function.Buzzer ^= FUNC_ON;
        WriteFunction();
        ChangeSCMode();
        fPreviousPage = MANUAL_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(ControlMsg.KeyValue == KEY_DETECT)
      {
        RobotCfg.Function.Detection ^= FUNC_ON;
        WriteFunction();
        ChangeSCMode();
        fPreviousPage = MANUAL_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(ControlMsg.KeyValue == KEY_EJECTOR)
      {
        RobotCfg.Function.Ejector ^= FUNC_ON;
        WriteFunction();
        ChangeSCMode();
        fPreviousPage = MANUAL_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(ControlMsg.KeyValue == KEY_REJECT)
      {
        RobotCfg.Function.Reject ^= FUNC_ON;
        WriteFunction();
        ChangeSCMode();
        fPreviousPage = MANUAL_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        RobotCfg.ErrorState = ERROR_STATE_NONE;
        ChangeSCMode();
        ClearPage();
      }
      break;
      
    case MANUAL_HOMING :
      if(ControlMsg.CommStateForDL == 1)
      {
        ControlMsg.fDoHoming = 0;
        ControlMsg.CommMode = MODE_SEQ;
      }
      if((PageChange == PAGE_MANUAL) && (ControlMsg.fHomingDone == HOMING_DONE))
      {
        ControlMsg.fDoHoming = 0;
        ControlMsg.CommStateForDL = 2;
        ControlMsg.TpMode = MANUAL_OPERATING;
        PageChange = 0;
      }
      else if((PageChange == PAGE_STEP) && (ControlMsg.fHomingDone == HOMING_DONE))
      {
        ControlMsg.fDoHoming = 0;
        //		  ControlMsg.StepSeq = SeqMsg.SeqDispArr[SeqMsg.SeqDispPos];
        PageChange = PAGE_NONE;
        ControlMsg.TpMode = MANUAL_STEP_OPERATING;
      }
      else if((PageChange == PAGE_CYCLE) && (ControlMsg.fHomingDone == HOMING_DONE))
      {
        ControlMsg.fDoHoming = 0;
        PageChange = PAGE_NONE;
        ControlMsg.TpMode = MANUAL_CYCLE_OPERATING;
      }
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue  == KEY_MENU_STOP)
      {
        ControlMsg.fDoHoming = 0;
        ChangeSCMode();
        PageChange = PAGE_NONE;
        ControlMsg.TpMode = MANUAL_OPERATING;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case AUTO_MESSAGE_INIT:
      memcpy(&CycleDelay, &(RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
      TimerDispCheck();
      ModeDispCheck(RobotCfg.MoldNo);
      StepCheck();
      ControlMsg.TpMode = AUTO_MESSAGE;
      break;
      
    case AUTO_MESSAGE:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_AUTO)
      {
        if((RobotCfg.Setting.ItlAutoInjection == USE) && (IOMsg.X19_AutoInjection == SIGNAL_ON))
          IOMsg.Y29_CycleStart = SIGNAL_OFF;
        //		  else IOMsg.Y29_CycleStart = SIGNAL_ON;
        //		  if (RobotCfg.Function.Ejector == FUNC_ON) IOMsg.Y2B_Ejector = SIGNAL_OFF;
        ControlMsg.TpMode = AUTO_OPERATING;
        ControlMsg.CommMode = MODE_AUTO;
        ClearPage();
      }
      else if(ControlMsg.KeyValue  == KEY_MENU_STOP)
      {
        ControlMsg.TpMode = MANUAL_OPERATING;
        ControlMsg.CommStateForDL = 2;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case AUTO_OPERATING:
      if(ControlMsg.fHomingDone == HOMING_DONE)
        AutoState(MODE_AUTO_OP);				//To continue to auto-operate even when the page changes
      else if((ControlMsg.fHomingDone == HOMING_START) && (ControlMsg.KeyValue == KEY_MENU_STOP)) 
      {
        ClearAuto();
        ControlMsg.TpMode = MANUAL_OPERATING;		  //201123
        ControlMsg.CommStateForDL = 2;
        PageChange = 0;
      }
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_MODE)
      {
        ClearPage();
        memcpy(&MotionModeTemp, &(RobotCfg.MotionMode), sizeof(MOTION_MODE));
        memset(ControlMsg.ModeDisp,0,sizeof(ControlMsg.ModeDisp));
        memset(ControlMsg.ModeDispTemp,0,sizeof(ControlMsg.ModeDispTemp));
        ModeDispCheck(RobotCfg.MoldNo);
        ControlMsg.TpMode = AUTO_MODE;
      }
      else if(ControlMsg.KeyValue == KEY_MENU_IO)
      {
        ControlMsg.TpMode = AUTO_IO;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_MENU_TIMER)
      {
        memcpy(&MotionDelayTemp, &(RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
        memset(ControlMsg.TimerDisp,0,sizeof(ControlMsg.TimerDisp));
        memset(ControlMsg.TimerDispTemp,0,sizeof(ControlMsg.TimerDispTemp));
        TimerDispCheck();
        ControlMsg.TpMode = AUTO_TIMER;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_MENU_COUNTER)
      {
        memcpy(&CountMsg, &(RobotCfg.Count), sizeof(COUNT_MSG));
        ControlMsg.TpMode = AUTO_COUNTER;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case AUTO_MODE:	
      AutoState(MODE_AUTO_OP);					//To continue to auto-operate even when the page changes
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_AUTO)
      {
        ControlMsg.TpMode = AUTO_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT)	InMenuUpButton(MAX_LINE_OUTLINE);
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT)	InMenuDownButton(MAX_LINE_OUTLINE, MAX_LINE_MENU, ControlMsg.ModeDispTemp);
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case AUTO_COUNTER:
      AutoState(MODE_AUTO_OP);					//To continue to auto-operate even when the page changes
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_AUTO)
      {
        ControlMsg.TpMode = AUTO_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.LineNo > 0) ControlMsg.LineNo--;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.LineNo < MAX_LINE_TITLE) ControlMsg.LineNo++;
      }
      else if(ControlMsg.KeyValue == KEY_LONG_CLEAR)
      {
        if(ControlMsg.LineNo == 0) CountMsg.TotalCnt = 0;
        else if(ControlMsg.LineNo == 1) CountMsg.RejectCnt = 0;
        else if(ControlMsg.LineNo == 2) CountMsg.DetectFail = 0;
        memcpy(&(RobotCfg.Count), &CountMsg, sizeof(CountMsg));
        memcpy(&(MoldMsg.Count), &CountMsg, sizeof(CountMsg));
        //		  WriteMold(&MoldMsg);
        WriteRobotCfg(&RobotCfg);
        fPreviousPage = AUTO_COUNTER_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case AUTO_TIMER:
      AutoState(MODE_AUTO_OP);					//To continue to auto-operate even when the page changes
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_AUTO)
      {
        ControlMsg.TpMode = AUTO_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(ControlMsg.EnableEdit == ENABLE_EDIT)
        {
          memcpy(&MotionDelayTemp, &(RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
          ControlMsg.EnableEdit = DISABLE_EDIT;
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuUpButton(MAX_LINE_TITLE);
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuDownButton(MAX_LINE_TITLE, MAX_LINE_OUTLINE, ControlMsg.TimerDispTemp);
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        ControlMsg.EnableEdit = DISABLE_EDIT;
        memcpy(&(RobotCfg.MotionDelay), &MotionDelayTemp, sizeof(MOTION_DELAY));
        memcpy(&CycleDelay, &MotionDelayTemp, sizeof(MOTION_DELAY));
        memcpy(&(MoldMsg.MotionDelay), &MotionDelayTemp, sizeof(MOTION_DELAY));
        //		  WriteMold(&MoldMsg);
        WriteRobotCfg(&RobotCfg);
        StepCheck();
        fPreviousPage = AUTO_TIMER_PAGE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(KeyIsNum(&ControlMsg.KeyValue))
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        ImportDelayTime();
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case AUTO_IO:
      AutoState(MODE_AUTO_OP);					//To continue to auto-operate even when the page changes
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_AUTO)
      {
        ControlMsg.TpMode = AUTO_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.PageNo > 0) ControlMsg.PageNo--;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.PageNo < IO_MAX_PAGE) ControlMsg.PageNo++;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT) ControlMsg.LineNo = IO_OUTPUT;
      else if(ControlMsg.KeyValue == KEY_ARROW_LEFT) ControlMsg.LineNo = IO_INPUT;
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_MODE:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ControlMsg.fDoHoming = 1;
        ChangeSCMode();
        ControlMsg.TpMode = MANUAL_HOMING;
        PageChange = PAGE_MANUAL;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(ControlMsg.EnableEdit == ENABLE_EDIT)
        {
          memcpy(&MotionModeTemp, &(RobotCfg.MotionMode), sizeof(MOTION_MODE));
          ControlMsg.EnableEdit = DISABLE_EDIT;
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuUpButton(MAX_LINE_OUTLINE);
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuDownButton(MAX_LINE_OUTLINE, MAX_LINE_MENU, ControlMsg.ModeDispTemp);
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        ControlMsg.EnableEdit = DISABLE_EDIT;
        memcpy(&(RobotCfg.MotionMode), &MotionModeTemp, sizeof(MOTION_MODE));
        memcpy(&(MoldMsg.MotionMode), &MotionModeTemp, sizeof(MOTION_MODE));
        memset(SeqMsg.SeqDispArr,0,sizeof(SeqMsg.SeqDispArr));
        memset(ControlMsg.StepDispTemp,0,sizeof(ControlMsg.StepDispTemp));
        memset(SeqMsg.SeqArr,0,sizeof(SeqMsg.SeqArr));
        MakeSequence();
        //		  WriteMold(&MoldMsg);
        WriteRobotCfg(&RobotCfg);
        fPreviousPage = MANUAL_MODE_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_LEFT)
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        ImportMode();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT)
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        ImportMode();
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_COUNTER:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ControlMsg.TpMode = MANUAL_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.LineNo > 0) ControlMsg.LineNo--;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.LineNo < MAX_LINE_TITLE) ControlMsg.LineNo++;
      }
      else if(ControlMsg.KeyValue == KEY_LONG_CLEAR)
      {
        if(ControlMsg.LineNo == 0) CountMsg.TotalCnt = 0;
        else if(ControlMsg.LineNo == 1) CountMsg.RejectCnt = 0;
        else if(ControlMsg.LineNo == 2) CountMsg.DetectFail = 0;
        memcpy(&(RobotCfg.Count), &CountMsg, sizeof(CountMsg));
        memcpy(&(MoldMsg.Count), &CountMsg, sizeof(CountMsg));
        //		  WriteMold(&MoldMsg);
        WriteRobotCfg(&RobotCfg);
        ControlMsg.CommMode = MODE_SEQ;
        fPreviousPage = MANUAL_COUNTER_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_TIMER:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ChangeSCMode();
        ControlMsg.TpMode = MANUAL_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(ControlMsg.EnableEdit == ENABLE_EDIT)
        {
          memcpy(&MotionDelayTemp, &(RobotCfg.MotionDelay), sizeof(MOTION_DELAY));
          ControlMsg.EnableEdit = DISABLE_EDIT;
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuUpButton(MAX_LINE_TITLE);
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuDownButton(MAX_LINE_TITLE, MAX_LINE_OUTLINE, ControlMsg.TimerDispTemp);
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        ControlMsg.EnableEdit = DISABLE_EDIT;
        memcpy(&(RobotCfg.MotionDelay), &MotionDelayTemp, sizeof(MOTION_DELAY));
        memcpy(&CycleDelay, &MotionDelayTemp, sizeof(MOTION_DELAY));
        memcpy(&(MoldMsg.MotionDelay), &MotionDelayTemp, sizeof(MOTION_DELAY));
        MakeSequence();
        //		  WriteMold(&MoldMsg);
        WriteRobotCfg(&RobotCfg);
        fPreviousPage = MANUAL_TIMER_SD;
        SetSysTick(&SDTempTick, TIME_SC_DELAY);	
        ControlMsg.TpMode = SD_BACKUP;
      }
      else if(KeyIsNum(&ControlMsg.KeyValue))
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        ImportDelayTime();
      }  
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_IO:
      ManualInterlock();
      if(ControlMsg.KeyReady == KEY_READY) break;
      ManualIO();
      if(ControlMsg.KeyValue == KEY_MENU_STOP) 
      {
        ClearPage();
        ControlMsg.TpMode = MANUAL_OPERATING;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.PageNo > 0) ControlMsg.PageNo--;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.PageNo < IO_MAX_PAGE) ControlMsg.PageNo++;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT) ControlMsg.LineNo = IO_OUTPUT;
      else if(ControlMsg.KeyValue == KEY_ARROW_LEFT) ControlMsg.LineNo = IO_INPUT;
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_STEP_OPERATING:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ControlMsg.CommStateForDL = 2;
        ControlMsg.TpMode = MANUAL_OPERATING;
        SeqMsg.SeqDispPos = 0;
        ControlMsg.StepSeq = 0;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if (IOMsg.X18_MoldOpenComplete == SIGNAL_ON)
        {
          if (SeqMsg.SeqDispArr[SeqMsg.SeqDispPos] == SEQ_END)
          {
            ControlMsg.StepSeq = 0;
            SeqMsg.SeqDispPos = 0;
          }
          if(SeqMsg.SeqPos == ControlMsg.StepSeq)
          {
            SeqMsg.SeqDispPos++;
            ControlMsg.StepSeq = SeqMsg.SeqDispArr[SeqMsg.SeqDispPos];
            //Last page and Last line --> page line reset. restart
            if ((ControlMsg.PageNo == ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX]/MAX_LINE_OUTLINE) &&
                (ControlMsg.LineNo == ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX]%MAX_LINE_OUTLINE-1))
              ClearPage();
            else if(ControlMsg.LineNo < MAX_LINE_TITLE)
            {
              //last page and last line check
              if ((ControlMsg.PageNo*MAX_LINE_OUTLINE  + ControlMsg.LineNo) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX] -1)
              {
                if (StepCursor == DISPLAY_ON) ControlMsg.LineNo++; 
              }
            }
            else
            { 
              //last page check
              if ((ControlMsg.PageNo == ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX]/MAX_LINE_OUTLINE -1) &&
                  (ControlMsg.LineNo == MAX_LINE_TITLE) && (ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX]%MAX_LINE_OUTLINE == 0))
                ClearPage();		//page, line reset
              else
              {
                //next page
                ControlMsg.PageNo++;
                ControlMsg.LineNo = 0;
              }
            }
          }
        }
        else ControlMsg.KeyValue = KEY_NONE;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_CYCLE_OPERATING: 
      AutoState(MODE_CYCLE_OP);
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_MOLD_SEARCH:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ClearPage();
        ControlMsg.TpMode = MANUAL_OPERATING;
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        MoldMsg.MoldNo = MoldNoTemp;
        //Check Mold No
        if((MoldMsg.MoldNo != NEW_MOLD) && (CheckExistMoldFile(MoldMsg.MoldNo,MoldMsg.MoldName) == SD_DO_NOT_EXIST_FILE))
        {
          SetSysTick(&MoldOpenTempTick, TIME_MOLD_OPEN_ERROR);
          RobotCfg.ErrCode = MANUAL_ERR_NO_MOLDNO;
          ClearPage();
          ControlMsg.TpMode = MANUAL_ERROR;
          MoldMsg.MoldNo = 0;
          
          //Don't Exist Mold File. Move Cursor near MoldNo
          for (ExistMoldNoIndex = 1; ExistMoldNoIndex <= MoldMsg.MoldCount-2; ExistMoldNoIndex++)
          {
            if ((abs(MoldNoTemp - MoldMsg.ExistMold[ExistMoldNoIndex]) > (abs(MoldNoTemp - MoldMsg.ExistMold[ExistMoldNoIndex+1]))))
              IndexTemp =  ExistMoldNoIndex+1;
          }
          ControlMsg.PageNo = IndexTemp/MAX_LINE_OUTLINE;
          ControlMsg.LineNo = IndexTemp%MAX_LINE_OUTLINE;
        }
        else
        {
          //Exist Mold File. Move Cursor MoldNo
          for(ExistMoldNoIndex = 0 ; ExistMoldNoIndex <= MoldMsg.MoldCount-1 ; ExistMoldNoIndex++)
          {
            if(MoldMsg.ExistMold[ExistMoldNoIndex] == MoldMsg.MoldNo)
            {
              ControlMsg.PageNo = ExistMoldNoIndex/MAX_LINE_OUTLINE;
              ControlMsg.LineNo = ExistMoldNoIndex%MAX_LINE_OUTLINE;
            }
          }
          ControlMsg.TpMode = MANUAL_MOLD_MANAGE;
        }
        ExistMoldNoIndex = 1;
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR) MoldNoTemp = 0;
      else if(KeyIsNum(&ControlMsg.KeyValue))
      {
        MoldNoTemp = ((MoldNoTemp)*10 + KeyNum(&ControlMsg.KeyValue));
        if(MoldNoTemp > MAX_MOLDNO_LENGTH) MoldNoTemp = MoldNoTemp % (MAX_MOLDNO_LENGTH +1);
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      
      break;
      
    case MANUAL_MOLD_MANAGE:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ClearPage();
        ControlMsg.TpMode = MANUAL_OPERATING;
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        //Open Mold No
        MoldMsg.MoldNo = MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+ControlMsg.LineNo];
        
        //Create New Mold(Mode Page)
        if(MoldMsg.MoldNo == NEW_MOLD)
        {
          MoldMsg.MoldNo = MoldNoTemp;
          MoldInit();
          memcpy(&MoldMsg.MotionMode, &(MotionModeTemp), sizeof(MOTION_MODE));
          memset(ControlMsg.ModeDisp,0,sizeof(ControlMsg.ModeDisp));
          memset(ControlMsg.ModeDispTemp,0,sizeof(ControlMsg.ModeDispTemp));
          ModeDispCheck(MoldMsg.MoldNo);
          CheckMoldNo = POSSIBLE_MOLD_NO;
          ControlMsg.TpMode = MANUAL_MOLD_MODE;
          ClearPage();
        }
        else
        {
          if((MoldMsg.MoldNo == 23) || (MoldMsg.MoldNo == 33))
          {
            if(RobotCfg.RobotType != ROBOT_TYPE_A) 
            {
              OpenMold(MoldMsg.MoldNo);
              fPreviousPage = MANUAL_OPENMOLD_SD;
              ControlMsg.TpMode = SD_WRITE;
              //				  WriteRobotCfg(&RobotCfg);
              //				  ChangeSCMode();
              //				  ControlMsg.fDoHoming = 1;
              //				  ControlMsg.TpMode = MANUAL_HOMING;
              //				  PageChange = PAGE_MANUAL;
              //				  ClearPage();
            }
            else
            {
              RobotCfg.ErrCode = MANUAL_ERR_ROBOT_TYPE;
              ControlMsg.TpMode = MANUAL_ERROR;
            }
          }
          else if((MoldMsg.MoldNo == 36) || (MoldMsg.MoldNo == 37))
          {
            if((RobotCfg.RobotType == ROBOT_TYPE_XC) || (RobotCfg.RobotType == ROBOT_TYPE_TWIN))
            {
              OpenMold(MoldMsg.MoldNo);
              fPreviousPage = MANUAL_OPENMOLD_SD;
              ControlMsg.TpMode = SD_WRITE;
              //				  WriteRobotCfg(&RobotCfg);
              //				  ChangeSCMode();
              //				  ControlMsg.fDoHoming = 1;
              //				  ControlMsg.TpMode = MANUAL_HOMING;
              //				  PageChange = PAGE_MANUAL;
              //				  ClearPage();
            }
            else
            {
              RobotCfg.ErrCode = MANUAL_ERR_ROBOT_TYPE;
              ControlMsg.TpMode = MANUAL_ERROR;
            }
          }
          else if((MoldMsg.MoldNo == 40) || (MoldMsg.MoldNo == 41) || (MoldMsg.MoldNo == 43) || (MoldMsg.MoldNo == 45) 
                  || (MoldMsg.MoldNo == 82) || (MoldMsg.MoldNo == 84))
          {
            if(RobotCfg.RobotType == ROBOT_TYPE_TWIN)
            {
              OpenMold(MoldMsg.MoldNo);
              fPreviousPage = MANUAL_OPENMOLD_SD;
              ControlMsg.TpMode = SD_WRITE;
              //				  WriteRobotCfg(&RobotCfg);
              //				  ChangeSCMode();
              //				  ControlMsg.fDoHoming = 1;
              //				  ControlMsg.TpMode = MANUAL_HOMING;
              //				  PageChange = PAGE_MANUAL;
              //				  ClearPage();
            }
            else
            {
              RobotCfg.ErrCode = MANUAL_ERR_ROBOT_TYPE;
              ControlMsg.TpMode = MANUAL_ERROR;
            }
          }
          else if((MoldMsg.MoldNo == 22) || (MoldMsg.MoldNo == 32))	//IMMType
          {
            if(((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC)) && (RobotCfg.Setting.IMMType == VERTI))
            {
              RobotCfg.ErrCode = MANUAL_ERR_ROBOT_TYPE;
              ControlMsg.TpMode = MANUAL_ERROR;
            }
            else
            {
              OpenMold(MoldMsg.MoldNo);
              fPreviousPage = MANUAL_OPENMOLD_SD;
              ControlMsg.TpMode = SD_WRITE;
              //				  WriteRobotCfg(&RobotCfg);
              //				  ChangeSCMode();
              //				  ControlMsg.fDoHoming = 1;
              //				  ControlMsg.TpMode = MANUAL_HOMING;
              //				  PageChange = PAGE_MANUAL;
              //				  ClearPage();
            }
          }
          else
          {
            OpenMold(MoldMsg.MoldNo);
            fPreviousPage = MANUAL_OPENMOLD_SD;
            ControlMsg.TpMode = SD_WRITE;
            //				WriteRobotCfg(&RobotCfg);
            //				ChangeSCMode();
            //				ControlMsg.fDoHoming = 1;
            //				ControlMsg.TpMode = MANUAL_HOMING;
            //				PageChange = PAGE_MANUAL;
            //				ClearPage();
          }
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP) InMenuUpButton(MAX_LINE_TITLE);
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.LineNo < MAX_LINE_TITLE)
        {
          if ((ControlMsg.PageNo*MAX_LINE_OUTLINE  + ControlMsg.LineNo) != MoldMsg.MoldCount -1) ControlMsg.LineNo++;  
        }
        else
        { 
          if (ControlMsg.PageNo != MoldMsg.MoldCount/MAX_LINE_OUTLINE)
          {
            if ((ControlMsg.PageNo == MoldMsg.MoldCount/MAX_LINE_OUTLINE -1) &&
                (ControlMsg.LineNo == MAX_LINE_TITLE) && (MoldMsg.MoldCount%MAX_LINE_OUTLINE == 0));
            else
            {
              ControlMsg.PageNo++;
              ControlMsg.LineNo = 0;
            }
          }
        }
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR) ControlMsg.TpMode = MANUAL_MOLD_DELETE;
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_MOLD_MODE:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        //Mold No and Name default setting
        if(ControlMsg.EnableEdit == DISABLE_EDIT)
        {
          for(ExistMoldNoIndex = NEW_MOLD+1 ; ExistMoldNoIndex <= MoldMsg.MoldCount-1 ; ExistMoldNoIndex++)
          {
            if(MoldMsg.ExistMold[ExistMoldNoIndex] >= POSSIBLE_MOLD_NO)
            {
              if(MoldMsg.ExistMold[ExistMoldNoIndex] == CheckMoldNo) CheckMoldNo++;
              else break;
            }
          }
          MoldNoTemp = CheckMoldNo;
          NewMoldInit(MoldNoTemp);
          CheckMoldNo = 1;
          ClearPage();
          ControlMsg.TpMode = MANUAL_MOLD_NEW;
          memcpy(&(MoldMsg.MotionMode), &MotionModeTemp, sizeof(MOTION_MODE));
        }
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(ControlMsg.EnableEdit == ENABLE_EDIT)
        {
          memcpy(&MoldMsg.MotionMode, &(MotionModeTemp), sizeof(MOTION_MODE));
          ControlMsg.EnableEdit = DISABLE_EDIT;
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuUpButton(MAX_LINE_OUTLINE);
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT) InMenuDownButton(MAX_LINE_OUTLINE, MAX_LINE_MENU, ControlMsg.ModeDispTemp);
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_LEFT)
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        ImportMode();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT)
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        ImportMode();
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        ControlMsg.EnableEdit = DISABLE_EDIT;
        memcpy(&(MoldMsg.MotionMode), &MotionModeTemp, sizeof(MOTION_MODE));
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_MOLD_NEW:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        if(UnderbarPos == NO_BLINK)
        {
          //Mold Load
          MoldMsg.MoldNo = MoldNoTemp;
          for(uint8_t MoldNameChar =0 ; MoldNameChar<= MAX_MOLDNAME_LENGTH ; MoldNameChar++)
          {
            MoldMsg.MoldName[MoldNameChar] = NewMoldName[MoldNameChar];
          }
          WriteMold(&MoldMsg);
          //			 OpenMold(MoldMsg.MoldNo);
          //			 WriteRobotCfg(&RobotCfg);
          ChangeSCMode();
          
          //Ascending order Mold List
          if(MoldMsg.ExistMold[MoldMsg.MoldCount-1] < MoldMsg.MoldNo)		//Last Mold File
          {
            MoldMsg.ExistMold[MoldMsg.MoldCount] = MoldMsg.MoldNo;
            for(uint8_t MoldNameChar =0 ; MoldNameChar <= MAX_MOLDNAME_LENGTH ; MoldNameChar++)
            {
              MoldMsg.ExistMoldName[MoldMsg.MoldCount][MoldNameChar] = MoldMsg.MoldName[MoldNameChar];
            }						
          }
          else
          {
            for (ExistMoldNoIndex = 1; ExistMoldNoIndex <= MoldMsg.MoldCount-2; ExistMoldNoIndex++)
            {
              if ((MoldMsg.ExistMold[ExistMoldNoIndex] < MoldNoTemp) && (MoldNoTemp < MoldMsg.ExistMold[ExistMoldNoIndex+1]))
                IndexTemp =  ExistMoldNoIndex+1;
            }
            ExistMoldNoTemp = MoldMsg.ExistMold[IndexTemp];
            MoldMsg.ExistMold[IndexTemp] = MoldMsg.MoldNo;
            
            for(uint8_t MoldNameChar =0 ; MoldNameChar <= MAX_MOLDNAME_LENGTH ; MoldNameChar++)
            {
              ExistMoldNameTemp[MoldNameChar] = MoldMsg.ExistMoldName[IndexTemp][MoldNameChar];
              MoldMsg.ExistMoldName[IndexTemp][MoldNameChar] = MoldMsg.MoldName[MoldNameChar];
            }
            
            for(uint16_t IndexOffset = 1; IndexOffset <= MoldMsg.MoldCount-1; IndexOffset+=2)
            {
              ExistMoldNoTempTemp = MoldMsg.ExistMold[IndexTemp+IndexOffset];
              MoldMsg.ExistMold[IndexTemp+IndexOffset] = ExistMoldNoTemp;
              ExistMoldNoTemp = MoldMsg.ExistMold[IndexTemp+IndexOffset+1];
              MoldMsg.ExistMold[IndexTemp+IndexOffset+1] = ExistMoldNoTempTemp;
              
              for(uint8_t MoldNameChar = 0; MoldNameChar <= MAX_MOLDNAME_LENGTH; MoldNameChar++)
              {
                ExistMoldNameTempTemp[MoldNameChar] = MoldMsg.ExistMoldName[IndexTemp+IndexOffset][MoldNameChar];
                MoldMsg.ExistMoldName[IndexTemp+IndexOffset][MoldNameChar] = ExistMoldNameTemp[MoldNameChar];
                ExistMoldNameTemp[MoldNameChar] = MoldMsg.ExistMoldName[IndexTemp+IndexOffset+1][MoldNameChar];
                MoldMsg.ExistMoldName[IndexTemp+IndexOffset+1][MoldNameChar] = ExistMoldNameTempTemp[MoldNameChar];
              }
            }
          }
          OpenMold(MoldMsg.MoldNo);
          MoldMsg.MoldCount++;
          IndexTemp = 0;
          ExistMoldNoIndex = 1;
          UnderbarPos = 0;
          ControlMsg.fDoHoming = 1;
          fPreviousPage = MANUAL_NEWMOLD_SD;
          //			 ControlMsg.TpMode = MANUAL_HOMING;
          PageChange = PAGE_MANUAL;
          MoldNamePos = 0;
          MoldNameCharPos = 5;
          ClearPage();
          memset(NewMoldNameTemp,0,sizeof(NewMoldNameTemp));
          memset(&RobotCfg.Count,0,sizeof(RobotCfg.Count));
          memset(&CountMsg,0,sizeof(CountMsg));
          ControlMsg.TpMode = SD_WRITE;
        }
        else
        {
          UnderbarPos = 0;
          ClearPage();
          ControlMsg.TpMode = MANUAL_OPERATING;
        }
      }
      //Numbering MoldNo
      else if(KeyIsNum(&ControlMsg.KeyValue))
      {
        if (UnderbarPos == MOLDNO_BLINK)
        {
          MoldNoTemp = ((MoldNoTemp)*10 + KeyNum(&ControlMsg.KeyValue));
          if(MoldNoTemp > MAX_MOLDNO_LENGTH) MoldNoTemp = MoldNoTemp % (MAX_MOLDNO_LENGTH +1);
        }
      }
      //Naming MoldName
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(UnderbarPos == MOLENAME_UNDERBAR_BLINK)
        {
          MoldNameCharPos++;
          if(MoldNameCharPos > MAX_MOLDNAME_CHAR_POS) MoldNameCharPos = MIN_MOLDNAME_CHAR_POS;
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(UnderbarPos == MOLENAME_UNDERBAR_BLINK)
        {
          if ((MoldNameCharPos == MIN_MOLDNAME_CHAR_POS) && (ControlMsg.KeyValue == KEY_ARROW_DOWM)) 
            MoldNameCharPos = MAX_MOLDNAME_CHAR_POS + 1;
          MoldNameCharPos--;
        }
      }
      //Underbar Position
      else if(ControlMsg.KeyValue == KEY_ARROW_LEFT)
      {
        if(UnderbarPos == NO_BLINK) UnderbarPos = MOLENAME_UNDERBAR_BLINK;
        else if(UnderbarPos == MOLENAME_UNDERBAR_BLINK)
        {
          NewMoldNameTemp[MoldNamePos] = MoldNameCharPos;
          if ((MoldNamePos == MIN_MOLDNAME_POS) && (ControlMsg.KeyValue == KEY_ARROW_LEFT)) 
            MoldNamePos = MAX_MOLDNAME_POS;
          MoldNamePos--;
          MoldNameCharPos = NewMoldNameTemp[MoldNamePos];
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT)
      {
        if(UnderbarPos == NO_BLINK) UnderbarPos = MOLENAME_UNDERBAR_BLINK;
        else if(UnderbarPos == MOLENAME_UNDERBAR_BLINK)
        {
          NewMoldNameTemp[MoldNamePos] = MoldNameCharPos;
          MoldNamePos++;
          if(MoldNamePos > MAX_MOLDNAME_POS -1) MoldNamePos = MIN_MOLDNAME_POS;
          MoldNameCharPos = NewMoldNameTemp[MoldNamePos];
        }
      }
      else if(ControlMsg.KeyValue == KEY_ENTER) 
      {
        if(CheckExistMoldFile(MoldNoTemp,MoldMsg.MoldName) == SD_FILE_EXIST)
          MoldNoTemp = 0;
        if(((MIN_FIX_MOLD_NO <= MoldNoTemp) && (MoldNoTemp <= MAX_FIX_MOLD_NO)) || (MoldNoTemp == NEW_MOLD))
          MoldNoTemp = NEW_MOLD;
        else if(UnderbarPos == MOLENAME_UNDERBAR_BLINK)
        {
          NewMoldNameTemp[MoldNamePos] = MoldNameCharPos;
          UnderbarPos = NO_BLINK;
        }
        else UnderbarPos = NO_BLINK;
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(UnderbarPos == MOLDNO_BLINK)
          MoldNoTemp = 0;
        else if(UnderbarPos == MOLENAME_UNDERBAR_BLINK)
        {
          NewMoldNameTemp[0] = 38;				//' '
          NewMoldNameTemp[1] = 38;				//' '
          NewMoldNameTemp[2] = 38;				//' '
          NewMoldNameTemp[3] = 38;				//' '
          NewMoldNameTemp[4] = 38;				//' '
          NewMoldNameTemp[5] = 38;				//' '
          NewMoldNameTemp[6] = 38;				//' '
          NewMoldNameTemp[7] = 38;				//' '
          MoldNameCharPos = 38;
        }
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_MOLD_DELETE:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ClearPage();
        ControlMsg.TpMode = MANUAL_OPERATING;
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        if((RobotCfg.MoldNo == MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+ControlMsg.LineNo])
           || (MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+ControlMsg.LineNo] < POSSIBLE_MOLD_NO))
        {
          RobotCfg.ErrCode = MANUAL_ERR_MOLD_DELETE;
          ControlMsg.TpMode = MANUAL_ERROR;
        }
        else
        {
          DeleteMoldFile(MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+ControlMsg.LineNo]);
          MoldMsg.ExistMold[0] = NEW_MOLD;										//NewMold MoldNO 0
          ControlMsg.TpMode = MANUAL_MOLD_DELETE_ING;
        }
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_MOLD_DELETE_ING:
      if(CheckMoldNo <= MAX_MOLDNO_LENGTH)
      {
        SdRes = CheckExistMoldFile(CheckMoldNo,CheckMoldName);	
        if(SdRes == SD_FILE_EXIST)
        {
          MoldMsg.ExistMold[ExistMoldNoIndex] = CheckMoldNo;
          for(uint8_t MoldNameChar=0; MoldNameChar<=MAX_MOLDNAME_LENGTH ; MoldNameChar++)
          {
            MoldMsg.ExistMoldName[ExistMoldNoIndex][MoldNameChar] = CheckMoldName[MoldNameChar];
          }
          ExistMoldNoIndex++;
        }
        CheckMoldNo++;
        if(CheckMoldNo == MAX_MOLDNO_LENGTH+1) MoldMsg.MoldCount = ExistMoldNoIndex;
      }
      if((MoldMsg.MoldCount == ExistMoldNoIndex))
      {
        CheckMoldNo = 1;
        ExistMoldNoIndex = 1;
        ControlMsg.TpMode = MANUAL_OPERATING;
        ClearPage();
      }
      if(SettingTemp.DelMoldData == USE)
      {
        RobotCfg.ErrCode = MANUAL_ERR_REBOOTING;
        ControlMsg.TpMode = MANUAL_ERROR;
        SettingTemp.DelMoldData = NO_USE;
        memcpy(&(RobotCfg.Setting), &SettingTemp, sizeof(MANUFACTURER_SETTING));
        WriteRobotCfg(&RobotCfg);
      }
      break;
      
    case MANUAL_VERSION:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP) ControlMsg.TpMode = MANUAL_OPERATING;
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case SETTING_MANUFACTURER:
      if(ControlMsg.EnableEdit == DISABLE_EDIT)
        if(ChkExpireSysTick(&RTCTempTick)) SettingTemp.Time = GetTime();
      if (UnderbarPos == 0) UnderbarPos = YEAR_POS;
      
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ClearPage();
        UnderbarPos = 0;
        ControlMsg.CommMode = MODE_SEQ;
        if(ControlMsg.fHomingDone == HOMING_START)
        {
          ControlMsg.TpMode = MANUAL_HOMING;
          PageChange = PAGE_MANUAL;
        }
        else if(ControlMsg.fHomingDone == HOMING_DONE) 
        {
          ControlMsg.TpMode = MANUAL_OPERATING;
          PageChange = PAGE_NONE;
        }
      }
      else if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(ControlMsg.EnableEdit == ENABLE_EDIT)
        {
          memcpy(&SettingTemp, &(RobotCfg.Setting), sizeof(MANUFACTURER_SETTING));
          ControlMsg.EnableEdit = DISABLE_EDIT;
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT)
        {
          //Date
          if (ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3)
          {
            UnderbarPos--;
            if (UnderbarPos == DATE_POS) ControlMsg.LineNo--;
          }
          //Time
          else if (ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2)
          {
            UnderbarPos--;
            if (UnderbarPos == 0) ControlMsg.LineNo--;
          }
          else if(ControlMsg.LineNo > 0) ControlMsg.LineNo--;
          else
          {
            if(ControlMsg.PageNo > 0)
            {
              ControlMsg.PageNo--;
              ControlMsg.LineNo = MAX_LINE_OUTLINE;
            }
          }
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if(ControlMsg.EnableEdit == DISABLE_EDIT)
        {
          if(ControlMsg.LineNo < MAX_LINE_OUTLINE)
          {
            //Time
            if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2) 
            {
              if (UnderbarPos < HOUR_POS) UnderbarPos++;
              if (UnderbarPos == HOUR_POS) ControlMsg.LineNo++;
            }
            if (!((ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3) || (ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2))) ControlMsg.LineNo++;
            //IMMType
            if((RobotCfg.RobotType == ROBOT_TYPE_XC) || (RobotCfg.RobotType == ROBOT_TYPE_X))
            {
              if(ControlMsg.PageNo == SETTING_MAX_PAGE_IMMTYPE && ControlMsg.LineNo > 0) ControlMsg.LineNo--;	
            }
            else 
            {
              if(ControlMsg.PageNo == SETTING_MAX_PAGE && ControlMsg.LineNo > 2) ControlMsg.LineNo--;
            }
          }
          //Date
          else if (ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3)
          {
            UnderbarPos++;
            if (UnderbarPos == SECOND_POS+1)
            {  
              UnderbarPos = SECOND_POS;
              ControlMsg.LineNo = 0;
              ControlMsg.PageNo++;
            }
          }
          else
          {
            //IMMType
            if((RobotCfg.RobotType == ROBOT_TYPE_XC) ||  (RobotCfg.RobotType == ROBOT_TYPE_X))
            {
              if(ControlMsg.PageNo < SETTING_MAX_PAGE_IMMTYPE)
              {
                ControlMsg.PageNo++;
                ControlMsg.LineNo = 0;
              }
            }
            else
            {
              if(ControlMsg.PageNo < SETTING_MAX_PAGE)
              {
                ControlMsg.PageNo++;
                ControlMsg.LineNo = 0;
              }
            }
          }
        }
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_LEFT)
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 0)
        {
          if(SettingTemp.ErrorTime == 0) SettingTemp.ErrorTime = 10;
          SettingTemp.ErrorTime--;
        }
        else if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 1) SettingTemp.ItlFullAuto ^= USE;
        else if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 2) SettingTemp.ItlSaftyDoor ^= USE;
        else if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 3) SettingTemp.ItlAutoInjection ^= USE;
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 0) SettingTemp.ItlReject ^= USE;
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 1)
        {
          if(SettingTemp.ProcessTime == 0) SettingTemp.ProcessTime = 100;
          SettingTemp.ProcessTime--;
        }
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2)
        {
          if(ControlMsg.CursorPos == 0) ControlMsg.CursorPos = 3;
          ControlMsg.CursorPos--;
        }
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3)
        {
          if(ControlMsg.CursorPos == 0) ControlMsg.CursorPos = 3;
          ControlMsg.CursorPos--;				  
        }
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 0) SettingTemp.DelMoldData ^= USE;
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 1) SettingTemp.DelErrHistory ^= USE;
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 2) SettingTemp.DoorSignalChange ^= USE;
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 3) SettingTemp.IMMType ^= USE;				//IMMType
        else if((ControlMsg.PageNo == 3 && ControlMsg.LineNo == 0) && (SettingTemp.IMMType == USE)) SettingTemp.RotationState ^= NO_ROTATION;		//IMMType
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_RIGHT)
      {
        ControlMsg.EnableEdit = ENABLE_EDIT;
        if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 0)
        {
          SettingTemp.ErrorTime++;
          if(SettingTemp.ErrorTime > MAX_TIME_FIND_ERR) SettingTemp.ErrorTime = 0;				  
        }
        else if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 1) SettingTemp.ItlFullAuto ^= USE;
        else if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 2) SettingTemp.ItlSaftyDoor ^= USE;
        else if(ControlMsg.PageNo == 0 && ControlMsg.LineNo == 3) SettingTemp.ItlAutoInjection ^= USE;
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 0) SettingTemp.ItlReject ^= USE;
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 1)
        {
          SettingTemp.ProcessTime++;
          if(SettingTemp.ProcessTime > MAX_TIME_PROCESS) SettingTemp.ProcessTime = 0;
        }
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2)
        {
          ControlMsg.CursorPos++;
          if(ControlMsg.CursorPos == 3) ControlMsg.CursorPos = 0;
        }
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3)
        {
          ControlMsg.CursorPos++;
          if(ControlMsg.CursorPos == 3) ControlMsg.CursorPos = 0;				  
        }
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 0) SettingTemp.DelMoldData ^= USE;
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 1) SettingTemp.DelErrHistory ^= USE;
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 2) SettingTemp.DoorSignalChange ^= USE;
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 3) SettingTemp.IMMType ^= USE;				//IMMType
        else if((ControlMsg.PageNo == 3 && ControlMsg.LineNo == 0) && (SettingTemp.IMMType == USE)) SettingTemp.RotationState ^= NO_ROTATION;		//IMMType
      }
      else if(ControlMsg.KeyValue == KEY_ENTER)
      {
        if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 0)
        {
          if(SettingTemp.DelMoldData == USE)  
          {
            for(uint16_t i=1 ; i < MAX_MOLDNO_LENGTH+1; i++)
            {
              if((POSSIBLE_MOLD_NO-1 < MoldMsg.ExistMold[i]) && (RobotCfg.MoldNo != MoldMsg.ExistMold[i]))
                DeleteMoldFile(MoldMsg.ExistMold[i]);
            }
            ControlMsg.TpMode = MANUAL_MOLD_DELETE_ING;
          }
        }
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 1)
        {
          if(SettingTemp.DelErrHistory == USE)  
          {
            ClearError(); 
            SettingTemp.DelErrHistory = NO_USE;
          }
        }
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2)
        {
          if((SettingTemp.Time.Date.Month < 13) && (SettingTemp.Time.Date.Date < 32))
          {
            SetTime(&(SettingTemp.Time));
            ControlMsg.EnableEdit = DISABLE_EDIT;
          }
        }
        else if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3)
        {
          if((SettingTemp.Time.Time.Hours < 23) && (SettingTemp.Time.Time.Minutes < 60) && (SettingTemp.Time.Time.Seconds < 60))
          {
            SetTime(&(SettingTemp.Time));
            ControlMsg.EnableEdit = DISABLE_EDIT;
          }
        }
        //IMMType
        else if(ControlMsg.PageNo == 2 && ControlMsg.LineNo == 3)
        {
          if(SettingTemp.IMMType == NO_USE) SettingTemp.RotationState = ROTATION;
        }
        
        ControlMsg.EnableEdit = DISABLE_EDIT;
        memcpy(&(RobotCfg.Setting), &SettingTemp, sizeof(MANUFACTURER_SETTING));
        memset(SeqMsg.SeqDispArr,0,sizeof(SeqMsg.SeqDispArr));
        memset(ControlMsg.StepDispTemp,0,sizeof(ControlMsg.StepDispTemp));
        memset(SeqMsg.SeqArr,0,sizeof(SeqMsg.SeqArr));
        if(RobotCfg.Setting.RotationState == NO_ROTATION) IMMTypeINIT(RobotCfg.MoldNo);	//IMMType
        MakeSequence();
        SdRes = WriteRobotCfg(&RobotCfg);
        if(SdRes == SD_OK) WriteRobotCfgBuakUp(&RobotCfg);
        ControlMsg.fDoHoming = 1;
        ControlMsg.CommMode = MODE_INIT;
      }
      else if(KeyIsNum(&ControlMsg.KeyValue))
      {
        if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 2)
        {
          ControlMsg.EnableEdit = ENABLE_EDIT;
          if(ControlMsg.EnableEdit == ENABLE_EDIT)
          {
            if(UnderbarPos == YEAR_POS)
              SettingTemp.Time.Date.Year = ((SettingTemp.Time.Date.Year)*10 + KeyNum(&ControlMsg.KeyValue)) % 100;
            else if(UnderbarPos == MONTH_POS)
              SettingTemp.Time.Date.Month = ((SettingTemp.Time.Date.Month)*10 + KeyNum(&ControlMsg.KeyValue)) % 100;
            else if(UnderbarPos == DATE_POS)
              SettingTemp.Time.Date.Date = ((SettingTemp.Time.Date.Date)*10 + KeyNum(&ControlMsg.KeyValue)) % 100;
          }
        }
        if(ControlMsg.PageNo == 1 && ControlMsg.LineNo == 3)
        {
          ControlMsg.EnableEdit = ENABLE_EDIT;
          if(ControlMsg.EnableEdit == ENABLE_EDIT)
          {
            if(UnderbarPos == HOUR_POS)
              SettingTemp.Time.Time.Hours = ((SettingTemp.Time.Time.Hours)*10 + KeyNum(&ControlMsg.KeyValue)) % 100;
            else if(UnderbarPos == MINUTE_POS)
              SettingTemp.Time.Time.Minutes = ((SettingTemp.Time.Time.Minutes)*10 + KeyNum(&ControlMsg.KeyValue)) % 100;
            else if(UnderbarPos == SECOND_POS)
              SettingTemp.Time.Time.Seconds = ((SettingTemp.Time.Time.Seconds)*10 + KeyNum(&ControlMsg.KeyValue)) % 100;
          }
        }
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;	  
      
    case MANUAL_ERROR_LIST:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ControlMsg.TpMode = MANUAL_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.PageNo > 0)
          ControlMsg.PageNo--;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if (ControlMsg.PageNo != ErrMsg.ErrorCount-1)
          ControlMsg.PageNo++;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case AUTO_ERROR_LIST:
      if(ControlMsg.KeyReady == KEY_READY) break;
      if(ControlMsg.KeyValue == KEY_MENU_STOP)
      {
        ControlMsg.TpMode = MANUAL_OPERATING;
        ClearPage();
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_UP)
      {
        if(ControlMsg.PageNo > 0)
          ControlMsg.PageNo--;
      }
      else if(ControlMsg.KeyValue == KEY_ARROW_DOWM)
      {
        if (ControlMsg.PageNo != ErrMsg.ErrorCount-1)
          ControlMsg.PageNo++;
      }
      else if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case MANUAL_ERROR:
      if(RobotCfg.ErrCode == MANUAL_ERR_REBOOTING) break;
      if(ControlMsg.KeyValue == KEY_CLEAR)
      {
        if(RobotCfg.ErrCode == MANUAL_ERR_NO_MOLDNO) ControlMsg.TpMode = MANUAL_MOLD_MANAGE;
        else if (RobotCfg.ErrCode != MANUAL_ERR_SAFTYDOOR_CHANGE) ControlMsg.TpMode = MANUAL_OPERATING;
        RobotCfg.ErrorState = ERROR_STATE_NONE;
        ClearPage();
      }
      if(RobotCfg.ErrCode == MANUAL_ERR_NO_MOLDNO)
      {
        if (ControlMsg.KeyValue == KEY_ENTER) ControlMsg.TpMode = MANUAL_MOLD_MANAGE;
        if (!(ChkExpireSysTick(&MoldOpenTempTick))) break;
        RobotCfg.ErrorState = ERROR_STATE_NONE;
        ControlMsg.TpMode = MANUAL_MOLD_MANAGE;
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_SAFTYDOOR_CHANGE)
      {
        if (IOMsg.X1A_SaftyDoor == SIGNAL_ON)
        {
          if(ControlMsg.fHomingDone == HOMING_DONE) 
          {
            ControlMsg.CommStateForDL = 2;
            ControlMsg.TpMode = MANUAL_OPERATING;
            PageChange = PAGE_NONE;
          }
          else 
          {
            ChangeSCMode();
            ControlMsg.TpMode = MANUAL_HOMING;
            PageChange = PAGE_MANUAL;
          }
        }
      }
      if(ControlMsg.KeyValue == KEY_LANGUAGE) ChangeLanguage();
      break;
      
    case OCCUR_ERROR:
      break;
      
    case SD_BACKUP:
      if (!(ChkExpireSysTick(&SDTempTick))) break;
      //		WriteMold(&MoldMsg);
      SdRes = WriteMold(&MoldMsg);
      if(SdRes == SD_OK)
      {
        if(fPreviousPage == MANUAL_PAGE_SD) 
        {
          ControlMsg.TpMode = MANUAL_OPERATING;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
        else if(fPreviousPage == MANUAL_MODE_SD) 
        {
          ControlMsg.TpMode = MANUAL_MODE;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
        else if(fPreviousPage == MANUAL_COUNTER_SD) 
        {
          ControlMsg.TpMode = MANUAL_COUNTER;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
        else if(fPreviousPage == MANUAL_TIMER_SD) 
        {
          ControlMsg.TpMode = MANUAL_TIMER;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
        else if(fPreviousPage == MANUAL_HOMING_SD) 
        {
          ControlMsg.TpMode = MANUAL_HOMING;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          break;
        }
        else if(fPreviousPage == MANUAL_ERROR_SD) 
        {
          ControlMsg.TpMode = OCCUR_ERROR;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          break;
        }
        else if(fPreviousPage == AUTO_PAGE_SD) 
        {
          ControlMsg.TpMode = AUTO_OPERATING;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          break;
        }
        else if(fPreviousPage == AUTO_COUNTER_PAGE_SD) 
        {
          ControlMsg.TpMode = AUTO_COUNTER;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          break;
        }
        else if(fPreviousPage == AUTO_TIMER_PAGE_SD) 
        {
          ControlMsg.TpMode = AUTO_TIMER;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);	
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
      }
      //		else 
      //		{
      //		  //Retry #3, Delay 300ms
      //		  if(!(ChkExpireSysTick(&SDTempTick))) break;
      //		  BootingRetry++;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);
      //		  if(BootingRetry < BOOTING_ERROR_COUNT) SdRes = WriteMold(&MoldMsg);
      //		  else
      //		  {
      //			 BootingRetry = 0;
      //			 RobotCfg.ErrorState = ERROR_STATE_OCCUR;
      //			 RobotCfg.ErrCode = ERRCODE_SD_NOT_FILE;
      //		  }
      //		}
      break;
      
    case SD_WRITE:
      if (!(ChkExpireSysTick(&SDTempTick))) break;
      //		WriteRobotCfg(&RobotCfg);
      SdRes = WriteRobotCfg(&RobotCfg);
      if(SdRes == SD_OK)
      {
        if(fPreviousPage == MANUAL_OPENMOLD_SD) 
        {
          ChangeSCMode();
          fPreviousPage = WAITING_SD;
          ControlMsg.fDoHoming = 1;
          ControlMsg.TpMode = MANUAL_HOMING;
          PageChange = PAGE_MANUAL;
          ClearPage();
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
        else if(fPreviousPage == MANUAL_NEWMOLD_SD) 
        {
          ControlMsg.TpMode = MANUAL_HOMING;
          fPreviousPage = WAITING_SD;
          SetSysTick(&SDTempTick, TIME_SC_DELAY);
          WriteRobotCfgBuakUp(&RobotCfg);
          break;
        }
      }
      //		else 
      //		{
      //		  if(fPreviousPage == MANUAL_OPENMOLD_SD) 
      //		  {
      //			 ChangeSCMode();
      //			 fPreviousPage = WAITING_SD;
      //			 ControlMsg.fDoHoming = 1;
      //			 ControlMsg.TpMode = MANUAL_HOMING;
      //			 PageChange = PAGE_MANUAL;
      //			 ClearPage();
      //			 break;
      //		  }
      //		  else if(fPreviousPage == MANUAL_NEWMOLD_SD) 
      //		  {
      //			 ControlMsg.TpMode = MANUAL_HOMING;
      //			 fPreviousPage = WAITING_SD;
      //			 SetSysTick(&SDTempTick, TIME_SC_DELAY);
      //			 break;
      //		  }
      
      //Retry #3, Delay 300ms
      //		  if(!(ChkExpireSysTick(&SDTempTick))) break;
      //		  BootingRetry++;
      //		  SetSysTick(&SDTempTick, TIME_SC_DELAY);
      //		  if(BootingRetry < BOOTING_ERROR_COUNT) SdRes = WriteRobotCfg(&RobotCfg);
      //		  else
      //		  {
      //			 BootingRetry = 0;
      //			 RobotCfg.ErrorState = ERROR_STATE_OCCUR;
      //			 RobotCfg.ErrCode = ERRCODE_SD_NOT_FILE;
      //		  }
      //		}
      break;
    default:
      break;
    }
    ControlMsg.IsChangedLCD = NEED_CHANGE;
    //		MainLoopState = MANUAL_OPERATING_COMM_LCD;
    break;
    
  case MANUAL_OPERATING_COMM_LCD:
    //		RefreshState();
    //		ControlMsg.TpMode = MANUAL_OPERATING;
    ControlMsg.IsChangedLCD = NEED_CHANGE;
    MainLoopState = ACTIVE_RUN;
    break;
  }
  ControlMsg.KeyReady = KEY_READY;
}
