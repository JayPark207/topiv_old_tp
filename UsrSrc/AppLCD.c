#include "main.h"
#include "AppLCD.h"
#include <string.h>
#include "PageBackground.h"
#include "Icon.h"
#include "page_msg.h"
#include "ksc2kssm.h"
#include "Han10x10.h"
#include "rom8x8.h"
#include <stdarg.h>
#include "AppKey.h"

extern IO_MSG IOMsg;
extern ERROR_MSG ErrMsg;
extern ERROR_MSG ErrMsgTemp[100];
extern CONTROL_MSG ControlMsg;
extern ROBOT_CONFIG RobotCfg;
extern MOTION_DELAY MotionDelayTemp;
extern MOTION_MODE MotionModeTemp;
extern MANUFACTURER_SETTING SettingTemp;
extern MOTION_DELAY CycleDelay;
extern MOLD_MSG MoldMsg;

extern uint8_t StepCursor;

extern char NewMoldName[MAX_MOLDNAME_LENGTH];
extern char NewMoldNameTemp[MAX_MOLDNAME_LENGTH];
extern uint8_t MoldNameCharPos;
extern uint8_t MoldNamePos;
extern uint16_t MoldNoTemp;
extern uint8_t UnderbarPos;

extern uint8_t AutoStepDelay[20];

char str0[5];
char str1[5];
char str2[5];
char str3[5];
char str4[5];
char str5[5];

char MoldNameStr0[10];
char MoldNameStr1[10];
char MoldNameStr2[10];

uint8_t Display_Scr[1024];
uint8_t lcd_init = 0;
//uint8_t rom8x8_bits[];

uint8_t* Font;

void ControlBacklight(uint8_t cmd)
{
  if(cmd)
    HAL_GPIO_WritePin(LCD_BACKLIGHT_GPIO_Port, LCD_BACKLIGHT_Pin, GPIO_PIN_SET);
  else
    HAL_GPIO_WritePin(LCD_BACKLIGHT_GPIO_Port, LCD_BACKLIGHT_Pin, GPIO_PIN_RESET);
}
void Lcd_Display_On_Off(uint32_t board, uint8_t on_off)
{
  uint32_t addr;
  
  addr = (uint32_t)(board | LCD_ENABLE | LCD_WRITE | LCD_INST);
  
  //	printf("On_Off address = %x, board = %X\n", addr, board);
  *(volatile uint8_t *)addr = (uint8_t)(on_off);
}
uint8_t LCD_ReadStatus(uint32_t board)		//board = LCD_CS1 or LCD_CS2
{
  uint32_t addr;
  uint8_t data;
  
  addr = (uint32_t)(board | LCD_ENABLE | LCD_READ | LCD_INST);
  data = *(volatile uint8_t *)addr;
  
  return data;
}
void Lcd_Write_Display_Data(uint32_t board, uint8_t data)
{
  uint32_t addr;
  
  addr = (uint32_t)(board | LCD_ENABLE | LCD_WRITE | LCD_DATA);
  *(volatile uint8_t *)addr = data;
}

void Lcd_Page_Address_Set(uint32_t board, uint8_t page_addr)
{
  uint32_t addr;
  
  addr = (uint32_t)(board | LCD_ENABLE | LCD_WRITE | LCD_INST);
  
  *(volatile uint8_t *)addr = (uint8_t)(LCD_INST_X_ADDR | page_addr);
}

void Lcd_Display_Start_Line(uint32_t board, uint8_t start_line)
{
  uint32_t addr;
  
  addr = (uint32_t)(board | LCD_ENABLE | LCD_WRITE | LCD_INST);
  *(volatile uint8_t *)addr = (uint8_t)(LCD_INST_Z_ADDR | start_line);
}

void Lcd_Set_Address(uint32_t board, uint8_t address)
{
  uint32_t addr;
  addr = (uint32_t)(board | LCD_ENABLE | LCD_WRITE | LCD_INST);
  
  *(volatile uint8_t *)addr = (uint8_t)(LCD_INST_Y_ADDR | address);
}

void LCD_WaitBusy(uint32_t board)	//board = LCD_CS1 or LCD_CS2
{
  uint8_t data;
  uint8_t chk = 0;
  
  while(chk == 0)
  {
    data = LCD_ReadStatus(board);
    if((data & LCD_INST_STATUS_BUSY) == 0)
    {
      chk = 1;
      //			if(lcd_init) printf("LCD Status = %X\n", data);
    }
    //		else if(lcd_init)
    //			printf("LCD Status = %X\n", data);
  }
}
void Draw_Clear_Box(uint8_t x, uint8_t y, uint8_t width, uint8_t height)
{
  uint8_t cur_page, end_page;
  uint8_t i, j;
  
  if(((y % 8) == 0) && ((height % 8) == 0))
  {
    cur_page = y / 8;
    end_page = cur_page + (height / 8);
    if(cur_page == 0)
    {
      memset((Display_Scr + x), 0x01, width);
      cur_page++;
    }
    if(end_page == 8)
    {
      memset((Display_Scr + 896 + x), 0x80, width);
      end_page--;
    }
    for(i = cur_page ; i < end_page ; i++)
    {
      memset((Display_Scr + (i * 128) + x), 0x00, width);
    }
  }
  else
  {
    for(i = 0 ; i < height ; i++)
    {
      cur_page = (y - (y % 8)) / 8;
      for(j = 0 ; j < width ; j++)
      {
        *(Display_Scr + (cur_page * 128) + x + j) = (uint8_t)((*(Display_Scr + (cur_page * 128) + x + j) & (~(0x01 << (y % 8)))));
      }
      y++;
    }
  }
}

uint8_t LCD_Draw(uint8_t left, uint8_t top, uint8_t width, uint8_t height)
{
  uint8_t status;
  uint32_t x, y;
  
  if(top >= 64)
  {
    return ERROR;
  }
  top = (uint8_t)((top - (top % 8)) / 8);
  
  if(left >= 128)
  {
    return ERROR;
  }
  
  if((width > 128) || (width < left))
  {
    return ERROR;
  }
  
  if((height > 64) || (height < top))
  {
    return ERROR;
  }
  if((height % 8) != 0)
  {
    height = (uint8_t)((height - (height % 8) + 8) / 8);
  }
  else
  {
    height = (uint8_t)(height / 8);
  }
  
  if(left >= 64)
  {
    status = 1;
  }
  else if(width <= 64)
  {
    status = 0;
  }
  else
  {
    status = 2;
  }
  
  if(status == 0)
  {
    for(y = top ; y < height ; y++)
    {
      LCD_WaitBusy(LCD_CS1);
      Lcd_Page_Address_Set(LCD_CS1, y);
      LCD_WaitBusy(LCD_CS1);
      Lcd_Set_Address(LCD_CS1, left);
      
      for(x = left ; x < width ; x++)
      {
        LCD_WaitBusy(LCD_CS1);
        Lcd_Write_Display_Data(LCD_CS1, *(volatile uint8_t *)(Display_Scr + (y * 128) + x));
      }
    }
  }
  else if(status == 1)
  {
    for(y = top ; y < height ; y++)
    {
      LCD_WaitBusy(LCD_CS2);
      Lcd_Page_Address_Set(LCD_CS2, y);
      LCD_WaitBusy(LCD_CS2);
      Lcd_Set_Address(LCD_CS2, (left - 64));
      
      for(x = (left - 64) ; x < (width - 64) ; x++)
      {
        LCD_WaitBusy(LCD_CS2);
        Lcd_Write_Display_Data(LCD_CS2, *(volatile uint8_t *)(Display_Scr + (y * 128) + (x + 64)));
      }
    }
  }
  else
  {
    for(y = top ; y < height ; y++)
    {
      LCD_WaitBusy(LCD_CS1);
      Lcd_Page_Address_Set(LCD_CS1, y);
      LCD_WaitBusy(LCD_CS1);
      Lcd_Set_Address(LCD_CS1, left);
      
      for(x = left ; x < 64 ; x++)
      {
        LCD_WaitBusy(LCD_CS1);
        Lcd_Write_Display_Data(LCD_CS1, *(volatile uint8_t *)(Display_Scr + (y * 128) + x));
      }
    }
    
    for(y = top ; y < height ; y++)
    {
      LCD_WaitBusy(LCD_CS2);
      Lcd_Page_Address_Set(LCD_CS2, y);
      LCD_WaitBusy(LCD_CS2);
      Lcd_Set_Address(LCD_CS2, 0);
      
      for(x = 0 ; x < (width - 64) ; x++)
      {
        LCD_WaitBusy(LCD_CS2);
        Lcd_Write_Display_Data(LCD_CS2, *(volatile uint8_t *)(Display_Scr + (y * 128) + (x + 64)));
      }
    }
  }
  return SUCCESS;
}

void Draw_Icon(uint32_t x, uint32_t y, uint32_t width, uint8_t icon_num, uint8_t clear_flag)
{
  uint32_t cur_page;
  int32_t i;
  uint8_t *image;
  
  image = (Icon_Graphic + (icon_num * 32));
  if(clear_flag == REDRAW_OFF)
  {
    y = (y / 16) * 2;
    cur_page = y;
    cur_page *= 128;
    cur_page += x;
    width--;
    for(i = width ; i >= 0 ; i--)
    {
      *(Display_Scr + cur_page + i) |= *(image + i);
    }
    
    cur_page = y + 1;
    cur_page *= 128;
    cur_page += x;
    image += 16;
    for(i = width ; i >= 0 ; i--)
    {
      *(Display_Scr + cur_page + i) |= *(image + i);
    }
  }
  else
  {
    y = (y / 16) * 2;
    cur_page = y;
    cur_page *= 128;
    cur_page += x;
    memcpy(Display_Scr + cur_page, image, width);
    
    cur_page = y + 1;
    cur_page *= 128;
    cur_page += x;
    image += 16;
    memcpy(Display_Scr + cur_page, image, width);
  }
}


//////////////////////////////////////////////  한글  //////////////////////////////////////////////
void Or_Hancode(uint8_t * addr, uint8_t * buffer)
{
  int32_t i;
  
  for(i = 28 ; i >= 0 ; i -= 4)
  {
    *(uint32_t *)(buffer + i) |= *(uint32_t *)(addr + i);
  }
}
void Copy_Hancode(uint8_t * addr, uint8_t * buffer)
{
  memcpy(buffer, addr, 32);
}


void Hancode(uint16_t code, uint8_t *buffer)
{
  uint8_t first2[] = {0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
  uint8_t middle2[] = {0, 0, 0, 1, 2, 3, 4, 5, 0, 0, 6, 7, 8, 9, 10, 11, 0, 0, 12, 13, 14, 15, 16, 17, 0, 0, 18, 19, 20, 21};
  uint8_t last2[] = {0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 0, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27};
  uint8_t cho[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 1, 2, 4, 4, 4, 2, 1, 3, 0};
  uint8_t cho2[] = {0, 5, 5, 5, 5, 5, 5, 5, 5, 6, 7, 7, 7, 6, 6, 7, 7, 7, 6, 6, 7, 5};
  uint8_t jong[] = {0, 0, 2, 0, 2, 1, 2, 1, 2, 3, 0, 2, 1, 3, 3, 1, 2, 1, 3, 3, 1, 1};
  
  uint32_t first, middle, last;
  uint32_t offset;
  uint32_t value;
  uint8_t * hangle_addr;
  
  value = 32;
  hangle_addr = Hangle10x10;
  
  first = first2[(code >> 10) & 0x1F];			
  middle = middle2[(code >> 5) & 0x1F];			
  last = last2[(code) & 0x1F];					
  
  if(last == 0)
  {
    offset = cho[middle] * 20 * value;
    offset += first * value;
    Copy_Hancode((hangle_addr + offset), buffer);
    
    if((first == 1) || (first == 24))
    {
      offset = 160 * value;
    }
    else
    {
      offset = 182 * value;
    }
    offset += middle * value;
    Or_Hancode((hangle_addr + offset), buffer);
  }
  else
  {
    offset = cho2[middle] * 20 * value;
    offset += first * value;
    Copy_Hancode((hangle_addr + offset), buffer);
    
    if((first == 1) || (first == 24))
    {
      offset = 204 * value;
    }
    else
    {
      offset = 226 * value;
    }
    offset += middle * value;
    Or_Hancode((hangle_addr + offset), buffer);
    offset = (248 * value) + (jong[middle] * 28 * value);
    offset += last * value;
    Or_Hancode((hangle_addr + offset), buffer);
  }
}


/****************************************************/
/*	func:	Draw_Hanfont										 */
/*--------------------------------------------------*/
/*	return	void												 */
/****************************************************/

void Draw_Hanfont(uint32_t x, uint32_t y, uint8_t *image)
{
  uint32_t cur_page;
  int32_t i;
  
  y = (y / 16) * 2;
  cur_page = y;
  cur_page *= 128;
  cur_page += x;
  for(i = 9 ; i >= 0 ; i--)
  {
    *(Display_Scr + cur_page + i) |= *(image + i);
  }
  
  cur_page = y + 1;
  cur_page *= 128;
  cur_page += x;
  image += 16;
  for(i = 9 ; i >= 0 ; i--)
  {
    *(Display_Scr + cur_page + i) |= *(image + i);
  }
}

void Get_Textbits(uint32_t ch, uint8_t *retmap)
{
  uint8_t *	bits;
  
  if(ch >= 256)
  {
    ch = 0;
  }
  bits = Font + (ch * 16);
  memcpy(retmap, bits, 16);
}

void Draw_Font(uint32_t x, uint32_t y, uint8_t *table)
{
  uint32_t cur_page;
  int32_t i;
  
  y = (y / 16) * 2;
  cur_page = y;
  cur_page *= 128;
  cur_page += x;
  for(i = 7 ; i >= 0 ; i--)
  {
    *(Display_Scr + cur_page + i) |= *(table + i);
  }
  
  cur_page = y + 1;
  cur_page *= 128;
  cur_page += x;
  table += 8;
  for(i = 7 ; i >= 0 ; i--)
  {
    *(Display_Scr + cur_page + i) |= *(table + i);
  }
}

void Lcd_Printf(uint32_t x, uint32_t y, uint32_t w, char * str, uint32_t align, uint8_t clear_flag)
{
  uint16_t code = 0;
  
  uint16_t test = 0;
  uint16_t ustemp = 0;
  uint16_t base_offset = 0xB0A1;
  uint16_t base_char = 0;
  uint8_t temp[32];
  
  uint8_t n = 0, e = 0;
  char * text = str;
  uint8_t bitmap[16];
  
  if(clear_flag == REDRAW_ON)
  {
    Draw_Clear_Box(x, (y / 16) * 16, w, 16);
  }
  
  Font = rom8x8_bits;
  //  Font = Hangle10x10;
  while(*text)
  {
    code = *text++;
    if(code >= 128)
    {
      text++;
      n++;
    }
    else
    {
      e++;
    }
  }
  
  if(align == RIGHT_ALIGN)
  {
    x = x + w - ((n * 10) + (e * 8));
  }
  else if(align == CENTER_ALIGN)
  {
    x = x + ((w - ((n * 10) + (e * 8))) / 2);
  }
  else
  {
    
  }
  
  while(*str)
  {
    code = *str++;
    
    //	 Get_Textbits(code, bitmap);
    ////	 Draw_Font(x, y, bitmap);
    //	 Draw_Hanfont(x, y, bitmap);
    //	 x += 8;
    if(code >= 128)
    {
      code *= 256;
      code |= *str++;
      
      ustemp = code & 0x0F00;
      if(code > 0xBFFE)
      {
        base_offset = 0xC0A1;
        base_char = 1504;
      }
      else
      {
        base_offset = 0xB0A1;
        base_char = 0;
      }
      
      test = code - (base_offset | ustemp) + (base_char + (94 * (ustemp >> 8)));
      code = *(hcode + test);
      
      Hancode(code, temp);
      Draw_Hanfont(x, y, temp);
      x += 10;
    }
    else
    {
      Get_Textbits(code, bitmap);
      Draw_Font(x, y, bitmap);
      x += 8;
    }
  }
}

uint8_t Lcd_Initial(void)
{
  uint8_t error;
  
  LCD_WaitBusy(LCD_CS1);
  LCD_WaitBusy(LCD_CS2);
  
  Draw_Clear_Box(0, 0, 128, 64);
  memset(Display_Scr, 0, 1024);
  
  //	  LCD_Draw(0, 0, 128, 64);
  
  error = LCD_Draw(0, 0, 128, 64);
  
  LCD_WaitBusy(LCD_CS1);
  Lcd_Display_Start_Line(LCD_CS1, 0x00);
  LCD_WaitBusy(LCD_CS2);
  Lcd_Display_Start_Line(LCD_CS2, 0x00);
  
  LCD_WaitBusy(LCD_CS1);
  Lcd_Display_On_Off(LCD_CS1, LCD_INST_DISPLAY_ON);
  LCD_WaitBusy(LCD_CS2);
  Lcd_Display_On_Off(LCD_CS2, LCD_INST_DISPLAY_ON);
  
  //	 printf("Initialized LCD......\n");
  return error;
}


void Language_Initial(void)
{
  if(RobotCfg.Language == LANGUAGE_KOR)
  {
    Page_Name_List = Page_Name_List_Kor;
    Factory_Setup_Msg = Factory_Setup_Msg_Kor;
    Factory_Setup_Option = Factory_Setup_Option_Kor;
    File_Search_Msg = File_Search_Msg_Kor;
    Delete_Page_Msg = Delete_Page_Msg_Kor;
    Mode_Page_Msg = Mode_Page_Msg_Kor;
    Mode_Page_Option = Mode_Page_Option_Kor;
    IO_Page_Msg = IO_Page_Msg_Kor;
    Timer_Page_Msg = Timer_Page_Msg_Kor;
    Count_Setup_Page_Msg = Count_Setup_Page_Msg_Kor;
    Step_Name_Table = Step_Name_Table_Kor;
    Error_Pop_Page_Msg = Error_Pop_Page_Msg_Kor;
    Massage_Pop_Msg = Massage_Pop_Msg_Kor;
    Basic_Mode_Page_Msg = Basic_Mode_Page_Msg_Kor;
    Basic_Mode_Page_Option = Basic_Mode_Page_Option_Kor;
    Auto_Run_Popup_Msg = Auto_Run_Popup_Msg_Kor;
    Error_Pop_Page_Msg = Error_Pop_Page_Msg_Kor;
    Error_Msg = Error_Msg_Kor;
    Error_Action_Msg = Error_Action_Msg_Kor;
	 Factory_Setup_Option_Setting = Factory_Setup_Option_SettingKor;
  }
  else if(RobotCfg.Language == LANGUAGE_ENG)//ENG
  {
    Page_Name_List = Page_Name_List_Eng;
    Factory_Setup_Msg = Factory_Setup_Msg_Eng;
    Factory_Setup_Option = Factory_Setup_Option_Eng;
    File_Search_Msg = File_Search_Msg_Eng;
    Delete_Page_Msg = Delete_Page_Msg_Eng;
    Mode_Page_Msg = Mode_Page_Msg_Eng;
    Mode_Page_Option = Mode_Page_Option_Eng;
    IO_Page_Msg = IO_Page_Msg_Eng;
    Timer_Page_Msg = Timer_Page_Msg_Eng;
    Count_Setup_Page_Msg = Count_Setup_Page_Msg_Eng;
    Step_Name_Table = Step_Name_Table_Eng;
    Error_Pop_Page_Msg = Error_Pop_Page_Msg_Eng;
    Massage_Pop_Msg = Massage_Pop_Msg_Eng;
    Basic_Mode_Page_Msg = Basic_Mode_Page_Msg_Eng;
    Basic_Mode_Page_Option = Basic_Mode_Page_Option_Eng;
    Auto_Run_Popup_Msg = Auto_Run_Popup_Msg_Eng;
    Error_Pop_Page_Msg = Error_Pop_Page_Msg_Eng;
    Error_Msg = Error_Msg_Eng;
    Error_Action_Msg = Error_Action_Msg_Eng;
	 Factory_Setup_Option_Setting = Factory_Setup_Option_SettingEng;
  }
}

//void Delay_100ms(uint8_t test)
//	{
//  SetSysTick(&TempTick, TimerValueTick);
//  if(!ChkExpireSysTick(&TempTick));
//  else
//    RobotCfg.MotionDelay.ChuckDelay = RobotCfg.MotionDelay.ChuckDelay-TimerValueTick;
//	}

void TimerSetting(void)
{
  for (uint8_t TimeDispSetting = 0 ; TimeDispSetting < 20 ; TimeDispSetting++)
  {
    if (ControlMsg.TimerDispTemp[TimeDispSetting] == DOWN_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.DownDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == KICK_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.KickDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == EJECTOR_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.EjectorDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == CHUCK_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.ChuckDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == KICK_RETURN_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.KickReturnDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == UP_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.UpDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SWING_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.SwingDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SECOND_DOWN_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.Down2ndDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == RELEASE_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.OpenDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SECOND_UP_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.Up2ndDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == CHUCK_ROTATION_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.ChuckRotateReturnDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SWING_RETURN_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.SwingReturnDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == NIPPER_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.NipperOnDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == CONVEYOR_TIME)
      ControlMsg.TimerDelay[TimeDispSetting] = RobotCfg.MotionDelay.ConveyorDelay;
  }
  
  if(ControlMsg.PageNo == 0)
  {
    sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[DOWN_TIME] / 10));
    sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[KICK_TIME] / 10));
    sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[EJECTOR_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 1)
  {
    sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[CHUCK_TIME] / 10));
    sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[KICK_RETURN_TIME] / 10));
    sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[UP_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 2)
  {
    sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[SWING_TIME] / 10));
    sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[SECOND_DOWN_TIME] / 10));
    sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[RELEASE_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 3)
  {
    sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[SECOND_UP_TIME] / 10));
    sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[CHUCK_ROTATION_TIME] / 10));
    sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[SWING_RETURN_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 4)
  {
    sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[NIPPER_TIME] / 10));
    sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[CONVEYOR_TIME] / 10));
  }
  
  for (uint8_t TimeDispSetting = 0 ; TimeDispSetting < 20 ; TimeDispSetting++)
  {
    if (ControlMsg.TimerDispTemp[TimeDispSetting] == DOWN_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.DownDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == KICK_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.KickDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == EJECTOR_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.EjectorDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == CHUCK_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.ChuckDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == KICK_RETURN_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.KickReturnDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == UP_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.UpDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SWING_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.SwingDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SECOND_DOWN_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.Down2ndDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == RELEASE_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.OpenDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SECOND_UP_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.Up2ndDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == CHUCK_ROTATION_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.ChuckRotateReturnDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == SWING_RETURN_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.SwingReturnDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == NIPPER_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.NipperOnDelay;
    else if (ControlMsg.TimerDispTemp[TimeDispSetting] == CONVEYOR_TIME)
      ControlMsg.TimerDelayTemp[TimeDispSetting] = MotionDelayTemp.ConveyorDelay;
  }
  
  if(ControlMsg.PageNo == 0)
  {
    sprintf(str3, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[DOWN_TIME] / 10));
    sprintf(str4, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[KICK_TIME] / 10));
    sprintf(str5, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[EJECTOR_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 1)
  {
    sprintf(str3, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[CHUCK_TIME] / 10));
    sprintf(str4, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[KICK_RETURN_TIME] / 10));
    sprintf(str5, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[UP_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 2)
  {
    sprintf(str3, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[SWING_TIME] / 10));
    sprintf(str4, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[SECOND_DOWN_TIME] / 10));
    sprintf(str5, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[RELEASE_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 3)
  {
    sprintf(str3, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[SECOND_UP_TIME] / 10));
    sprintf(str4, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[CHUCK_ROTATION_TIME] / 10));
    sprintf(str5, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[SWING_RETURN_TIME] / 10));
  }
  else if(ControlMsg.PageNo == 4)
  {
    sprintf(str3, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[NIPPER_TIME] / 10));
    sprintf(str4, "%1.1f", (float)((float)ControlMsg.TimerDelayTemp[CONVEYOR_TIME] / 10));
  }
}

void StepTimerSetting(void)
{
  for (uint8_t StepDispSetting = 0 ; StepDispSetting <= sizeof(ControlMsg.StepDispTemp) ; StepDispSetting++)
  {
    if (ControlMsg.StepDispTemp[StepDispSetting] == DOWN_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.DownDelay;
    else if (ControlMsg.StepDispTemp[StepDispSetting] == KICK_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.KickDelay;
    else if (ControlMsg.StepDispTemp[StepDispSetting] == EJECTOR_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.EjectorDelay;
    else if (ControlMsg.StepDispTemp[StepDispSetting] == CHUCK_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.ChuckDelay;
    else if (ControlMsg.StepDispTemp[StepDispSetting] == KICK_RETURN_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.KickReturnDelay;
    else if (ControlMsg.StepDispTemp[StepDispSetting] == UP_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.UpDelay;
    else if (ControlMsg.StepDispTemp[StepDispSetting] == SWING_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.SwingDelay;
    else if (ControlMsg.StepDispTemp[StepDispSetting] == SECOND_DOWN_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.Down2ndDelay;
    else if (ControlMsg.StepDispTemp[StepDispSetting] == CHOUCK_RELEASE_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.OpenDelay;
    else if (ControlMsg.StepDispTemp[StepDispSetting] == SECOND_UP_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.Up2ndDelay;
    else if (ControlMsg.StepDispTemp[StepDispSetting] == CHUCK_ROTATION_RETURN_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.ChuckRotateReturnDelay;
    else if (ControlMsg.StepDispTemp[StepDispSetting] == SWING_RETURN_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.SwingReturnDelay;
    else if (ControlMsg.StepDispTemp[StepDispSetting] == NIPPERCUT_STEP)
      ControlMsg.TimerDelay[StepDispSetting] = RobotCfg.MotionDelay.NipperOnDelay;
    else
      ControlMsg.TimerDelay[StepDispSetting] = 0;
  }
  
  if((ControlMsg.PageNo == 0) || (ControlMsg.AutoPageNo == 0))
  {
    sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[DOWN_STEP] / 10));
    sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[KICK_STEP] / 10));
    sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[EJECTOR_STEP] / 10));
  }
  if((ControlMsg.PageNo == 1) || (ControlMsg.AutoPageNo == 1))
  {
    sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[CHUCK_STEP] / 10));
    sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[KICK_RETURN_STEP] / 10));
    sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[UP_STEP] / 10));
  }
  if((ControlMsg.PageNo == 2) || (ControlMsg.AutoPageNo == 2))
  {
    sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[SWING_STEP] / 10));
    sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[CHUCK_ROTATION_STEP] / 10));
    sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[SECOND_DOWN_STEP] / 10));
  }
  if((ControlMsg.PageNo == 3) || (ControlMsg.AutoPageNo == 3))
  {
    sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[NIPPERCUT_STEP] / 10));
    sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[CHOUCK_RELEASE_STEP] / 10));
    sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[SECOND_UP_STEP] / 10));
  }
  if((ControlMsg.PageNo == 4) || (ControlMsg.AutoPageNo == 4))
  {
    sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[CHUCK_ROTATION_RETURN_STEP] / 10));
    sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[SWING_RETURN_STEP] / 10));
    sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[14] / 10));
  }
  if((ControlMsg.PageNo == 5) || (ControlMsg.AutoPageNo == 5))
  {
    sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[15] / 10));
    sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[16] / 10));
    sprintf(str2, "%1.1f", (float)((float)ControlMsg.TimerDelay[17] / 10));
  }
  if((ControlMsg.PageNo == 6) || (ControlMsg.AutoPageNo == 6))
  {
    sprintf(str0, "%1.1f", (float)((float)ControlMsg.TimerDelay[18] / 10));
    sprintf(str1, "%1.1f", (float)((float)ControlMsg.TimerDelay[19] / 10));
  }
  
  if((ControlMsg.PageNo == 0) || (ControlMsg.AutoPageNo == 0))
  {
    sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[0] / 10));
    sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[1] / 10));
    sprintf(str5, "%1.1f", (float)((float)AutoStepDelay[2] / 10));
  }
  if((ControlMsg.PageNo == 1) || (ControlMsg.AutoPageNo == 1))
  {
    sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[3] / 10));
    sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[4] / 10));
    sprintf(str5, "%1.1f", (float)((float)AutoStepDelay[5] / 10));
  }
  if((ControlMsg.PageNo == 2) || (ControlMsg.AutoPageNo == 2))
  {
    sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[6] / 10));
    sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[7] / 10));
    sprintf(str5, "%1.1f", (float)((float)AutoStepDelay[8] / 10));
  }
  if((ControlMsg.PageNo == 3) || (ControlMsg.AutoPageNo == 3))
  {
    sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[9] / 10));
    sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[10] / 10));
    sprintf(str5, "%1.1f", (float)((float)AutoStepDelay[11] / 10));
  }
  if((ControlMsg.PageNo == 4) || (ControlMsg.AutoPageNo == 4))
  {
    sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[12] / 10));
    sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[13] / 10));
    sprintf(str5, "%1.1f", (float)((float)AutoStepDelay[14] / 10));
  }
  if((ControlMsg.PageNo == 5) || (ControlMsg.AutoPageNo == 5))
  {
    sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[15] / 10));
    sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[16] / 10));
    sprintf(str5, "%1.1f", (float)((float)AutoStepDelay[17] / 10));
  }
  if((ControlMsg.PageNo == 6) || (ControlMsg.AutoPageNo == 6))
  {
    sprintf(str3, "%1.1f", (float)((float)AutoStepDelay[18]/ 10));
    sprintf(str4, "%1.1f", (float)((float)AutoStepDelay[19] / 10));
  }
  
  //  printf("ControlMsg.TimerDelayTemp[CHUCK_ROTATION_RETURN_STEP] : %d\n ",ControlMsg.TimerDelayTemp[CHUCK_ROTATION_RETURN_STEP]);
}

void ModeSetting(void)
{
  for (uint8_t ModeDispSetting = 0 ; ModeDispSetting < 20 ; ModeDispSetting++)
  {
    if (ControlMsg.ModeDispTemp[ModeDispSetting] == ARMSET_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.MotionArm) + POINTER_MODE_MOTIONARM;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == CHUCK_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.MainChuck) + POINTER_MODE_USAGE;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == VACUUM_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.MainVaccum) + POINTER_MODE_USAGE;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == CHUCK_ROTATION_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.MainRotateChuck) + POINTER_MODE_USAGE;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == OUTSIDE_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.OutsideWait) + POINTER_MODE_USAGE;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == MAIN_TAKEOUT_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.MainArmType) + POINTER_MODE_MOTION;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == SUB_TAKEOUT_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.SubArmType) + POINTER_MODE_MOTION;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == MAIN_DOWN_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.MainArmDownPos) + POINTER_MODE_DOWN;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == SUB_DOWN_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.SubArmDownPos) + POINTER_MODE_DOWN;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == MAIN_CHUCK_RELEASE_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.ChuckOff) + POINTER_MODE_OPEN;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == VACUUM_RELEASE_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.VaccumOff) + POINTER_MODE_OPEN;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == SUB_CHUCK_RELEASE_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.SubArmOff) + POINTER_MODE_OPEN;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == MAIN_CHUCK_REJECT_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.ChuckRejectPos) + POINTER_MODE_REJECT;
    else if (ControlMsg.ModeDispTemp[ModeDispSetting] == MAIN_VAUUM_REJECT_MODE)
      ControlMsg.ModeSelect[ModeDispSetting] = (MotionModeTemp.VaccumRejectPos) + POINTER_MODE_REJECT;
  }
}

void LoopAppLCD(void)
{
  uint8_t TimerLineCheck = 1;
  uint8_t ModeLineCheck = 1;
  uint8_t StepLineCheck = 1;
  uint8_t MoldLineCheck = 1;
  
  uint8_t KickTemp = 0;
  static uint8_t SwingTemp = 0;
  uint16_t ErrPage = 0;
  
  //NewMoldPage
  static uint8_t NewMoldBlink = 0;
  static uint64_t NewMoldTick = 0;
  
  static char str[10];
  
  //ErrorListPage
  static char Error0[10];
  static char Error1[10];
  static char Error2[10];
  static char Error3[10];
  
  static char strMoldNoTemp[10];
  
  static uint8_t IO_Display[3];
  static enum
  {
    LCD_INIT = 0,
    
    CHECK_UPDATE,
    DRAW_BACK_GROUND,
    DRAW_ICON,
    DRAW_TXT,
    
    LCD_DISTINGUISH_CHANGE,
    LCD_DRAW,
    LCD_DISPLAY,
    
    LCD_OFF,
    LCD_ON,
    
    LCD_READ_STATUS,
    
    LCD_PAGE_WRITE,
    
  }LCDLoopState = LCD_INIT; 
  switch(LCDLoopState)
  {
  case LCD_INIT:
    ControlBacklight(LCD_BACKLIGHT_ON);
    HAL_GPIO_WritePin(LCD_RESET_GPIO_Port, LCD_RESET_Pin, GPIO_PIN_SET);		//NOW Disable
    //	  HAL_GPIO_WritePin(LCD_RESET_GPIO_Port, LCD_RESET_Pin, GPIO_PIN_RESET);		//NOW Disable
    
    Lcd_Initial();
    
    LCDLoopState = CHECK_UPDATE;
    break;	
    
  case CHECK_UPDATE:
    if(ControlMsg.IsChangedBL == NEED_CHANGE)
    {
      //		  if(ControlMsg.BackLight == ON) LCDLoopState = LCD_BL_ON;
      //		  else if(ControlMsg.BackLight == OFF) LCDLoopState = LCD_BL_OFF;
    }
    //	 printf("RobotCfg.Language : %d\n ",RobotCfg.Language);
    Language_Initial();
    
    if(ControlMsg.IsChangedLCD == NEED_CHANGE) LCDLoopState = DRAW_BACK_GROUND;
    
    break;
    
  case DRAW_BACK_GROUND:
    switch(ControlMsg.TpMode)
    {
    case INIT_READ_TYPE:
    case INIT_DISP_LOGO:
    case INIT_LOAD_SD:
    case INIT_SC:
      memcpy(Display_Scr, Background_Graphic + (BACKGROUND_LOGO * 1024), 1024);
      break;
      
      
    case AUTO_MESSAGE:
    case AUTO_MODE:	
    case MANUAL_MODE:
    case MANUAL_MOLD_MODE:
    case SETTING_MANUFACTURER:
    case MANUAL_VERSION:
      memcpy(Display_Scr, Background_Graphic + (BACKGROUND_OUTLINE * 1024), 1024);
      break;
      
    case OCCUR_ERROR:
    case MANUAL_ERROR_LIST:
    case AUTO_ERROR_LIST:	 
      memcpy(Display_Scr, Background_Graphic + (BACKGROUND_ERROR * 1024), 1024);
      break;
      
    case AUTO_OPERATING:
    case AUTO_COUNTER:
    case AUTO_TIMER:
    case MANUAL_COUNTER:
    case MANUAL_TIMER:
    case MANUAL_STEP_OPERATING:
    case MANUAL_CYCLE_OPERATING:
    case MANUAL_MOLD_MANAGE:
    case MANUAL_MOLD_NEW:
    case MANUAL_MOLD_DELETE:
      memcpy(Display_Scr, Background_Graphic + (BACKGROUND_TOP_HALF * 1024), 1024);
      break;
      
    case MANUAL_MOLD_SEARCH:
      memcpy(Display_Scr, Background_Graphic + (BACKGROUND_TOP_HALF * 1024), 1024);
      break;
      
    case MANUAL_OPERATING:
      memcpy(Display_Scr, Background_Graphic + (BACKGROUND_HALF * 1024), 1024);
      break;
      
    case AUTO_IO:
    case MANUAL_IO:
      memcpy(Display_Scr, Background_Graphic + (BACKGROUND_IO * 1024), 1024);
      break;
      
    case MANUAL_ERROR:
    case MANUAL_HOMING : 
    case MANUAL_MOLD_DELETE_ING:
      memcpy(Display_Scr, Background_Graphic + (BACKGROUND_OUTLINE * 1024), 1024);
      break;
      
    default:
      break;
    }
    LCDLoopState = DRAW_ICON;
    break;
    
  case DRAW_ICON://DRAW_ICON DRAW_ICON DRAW_ICON DRAW_ICON DRAW_ICON DRAW_ICON DRAW_ICON DRAW_ICON
    switch(ControlMsg.TpMode)
    {
    case MANUAL_OPERATING:
      if((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS))
      {
        if (IOMsg.Y20_Down == SIGNAL_OFF)
          Draw_Icon(MANUAL_PAGE_MSG2_OP0_LEFT, MANUAL_PAGE_MSG2_OP0_BOT, MANUAL_PAGE_MSG2_OP0_WIDTH, 
                    IOMsg.Y20_Down + GRP_ARROW_UP_Complete, REDRAW_ON);
        else if (IOMsg.Y20_Down == SIGNAL_ON)
          Draw_Icon(MANUAL_PAGE_MSG2_OP0_LEFT, MANUAL_PAGE_MSG2_OP0_BOT, MANUAL_PAGE_MSG2_OP0_WIDTH, 
                    IOMsg.Y20_Down + GRP_ARROW_UP, REDRAW_ON);
        
        if(RobotCfg.MotionMode.MainChuck == USE)
          Draw_Icon(MANUAL_PAGE_MSG2_OP4_LEFT, MANUAL_PAGE_MSG2_OP4_BOT, MANUAL_PAGE_MSG2_OP4_WIDTH,
                    IOMsg.Y22_Chuck + GRP_P_CHUCK_OFF, REDRAW_ON);
        if((RobotCfg.RobotType == ROBOT_TYPE_XC) || (RobotCfg.RobotType == ROBOT_TYPE_TWIN))
        {
          if(RobotCfg.MotionMode.MainVaccum == USE)
            Draw_Icon(MANUAL_PAGE_MSG2_OP5_LEFT, MANUAL_PAGE_MSG2_OP5_BOT, MANUAL_PAGE_MSG2_OP5_WIDTH,
                      IOMsg.Y25_Vaccum + GRP_P_SUCTION_OFF, REDRAW_ON);
        }
        if(RobotCfg.RobotType != ROBOT_TYPE_A)
        {
          if(RobotCfg.MotionMode.MainRotateChuck == USE)
            Draw_Icon(MANUAL_PAGE_MSG2_OP6_LEFT, MANUAL_PAGE_MSG2_OP6_BOT, MANUAL_PAGE_MSG2_OP6_WIDTH, 
                      IOMsg.Y24_ChuckRotation + GRP_CHUCK_ROTATE, REDRAW_ON);
        }
        //			 if(RobotCfg.MotionMode.MainNipper == USE)
        //				Draw_Icon(MANUAL_PAGE_MSG2_OP8_LEFT, MANUAL_PAGE_MSG2_OP8_BOT, MANUAL_PAGE_MSG2_OP8_WIDTH, 
        //							 IOMsg.Y26_Nipper + GRP_NIPPER_OFF, REDRAW_ON);
      }
      if((RobotCfg.RobotType == ROBOT_TYPE_TWIN) && 
         ((RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB) || (RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)))//When Use SUB Arm
      {
        if (IOMsg.Y2D_SubUp == SIGNAL_OFF)
          Draw_Icon(MANUAL_PAGE_MSG2_OP3_LEFT, MANUAL_PAGE_MSG2_OP3_BOT, MANUAL_PAGE_MSG2_OP3_WIDTH,
                    IOMsg.Y2D_SubUp + GRP_R_ARROW_UP_Complete, REDRAW_ON);
        else if (IOMsg.Y2D_SubUp == SIGNAL_ON)
          Draw_Icon(MANUAL_PAGE_MSG2_OP3_LEFT, MANUAL_PAGE_MSG2_OP3_BOT, MANUAL_PAGE_MSG2_OP3_WIDTH,
                    IOMsg.Y2D_SubUp + GRP_R_ARROW_UP, REDRAW_ON);
        Draw_Icon(MANUAL_PAGE_MSG2_OP7_LEFT, MANUAL_PAGE_MSG2_OP7_BOT, MANUAL_PAGE_MSG2_OP7_WIDTH,
                  IOMsg.Y27_SubGrip + GRP_R_Grip_OFF, REDRAW_ON);
      }
      if(((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_SUB) && (IOMsg.Y21_Kick == SIGNAL_ON)) 
         || ((RobotCfg.MotionMode.MotionArm != ROBOT_MAIN_MAIN) && (IOMsg.Y2E_SubKick == SIGNAL_ON))) KickTemp = 1;
      else if((IOMsg.Y21_Kick == SIGNAL_OFF) && (IOMsg.Y2E_SubKick == SIGNAL_OFF)) KickTemp = 0;
      
      //		if((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y21_Kick == SIGNAL_OFF))
      //		  if((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y21_Kick == SIGNAL_ON)) 
      if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MS)
      {
        if(((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y21_Kick == SIGNAL_OFF))
           || ((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y2E_SubKick == SIGNAL_ON)) 
             || ((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE) && (IOMsg.Y21_Kick == SIGNAL_ON))
               || ((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE) && (IOMsg.Y2E_SubKick == SIGNAL_OFF)))
          KickTemp = 0;
        else KickTemp = 1;
      }
      else if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_MAIN)
      {
        if(((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y21_Kick == SIGNAL_OFF)) 
           || ((RobotCfg.MotionMode.MainArmDownPos == ARM_DOWNPOS_NOZZLE) && (IOMsg.Y21_Kick == SIGNAL_ON)))
          KickTemp = 0;
        else KickTemp = 1;
      }
      else if(RobotCfg.MotionMode.MotionArm == ROBOT_MAIN_SUB)
      {
        if(((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_CLAMP) && (IOMsg.Y2E_SubKick == SIGNAL_ON)) ||
           ((RobotCfg.MotionMode.SubArmDownPos == ARM_DOWNPOS_NOZZLE) && (IOMsg.Y2E_SubKick == SIGNAL_OFF)))
          KickTemp = 0;
        else KickTemp = 1;
      }
      Draw_Icon(MANUAL_PAGE_MSG2_OP1_LEFT, MANUAL_PAGE_MSG2_OP1_BOT, MANUAL_PAGE_MSG2_OP1_WIDTH,
                KickTemp + GRP_ARROW_BACKWARD, REDRAW_ON);
      
      if((IOMsg.X14_SwingOk == SIGNAL_ON) || (IOMsg.Y23_Swing == SIGNAL_ON)) SwingTemp = 1;
      else if((IOMsg.X15_SwingReturnOk == SIGNAL_ON) || (IOMsg.Y2F_SwingReturn == SIGNAL_ON)) SwingTemp = 0;
      
      //		if ((IOMsg.X14_SwingOk == SIGNAL_ON) || (IOMsg.X15_SwingReturnOk == SIGNAL_ON))
      Draw_Icon(MANUAL_PAGE_MSG2_OP2_LEFT, MANUAL_PAGE_MSG2_OP2_BOT, MANUAL_PAGE_MSG2_OP2_WIDTH, 
                SwingTemp + GRP_ARROW_ROTATE_RETURN_Complete, REDRAW_ON);		
      //		else if ((IOMsg.X14_SwingOk == SIGNAL_OFF) || (IOMsg.X15_SwingReturnOk == SIGNAL_OFF))
      //		  Draw_Icon(MANUAL_PAGE_MSG2_OP2_LEFT, MANUAL_PAGE_MSG2_OP2_BOT, MANUAL_PAGE_MSG2_OP2_WIDTH, 
      //						SwingTemp + GRP_ARROW_ROTATE_RETURN, REDRAW_ON);	
      
      //Interlock
      if(RobotCfg.Setting.ItlFullAuto == USE && IOMsg.X1H_FullAuto == SIGNAL_ON)
        Draw_Icon(MANUAL_PAGE_MSG3_OP0_LEFT, MANUAL_PAGE_MSG3_OP0_BOT, MANUAL_PAGE_MSG3_OP0_WIDTH, GRP_FULLAUTO, REDRAW_ON);
      if(RobotCfg.Setting.ItlAutoInjection  == USE && IOMsg.X19_AutoInjection == SIGNAL_ON)
        Draw_Icon(MANUAL_PAGE_MSG3_OP1_LEFT, MANUAL_PAGE_MSG3_OP1_BOT, MANUAL_PAGE_MSG3_OP1_WIDTH, GRP_AUTO_INJECTION, REDRAW_ON);
      if(IOMsg.X18_MoldOpenComplete == SIGNAL_ON)
        Draw_Icon(MANUAL_PAGE_MSG3_OP2_LEFT, MANUAL_PAGE_MSG3_OP2_BOT, MANUAL_PAGE_MSG3_OP2_WIDTH, GRP_MOLD_OPEN_Complete, REDRAW_ON);
      if(RobotCfg.Setting.ItlSaftyDoor  == USE && IOMsg.X1A_SaftyDoor == SIGNAL_ON)
        Draw_Icon(MANUAL_PAGE_MSG3_OP3_LEFT, MANUAL_PAGE_MSG3_OP3_BOT, MANUAL_PAGE_MSG3_OP3_WIDTH, GRP_SAFETY_DOOR, REDRAW_ON);
      if(IOMsg.Y2A_MoldOpenClose == SIGNAL_ON)
        Draw_Icon(MANUAL_PAGE_MSG3_OP4_LEFT, MANUAL_PAGE_MSG3_OP4_BOT, MANUAL_PAGE_MSG3_OP4_WIDTH, GRP_MOLD_OPEN_N_CLOSE, REDRAW_ON);
      if(IOMsg.Y2B_Ejector == SIGNAL_ON)
        Draw_Icon(MANUAL_PAGE_MSG3_OP5_LEFT, MANUAL_PAGE_MSG3_OP5_BOT, MANUAL_PAGE_MSG3_OP5_WIDTH, GRP_EJECTOR, REDRAW_ON);
      
      //Function
      Draw_Icon(ICON_LEFT, ICON_1_BOT, ICON_WIDTH, RobotCfg.Function.Buzzer + buzzer_ER, REDRAW_OFF);
      Draw_Icon(ICON_LEFT, ICON_2_BOT, ICON_WIDTH, RobotCfg.Function.Detection + detect_switch_ER, REDRAW_OFF);
      Draw_Icon(ICON_LEFT, ICON_3_BOT, ICON_WIDTH, RobotCfg.Function.Ejector + ejector_ER, REDRAW_OFF);
      Draw_Icon(ICON_LEFT, ICON_4_BOT, ICON_WIDTH, RobotCfg.Function.Reject + reject_ER, REDRAW_OFF);
      break;
      
    case MANUAL_CYCLE_OPERATING:
      //ICON ">"
      Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.AutoLineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
                 Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
      break;
      
    case AUTO_OPERATING:
      Draw_Icon(ICON_LEFT, ICON_1_BOT, ICON_WIDTH, RobotCfg.Function.Buzzer + buzzer_ER, REDRAW_OFF);
      Draw_Icon(ICON_LEFT, ICON_2_BOT, ICON_WIDTH, RobotCfg.Function.Detection + detect_switch_ER, REDRAW_OFF);
      Draw_Icon(ICON_LEFT, ICON_3_BOT, ICON_WIDTH, RobotCfg.Function.Ejector + ejector_ER, REDRAW_OFF);
      Draw_Icon(ICON_LEFT, ICON_4_BOT, ICON_WIDTH, RobotCfg.Function.Reject + reject_ER, REDRAW_OFF);
      
      Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.AutoLineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
                 Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
      break;
      
    case MANUAL_MODE:
      Draw_Icon(MODE_CURSOR_LEFT, OUTLINE_LO_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
                GRP_ARROW_RIGHT, REDRAW_OFF);
      break;
      
    case SETTING_MANUFACTURER:
      Draw_Icon(SETTING_CURSOR_LEFT, OUTLINE_LO_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), SETTING_CURSOR_WIDTH,
                GRP_ARROW_RIGHT, REDRAW_OFF);
      break;
      
    case MANUAL_COUNTER:
      //ICON ">"
      Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
                 Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
      break;
      
    case MANUAL_IO:
      if(ControlMsg.LineNo == IO_INPUT)
      {
        if(ControlMsg.PageNo == 0)
        {
          IO_Display[0] = IOMsg.X11_MainArmUpComplete;
          IO_Display[1] = IO_NOT_DISPLAY;
          IO_Display[2] = IOMsg.X16_MainChuckOk;
        }
        else if(ControlMsg.PageNo == 1)
        {
          IO_Display[0] = IOMsg.X14_SwingOk;
          IO_Display[1] = IOMsg.X15_SwingReturnOk;
          IO_Display[2] = IO_NOT_DISPLAY;
        }
        else if(ControlMsg.PageNo == 2)
        {
          IO_Display[0] = IOMsg.X17_MainVaccumOk;
          IO_Display[1] = IO_NOT_DISPLAY;
          IO_Display[2] = IOMsg.X1G_SubUpComplete;
        }
        else if(ControlMsg.PageNo == 3)
        {
          IO_Display[0] = IO_NOT_DISPLAY;
          IO_Display[1] = IOMsg.X1F_SubGripComplete;
          IO_Display[2] = IO_NOT_DISPLAY;
        }
        else if(ControlMsg.PageNo == 4)
        {
          IO_Display[0] = IOMsg.X1H_FullAuto;
          IO_Display[1] = IOMsg.X19_AutoInjection;
          IO_Display[2] = IOMsg.X18_MoldOpenComplete;
        }
        else if(ControlMsg.PageNo == 5)
        {
          IO_Display[0] = IOMsg.X1A_SaftyDoor;
          IO_Display[1] = IOMsg.X1B_Reject;
          IO_Display[2] = IOMsg.X1I_EMOFromIMM;
        }
      }
      else if(ControlMsg.LineNo == IO_OUTPUT)
      {
        if(ControlMsg.PageNo == 0)
        {
          IO_Display[0] = IOMsg.Y20_Down;
          IO_Display[1] = IOMsg.Y21_Kick;
          IO_Display[2] = IOMsg.Y22_Chuck;
        }
        else if(ControlMsg.PageNo == 1)
        {
          IO_Display[0] = IOMsg.Y23_Swing;
          IO_Display[1] = IOMsg.Y2F_SwingReturn;
          IO_Display[2] = IOMsg.Y24_ChuckRotation;
        }
        else if(ControlMsg.PageNo == 2)
        {
          IO_Display[0] = IOMsg.Y25_Vaccum;
          IO_Display[1] = IOMsg.Y26_Nipper;
          IO_Display[2] = IOMsg.Y2D_SubUp;
        }
        else if(ControlMsg.PageNo == 3)
        {
          IO_Display[0] = IOMsg.Y2E_SubKick;
          IO_Display[1] = IOMsg.Y27_SubGrip;
          IO_Display[2] = IOMsg.Y28_Alarm;
        }
        else if(ControlMsg.PageNo == 4)
        {
          IO_Display[0] = IOMsg.Y29_CycleStart;
          IO_Display[1] = IOMsg.Y2A_MoldOpenClose;
          IO_Display[2] = IOMsg.Y2B_Ejector;
        }
        else if(ControlMsg.PageNo == 5)
        {
          IO_Display[0] = IOMsg.Y2C_Conveyor;
          IO_Display[1] = IOMsg.Y28_Alarm;
          IO_Display[2] = IO_NOT_DISPLAY;
        }
      }
      
      if(IO_Display[0] != IO_NOT_DISPLAY) Draw_Icon(IO_ICON_LEFT, TITLE_L0_BOT, IO_ICON_WIDTH, GRP_RAMP_OFF + IO_Display[0], REDRAW_ON);
      if(IO_Display[1] != IO_NOT_DISPLAY) Draw_Icon(IO_ICON_LEFT, TITLE_L1_BOT, IO_ICON_WIDTH, GRP_RAMP_OFF + IO_Display[1], REDRAW_ON);
      if(IO_Display[2] != IO_NOT_DISPLAY) Draw_Icon(IO_ICON_LEFT, TITLE_L2_BOT, IO_ICON_WIDTH, GRP_UNDER_RAMP_OFF + IO_Display[2], REDRAW_ON);
      
      break;
      
      
      
      
      break;
      
    case MANUAL_TIMER:
      //ICON "<"
      Lcd_Printf(TIMER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
                 Normal_Option[0], CENTER_ALIGN, REDRAW_OFF);
      break;
      
    case MANUAL_STEP_OPERATING:
      //ICON ">"
      if (((ControlMsg.KeyValue == KEY_ARROW_DOWM) || (StepCursor != DISPLAY_OFF)) && (IOMsg.X18_MoldOpenComplete == SIGNAL_ON))
      {
        Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
                   Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
        StepCursor = DISPLAY_ON;
      }
      break;
      
    case MANUAL_MOLD_SEARCH:
      break;
      
    case MANUAL_MOLD_MANAGE:
      Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
                 Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
      break;
      
    case MANUAL_MOLD_MODE:
      //ICON ">"
      //      Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
      //                 Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
      Draw_Icon(MODE_CURSOR_LEFT, OUTLINE_LO_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
                GRP_ARROW_LEFT, REDRAW_OFF);
      break;
      
    case MANUAL_MOLD_NEW:
      break;
      
    case MANUAL_ERROR_LIST:
      break;
      
    case MANUAL_VERSION:
      break;
      
    case AUTO_MODE:
      Draw_Icon(MODE_CURSOR_LEFT, OUTLINE_LO_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
                GRP_ARROW_RIGHT, REDRAW_OFF);
      break;
      
    case AUTO_COUNTER:
      Lcd_Printf(COUNTER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
                 Normal_Option[1], CENTER_ALIGN, REDRAW_OFF);
      break;
      
    case AUTO_TIMER:
      //ICON "<"
      Lcd_Printf(TIMER_CURSOR_LEFT, TITLE_L0_BOT + (ControlMsg.LineNo*TITLE_LINE_HEIGHT), CURSOR_WIDTH,
                 Normal_Option[0], CENTER_ALIGN, REDRAW_OFF);
      break;
      
    case AUTO_IO:
      if(ControlMsg.LineNo == IO_INPUT)
      {
        if(ControlMsg.PageNo == 0)
        {
          IO_Display[0] = IOMsg.X11_MainArmUpComplete;
          IO_Display[1] = IO_NOT_DISPLAY;
          IO_Display[2] = IOMsg.X16_MainChuckOk;
        }
        else if(ControlMsg.PageNo == 1)
        {
          IO_Display[0] = IOMsg.X14_SwingOk;
          IO_Display[1] = IOMsg.X15_SwingReturnOk;
          IO_Display[2] = IO_NOT_DISPLAY;
        }
        else if(ControlMsg.PageNo == 2)
        {
          IO_Display[0] = IOMsg.X17_MainVaccumOk;
          IO_Display[1] = IO_NOT_DISPLAY;
          IO_Display[2] = IOMsg.X1G_SubUpComplete;
        }
        else if(ControlMsg.PageNo == 3)
        {
          IO_Display[0] = IO_NOT_DISPLAY;
          IO_Display[1] = IOMsg.X1F_SubGripComplete;
          IO_Display[2] = IO_NOT_DISPLAY;
        }
        else if(ControlMsg.PageNo == 4)
        {
          IO_Display[0] = IOMsg.X1H_FullAuto;
          IO_Display[1] = IOMsg.X19_AutoInjection;
          IO_Display[2] = IOMsg.X18_MoldOpenComplete;
        }
        else if(ControlMsg.PageNo == 5)
        {
          IO_Display[0] = IOMsg.X1A_SaftyDoor;
          IO_Display[1] = IOMsg.X1B_Reject;
          IO_Display[2] = IOMsg.X1I_EMOFromIMM;
        }
      }
      else if(ControlMsg.LineNo == IO_OUTPUT)
      {
        if(ControlMsg.PageNo == 0)
        {
          IO_Display[0] = IOMsg.Y20_Down;
          IO_Display[1] = IOMsg.Y21_Kick;
          IO_Display[2] = IOMsg.Y22_Chuck;
        }
        else if(ControlMsg.PageNo == 1)
        {
          IO_Display[0] = IOMsg.Y23_Swing;
          IO_Display[1] = IOMsg.Y2F_SwingReturn;
          IO_Display[2] = IOMsg.Y24_ChuckRotation;
        }
        else if(ControlMsg.PageNo == 2)
        {
          IO_Display[0] = IOMsg.Y25_Vaccum;
          IO_Display[1] = IOMsg.Y26_Nipper;
          IO_Display[2] = IOMsg.Y2D_SubUp;
        }
        else if(ControlMsg.PageNo == 3)
        {
          IO_Display[0] = IOMsg.Y2E_SubKick;
          IO_Display[1] = IOMsg.Y27_SubGrip;
          IO_Display[2] = IOMsg.Y28_Alarm;
        }
        else if(ControlMsg.PageNo == 4)
        {
          IO_Display[0] = IOMsg.Y29_CycleStart;
          IO_Display[1] = IOMsg.Y2A_MoldOpenClose;
          IO_Display[2] = IOMsg.Y2B_Ejector;
        }
        else if(ControlMsg.PageNo == 5)
        {
          IO_Display[0] = IOMsg.Y2C_Conveyor;
          IO_Display[1] = IOMsg.Y28_Alarm;
          IO_Display[2] = IO_NOT_DISPLAY;
        }
      }
      
      if(IO_Display[0] != IO_NOT_DISPLAY) Draw_Icon(IO_ICON_LEFT, TITLE_L0_BOT, IO_ICON_WIDTH, GRP_RAMP_OFF + IO_Display[0], REDRAW_ON);
      if(IO_Display[1] != IO_NOT_DISPLAY) Draw_Icon(IO_ICON_LEFT, TITLE_L1_BOT, IO_ICON_WIDTH, GRP_RAMP_OFF + IO_Display[1], REDRAW_ON);
      if(IO_Display[2] != IO_NOT_DISPLAY) Draw_Icon(IO_ICON_LEFT, TITLE_L2_BOT, IO_ICON_WIDTH, GRP_UNDER_RAMP_OFF + IO_Display[2], REDRAW_ON);
      
      break;
      
    case OCCUR_ERROR:
      break;
      
    default:
      break;
    }
    LCDLoopState = DRAW_TXT;
    break;
    
  case DRAW_TXT://DRAW_TXT DRAW_TXT DRAW_TXT DRAW_TXT DRAW_TXT DRAW_TXT DRAW_TXT DRAW_TXT DRAW_TXT 
    switch(ControlMsg.TpMode)
    {
    case INIT_READ_TYPE:
    case INIT_DISP_LOGO:
    case INIT_LOAD_SD:
    case INIT_SC:
      Lcd_Printf(INIT_PAGE_MSG0_LEFT, INIT_PAGE_MSG0_BOT, INIT_PAGE_MSG0_WIDTH,
                 Page_Name_List[INIT_PAGE_TEXT], CENTER_ALIGN, REDRAW_ON);
      break;
      
    case MANUAL_OPERATING:
      Lcd_Printf(MANUAL_PAGE_MSG0_LEFT, MANUAL_PAGE_MSG0_BOT, MANUAL_PAGE_MSG0_WIDTH,
                 Page_Name_List[MANUAL_PAGE], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(MANUAL_PAGE_MSG1_LEFT, MANUAL_PAGE_MSG1_BOT, MANUAL_PAGE_MSG1_WIDTH, 
                 str, CENTER_ALIGN, REDRAW_OFF);
      break;
      
    case MANUAL_HOMING : 
      Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, Massage_Pop_Msg[0], LEFT_ALIGN, REDRAW_OFF);
      break;
      
    case AUTO_MESSAGE:
      Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, Auto_Run_Popup_Msg[0], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, Auto_Run_Popup_Msg[1], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, Auto_Run_Popup_Msg[2], LEFT_ALIGN, REDRAW_OFF);
      break;
      
    case AUTO_OPERATING:
      StepTimerSetting();
      
      Lcd_Printf(AUTO_PAGE_MSG0_LEFT, AUTO_PAGE_MSG0_BOT, AUTO_PAGE_MSG0_WIDTH,
                 Page_Name_List[FILE_AUTO_PAGE], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(AUTO_PAGE_MSG1_LEFT, AUTO_PAGE_MSG1_BOT, AUTO_PAGE_MSG1_WIDTH, 
                 str, CENTER_ALIGN, REDRAW_OFF);
      
      if (StepLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.AutoPageNo*TITLE_LINE_MAX) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(AUTO_PAGE_MSG2_LEFT, AUTO_PAGE_MSG2_BOT, AUTO_PAGE_MSG2_WIDTH, 
                     Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.AutoPageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(AUTO_PAGE_MSG2_OP0_LEFT, AUTO_PAGE_MSG2_OP0_BOT, AUTO_PAGE_MSG2_OP0_WIDTH, 
                     str0, RIGHT_ALIGN, REDRAW_OFF);
          Lcd_Printf(AUTO_PAGE_MSG2_OP1_LEFT, AUTO_PAGE_MSG2_OP1_BOT, AUTO_PAGE_MSG2_OP1_WIDTH, 
                     str3, RIGHT_ALIGN, REDRAW_OFF);
        }
        else StepLineCheck = DISPLAY_OFF;
      }
      if (StepLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.AutoPageNo*TITLE_LINE_MAX+1) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(AUTO_PAGE_MSG3_LEFT, AUTO_PAGE_MSG3_BOT, AUTO_PAGE_MSG3_WIDTH,
                     Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.AutoPageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(AUTO_PAGE_MSG3_OP0_LEFT, AUTO_PAGE_MSG3_OP0_BOT, AUTO_PAGE_MSG3_OP0_WIDTH, 
                     str1, RIGHT_ALIGN, REDRAW_OFF);
          Lcd_Printf(AUTO_PAGE_MSG3_OP1_LEFT, AUTO_PAGE_MSG3_OP1_BOT, AUTO_PAGE_MSG3_OP1_WIDTH, 
                     str4, RIGHT_ALIGN, REDRAW_OFF);
        }
        else StepLineCheck = DISPLAY_OFF;
      }
      if (StepLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.AutoPageNo*TITLE_LINE_MAX+2) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(AUTO_PAGE_MSG4_LEFT, AUTO_PAGE_MSG4_BOT, AUTO_PAGE_MSG4_WIDTH,
                     Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.AutoPageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(AUTO_PAGE_MSG4_OP0_LEFT, AUTO_PAGE_MSG4_OP0_BOT, AUTO_PAGE_MSG4_OP0_WIDTH, 
                     str2, RIGHT_ALIGN, REDRAW_OFF);
          Lcd_Printf(AUTO_PAGE_MSG4_OP1_LEFT, AUTO_PAGE_MSG4_OP1_BOT, AUTO_PAGE_MSG4_OP1_WIDTH, 
                     str5, RIGHT_ALIGN, REDRAW_OFF);
        }
        else StepLineCheck = DISPLAY_OFF;
      }
      break;
      
    case AUTO_COUNTER:  
      Lcd_Printf(TITLE_MSG_LEFT, TITLE_MSG_BOT, TITLE_MSG_WIDTH,
                 Page_Name_List[COUNT_SETUP_PAGE], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(TITLE_MOLD_LEFT, TITLE_MOLD_BOT, TITLE_MOLD_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);
      //Msg
      Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L0_BOT, COUNTER_MSG_WIDTH, 
                 Count_Setup_Page_Msg[0], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L1_BOT, COUNTER_MSG_WIDTH,
                 Count_Setup_Page_Msg[1], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L2_BOT, COUNTER_MSG_WIDTH, 
                 Count_Setup_Page_Msg[2], LEFT_ALIGN, REDRAW_OFF);
      
      //Number
      sprintf(str, "%5d", RobotCfg.Count.TotalCnt);
      Lcd_Printf(COUNTER_VALUE_LEFT, TITLE_L0_BOT, COUNTER_VALUE_WIDTH,
                 str, RIGHT_ALIGN, REDRAW_OFF);
      
      sprintf(str, "%4d", RobotCfg.Count.RejectCnt);
      Lcd_Printf(COUNTER_VALUE_LEFT, TITLE_L1_BOT, COUNTER_VALUE_WIDTH,
                 str, RIGHT_ALIGN, REDRAW_OFF);
      
      sprintf(str, "%4d", RobotCfg.Count.DetectFail);
      Lcd_Printf(COUNTER_VALUE_LEFT, TITLE_L2_BOT, COUNTER_VALUE_WIDTH,
                 str, RIGHT_ALIGN, REDRAW_OFF);
      
      break;
      
    case AUTO_TIMER:
      TimerSetting();
      //Title
      Lcd_Printf(TITLE_MSG_LEFT, TITLE_MSG_BOT, TITLE_MSG_WIDTH,
                 Page_Name_List[TIMER_EDIT_PAGE], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(TITLE_MOLD_LEFT, TITLE_MOLD_BOT, TITLE_MOLD_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);     
      //T00,T01....
      if (TimerLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*TITLE_LINE_MAX) != ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(TIMER_MSG_LEFT, TITLE_L0_BOT, TIMER_MSG_WIDTH, 
                     Timer_Page_Num_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_MSG2_LEFT, TITLE_L0_BOT, TIMER_MSG2_WIDTH, 
                     Timer_Page_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L0_BOT, TIMER_VALUE_WIDTH, str0, RIGHT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L0_BOT, TIMER_TEMP_WIDTH, str3, RIGHT_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L0_BOT, TIMER_TEMP_WIDTH, str3, RIGHT_ALIGN, REDRAW_OFF);
        }
        else TimerLineCheck = DISPLAY_OFF;
      }
      if (TimerLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*TITLE_LINE_MAX+1) != ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(TIMER_MSG_LEFT, TITLE_L1_BOT, TIMER_MSG_WIDTH,
                     Timer_Page_Num_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_MSG2_LEFT, TITLE_L1_BOT, TIMER_MSG2_WIDTH,
                     Timer_Page_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L1_BOT, TIMER_VALUE_WIDTH, str1, RIGHT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L1_BOT, TIMER_TEMP_WIDTH, str4, RIGHT_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L1_BOT, TIMER_TEMP_WIDTH, str4, RIGHT_ALIGN, REDRAW_OFF);
        }
        else TimerLineCheck = DISPLAY_OFF;
      }
      if (TimerLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*TITLE_LINE_MAX+2) != ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(TIMER_MSG_LEFT, TITLE_L2_BOT, TIMER_MSG_WIDTH,
                     Timer_Page_Num_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_MSG2_LEFT, TITLE_L2_BOT, TIMER_MSG2_WIDTH,
                     Timer_Page_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L2_BOT, TIMER_VALUE_WIDTH, str2, RIGHT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 2))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L2_BOT, TIMER_TEMP_WIDTH, str5, RIGHT_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L2_BOT, TIMER_TEMP_WIDTH, str5, RIGHT_ALIGN, REDRAW_OFF);
        }
        else TimerLineCheck = DISPLAY_OFF;
      }
      break;
      
    case AUTO_IO:     
      Lcd_Printf(TITLE_MSG_LEFT, IO_TITLE_BOT, IO_TITLE_WIDTH,
                 Page_Name_List[IO_PAGE - ControlMsg.LineNo], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(IO_TITLE_MOLD_LEFT, IO_TITLE_BOT, IO_TITLE_MOLD_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);
      
      //IO Signal : X11, X14, Y13.....
      Lcd_Printf(IO_SIGNAL_LEFT, TITLE_L0_BOT, IO_SIGNAL_WIDTH,
                 IO_Page_Num_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo)], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(IO_SIGNAL_LEFT, TITLE_L1_BOT, IO_SIGNAL_WIDTH,
                 IO_Page_Num_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 1], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(IO_SIGNAL_LEFT, TITLE_L2_BOT, IO_SIGNAL_WIDTH,
                 IO_Page_Num_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 2], LEFT_ALIGN, REDRAW_OFF);
      
      //IO Name(MSG)
      Lcd_Printf(IO_MSG_LEFT, TITLE_L0_BOT, IO_MSG_WIDTH,
                 IO_Page_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo)], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(IO_MSG_LEFT, TITLE_L1_BOT, IO_MSG_WIDTH,
                 IO_Page_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 1], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(IO_MSG_LEFT, TITLE_L2_BOT, IO_MSG_WIDTH,
                 IO_Page_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 2], LEFT_ALIGN, REDRAW_OFF);
      break;
      
    case AUTO_MODE:
      ModeSetting();
      
      if (ModeLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*OUTLINE_LINE_MAX) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(MODE_NUM_LEFT, OUTLINE_LO_BOT, MODE_NUM_WIDTH, 
                     Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_MSG_LEFT, OUTLINE_LO_BOT, MODE_MSG_WIDTH, 
                     Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_LO_BOT, MODE_OPTION_WIDTH, 
                     Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX]], CENTER_ALIGN, REDRAW_OFF);
        }
        else ModeLineCheck = DISPLAY_OFF;
      }
      if (ModeLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+1) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L1_BOT, MODE_NUM_WIDTH,
                     Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L1_BOT, MODE_MSG_WIDTH,
                     Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L1_BOT, MODE_OPTION_WIDTH, 
                     Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], CENTER_ALIGN, REDRAW_OFF);
        }
        else ModeLineCheck = DISPLAY_OFF;
      }
      if (ModeLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+2) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L2_BOT, MODE_NUM_WIDTH,
                     Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L2_BOT, MODE_MSG_WIDTH,
                     Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L2_BOT, MODE_OPTION_WIDTH, 
                     Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], CENTER_ALIGN, REDRAW_OFF);
        }
        else ModeLineCheck = DISPLAY_OFF;
      }
      if (ModeLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+3) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L3_BOT, MODE_NUM_WIDTH,
                     Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L3_BOT, MODE_MSG_WIDTH,
                     Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L3_BOT, MODE_OPTION_WIDTH, 
                     Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], CENTER_ALIGN, REDRAW_OFF);
        }
        else ModeLineCheck = DISPLAY_OFF;
      }
      break;
      
    case MANUAL_MODE:
      ModeSetting();
      
      if (ModeLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*OUTLINE_LINE_MAX) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(MODE_NUM_LEFT, OUTLINE_LO_BOT, MODE_NUM_WIDTH, 
                     Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_MSG_LEFT, OUTLINE_LO_BOT, MODE_MSG_WIDTH, 
                     Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_LO_BOT, MODE_OPTION_WIDTH, 
                         Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX]], CENTER_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_LO_BOT, MODE_OPTION_WIDTH, 
                          Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX]], CENTER_ALIGN, REDRAW_OFF);
        }
        else ModeLineCheck = DISPLAY_OFF;
      }
      if (ModeLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+1) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L1_BOT, MODE_NUM_WIDTH,
                     Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L1_BOT, MODE_MSG_WIDTH,
                     Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L1_BOT, MODE_OPTION_WIDTH, 
                         Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], CENTER_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L1_BOT, MODE_OPTION_WIDTH, 
                          Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], CENTER_ALIGN, REDRAW_OFF);
        }
        else ModeLineCheck = DISPLAY_OFF;
      }
      
      
      if (ModeLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+2) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L2_BOT, MODE_NUM_WIDTH,
                     Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L2_BOT, MODE_MSG_WIDTH,
                     Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 2))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L2_BOT, MODE_OPTION_WIDTH, 
                         Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], CENTER_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L2_BOT, MODE_OPTION_WIDTH, 
                          Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], CENTER_ALIGN, REDRAW_OFF);
        }
        else ModeLineCheck = DISPLAY_OFF;
      }
      if (ModeLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+3) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L3_BOT, MODE_NUM_WIDTH,
                     Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L3_BOT, MODE_MSG_WIDTH,
                     Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], LEFT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 3))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L3_BOT, MODE_OPTION_WIDTH, 
                         Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], CENTER_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L3_BOT, MODE_OPTION_WIDTH, 
                          Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], CENTER_ALIGN, REDRAW_OFF);
        }
        else ModeLineCheck = DISPLAY_OFF;
      }
      break;
      
    case SETTING_MANUFACTURER:
      //MSG
//      Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_WIDTH, 
//                 Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX)], LEFT_ALIGN, REDRAW_OFF);
//      Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_WIDTH,
//                 Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 1], LEFT_ALIGN, REDRAW_OFF);
//      Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_WIDTH,
//                 Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 2], LEFT_ALIGN, REDRAW_OFF);
//      		if(ControlMsg.PageNo < SETTING_MAX_PAGE)		//IMMType
//      Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_WIDTH,
//                 Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 3], LEFT_ALIGN, REDRAW_OFF);
      //Option
      if(ControlMsg.PageNo == 0)
      {
        Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_WIDTH, 
                   Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX)], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_WIDTH,
                   Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 1], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_WIDTH,
                   Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 2], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_WIDTH,
                   Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 3], LEFT_ALIGN, REDRAW_OFF);
        sprintf(str, "%.1d", SettingTemp.ErrorTime);
        if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
        {
          if (NewMoldBlink == BLINK_ON)
          {
            Lcd_Printf(SETTING_SEC_LEFT, OUTLINE_LO_BOT, SETTING_SEC_WIDTH,
                       str, CENTER_ALIGN, REDRAW_OFF);
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);
              NewMoldBlink = BLINK_OFF;
            } 
          }
          else if(NewMoldBlink == BLINK_OFF)
          {
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);	 
              NewMoldBlink = BLINK_ON;
            }
          }
        }
        else Lcd_Printf(SETTING_SEC_LEFT, OUTLINE_LO_BOT, SETTING_SEC_WIDTH,
                        str, CENTER_ALIGN, REDRAW_OFF);
        Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH - 8, 
                   Factory_Setup_Option[POS_FAC_SEC], RIGHT_ALIGN, REDRAW_OFF);
        if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
        {
          if (NewMoldBlink == BLINK_ON)
          {
            Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_OPT_WIDTH, 
                       Factory_Setup_Option[(SettingTemp.ItlFullAuto) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);
              NewMoldBlink = BLINK_OFF;
            } 
          }
          else if(NewMoldBlink == BLINK_OFF)
          {
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);	 
              NewMoldBlink = BLINK_ON;
            }
          }
        }
        else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_OPT_WIDTH, 
                        Factory_Setup_Option[(SettingTemp.ItlFullAuto) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
        if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 2))
        {
          if (NewMoldBlink == BLINK_ON)
          {
            Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_OPT_WIDTH, 
                       Factory_Setup_Option[(SettingTemp.ItlSaftyDoor) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);
              NewMoldBlink = BLINK_OFF;
            } 
          }
          else if(NewMoldBlink == BLINK_OFF)
          {
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);	 
              NewMoldBlink = BLINK_ON;
            }
          }
        }
        else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_OPT_WIDTH, 
                        Factory_Setup_Option[(SettingTemp.ItlSaftyDoor) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
        if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 3))
        {
          if (NewMoldBlink == BLINK_ON)
          {
            Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_OPT_WIDTH, 
                       Factory_Setup_Option[(SettingTemp.ItlAutoInjection) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);
              NewMoldBlink = BLINK_OFF;
            } 
          }
          else if(NewMoldBlink == BLINK_OFF)
          {
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);	 
              NewMoldBlink = BLINK_ON;
            }
          }
        }
        else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_OPT_WIDTH, 
                        Factory_Setup_Option[(SettingTemp.ItlAutoInjection) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
      }
      else if(ControlMsg.PageNo == 1)
      {
        Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_WIDTH, 
                   Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX)], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_WIDTH,
                   Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 1], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_WIDTH,
                   Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 2], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_WIDTH,
                   Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 3], LEFT_ALIGN, REDRAW_OFF);
        
        if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
        {
          if (NewMoldBlink == BLINK_ON)
          {
            Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH, 
                       Factory_Setup_Option[(SettingTemp.ItlReject) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);
              NewMoldBlink = BLINK_OFF;
            } 
          }
          else if(NewMoldBlink == BLINK_OFF)
          {
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);	 
              NewMoldBlink = BLINK_ON;
            }
          }
        }
        else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH, 
                        Factory_Setup_Option[(SettingTemp.ItlReject) + POS_FAC_NORUN], CENTER_ALIGN, REDRAW_OFF);
        sprintf(str, "%.2d", SettingTemp.ProcessTime);
        if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
        {
          if (NewMoldBlink == BLINK_ON)
          {
            Lcd_Printf(SETTING_SEC_LEFT, OUTLINE_L1_BOT, SETTING_SEC_WIDTH,
                       str, CENTER_ALIGN, REDRAW_OFF);
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);
              NewMoldBlink = BLINK_OFF;
            } 
          }
          else if(NewMoldBlink == BLINK_OFF)
          {
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);	 
              NewMoldBlink = BLINK_ON;
            }
          }
        }
        else Lcd_Printf(SETTING_SEC_LEFT, OUTLINE_L1_BOT, SETTING_SEC_WIDTH,
                        str, CENTER_ALIGN, REDRAW_OFF);
        
        Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_OPT_WIDTH - 8, 
                   Factory_Setup_Option[POS_FAC_SEC], RIGHT_ALIGN, REDRAW_OFF);
        //DATE
        sprintf(str, "%.2d/%.2d/%.2d", SettingTemp.Time.Date.Year, SettingTemp.Time.Date.Month, SettingTemp.Time.Date.Date);
        Lcd_Printf(FACTORY_SETTING_TIME_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_TIME_WIDTH,
                   str, CENTER_ALIGN, REDRAW_OFF);
        
        sprintf(str, "__");
        if(UnderbarPos == YEAR_POS)
          Lcd_Printf(SETTING_UNDERBAR0_LEFT, OUTLINE_L2_BOT, SETTING_UNDERBAR_WIDTH,
                     str, LEFT_ALIGN, REDRAW_OFF);
        if(UnderbarPos == MONTH_POS)
          Lcd_Printf(SETTING_UNDERBAR1_LEFT, OUTLINE_L2_BOT, SETTING_UNDERBAR_WIDTH,
                     str, LEFT_ALIGN, REDRAW_OFF);
        if(UnderbarPos == DATE_POS)
          Lcd_Printf(SETTING_UNDERBAR2_LEFT, OUTLINE_L2_BOT, SETTING_UNDERBAR_WIDTH,
                     str, LEFT_ALIGN, REDRAW_OFF);
        //TIME
        sprintf(str, "%.2d:%.2d:%.2d", SettingTemp.Time.Time.Hours, SettingTemp.Time.Time.Minutes, SettingTemp.Time.Time.Seconds);
        Lcd_Printf(FACTORY_SETTING_TIME_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_TIME_WIDTH,
                   str, CENTER_ALIGN, REDRAW_OFF);
        sprintf(str, "__");
        if(UnderbarPos == HOUR_POS)
          Lcd_Printf(SETTING_UNDERBAR0_LEFT, OUTLINE_L3_BOT, SETTING_UNDERBAR_WIDTH,
                     str, LEFT_ALIGN, REDRAW_OFF);
        if(UnderbarPos == MINUTE_POS)
          Lcd_Printf(SETTING_UNDERBAR1_LEFT, OUTLINE_L3_BOT, SETTING_UNDERBAR_WIDTH,
                     str, LEFT_ALIGN, REDRAW_OFF);
        if(UnderbarPos == SECOND_POS)
          Lcd_Printf(SETTING_UNDERBAR2_LEFT, OUTLINE_L3_BOT, SETTING_UNDERBAR_WIDTH,
                     str, LEFT_ALIGN, REDRAW_OFF);
        if((ControlMsg.EnableEdit == ENABLE_EDIT) && (UnderbarPos == SECOND_POS))
        {
          if (NewMoldBlink == BLINK_ON)
          {
            Lcd_Printf(SETTING_UNDERBAR2_LEFT, OUTLINE_L3_BOT, SETTING_UNDERBAR_WIDTH,
                       str, LEFT_ALIGN, REDRAW_OFF);
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);
              NewMoldBlink = BLINK_OFF;
            } 
          }
          else if(NewMoldBlink == BLINK_OFF)
          {
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);	 
              NewMoldBlink = BLINK_ON;
            }
          }
        }
      }
      else if(ControlMsg.PageNo == 2)
      {
        Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_WIDTH, 
                   Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX)], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_WIDTH,
                   Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 1], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_WIDTH,
                   Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 2], LEFT_ALIGN, REDRAW_OFF);
        
        if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
        {
          if (NewMoldBlink == BLINK_ON)
          {
            Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH, 
                       Factory_Setup_Option[(SettingTemp.DelMoldData) + POS_FAC_NO], CENTER_ALIGN, REDRAW_OFF);
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);
              NewMoldBlink = BLINK_OFF;
            } 
          }
          else if(NewMoldBlink == BLINK_OFF)
          {
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);	 
              NewMoldBlink = BLINK_ON;
            }
          }
        }
        else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH, 
                        Factory_Setup_Option[(SettingTemp.DelMoldData) + POS_FAC_NO], CENTER_ALIGN, REDRAW_OFF);
        if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
        {
          if (NewMoldBlink == BLINK_ON)
          {
            Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_OPT_WIDTH, 
                       Factory_Setup_Option[(SettingTemp.DelErrHistory) + POS_FAC_NO], CENTER_ALIGN, REDRAW_OFF);
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);
              NewMoldBlink = BLINK_OFF;
            } 
          }
          else if(NewMoldBlink == BLINK_OFF)
          {
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);	 
              NewMoldBlink = BLINK_ON;
            }
          }
        }
        else  Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L1_BOT, FACTORY_SETTING_OPT_WIDTH, 
                         Factory_Setup_Option[(SettingTemp.DelErrHistory) + POS_FAC_NO], CENTER_ALIGN, REDRAW_OFF);
        if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 2))
        {
          if (NewMoldBlink == BLINK_ON)
          {
            Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_OPT_WIDTH, 
                       Factory_Setup_Option[(SettingTemp.DoorSignalChange) + POS_FAC_MSTOP], CENTER_ALIGN, REDRAW_OFF);
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);
              NewMoldBlink = BLINK_OFF;
            } 
          }
          else if(NewMoldBlink == BLINK_OFF)
          {
            if(ChkExpireSysTick(&NewMoldTick))
            {
              SetSysTick(&NewMoldTick,TIME_BLINK);	 
              NewMoldBlink = BLINK_ON;
            }
          }
        }
        else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L2_BOT, FACTORY_SETTING_OPT_WIDTH, 
                        Factory_Setup_Option[(SettingTemp.DoorSignalChange) + POS_FAC_MSTOP], CENTER_ALIGN, REDRAW_OFF);
        
        //IMMType
        if((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC))
        {
          Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_WIDTH,
                     Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX) + 3], LEFT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 3))
          {
            
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_OPT_WIDTH, 
                         Factory_Setup_Option[(SettingTemp.IMMType) + POS_FAC_IMMType], CENTER_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_L3_BOT, FACTORY_SETTING_OPT_WIDTH, 
                          Factory_Setup_Option[(SettingTemp.IMMType) + POS_FAC_IMMType], CENTER_ALIGN, REDRAW_OFF);
        }
      }
      //IMMType
      else if(ControlMsg.PageNo == 3)
      {
        if((RobotCfg.RobotType == ROBOT_TYPE_X) || (RobotCfg.RobotType == ROBOT_TYPE_XC))
        {
          Lcd_Printf(FACTORY_SETTING_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_WIDTH, 
                     Factory_Setup_Msg[(ControlMsg.PageNo*OUTLINE_LINE_MAX)], LEFT_ALIGN, REDRAW_OFF);
          
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH, 
                         Factory_Setup_Option_Setting[(SettingTemp.RotationState) + POS_FAC_ROTATION], CENTER_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(FACTORY_SETTING_OPT_LEFT, OUTLINE_LO_BOT, FACTORY_SETTING_OPT_WIDTH, 
                          Factory_Setup_Option_Setting[(SettingTemp.RotationState) + POS_FAC_ROTATION], CENTER_ALIGN, REDRAW_OFF);
        }
      }
      break;
      
    case MANUAL_COUNTER:
      //Title
      Lcd_Printf(TITLE_MSG_LEFT, TITLE_MSG_BOT, TITLE_MSG_WIDTH,
                 Page_Name_List[COUNT_SETUP_PAGE], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(TITLE_MOLD_LEFT, TITLE_MOLD_BOT, TITLE_MOLD_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);
      //Msg
      Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L0_BOT, COUNTER_MSG_WIDTH, 
                 Count_Setup_Page_Msg[0], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L1_BOT, COUNTER_MSG_WIDTH,
                 Count_Setup_Page_Msg[1], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L2_BOT, COUNTER_MSG_WIDTH, 
                 Count_Setup_Page_Msg[2], LEFT_ALIGN, REDRAW_OFF);
      
      //Number
      sprintf(str, "%5d", RobotCfg.Count.TotalCnt);
      Lcd_Printf(COUNTER_VALUE_LEFT, TITLE_L0_BOT, COUNTER_VALUE_WIDTH,
                 str, RIGHT_ALIGN, REDRAW_OFF);
      
      sprintf(str, "%4d", RobotCfg.Count.RejectCnt);
      Lcd_Printf(COUNTER_VALUE_LEFT, TITLE_L1_BOT, COUNTER_VALUE_WIDTH,
                 str, RIGHT_ALIGN, REDRAW_OFF);
      
      sprintf(str, "%4d", RobotCfg.Count.DetectFail);
      Lcd_Printf(COUNTER_VALUE_LEFT, TITLE_L2_BOT, COUNTER_VALUE_WIDTH,
                 str, RIGHT_ALIGN, REDRAW_OFF);
      break;
      
    case MANUAL_TIMER:
      TimerSetting();
      //Title
      Lcd_Printf(TITLE_MSG_LEFT, TITLE_MSG_BOT, TITLE_MSG_WIDTH,
                 Page_Name_List[TIMER_EDIT_PAGE], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(TITLE_MOLD_LEFT, TITLE_MOLD_BOT, TITLE_MOLD_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);
      
      //T00,T01....
      if (TimerLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*TITLE_LINE_MAX) != ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(TIMER_MSG_LEFT, TITLE_L0_BOT, TIMER_MSG_WIDTH, 
                     Timer_Page_Num_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_MSG2_LEFT, TITLE_L0_BOT, TIMER_MSG2_WIDTH, 
                     Timer_Page_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L0_BOT, TIMER_VALUE_WIDTH, str0, RIGHT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L0_BOT, TIMER_TEMP_WIDTH, str3, RIGHT_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L0_BOT, TIMER_TEMP_WIDTH, str3, RIGHT_ALIGN, REDRAW_OFF); 
        }
        else TimerLineCheck = DISPLAY_OFF;
      }
      if (TimerLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*TITLE_LINE_MAX+1) != ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(TIMER_MSG_LEFT, TITLE_L1_BOT, TIMER_MSG_WIDTH,
                     Timer_Page_Num_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_MSG2_LEFT, TITLE_L1_BOT, TIMER_MSG2_WIDTH,
                     Timer_Page_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L1_BOT, TIMER_VALUE_WIDTH, str1, RIGHT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L1_BOT, TIMER_TEMP_WIDTH, str4, RIGHT_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L1_BOT, TIMER_TEMP_WIDTH, str4, RIGHT_ALIGN, REDRAW_OFF);
        }
        else TimerLineCheck = DISPLAY_OFF;
      }
      if (TimerLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*TITLE_LINE_MAX+2) != ControlMsg.TimerDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(TIMER_MSG_LEFT, TITLE_L2_BOT, TIMER_MSG_WIDTH,
                     Timer_Page_Num_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_MSG2_LEFT, TITLE_L2_BOT, TIMER_MSG2_WIDTH,
                     Timer_Page_Msg[ControlMsg.TimerDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L2_BOT, TIMER_VALUE_WIDTH, str2, RIGHT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 2))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L2_BOT, TIMER_TEMP_WIDTH, str5, RIGHT_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L2_BOT, TIMER_TEMP_WIDTH, str5, RIGHT_ALIGN, REDRAW_OFF);
        }
        else TimerLineCheck = DISPLAY_OFF;
      }
      break;
      
    case MANUAL_IO:
      //Title
      Lcd_Printf(TITLE_MSG_LEFT, IO_TITLE_BOT, IO_TITLE_WIDTH,
                 Page_Name_List[IO_PAGE - ControlMsg.LineNo], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(IO_TITLE_MOLD_LEFT, IO_TITLE_BOT, IO_TITLE_MOLD_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);
      //IO Signal : X11, X14, Y13.....
      Lcd_Printf(IO_SIGNAL_LEFT, TITLE_L0_BOT, IO_SIGNAL_WIDTH,
                 IO_Page_Num_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo)], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(IO_SIGNAL_LEFT, TITLE_L1_BOT, IO_SIGNAL_WIDTH,
                 IO_Page_Num_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 1], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(IO_SIGNAL_LEFT, TITLE_L2_BOT, IO_SIGNAL_WIDTH,
                 IO_Page_Num_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 2], LEFT_ALIGN, REDRAW_OFF);
      
      //IO Name(MSG)
      Lcd_Printf(IO_MSG_LEFT, TITLE_L0_BOT, IO_MSG_WIDTH,
                 IO_Page_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo)], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(IO_MSG_LEFT, TITLE_L1_BOT, IO_MSG_WIDTH,
                 IO_Page_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 1], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(IO_MSG_LEFT, TITLE_L2_BOT, IO_MSG_WIDTH,
                 IO_Page_Msg[(ControlMsg.PageNo * TITLE_LINE_MAX) + (IO_GAP*ControlMsg.LineNo) + 2], LEFT_ALIGN, REDRAW_OFF);
      break;
      
    case MANUAL_STEP_OPERATING:
      StepTimerSetting();
      
      //Title
      Lcd_Printf(STEP_PAGE_MSG0_LEFT, STEP_PAGE_MSG0_BOT, STEP_PAGE_MSG0_WIDTH,
                 Page_Name_List[FILE_STEP_RUN_PAGE], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(STEP_PAGE_MSG1_LEFT, STEP_PAGE_MSG1_BOT, STEP_PAGE_MSG1_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);
      
      //Step Name
      if (StepLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*TITLE_LINE_MAX) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
          Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L0_BOT, COUNTER_MSG_WIDTH, 
                     Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
        else StepLineCheck = DISPLAY_OFF;
      }
      if (StepLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*TITLE_LINE_MAX+1) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
          Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L1_BOT, COUNTER_MSG_WIDTH,
                     Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
        else StepLineCheck = DISPLAY_OFF;
      }
      if (StepLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*TITLE_LINE_MAX+2) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
          Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L2_BOT, COUNTER_MSG_WIDTH,
                     Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.PageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
        else StepLineCheck = DISPLAY_OFF;
      }
      break;
      
    case MANUAL_CYCLE_OPERATING:
      StepTimerSetting();
      //Title
      Lcd_Printf(AUTO_PAGE_MSG0_LEFT, AUTO_PAGE_MSG0_BOT, AUTO_PAGE_MSG0_WIDTH,
                 Page_Name_List[FILE_CYCLE_RUN_PAGE], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(STEP_PAGE_MSG1_LEFT, STEP_PAGE_MSG1_BOT, STEP_PAGE_MSG1_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);
      //Name
      if (StepLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.AutoPageNo*TITLE_LINE_MAX) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L0_BOT, COUNTER_MSG_WIDTH, 
                     Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.AutoPageNo*TITLE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L0_BOT, TIMER_VALUE_WIDTH, str0, RIGHT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L0_BOT, TIMER_TEMP_WIDTH, str3, RIGHT_ALIGN, REDRAW_OFF);
        }
        else StepLineCheck = DISPLAY_OFF;
      }
      if (StepLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.AutoPageNo*TITLE_LINE_MAX+1) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L1_BOT, COUNTER_MSG_WIDTH,
                     Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.AutoPageNo*TITLE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L1_BOT, TIMER_VALUE_WIDTH, str1, RIGHT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L1_BOT, TIMER_TEMP_WIDTH, str4, RIGHT_ALIGN, REDRAW_OFF);
        }
        else StepLineCheck = DISPLAY_OFF;
      }
      if (StepLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.AutoPageNo*TITLE_LINE_MAX+2) != ControlMsg.StepDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(COUNTER_MSG_LEFT, TITLE_L2_BOT, COUNTER_MSG_WIDTH,
                     Step_Name_Table[ControlMsg.StepDispTemp[ControlMsg.AutoPageNo*TITLE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_VALUE_LEFT, TITLE_L2_BOT, TIMER_VALUE_WIDTH, str2, RIGHT_ALIGN, REDRAW_OFF);
          Lcd_Printf(TIMER_TEMP_LEFT, TITLE_L2_BOT, TIMER_TEMP_WIDTH, str5, RIGHT_ALIGN, REDRAW_OFF);
        }
        else StepLineCheck = DISPLAY_OFF;
      }
      break;
      
    case MANUAL_MOLD_SEARCH:
      Lcd_Printf(FILE_SEARCH_PAGE_MSG0_LEFT, FILE_SEARCH_PAGE_MSG0_BOT, FILE_SEARCH_PAGE_MSG0_WIDTH,
                 Page_Name_List[FILE_SEARCH_PAGE], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(FILE_SEARCH_PAGE_MSG1_LEFT, FILE_SEARCH_PAGE_MSG1_BOT, FILE_SEARCH_PAGE_MSG1_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);
      
      Lcd_Printf(FILE_SEARCH_PAGE_MSG2_LEFT, FILE_SEARCH_PAGE_MSG2_BOT, FILE_SEARCH_PAGE_MSG2_WIDTH,
                 File_Search_Msg[(ControlMsg.PageNo*TITLE_LINE_MAX)], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(FILE_SEARCH_PAGE_MSG3_LEFT, FILE_SEARCH_PAGE_MSG3_BOT, FILE_SEARCH_PAGE_MSG3_WIDTH,
                 File_Search_Msg[(ControlMsg.PageNo*TITLE_LINE_MAX)+1], LEFT_ALIGN, REDRAW_OFF);
      
      sprintf(strMoldNoTemp, "%.3d", MoldNoTemp);
      Lcd_Printf(FILE_SEARCH_PAGE_MSG4_LEFT, FILE_SEARCH_PAGE_MSG4_BOT, FILE_SEARCH_PAGE_MSG4_WIDTH,
                 strMoldNoTemp, CENTER_ALIGN, REDRAW_OFF);
      break;
      
    case MANUAL_MOLD_MANAGE:
      Lcd_Printf(FILE_MANAGE_PAGE_MSG0_LEFT, FILE_MANAGE_PAGE_MSG0_BOT, FILE_MANAGE_PAGE_MSG0_WIDTH,
                 Page_Name_List[FILE_MANAGEMENT_PAGE], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(FILE_MANAGE_PAGE_MSG1_LEFT, FILE_MANAGE_PAGE_MSG1_BOT, FILE_MANAGE_PAGE_MSG1_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);
      //MoldNO : 000 Mold Name : NEW_MOLD
      MoldMsg.ExistMoldName[0][0] = Char_Input_Data[13];
      MoldMsg.ExistMoldName[0][1] = Char_Input_Data[4];
      MoldMsg.ExistMoldName[0][2] = Char_Input_Data[22];
      MoldMsg.ExistMoldName[0][3] = Char_Input_Data[38];
      MoldMsg.ExistMoldName[0][4] = Char_Input_Data[12];
      MoldMsg.ExistMoldName[0][5] = Char_Input_Data[14];
      MoldMsg.ExistMoldName[0][6] = Char_Input_Data[11];
      MoldMsg.ExistMoldName[0][7] = Char_Input_Data[3];
      
      if (MoldLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*TITLE_LINE_MAX) != MoldMsg.MoldCount)
        {
          for(uint8_t MoldNameChar = 0; MoldNameChar <= MAX_MOLDNAME_LENGTH; MoldNameChar++)
          {
            MoldNameStr0[MoldNameChar] = MoldMsg.ExistMoldName[ControlMsg.PageNo*TITLE_LINE_MAX][MoldNameChar];
          }
          sprintf(str0, "%.3d", MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX]);
          Lcd_Printf(FILE_MANAGE_PAGE_MSG2_LEFT, FILE_MANAGE_PAGE_MSG2_BOT, FILE_MANAGE_PAGE_MSG2_WIDTH,
                     str0, LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(FILE_MANAGE_PAGE_MSG2_OP_LEFT, FILE_MANAGE_PAGE_MSG2_OP_BOT, FILE_MANAGE_PAGE_MSG2_OP_WIDTH,
                     MoldNameStr0, LEFT_ALIGN, REDRAW_OFF);
        }
        else MoldLineCheck = DISPLAY_OFF;
      }
      if (MoldLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*TITLE_LINE_MAX+1) != MoldMsg.MoldCount)
        {
          for(uint8_t MoldNameChar = 0; MoldNameChar <= MAX_MOLDNAME_LENGTH; MoldNameChar++)
          {
            MoldNameStr1[MoldNameChar] = MoldMsg.ExistMoldName[ControlMsg.PageNo*TITLE_LINE_MAX+1][MoldNameChar];
          }
          sprintf(str1, "%.3d", MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+1]);
          Lcd_Printf(FILE_MANAGE_PAGE_MSG3_LEFT, FILE_MANAGE_PAGE_MSG3_BOT, FILE_MANAGE_PAGE_MSG3_WIDTH,
                     str1, LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(FILE_MANAGE_PAGE_MSG3_OP_LEFT, FILE_MANAGE_PAGE_MSG3_OP_BOT, FILE_MANAGE_PAGE_MSG3_OP_WIDTH,
                     MoldNameStr1, LEFT_ALIGN, REDRAW_OFF);
        }
        else MoldLineCheck = DISPLAY_OFF;
      }
      if (MoldLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*TITLE_LINE_MAX+2) != MoldMsg.MoldCount)
        {
          for(uint8_t MoldNameChar = 0; MoldNameChar <= MAX_MOLDNAME_LENGTH; MoldNameChar++)
          {
            MoldNameStr2[MoldNameChar] = MoldMsg.ExistMoldName[ControlMsg.PageNo*TITLE_LINE_MAX+2][MoldNameChar];
          }
          sprintf(str2, "%.3d", MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+2]);
          Lcd_Printf(FILE_MANAGE_PAGE_MSG4_LEFT, FILE_MANAGE_PAGE_MSG4_BOT, FILE_MANAGE_PAGE_MSG4_WIDTH,
                     str2, LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(FILE_MANAGE_PAGE_MSG4_OP_LEFT, FILE_MANAGE_PAGE_MSG4_OP_BOT, FILE_MANAGE_PAGE_MSG4_OP_WIDTH,
                     MoldNameStr2, LEFT_ALIGN, REDRAW_OFF);
        }
        else MoldLineCheck = DISPLAY_OFF;
      }
      break;
      
    case MANUAL_MOLD_MODE:
      ModeSetting();
      
      if (ModeLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*OUTLINE_LINE_MAX) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(MODE_NUM_LEFT, OUTLINE_LO_BOT, MODE_NUM_WIDTH, 
                     Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_MSG_LEFT, OUTLINE_LO_BOT, MODE_MSG_WIDTH, 
                     Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX]], LEFT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 0))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_LO_BOT, MODE_OPTION_WIDTH, 
                         Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX]], CENTER_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_LO_BOT, MODE_OPTION_WIDTH, 
                          Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX]], CENTER_ALIGN, REDRAW_OFF);
        }
        else ModeLineCheck = DISPLAY_OFF;
      }
      if (ModeLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+1) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L1_BOT, MODE_NUM_WIDTH,
                     Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L1_BOT, MODE_MSG_WIDTH,
                     Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], LEFT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 1))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L1_BOT, MODE_OPTION_WIDTH, 
                         Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], CENTER_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L1_BOT, MODE_OPTION_WIDTH, 
                          Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+1]], CENTER_ALIGN, REDRAW_OFF);
        }
        else ModeLineCheck = DISPLAY_OFF;
      }
      if (ModeLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+2) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L2_BOT, MODE_NUM_WIDTH,
                     Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L2_BOT, MODE_MSG_WIDTH,
                     Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], LEFT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 2))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L2_BOT, MODE_OPTION_WIDTH, 
                         Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], CENTER_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L2_BOT, MODE_OPTION_WIDTH, 
                          Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+2]], CENTER_ALIGN, REDRAW_OFF);
        }
        else ModeLineCheck = DISPLAY_OFF;
      }
      if (ModeLineCheck == DISPLAY_ON)
      {
        if ((ControlMsg.PageNo*OUTLINE_LINE_MAX+3) != ControlMsg.ModeDispTemp[TOTAL_COUNT_INDEX])
        {
          Lcd_Printf(MODE_NUM_LEFT, OUTLINE_L3_BOT, MODE_NUM_WIDTH,
                     Mode_Page_Common[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(MODE_MSG_LEFT, OUTLINE_L3_BOT, MODE_MSG_WIDTH,
                     Mode_Page_Msg[ControlMsg.ModeDispTemp[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], LEFT_ALIGN, REDRAW_OFF);
          if((ControlMsg.EnableEdit == ENABLE_EDIT) && (ControlMsg.LineNo == 3))
          {
            if (NewMoldBlink == BLINK_ON)
            {
              Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L3_BOT, MODE_OPTION_WIDTH, 
                         Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], CENTER_ALIGN, REDRAW_OFF);
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);
                NewMoldBlink = BLINK_OFF;
              } 
            }
            else if(NewMoldBlink == BLINK_OFF)
            {
              if(ChkExpireSysTick(&NewMoldTick))
              {
                SetSysTick(&NewMoldTick,TIME_BLINK);	 
                NewMoldBlink = BLINK_ON;
              }
            }
          }
          else Lcd_Printf(MODE_OPTION_LEFT, OUTLINE_L3_BOT, MODE_OPTION_WIDTH, 
                          Mode_Page_Option[ControlMsg.ModeSelect[ControlMsg.PageNo*OUTLINE_LINE_MAX+3]], CENTER_ALIGN, REDRAW_OFF);
        }
        else ModeLineCheck = DISPLAY_OFF;
      }
      break;
      
    case MANUAL_MOLD_NEW:
      Lcd_Printf(NEW_PAGE_MSG0_LEFT, NEW_PAGE_MSG0_BOT, NEW_PAGE_MSG0_WIDTH,
                 Page_Name_List[FILE_NEW_PAGE], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(NEW_PAGE_MSG1_LEFT, NEW_PAGE_MSG1_BOT, NEW_PAGE_MSG1_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);
      
      //MoldNo
      sprintf(strMoldNoTemp, "%.3d", MoldNoTemp);
      
      if (UnderbarPos == MOLD_NO)
      {
        if (NewMoldBlink == BLINK_ON)
        {
          Lcd_Printf(NEW_PAGE_MSG2_LEFT, NEW_PAGE_MSG2_BOT, NEW_PAGE_MSG2_WIDTH,
                     strMoldNoTemp, CENTER_ALIGN, REDRAW_OFF);
          if(ChkExpireSysTick(&NewMoldTick))
          {
            SetSysTick(&NewMoldTick,TIME_BLINK);
            NewMoldBlink = BLINK_OFF;
          } 
        }
        else if(NewMoldBlink == BLINK_OFF)
        {
          if(ChkExpireSysTick(&NewMoldTick))
          {
            SetSysTick(&NewMoldTick,TIME_BLINK);	 
            NewMoldBlink = BLINK_ON;
          }
        }
      }
      else if(UnderbarPos > MOLD_NO)
        Lcd_Printf(NEW_PAGE_MSG2_LEFT, NEW_PAGE_MSG2_BOT, NEW_PAGE_MSG2_WIDTH,
                   strMoldNoTemp, CENTER_ALIGN, REDRAW_OFF);
      
      //MoldNams
      NewMoldName[0] = Char_Input_Data[NewMoldNameTemp[0]];
      NewMoldName[1] = Char_Input_Data[NewMoldNameTemp[1]];
      NewMoldName[2] = Char_Input_Data[NewMoldNameTemp[2]];
      NewMoldName[3] = Char_Input_Data[NewMoldNameTemp[3]];
      NewMoldName[4] = Char_Input_Data[NewMoldNameTemp[4]];
      NewMoldName[5] = Char_Input_Data[NewMoldNameTemp[5]];
      NewMoldName[6] = Char_Input_Data[NewMoldNameTemp[6]];
      NewMoldName[7] = Char_Input_Data[NewMoldNameTemp[7]];
      NewMoldName[MoldNamePos] = Char_Input_Data[MoldNameCharPos];
      
      Lcd_Printf(NEW_PAGE_MSG3_LEFT, NEW_PAGE_MSG3_BOT, NEW_PAGE_MSG3_WIDTH,
                 NewMoldName, LEFT_ALIGN, REDRAW_OFF);
      
      //MoldNameUnderBar
      sprintf(str, "_");
      if (UnderbarPos == MOLD_NAME)
      {
        if (NewMoldBlink == BLINK_ON)
        {
          Lcd_Printf(NEW_PAGE_MSG3_LEFT+(MAX_UNDERBAR_POS*MoldNamePos), OUTLINE_L2_BOT, SETTING_UNDERBAR_WIDTH,
                     str, LEFT_ALIGN, REDRAW_OFF);
          if(ChkExpireSysTick(&NewMoldTick))
          {
            SetSysTick(&NewMoldTick,TIME_BLINK);	 
            NewMoldBlink = BLINK_OFF;
          }
        }
        else if(NewMoldBlink == BLINK_OFF)
        {
          if(ChkExpireSysTick(&NewMoldTick))
          {
            SetSysTick(&NewMoldTick,TIME_BLINK);	 
            NewMoldBlink = BLINK_ON;
          }
        }
      }
      break;
      
    case MANUAL_MOLD_DELETE:
      Lcd_Printf(DELETE_PAGE_MSG0_LEFT, DELETE_PAGE_MSG0_BOT, DELETE_PAGE_MSG0_WIDTH,
                 Page_Name_List[FILE_DEL_PAGE], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.MoldNo);
      Lcd_Printf(DELETE_PAGE_MSG1_LEFT, DELETE_PAGE_MSG1_BOT, DELETE_PAGE_MSG1_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);
      
      for(uint8_t MoldNameChar = 0; MoldNameChar <= MAX_MOLDNAME_LENGTH; MoldNameChar++)
      {
        MoldNameStr0[MoldNameChar] = MoldMsg.ExistMoldName[ControlMsg.PageNo*TITLE_LINE_MAX + ControlMsg.LineNo][MoldNameChar];
      }
      sprintf(str0, "%.3d", MoldMsg.ExistMold[ControlMsg.PageNo*TITLE_LINE_MAX+ControlMsg.LineNo]);
      
      
      Lcd_Printf(DELETE_PAGE_MSG2_LEFT, DELETE_PAGE_MSG2_BOT, DELETE_PAGE_MSG2_WIDTH,
                 str0, LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(DELETE_PAGE_MSG3_LEFT, DELETE_PAGE_MSG3_BOT, DELETE_PAGE_MSG3_WIDTH,
                 MoldNameStr0, LEFT_ALIGN, REDRAW_OFF);
      
      Lcd_Printf(DELETE_PAGE_MSG4_LEFT, DELETE_PAGE_MSG4_BOT, DELETE_PAGE_MSG4_WIDTH,
                 Delete_Page_Msg[0], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(DELETE_PAGE_MSG5_LEFT, DELETE_PAGE_MSG5_BOT, DELETE_PAGE_MSG5_WIDTH,
                 Delete_Page_Msg[1], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(DELETE_PAGE_MSG7_LEFT, DELETE_PAGE_MSG7_BOT, DELETE_PAGE_MSG7_WIDTH,
                 Delete_Page_Msg[2], LEFT_ALIGN, REDRAW_OFF);
      break;
      
    case MANUAL_MOLD_DELETE_ING:
      Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH,
                 Delete_Page_Msg[3], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH,
                 Delete_Page_Msg[4], LEFT_ALIGN, REDRAW_OFF);
      break;
      
      
    case MANUAL_ERROR_LIST:
      if(ErrMsg.ErrorCount != 0)
        ErrPage = ControlMsg.PageNo+1;
      sprintf(Error0, "%d/%d", ErrPage, ErrMsg.ErrorCount);
      sprintf(Error1, "%.2d/%.2d/%.2d", ErrMsgTemp[ControlMsg.PageNo].Time.Date.Year, 
              ErrMsgTemp[ControlMsg.PageNo].Time.Date.Month, ErrMsgTemp[ControlMsg.PageNo].Time.Date.Date);
      sprintf(Error2, "%.2d:%.2d:%.2d", ErrMsgTemp[ControlMsg.PageNo].Time.Time.Hours, 
              ErrMsgTemp[ControlMsg.PageNo].Time.Time.Minutes, ErrMsgTemp[ControlMsg.PageNo].Time.Time.Seconds);
      sprintf(Error3, "%.3d", ErrMsgTemp[ControlMsg.PageNo].ErrorCode);
      
      
      Lcd_Printf(ERROR_LIST_PAGE_MSG0_LEFT, ERROR_LIST_PAGE_MSG0_BOT, ERROR_LIST_PAGE_MSG0_WIDTH,
                 Page_Name_List[FILE_ERROR_PAGE], CENTER_ALIGN, REDRAW_OFF);
      Lcd_Printf(ERROR_LIST_PAGE_MSG1_LEFT, ERROR_LIST_PAGE_MSG1_BOT, ERROR_LIST_PAGE_MSG1_WIDTH,
                 Error0, LEFT_ALIGN, REDRAW_OFF);//에러 넘버
      //		Lcd_Printf(ERROR_LIST_PAGE_MSG2_1_LEFT, ERROR_LIST_PAGE_MSG2_1_BOT, ERROR_LIST_PAGE_MSG2_1_WIDTH,
      //					  Error_Pop_Page_Msg[1], LEFT_ALIGN, REDRAW_OFF);//날짜 문구
      Lcd_Printf(ERROR_LIST_PAGE_MSG2_LEFT, ERROR_LIST_PAGE_MSG2_BOT, ERROR_LIST_PAGE_MSG2_WIDTH,
                 Error1, RIGHT_ALIGN, REDRAW_OFF);//에러 발생 날짜
      //		Lcd_Printf(ERROR_LIST_PAGE_MSG3_1_LEFT, ERROR_LIST_PAGE_MSG3_1_BOT, ERROR_LIST_PAGE_MSG3_1_WIDTH,
      //					  Error_Pop_Page_Msg[2], LEFT_ALIGN, REDRAW_OFF);//시간 문구
      Lcd_Printf(ERROR_LIST_PAGE_MSG3_LEFT, ERROR_LIST_PAGE_MSG3_BOT, ERROR_LIST_PAGE_MSG3_WIDTH,
                 Error2, RIGHT_ALIGN, REDRAW_OFF);//에러 발생 시간
      Lcd_Printf(ERROR_LIST_PAGE_MSG4_LEFT, ERROR_LIST_PAGE_MSG4_BOT, ERROR_LIST_PAGE_MSG4_WIDTH,
                 Error3, LEFT_ALIGN, REDRAW_OFF);//에러 코드 번호
      
      //에러 내역
      if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_EMG)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[8], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_IMM_EMG)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[9], LEFT_ALIGN, REDRAW_OFF);
      
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SWING_DUPLICATE)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[10], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SUB_UP)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[11], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_MAIN_UP)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[12], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SUB_UP_AIR)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[13], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_MAIN_UP_AIR)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[14], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SWING)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[15], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SWING_RETURN)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[16], LEFT_ALIGN, REDRAW_OFF);
      
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_VACCUM)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[17], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_CHUCK)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[18], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_RUNNER_PICK)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[19], LEFT_ALIGN, REDRAW_OFF);
      
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_MO_SENSOR)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[20], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_MO_SENS_MISS)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[21], LEFT_ALIGN, REDRAW_OFF);
      
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_MO_DOWN)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[22], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_PROCESS_TIME)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[23], LEFT_ALIGN, REDRAW_OFF);
      
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SD_DISABLE)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[33], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SD_NOT_FILE)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[34], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SD_LOCKING)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[35], LEFT_ALIGN, REDRAW_OFF);
      else if(ErrMsgTemp[ControlMsg.PageNo].ErrorCode == ERRCODE_SD_EMPTY)
        Lcd_Printf(ERROR_LIST_PAGE_MSG5_LEFT, ERROR_LIST_PAGE_MSG5_BOT, ERROR_LIST_PAGE_MSG5_WIDTH,
                   Error_Msg[36], LEFT_ALIGN, REDRAW_OFF);
      break;
      
    case MANUAL_VERSION:
      Lcd_Printf(VERSION_PAGE_MSG0_LEFT, VERSION_PAGE_MSG0_BOT, VERSION_PAGE_MSG0_WIDTH,
                 Page_Name_List[VERSION_PAGE], CENTER_ALIGN, REDRAW_OFF);
      Lcd_Printf(VERSION_PAGE_MSG1_LEFT, VERSION_PAGE_MSG1_BOT, VERSION_PAGE_MSG1_WIDTH,
                 Machine_Type_Name[4], LEFT_ALIGN, REDRAW_OFF);
      Lcd_Printf(VERSION_PAGE_MSG2_LEFT, VERSION_PAGE_MSG2_BOT, VERSION_PAGE_MSG2_WIDTH,
                 Machine_Type_Name[5], LEFT_ALIGN, REDRAW_OFF);
      if(RobotCfg.RobotType == TYPE_A)
        Lcd_Printf(VERSION_PAGE_MSG3_LEFT, VERSION_PAGE_MSG3_BOT, VERSION_PAGE_MSG3_WIDTH,
                   Machine_Type_Name[0], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.RobotType == TYPE_X)
        Lcd_Printf(VERSION_PAGE_MSG3_LEFT, VERSION_PAGE_MSG3_BOT, VERSION_PAGE_MSG3_WIDTH,
                   Machine_Type_Name[1], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.RobotType == TYPE_XC)
        Lcd_Printf(VERSION_PAGE_MSG3_LEFT, VERSION_PAGE_MSG3_BOT, VERSION_PAGE_MSG3_WIDTH,
                   Machine_Type_Name[2], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.RobotType == TYPE_TWIN)
        Lcd_Printf(VERSION_PAGE_MSG3_LEFT, VERSION_PAGE_MSG3_BOT, VERSION_PAGE_MSG3_WIDTH,
                   Machine_Type_Name[3], LEFT_ALIGN, REDRAW_OFF);
      break;
      
    case OCCUR_ERROR: 
      Lcd_Printf(ERROR_POPUP_PAGE_MSG0_LEFT, ERROR_POPUP_PAGE_MSG0_BOT, ERROR_POPUP_PAGE_MSG0_WIDTH,
                 Error_Pop_Page_Msg[0], CENTER_ALIGN, REDRAW_OFF);
      sprintf(str, "%.3d", RobotCfg.ErrCode);
      Lcd_Printf(ERROR_POPUP_PAGE_MSG1_LEFT, ERROR_POPUP_PAGE_MSG1_BOT, ERROR_POPUP_PAGE_MSG1_WIDTH,
                 str, CENTER_ALIGN, REDRAW_OFF);
      
      
      if(RobotCfg.ErrCode == ERRCODE_EMG)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[8], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_IMM_EMG)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[9], LEFT_ALIGN, REDRAW_OFF);
      
      else if(RobotCfg.ErrCode == ERRCODE_SWING_DUPLICATE)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[10], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_SUB_UP)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[11], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_MAIN_UP)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[12], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_SUB_UP_AIR)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[13], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_MAIN_UP_AIR)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[14], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_SWING)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[15], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_SWING_RETURN)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[16], LEFT_ALIGN, REDRAW_OFF);
      
      else if(RobotCfg.ErrCode == ERRCODE_VACCUM)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[17], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_CHUCK)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[18], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_RUNNER_PICK)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[19], LEFT_ALIGN, REDRAW_OFF);
      
      else if(RobotCfg.ErrCode == ERRCODE_MO_SENSOR)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[20], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_MO_SENS_MISS)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[21], LEFT_ALIGN, REDRAW_OFF);
      
      else if(RobotCfg.ErrCode == ERRCODE_MO_DOWN)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[22], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_PROCESS_TIME)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[23], LEFT_ALIGN, REDRAW_OFF);
      
      else if(RobotCfg.ErrCode == ERRCODE_SD_DISABLE)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[33], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_SD_NOT_FILE)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[34], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_SD_LOCKING)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[35], LEFT_ALIGN, REDRAW_OFF);
      else if(RobotCfg.ErrCode == ERRCODE_SD_EMPTY)
        Lcd_Printf(ERROR_POPUP_PAGE_MSG2_LEFT, ERROR_POPUP_PAGE_MSG2_BOT, ERROR_POPUP_PAGE_MSG2_WIDTH,
                   Error_Msg[36], LEFT_ALIGN, REDRAW_OFF);
      
      
      
      if(RobotCfg.ErrCode == ERRCODE_EMG)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[2], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[3], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == ERRCODE_IMM_EMG)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[4], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[5], LEFT_ALIGN, REDRAW_OFF);
      }
      
      else if(RobotCfg.ErrCode == ERRCODE_SWING_DUPLICATE)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[6], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[7], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == ERRCODE_SUB_UP)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[8], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[9], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == ERRCODE_MAIN_UP)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[10], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[11], LEFT_ALIGN, REDRAW_OFF);
      }
      else if((RobotCfg.ErrCode == ERRCODE_SUB_UP_AIR) || (RobotCfg.ErrCode == ERRCODE_MAIN_UP_AIR))
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[12], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[13], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == ERRCODE_SWING)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[14], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[15], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == ERRCODE_SWING_RETURN)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[16], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[17], LEFT_ALIGN, REDRAW_OFF);
      }
      
      else if((RobotCfg.ErrCode == ERRCODE_VACCUM) || (RobotCfg.ErrCode == ERRCODE_CHUCK) || (RobotCfg.ErrCode == ERRCODE_RUNNER_PICK))
      {
        if(RobotCfg.Setting.ItlSaftyDoor == USE)
        {
          Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                     Error_Action_Msg[18], LEFT_ALIGN, REDRAW_OFF);
          Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                     Error_Action_Msg[19], LEFT_ALIGN, REDRAW_OFF);
        }
        else if(RobotCfg.Setting.ItlSaftyDoor == NO_USE)
        {
          if(RobotCfg.ErrCode == ERRCODE_VACCUM)
          {
            Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                       Error_Action_Msg[20], LEFT_ALIGN, REDRAW_OFF);
            Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                       Error_Action_Msg[21], LEFT_ALIGN, REDRAW_OFF);
          }
          if(RobotCfg.ErrCode == ERRCODE_CHUCK)
          {
            Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                       Error_Action_Msg[22], LEFT_ALIGN, REDRAW_OFF);
            Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                       Error_Action_Msg[23], LEFT_ALIGN, REDRAW_OFF);
          }
          if(RobotCfg.ErrCode == ERRCODE_RUNNER_PICK)
          {
            Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                       Error_Action_Msg[24], LEFT_ALIGN, REDRAW_OFF);
            Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                       Error_Action_Msg[25], LEFT_ALIGN, REDRAW_OFF);
          }
        }
      }
      else if(RobotCfg.ErrCode == ERRCODE_MO_SENSOR)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[26], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[27], LEFT_ALIGN, REDRAW_OFF);
      }
      if(RobotCfg.ErrCode == ERRCODE_MO_SENS_MISS)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[0], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[1], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == ERRCODE_MO_DOWN)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[28], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == ERRCODE_PROCESS_TIME)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[29], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[30], LEFT_ALIGN, REDRAW_OFF);
      }
      
      else if(RobotCfg.ErrCode == ERRCODE_SD_DISABLE)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[0], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[1], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == ERRCODE_SD_NOT_FILE)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[32], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == ERRCODE_SD_LOCKING)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[33], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(ERROR_POPUP_PAGE_MSG4_LEFT, ERROR_POPUP_PAGE_MSG4_BOT, ERROR_POPUP_PAGE_MSG4_WIDTH,
                   Error_Action_Msg[34], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == ERRCODE_SD_EMPTY)
      {
        Lcd_Printf(ERROR_POPUP_PAGE_MSG3_LEFT, ERROR_POPUP_PAGE_MSG3_BOT, ERROR_POPUP_PAGE_MSG3_WIDTH,
                   Error_Action_Msg[35], LEFT_ALIGN, REDRAW_OFF);
      }
      
      break;
      
    case MANUAL_ERROR:
      //MANUAL_OPERATING
      if(RobotCfg.ErrCode == MANUAL_ERR_CHYCK_RO)
      {
        Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
                   Massage_Pop_Msg[8], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
                   Massage_Pop_Msg[9], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
                   Massage_Pop_Msg[10], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_CHYCK_ROR)
      {
        Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
                   Massage_Pop_Msg[11], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
                   Massage_Pop_Msg[12], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
                   Massage_Pop_Msg[13], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_MOLD_DELETE)
      {
        Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
                   Massage_Pop_Msg[32], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
                   Massage_Pop_Msg[33], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_ROBOT_TYPE)
      {
        Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
                   Massage_Pop_Msg[5], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
                   Massage_Pop_Msg[6], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
                   Massage_Pop_Msg[7], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_NO_MOLDNO)
      {
        Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
                   Massage_Pop_Msg[1], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
                   Massage_Pop_Msg[2], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
                   Massage_Pop_Msg[3], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG3_LEFT, MASSAGE_MSG3_BOT, MASSAGE_MSG3_WIDTH, 
                   Massage_Pop_Msg[4], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_MOLD)
      {
        Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
                   Massage_Pop_Msg[14], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
                   Massage_Pop_Msg[15], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
                   Massage_Pop_Msg[16], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_SAFTYDOOR_STEP)
      {
        Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
                   Massage_Pop_Msg[25], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
                   Massage_Pop_Msg[26], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
                   Massage_Pop_Msg[27], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_SAFTYDOOR_CYCLE)
      {
        Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
                   Massage_Pop_Msg[25], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
                   Massage_Pop_Msg[26], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
                   Massage_Pop_Msg[28], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_SAFTYDOOR_AUTO)
      {
        Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
                   Massage_Pop_Msg[25], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
                   Massage_Pop_Msg[26], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
                   Massage_Pop_Msg[29], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_SAFTYDOOR_CHANGE)
      {
        Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
                   Massage_Pop_Msg[30], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
                   Massage_Pop_Msg[31], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_REBOOTING)
      {
        Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
                   Massage_Pop_Msg[34], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
                   Massage_Pop_Msg[35], LEFT_ALIGN, REDRAW_OFF);
      }
      else if(RobotCfg.ErrCode == MANUAL_ERR_SAFTYDOOR_CLOSE)
      {
        Lcd_Printf(MASSAGE_MSG0_LEFT, MASSAGE_MSG0_BOT, MASSAGE_MSG0_WIDTH, 
                   Massage_Pop_Msg[17], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG1_LEFT, MASSAGE_MSG1_BOT, MASSAGE_MSG1_WIDTH, 
                   Massage_Pop_Msg[18], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG2_LEFT, MASSAGE_MSG2_BOT, MASSAGE_MSG2_WIDTH, 
                   Massage_Pop_Msg[19], LEFT_ALIGN, REDRAW_OFF);
        Lcd_Printf(MASSAGE_MSG3_LEFT, MASSAGE_MSG3_BOT, MASSAGE_MSG3_WIDTH, 
                   Massage_Pop_Msg[20], LEFT_ALIGN, REDRAW_OFF);
      }
      break;
      
    default:
      break;
    }
    LCDLoopState = LCD_DISPLAY;
    break;
    
  case LCD_DISPLAY:
    LCD_Draw(0, 0, 128, 64);
    ControlMsg.IsChangedLCD = NO_CHANGE;
    LCDLoopState = CHECK_UPDATE;
    break;
  }
}
