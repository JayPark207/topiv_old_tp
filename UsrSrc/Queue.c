#include "Queue.h"
//#include <stdlib.h>
//#include <string.h>

//BUFF_TYPE BufferSensor[QUEUE_SIZE];
//BUFF_TYPE BufferPLC[QUEUE_SIZE];
//BUFF_TYPE BufferIon[QUEUE_SIZE];
//
//QUEUE	qUartSensor;
//QUEUE	qUartPLC;
//QUEUE	qUartIon;
//  
//QUEUE *pqUartSensor;
//QUEUE *pqUartPLC;
//QUEUE *pqUartIon;

//QUEUE	qUart;
//QUEUE *pqUart;
//BUFF_TYPE BufferUart[QUEUE_SIZE];


//void NewQueue(QUEUE *pQueue, BUFF_TYPE (*pBuff)[], uint16_t size);

//void InitQueue(void)
//{
////   pqUartSensor = &qUartSensor;
////   pqUartPLC = &qUartPLC;
////   pqUartIon = &qUartIon;
////   
////   NewQueue(pqUartSensor, &BufferSensor, QUEUE_SIZE);
////   NewQueue(pqUartPLC, &BufferPLC, QUEUE_SIZE);
////   NewQueue(pqUartIon, &BufferIon, QUEUE_SIZE);
//}
void NewQueue(QUEUE *pQueue, BUFF_TYPE (*pBuff)[], uint16_t size)
{
    memset(pQueue, 0, sizeof(QUEUE));
    pQueue->pBuff = pBuff;
    pQueue->Size = size;
	 pQueue->Head = 0;
	 pQueue->Tail = 0;
}
unsigned char  IsFull(QUEUE *pQueue)
{
    VAR_TYPE Head = pQueue->Head,
            Tail = pQueue->Tail;
    VAR_TYPE Size = pQueue->Size;
    
    if((Head+1)%Size == Tail)
        return TRUE;     //head>tail
    else return FALSE;
}
unsigned char IsEmpty(QUEUE *pQueue)
{
    VAR_TYPE Head = pQueue->Head,
            Tail = pQueue->Tail;
    if(Head == Tail)
        return TRUE;
    else return FALSE;
}

VAR_TYPE GetSpace(QUEUE *pQueue)
{
    VAR_TYPE Head = pQueue->Head,
            Tail = pQueue->Tail,
            Size = pQueue->Size;

    if(IsEmpty(pQueue))
        return Size;
    else if(IsFull(pQueue))
        return 0;

    if(Head > Tail)
        return (Tail + Size) - Head;
    else
        return Tail - Head;
}
void IncHead(QUEUE* pQueue, int cnt)
{
    pQueue->Head = (pQueue->Head + cnt) % (pQueue->Size);
}
void IncTail(QUEUE* pQueue, int cnt)
{
    pQueue->Tail = (pQueue->Tail + cnt) % (pQueue->Size);
}
void EmptyQueue(QUEUE *pQueue)
{
    pQueue->Tail = pQueue->Head;
}
void ResetQueue(QUEUE* pQueue)
{
	memset(pQueue->pBuff, 0, sizeof(QUEUE_SIZE));
	pQueue->Head = 0;
	pQueue->Tail = 0;
}
VAR_TYPE GetExistCnt(QUEUE *pQueue)
{
    VAR_TYPE Head = pQueue->Head,
             Tail = pQueue->Tail,
             Size = pQueue->Size;

    if(IsEmpty(pQueue))
        return 0;
    else if(IsFull(pQueue))
        return Size;

    if(Head < Tail)
        return (Head + Size) - Tail;
    else
        return Head - Tail;
}
void PrintQueue(QUEUE* pQueue)
{
	VAR_TYPE cnt, i;
	
	if(IsEmpty(pQueue)) return;
	cnt = GetExistCnt(pQueue);

	printf("[Tail : %5d, Head : %5d, Cnt : %5d]\n\r",pQueue->Tail, pQueue->Head, cnt);
	printf("Q : ");
	for(i=0; i<cnt; i++) printf("%5X", (*pQueue->pBuff)[pQueue->Tail+i]);
	printf("\n\r");
}

void WriteSingleByte(QUEUE *pQueue, VAR_TYPE Data)
{
    VAR_TYPE Head = pQueue->Head;
    
    if(IsFull(pQueue))
    {
        return;
    }
    (*pQueue->pBuff)[Head] = Data;
}
VAR_TYPE GetSingleByte(QUEUE *pQueue)
{
   return (*pQueue->pBuff)[pQueue->Tail];
}
VAR_TYPE GetQueueData(QUEUE *pQueue, VAR_TYPE pos)
{
   VAR_TYPE Size = pQueue->Size,
            Tail = pQueue->Tail;
    return (*pQueue->pBuff)[(Tail+pos)%Size];
}
VAR_TYPE GetTail(QUEUE *pQueue)
{
	return pQueue->Tail;
}
VAR_TYPE GetHead(QUEUE *pQueue)
{
	return pQueue->Head;
}

