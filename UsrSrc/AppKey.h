/* 
 * File:   AppKey.h
 * Author: Jay Park
 *
 * Created on 2016년 11월 14일 (Mon), PM 3:33
 */

#ifndef __APPTKEY_H
#define	__APPTKEY_H

#ifdef	__cplusplus
extern "C" {
#endif

//#include "timer.h"
#include "main.h"   
#include "interface.h"
#include "AppLCD.h"
	
#define TIME_KEY        			50	//50
#define TIME_SCAN						2	//2
#define TIME_BEEP       			70
#define TIME_EMO_BUZZER				2000
  
#define TIME_BACKLIGHT  			600000		//600000
	
#define LCD_BACKLIGHT_ON			GPIO_PIN_SET
#define LCD_BACKLIGHT_OFF			GPIO_PIN_RESET	

#define ReadKey()			(((GPIOG->IDR) & 0x1F))			//(((GPIOG->IDR) & 0x1F) ^ 0x1F)

	
#define ResetKey()		HAL_GPIO_WritePin(GPIOE, KIN0_Pin|KIN1_Pin|KIN2_Pin|KIN3_Pin|KIN4_Pin, GPIO_PIN_RESET)  //GPIO_PIN_RESET = 0, GPIO_PIN_SET
#define RowKey(x)			HAL_GPIO_WritePin(GPIOE, (KIN0_Pin << x), GPIO_PIN_SET)//GPIOE->BSRR = (0x01 << (x + 18))
 
#define MAX_ROW			5
	
#define KEY_BIT0		(uint32_t)0x00000001		//0,0
#define KEY_BIT1		(uint32_t)0x00000002     //0,1
#define KEY_BIT2		(uint32_t)0x00000004     //0,2		//SHIFT
#define KEY_BIT3		(uint32_t)0x00000008     //0,3
#define KEY_BIT4		(uint32_t)0x00000010     //0,4
#define KEY_BIT5		(uint32_t)0x00000020     //1,0
#define KEY_BIT6		(uint32_t)0x00000040     //1,1
#define KEY_BIT7		(uint32_t)0x00000080     //1,2
#define KEY_BIT8		(uint32_t)0x00000100     //1,3
#define KEY_BIT9		(uint32_t)0x00000200     //1,4
#define KEY_BIT10		(uint32_t)0x00000400     //2,0
#define KEY_BIT11		(uint32_t)0x00000800     //2,1
#define KEY_BIT12		(uint32_t)0x00001000     //2,2
#define KEY_BIT13		(uint32_t)0x00002000     //2,3
#define KEY_BIT14		(uint32_t)0x00004000     //2,4
#define KEY_BIT15		(uint32_t)0x00008000     //3,0
#define KEY_BIT16		(uint32_t)0x00010000     //3,1
#define KEY_BIT17		(uint32_t)0x00020000     //3,2
#define KEY_BIT18		(uint32_t)0x00040000     //3,3
#define KEY_BIT19		(uint32_t)0x00080000     //3,4
#define KEY_BIT20		(uint32_t)0x00100000     //4,0
#define KEY_BIT21		(uint32_t)0x00200000     //4,1
#define KEY_BIT22		(uint32_t)0x00400000     //4,2
#define KEY_BIT23		(uint32_t)0x00800000     //4,3
#define KEY_BIT24		(uint32_t)0x01000000     //4,4

	
#define KEY_MENU_TIMER	    		(uint32_t)(KEY_BIT24)
#define KEY_MENU_MODE	         (uint32_t)(KEY_BIT23)
#define KEY_MENU_STEP	         (uint32_t)(KEY_BIT22)
#define KEY_MENU_AUTO				(uint32_t)(KEY_BIT21)
#define KEY_MENU_STOP				(uint32_t)(KEY_BIT20)

#define KEY_MENU_COUNTER			(uint32_t)(KEY_BIT24 | KEY_BIT2)
#define KEY_MENU_MOLD            (uint32_t)(KEY_BIT23 | KEY_BIT2)
#define KEY_MENU_IO              (uint32_t)(KEY_BIT22 | KEY_BIT2) //(KEY_BIT3)  //(uint32_t)(KEY_BIT22 | KEY_BIT2)
#define KEY_MENU_CYCLE				(uint32_t)(KEY_BIT21 | KEY_BIT2)
#define KEY_MENU_MANUAL				(uint32_t)(KEY_BIT20 | KEY_BIT2)

#define KEY_ARROW_UP					(uint32_t)(KEY_BIT19)
#define KEY_ARROW_DOWM				(uint32_t)(KEY_BIT18)
#define KEY_ARROW_LEFT				(uint32_t)(KEY_BIT17)
#define KEY_ARROW_RIGHT				(uint32_t)(KEY_BIT16)
#define KEY_REJECT					(uint32_t)(KEY_BIT15)

#define KEY_ERROR_LOG				(uint32_t)(KEY_BIT19 | KEY_BIT2)
#define KEY_DISP_VERSION			(uint32_t)(KEY_BIT18 | KEY_BIT2)
#define KEY_LANGUAGE					(uint32_t)(KEY_BIT16 | KEY_BIT2)
#define KEY_DETECT_EN				(uint32_t)(KEY_BIT13 | KEY_BIT2)
	
#define KEY_ALARM						(uint32_t)(KEY_BIT14)
#define KEY_DETECT					(uint32_t)(KEY_BIT13| KEY_BIT2)
#define KEY_EJECTOR					(uint32_t)(KEY_BIT12)
#define KEY_SWING						(uint32_t)(KEY_BIT11)
#define KEY_RUNNER_UPDOWN			(uint32_t)(KEY_BIT10)
#define KEY_NO_7						(uint32_t)(KEY_BIT14)
#define KEY_NO_8                 (uint32_t)(KEY_BIT13)
#define KEY_NO_9                 (uint32_t)(KEY_BIT12)
#define KEY_NO_6						(uint32_t)(KEY_BIT11)
	
#define KEY_MAIN_UPDOWN				(uint32_t)(KEY_BIT9)
#define KEY_MAIN_FW					(uint32_t)(KEY_BIT8)
#define KEY_MAIN_VACUUM				(uint32_t)(KEY_BIT7)
#define KEY_MAIN_CHUCK_ROTATE		(uint32_t)(KEY_BIT6)
#define KEY_RUNNER_GRIPPER			(uint32_t)(KEY_BIT5)	
	
#define KEY_NO_4						(uint32_t)(KEY_BIT9)
#define KEY_NO_5                 (uint32_t)(KEY_BIT8)
#define KEY_NO_2                 (uint32_t)(KEY_BIT7)
#define KEY_NO_3				      (uint32_t)(KEY_BIT6)

#define KEY_MAIN_CHUCK				(uint32_t)(KEY_BIT4)
#define KEY_OP							(uint32_t)(KEY_BIT3)
#define KEY_SHIFT						(uint32_t)(KEY_BIT2)
#define KEY_CLEAR						(uint32_t)(KEY_BIT1)
#define KEY_ENTER						(uint32_t)(KEY_BIT0)
	
#define KEY_NO_1						(uint32_t)(KEY_BIT4)
#define KEY_NO_0				      (uint32_t)(KEY_BIT3)
	
#define KEY_NONE						(uint32_t)(0x00000000)
	
#define KEY_LONG_CLEAR				(uint32_t)(KEY_BIT1 | LONG_KEY)//Correct
#define KEY_MANUFACTURER			(uint32_t)(KEY_BIT0 | LONG_KEY)	//(KEY_BIT1 | LONG_KEY)
	
  
typedef uint32_t KeyTypeDef;	
#define LONG_COUNT					20 //400 x 50 = 2sec
#define LONG_KEY						(uint32_t)0x02000000  

void LoopAppKey(void);
void LoopAppEMO(void);

#ifdef	__cplusplus
}
#endif

#endif	/* __APPTKEY_H */