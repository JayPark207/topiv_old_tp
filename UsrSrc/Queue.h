/* 
 * File:   Queue.h
 * Author: Jay Park
 *
 * Created on 2016�뀈 11�썡 14�씪 (Mon), PM 3:33
 */

#ifndef __QUEUE_H
#define	__QUEUE_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
//#include "stm32f1xx_hal.h"
	#include "stm32f4xx_hal.h"
   
#define BUFF_TYPE	uint8_t
#define VAR_TYPE	uint8_t
   
#define QUEUE_SIZE		255
   
#define TRUE	1
#define FALSE	0
	
   
typedef struct _QUEUE {
    VAR_TYPE Head;
    VAR_TYPE Tail;
    uint16_t Size;
    BUFF_TYPE (*pBuff)[];
}QUEUE;


extern BUFF_TYPE BufferDisplay[QUEUE_SIZE];

extern QUEUE	qUartDisplay;
//
//QUEUE *pqUartDisplay;
extern QUEUE *pqUartDisplay;


extern QUEUE	qUart232Temp;
extern QUEUE *pqUart232Temp;
extern BUFF_TYPE BufferUart232Temp[QUEUE_SIZE];

extern QUEUE	qUart232Weight;
extern QUEUE *pqUart232Weight;
extern BUFF_TYPE BufferUart232Weight[QUEUE_SIZE];



//void InitQueue(void);
void NewQueue(QUEUE *pQueue, BUFF_TYPE (*pBuff)[], uint16_t size);
unsigned char  IsFull(QUEUE *pQueue);
unsigned char IsEmpty(QUEUE *pQueue);
VAR_TYPE GetSpace(QUEUE *pQueue);
void IncHead(QUEUE* pQueue, int cnt);
void IncTail(QUEUE* pQueue, int cnt);
void EmptyQueue(QUEUE *pQueue);
void PrintQueue(QUEUE* pQueue);
void WriteSingleByte(QUEUE *pQueue, VAR_TYPE Data);
VAR_TYPE GetSingleByte(QUEUE *pQueue);
VAR_TYPE GetExistCnt(QUEUE *pQueue);
VAR_TYPE GetQueueData(QUEUE *pQueue, VAR_TYPE pos);
void ResetQueue(QUEUE* pQueue);
VAR_TYPE GetTail(QUEUE *pQueue);
VAR_TYPE GetHead(QUEUE *pQueue);
#ifdef	__cplusplus
}
#endif

#endif	/* __QUEUE_H */

